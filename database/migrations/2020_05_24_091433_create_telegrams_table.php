<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTelegramsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('telegrams', function (Blueprint $table) {
            $table->increments('id_telegram');
            $table->integer('colla_id')->unsigned();
            $table->integer('casteller_id')->unsigned();
            $table->integer('casteller_multi_id',0)->unsigned();
            $table->integer('status',0)->unsigned();
            $table->string('name')->nullable();
            $table->string('language', 2)->default('ca');
            $table->timestamps();
    
            $table->foreign('casteller_id')
            ->references('id_casteller')->on('castellers')
            ->onDelete('cascade');

            $table->foreign('colla_id')
            ->references('id_colla')->on('colles')
            ->onDelete('cascade');

            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('telegrams');
    }
}
