<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBoardPositionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('board_positions', function (Blueprint $table) {
            $table->integer('board_id')->unsigned();
            $table->integer('event_id')->unsigned();
            $table->integer('casteller_id')->unsigned();
            $table->json('data');
            $table->timestamps();

            $table->foreign('board_id')
                ->references('id_board')->on('boards')
                ->onDelete('cascade');

            $table->foreign('event_id')
                ->references('id_event')->on('events')
                ->onDelete('cascade');

            $table->foreign('casteller_id')
                ->references('id_casteller')->on('castellers')
                ->onDelete('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('positions');
    }
}
