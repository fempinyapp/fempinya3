<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->increments('id_event');
            $table->integer('colla_id')->unsigned();
            $table->enum('type', ['ASSAIG', 'ACTUACIO', 'ACTIVITAT']);
            $table->string('name', 110);
            $table->string('address')->nullable();
            $table->text('comments')->nullable();
            $table->integer('duration')->nullable();
            $table->dateTime('start_date');
            $table->dateTime('open_date')->nullable();
            $table->dateTime('close_date')->nullable();
            $table->json('xdata')->nullable();
            $table->boolean('companions')->nullable();
            $table->tinyInteger('visible')->nullable();
            $table->timestamps();

            $table->foreign('colla_id')
                ->references('id_colla')->on('colles')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
