<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes(['register' => false]);

Route::group(['middleware' => ['auth', 'acces.admin']], function()
{
    Route::get('admin/colles', 'CollesController@getList')->name('admin.colles');
    Route::get('admin/colles/add-colla-modal', 'CollesController@getAddCollaModal')->name('admin.colles.add-colla-modal');
    Route::post('admin/colles/add', 'CollesController@postStoreColla')->name('admin.colles.add');
    Route::get('admin/colles/edit-colla-modal/{colla}', 'CollesController@getEditCollaModalAjax')->name('admin.colles.edit-colla-modal');
    Route::post('admin/colles/update/{colla}', 'CollesController@postUpdateColla')->name('admin.colles.update');
    Route::post('admin/colles/destroy/{colla}', 'CollesController@postDestroyColla')->name('admin.colles.destroy');
    Route::get('admin/users', 'UsersController@getList')->name('admin.users');

});

Route::group(['middleware' => ['auth']], function()
{
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('home', 'HomeController@index');
    Route::get('dashboard', 'HomeController@dashboard')->name('dashboard');

    //User profile
    Route::get('profile/user', 'ProfileController@getSetupUser')->name('profile.user');
    Route::post('profile/user', 'ProfileController@postUpdateUserProfile')->name('profile.user.update');
    Route::post('profile/user/update-password', 'ProfileController@postUpdateUserPassword')->name('profile.user.update-password');

    //Colla profile
    //middleware hasAcces?
    Route::get('profile/colla', 'ProfileController@getSetupColla')->name('profile.colla');
    Route::post('profile/colla', 'ProfileController@postUpdateColla')->name('profile.colla.update');
    Route::post('profile/colla/delete-user/{user}', 'ProfileController@postDestroyUser')->name('profile.colla.delete-user');
    Route::post('profile/colla/add-user', 'ProfileController@postAddUser')->name('profile.colla.add-user');
    Route::get('profile/colla/edit-user-modal/{user}', 'ProfileController@getEditUserModalAjax')->name('profile.colla.edit-user-modal');
    Route::post('profile/colla/update/{user}', 'ProfileController@postUpdateUser')->name('profile.colla.update-user');
});

Route::group(['middleware' => ['auth', 'acces.bbdd']], function()
{
    Route::get('castellers/list', 'CastellersController@getList')->name('castellers.list');
    Route::post('castellers/list-ajax', 'CastellersController@postListAjax')->name('castellers.list-ajax');
    Route::post('castellers/add', 'CastellersController@postAddCasteller')->name('castellers.add');
    Route::get('castellers/edit/{casteller}', 'CastellersController@getCardCasteller')->name('castellers.edit');
    Route::post('castellers/update/{casteller}', 'CastellersController@postUpdateCasteller')->name('castellers.update');
    Route::post('castellers/delete/{casteller}', 'CastellersController@postDestroyCasteller')->name('castellers.destroy');

    //Casteller card
    Route::get('castellers/edit/card-edit-ajax/{casteller}', 'CastellersController@getCardEditCastellerAjax')->name('castellers.edit.card-edit');

    //tags
    Route::get('castellers/tags', 'TagsController@getListTags')->name('castellers.tags');
    Route::get('castellers/tags/add-tag-modal/{type?}', 'TagsController@getAddTagModalAjax')->name('castellers.tags.add-modal');
    Route::post('castellers/tags/add', 'TagsController@postAddCastellerTag')->name('castellers.tags.add');
    Route::get('castellers/tags/edit-tag-modal/{tag}', 'TagsController@getEditTagsModalAjax')->name('castellers.tags.edit-tag-modal');
    Route::post('castellers/tags/update/{tag}', 'TagsController@postUpdateTag')->name('castellers.tags.update');
    Route::post('castellers/tags/destroy/{tag}', 'TagsController@postDestroyTag')->name('castellers.tags.destroy');
    Route::get('castellers/tags/toggle-group/{tag}/{group}', 'TagsController@getToggleGroupAjax')->name('castellers.tags.toggle-group');
    Route::post('castellers/position/add', 'TagsController@postAddPositionTag')->name('castellers.position.add');
});

Route::group(['middleware' => ['auth', 'acces.events']], function()
{
    Route::get('events/list', 'EventsController@getList')->name('events.list');
    Route::post('events/list-ajax{time}', 'EventsController@postListAjax')->name('events.list-ajax');
    Route::get('events/create', 'EventsController@getCreate')->name('events.create');
    Route::get('events/create-group', 'EventsController@getCreateGroup')->name('events.create-group');
    Route::post('events/add', 'EventsController@postStoreEvent')->name('events.add');
    Route::post('events/add-group', 'EventsController@postStoreEventGroup')->name('events.add-group');
    Route::get('events/edit/{event}', 'EventsController@getEditEvent')->name('events.edit');
    Route::post('events/update/{event}', 'EventsController@postUpdateEvent')->name('events.update');
    Route::post('events/destroy/{event}', 'EventsController@postDestroyEvent')->name('events.destroy');

    //events tags
    Route::get('events/tags', 'TagsController@getListEvents')->name('events.tags');
    Route::post('events/tags/add', 'TagsController@postAddEventTag')->name('events.tags.add');
    Route::get('events/answers', 'TagsController@getListAttendance')->name('events.answers');
    Route::get('events/answers/add-answer-modal/{type}', 'TagsController@getAddTagModalAjax')->name('events.answers.add-modal');
    Route::post('events/answers/add', 'TagsController@postAddAttendanceTag')->name('events.answers.add');

    //Attendance
    Route::get('event/attendance/{event}', 'EventAttendanceController@getIndex')->name('event.attendance');
    Route::post('event/attendance/list-attenders-ajax/{event}', 'EventAttendanceController@postListAttendersAjax')->name('event.attendance.list-attenders');
    Route::post('event/attendance/set-status-ajax', 'EventAttendanceController@postSetStatusAjax')->name('event.attendance.set-status');
    Route::post('event/attendance/set-status-verified-ajax', 'EventAttendanceController@postSetStatusVerifiedAjax')->name('event.attendance.set-status-verified');
    Route::post('event/attendance/set-answers-ajax', 'EventAttendanceController@postSetAnswersAjax')->name('event.attendance.set-answers');
    Route::post('event/attendance/set-companions-ajax', 'EventAttendanceController@postSetCompanionsAjax')->name('event.attendance.set-companions');
    Route::get('event/attendance/list-block/{event}', 'EventAttendanceController@getListBlocks')->name('event.attendance.list-block');
});

Route::group(['middleware' => ['auth', 'acces.boards']], function()
{
    Route::get('boards/list', 'BoardsController@getList')->name('boards.list');
    Route::get('boards/tags', 'TagsController@getListBoards')->name('boards.tags');
    Route::post('boards/tags/add', 'TagsController@postAddBoardTag')->name('boards.tags.add');
    Route::get('boards/add-board-modal', 'BoardsController@getAddBoardModalAjax')->name('boards.add-board-modal');

    Route::get('boards/preview-board-ajax/{board}', 'BoardsController@getModalPreviewBoard')->name('boards.preview-board-ajax');

    Route::post('boards/add', 'BoardsController@postAddBoard')->name('boards.add');
    //Route::get('boards/import', 'BoardsController@getAddBoard')->name('boards.import');
    Route::post('boards/ajax-upload-svg/{board}', 'BoardsController@postUploadSvg')->name('boards.upload-svg');
    Route::get('boards/tag-row-map/{board}/{map}', 'BoardsController@getTagRowMap')->name('boards.tag-row-map');
    Route::post('boards/ajax-tag-position/{board}/{map}', 'BoardsController@postTagPosition')->name('boards.tag-position');
    Route::post('boards/ajax-delete-position/{board}/{map}', 'BoardsController@postDeletePosition')->name('boards.delete-position');
    Route::get('boards/tag-all-map/{board}/{map}', 'BoardsController@getTagAllMap')->name('boards.tag-all-map');
    Route::get('boards/style/{board}/{map}', 'BoardsController@getStyleMap')->name('boards.style-map');
    Route::post('boards/style/{board}/{map}', 'BoardsController@postStyleMapAjax')->name('boards.style-map-ajax');
    Route::get('boards/add-map/{board}/{map}', 'BoardsController@getAddMapp')->name('boards.add-map');
});


/* news */
Route::resource('news', 'NewsController')->middleware('auth');

Route::get('news/editar/{id}', 'NewsController@editar')->name('news.editar');
Route::get('news/details/{id}', 'NewsController@details')->name('news.details');
Route::put('news/editar/{id}', 'NewsController@update' )->name('news.update');
Route::get('news/eliminar/{id}', 'NewsController@eliminar')->name('news.eliminar');


Route::get('/testing/bot', 'TelegramController@index');
