# Com començo?

## Amb Docker

1. Clona aquest repositori al teu ordinador:
```bash
git clone git@bitbucket.org:fempinyapp/fempinya3.git
cd fempinya3
```

2. Crea un arxiu `.env` copiant `.env.exmaple` i configura els valors de connexió a la base de dades de la següent forma:
```env
DB_HOST=db
DB_PORT=3306
```

3. Assegura't que tens Docker instal·lat al teu ordinador i engega els contenidors:
```bash
docker-compose up
```

4. Instal·la les dependències i prepara la base de dades:
```bash
docker-compose exec app composer install
docker-compose exec app php artisan key:generate
docker-compose exec app php artisan migrate
```

5. Ja pots accedir a Fem Pinya a [localhost:8000](http://localhost:8000)

## Amb XAMPP o similar

1. Clona aquest repositori al teu ordinador:

```bash
git clone git@bitbucket.org:fempinyapp/fempinya3.git
```

2. `...`

# Estructura d'arxius

Com està organitzat el repositori?

`...`

# Guía de contribució

Veure `CONTRIBUTING.md`
