<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Colla extends Model
{
    protected $table = 'colles';
    protected $primaryKey = "id_colla";
    public $timestamps = true;

    /** get current Colla from user authenticated */
    public static function getCurrent() : ?Colla
    {
        $user = Auth::user();

        if($user)
        {
            return Colla::find($user->colla->id_colla);
        }

        return null;
    }

    /** Num castellers from colla */
    public function numCastellers() : int
    {
        return Casteller::where('colla_id', $this->id_colla)->count();
    }

    /** true/false colla has users */
    public function hasUsers() : bool
    {
        $users = User::where('colla_id', $this->id_colla)->where('role', '!=', 'ADMIN')->count();

        if($users)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /** Last login from any user from Colla */
    public function lastLogin() : string
    {
        if($this->hasUsers())
        {
            $last_access = User::select('last_access_at')->where('colla_id', $this->id_colla)->orderBy('last_access_at', 'desc')->first()->last_access_at;

            return Carbon::create($last_access)->format('d/m/yy h:m');
        }
        else
        {
            return trans('admin.colla_no_users');
        }


    }
    /** get path profile image from logo */
    public function getLogoImage($colla) : string
    {
        $current_colla = Colla::find($colla);

        if($colla)
            {
                return asset('media/colles/'.$current_colla->shortname.'/'.$current_colla->logo);
            }
        else
            {
                return asset('media/image/logo.png');
            }

    }

    /** get families from Colla
     * @param null $colla
     * @return Collection
     */
    public static function getFamilies($colla = null) : Collection
    {
        if(is_null($colla) || empty($colla))
        {
            $colla = Colla::getCurrent();
        }

        return Casteller::select('family')->whereNotNull('family')->where('colla_id', $colla->id_colla)->distinct()->get();
    }

    /** relations */

    public function users()
    {
        return $this->hasMany(User::class, 'colla_id', 'id_colla');
    }

}
