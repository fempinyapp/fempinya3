<?php

namespace App;

use App\Enums\Gender;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Intervention\Image\ImageManagerStatic as Image;
use phpDocumentor\Reflection\Types\Collection;
use phpDocumentor\Reflection\Types\This;
use Symfony\Component\HttpFoundation\ParameterBag;

class Casteller extends Model
{
    protected $table = 'castellers';
    protected $primaryKey = "id_casteller";
    public $timestamps = true;


    public static function newCasteller(ParameterBag $parameterBag = null, Colla $colla = null) : Casteller
    {
        if(is_null($colla)) $colla = Colla::getCurrent();

        $casteller = new static();

        $casteller->colla_id = $colla->id_colla;
        $casteller->num_soci = $parameterBag->getInt('num_soci');
        $casteller->national_id_number = $parameterBag->get('national_id_number');
        $casteller->national_id_type = $parameterBag->get('national_id_type');
        $casteller->name =  $parameterBag->get('name');
        $casteller->last_name =  $parameterBag->get('last_name');
        $casteller->alias =  $parameterBag->get('alias');
        $casteller->family =  $parameterBag->get('family');

        //gender
        switch ($parameterBag->getInt('gender'))
        {
            case Gender::Male()->value():
                $casteller->gender = Gender::Male();
                break;
            case Gender::Female()->value():
                $casteller->gender = Gender::Female();
                break;
            case Gender::Noanswer()->value():
            case null:
                $casteller->gender = Gender::Noanswer();
                break;
            case Gender::Nobinary()->value():
                $casteller->gender = Gender::Nobinary();
                break;
        }

        //birthdate
        $casteller->birthdate = is_null($parameterBag->get('birthdate')) ? null : date_format(date_create_from_format("d/m/Y", $parameterBag->get('birthdate')), "Y-m-d");

        //subscription_date
        $casteller->subscription_date = is_null($parameterBag->get('subscription_date')) ? null : date_format(date_create_from_format("d/m/Y", $parameterBag->get('subscription_date')), "Y-m-d");

        $casteller->email = $parameterBag->get('email');
        $casteller->email2 = $parameterBag->get('email2');
        $casteller->phone = $parameterBag->get('phone');
        $casteller->mobile_phone = $parameterBag->get('mobile_phone');
        $casteller->emergency_phone = $parameterBag->get('emergency_phone');
        $casteller->address = $parameterBag->get('address');
        $casteller->postal_code = $parameterBag->get('postal_code');
        $casteller->city = $parameterBag->get('city');
        $casteller->comarca = $parameterBag->get('comarca');
        $casteller->province = $parameterBag->get('province');
        $casteller->country = $parameterBag->get('country');
        $casteller->comments = $parameterBag->get('comments');
        $casteller->height = (float) $parameterBag->get('height');
        $casteller->weight = (float) $parameterBag->get('weight');
        $casteller->shoulder_height = (float) $parameterBag->get('shoulder_height');

        $casteller->save();

        //tags
        foreach($casteller->tags() as $tag)
        {
            $casteller->removeTag($tag);
        }

        $tags = $parameterBag->get('tags') ?? [];
        foreach($tags as $tag)
        {
            $tag = Tag::where('colla_id', $colla->id_colla)->where('value', $tag)->first();
            $casteller->addTag($tag);
        }

        $casteller->save();

        //exist file, upload
        if($parameterBag->get('photo'))
        {
            $casteller->setPhoto($parameterBag->get('photo'));
        }

        return $casteller;
    }

    /** Update/add Casteller photo
     * @param UploadedFile $file
     */
    public function setPhoto(UploadedFile $file)
    {
        $colla = $this->colla;
        $random_str = Str::random(10);
        $image_input = $file;

        $imatge_xs = Image::make($image_input);
        $imatge_xs->fit(32);
        $imatge_xs->encode('jpg');
        $imatge_xs->save(public_path('media/colles/'.$colla->shortname.'/castellers/').$this->id_casteller.'_'.$random_str.'-xs'.'.jpg');

        $imatge_med = Image::make($image_input);
        $imatge_med->fit(128);
        $imatge_med->encode('jpg');
        $imatge_med->save(public_path('media/colles/'.$colla->shortname.'/castellers/').$this->id_casteller.'_'.$random_str.'-med'.'.jpg');

        $imatge_xl = Image::make($image_input);
        $imatge_xl->fit(1024);
        $imatge_xl->encode('jpg');
        $imatge_xl->save(public_path('media/colles/'.$colla->shortname.'/castellers/').$this->id_casteller.'_'.$random_str.'-xl'.'.jpg');


        $this->photo = $this->id_casteller.'_'.$random_str;
        $this->save();
    }

    /** filtered castellers of a Colla
     * @param string $tags
     * @param string $search_type
     * @param Colla|null $colla
     * @return Builder
     */
    public static function filterCastellersByTags($tags = 'all', string $search_type = 'OR', Colla $colla = null) : Builder
    {
        if (is_null($colla)) $colla = Colla::getCurrent();


        if(empty($tags) || is_null($tags))
        {
            $tags = 'all';
        }

        if(is_array($tags) && in_array("all", $tags))
        {
            $tags = 'all';
        }

        if($tags != 'all')
        {
            if(is_array($tags))
            {

                if($search_type == 'OR')
                {
                    $castellers = Casteller::join('casteller_tag', 'castellers.id_casteller', '=', 'casteller_tag.casteller_id')
                        ->join('tags', 'casteller_tag.tag_id', '=', 'tags.id_tag')
                        ->select('castellers.*')
                        ->distinct()
                        ->where('castellers.colla_id', $colla->id_colla)
                        ->where(function ($query) use ($tags){
                            foreach ($tags as $tag)
                            {
                                $query->orWhere('tags.value', $tag);
                            }
                        });
                }
                elseif($search_type == 'AND')
                {
                    $castellers = Casteller::leftJoin('casteller_tag', 'castellers.id_casteller', '=', 'casteller_tag.casteller_id')
                        ->leftJoin('tags', 'casteller_tag.tag_id', '=', 'tags.id_tag')
                        ->select(DB::raw('castellers.*, group_concat(tags.value) as tags'))
                        ->where('castellers.colla_id', $colla->id_colla)
                        ->groupBy('castellers.id_casteller');

                    foreach ($tags as $tag)
                    {
                        $castellers->having(DB::raw("Find_In_Set('".$tag."',tags)"), '>', 0);
                    }
                }
            }
            else
            {
                $castellers = Casteller::join('casteller_tag', 'castellers.id_casteller', '=', 'casteller_tag.casteller_id')
                                        ->join('tags', 'casteller_tag.tag_id', '=', 'tags.id_tag')
                                        ->select('castellers.*')
                                        ->where('castellers.colla_id', $colla->id_colla)
                                        ->where('tags.value', $tags);
            }
        }
        else
        {
            $castellers = Casteller::where('colla_id', $colla->id_colla);
        }

        return $castellers;
    }

    /** Add tag to casteller
     * @param Tag $tag
     * @return bool
     */
    public function addTag(Tag $tag) : bool
    {

        if(!$this->hasTag($tag->value) && $tag->type=='CASTELLERS' && $tag->colla_id == $this->colla_id)
        {
            DB::table('casteller_tag')->insert(['casteller_id' => $this->id_casteller, 'tag_id' => $tag->id_tag]);
            return true;
        }
        else
        {
            return false;
        }
    }

    /** remove tag of a casteller
     * @param Tag $tag
     * @return bool
     */
    public function removeTag(Tag $tag) : bool
    {
        if($tag->colla_id == $this->colla_id)
        {
            if($this->hasTag($tag->value))
            {
                if(DB::table('casteller_tag')->where('casteller_id', $this->id_casteller)->where('tag_id', $tag->id_tag)->delete())
                {
                    return true;
                }
                else
                {

                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }

    /** return true if Casteller has a Tag
     * @param $tag_value
     * @return bool
     */
    public function hasTag($tag_value) : bool
    {
        if(in_array($tag_value, $this->tagsArray('VALUE')))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /** get tags to Casteller */
    public function tags() : ? \Illuminate\Database\Eloquent\Collection
    {
        return Tag::join('casteller_tag', 'tags.id_tag', 'casteller_tag.tag_id')
                    ->join('castellers', 'casteller_tag.casteller_id', 'castellers.id_casteller')
                    ->where('id_casteller', $this->id_casteller)
                    ->where('type', 'CASTELLERS')
                    ->select('tags.*')
                    ->get();
    }

    /** get Array tags (only name or value) to Casteller
     * @param string $return_type
     * @return array
     */
    public function tagsArray($return_type = 'NAME') : array
    {
        $array = [];

        foreach($this->tags() as $tag)
        {
            if($return_type=='NAME')
            {
                array_push($array, $tag->name);
            }
            elseif($return_type=='VALUE')
            {
                array_push($array, $tag->value);
            }
        }

        return $array;
    }

    /** Get age from birthdate. */
    public function age()
    {
        if (is_null($this->birthdate) || $this->birthdate=='0000-00-00' || empty($this->birthdate))
        {
            return false;
        }

        $birthdate = new \DateTime($this->birthdate);
        $current_date = new \DateTime('now');
        $interval = $birthdate->diff($current_date); //http://php.net/manual/en/class.dateinterval.php

        return $interval->y;
    }

    /** get photo path of a casteller */
    public function getProfileImage() : string
    {
        $colla = Colla::getCurrent();

        if(empty($this->photo) || is_null($this->photo))
        {
            return asset('media/avatars/avatar.jpg');
        }
        else
        {
            return asset('media/colles/'.$colla->shortname.'/castellers/'.$this->photo.'-med.jpg');
        }
    }

    /** fet Casteller position */
    public function position()
    {
        return Tag::join('casteller_tag', 'tags.id_tag', 'casteller_tag.tag_id')
            ->join('castellers', 'casteller_tag.casteller_id', 'castellers.id_casteller')
            ->where('id_casteller', $this->id_casteller)
            ->where('type', 'POSITION')
            ->select('tags.*')
            ->first();
    }

    /** relations */

    public function Colla()
    {
        return $this->belongsTo('App\Colla', 'colla_id', 'id_colla');
    }
}
