<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use PhpParser\Node\Scalar\String_;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * Prymary Key table users
     * @var string
     */
    protected $primaryKey = "id_user";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'role', 'type', 'name', 'email', 'password', 'colla_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Set the password attribute.
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->attributes['password'] = Hash::make($password);
        $this->save();
    }

    /** get path profile image from user */
    public function getProfileImage() : string
    {
        if(Auth::user()->photo)
        {
            $colla = Colla::find(Auth::user()->colla_id);

            return asset('media/image/users/'.Auth::user()->photo.'-xs.jpg');
        }
        else
        {
            return asset('media/avatars/avatar.jpg');
        }

    }

    /** get array of permissions */
    public function getPermissions() : array
    {
        $permissions = [];

        if(!$this->hasAcces()) return [];

        if($this->accesColla()) $permissions['COLLA'] = ['READ'];
        if($this->accesWriteColla()) $permissions['COLLA'] = ['READ', 'WRITE'];

        if($this->accesAdmin()) $permissions['ADMIN'] = ['READ', 'WRITE'];

        if($this->accesBBDD()) $permissions['BBDD'] = ['READ'];
        if($this->accesWriteBBDD()) $permissions['BBDD'] = ['READ', 'WRITE'];

        if($this->accesEvents()) $permissions['EVENTS'] = ['READ'];
        if($this->accesWriteEvents()) $permissions['EVENTS'] = ['READ', 'WRITE'];

        if($this->accesBoards()) $permissions['BOARDS'] = ['READ'];
        if($this->accesWriteBoards()) $permissions['BOARDS'] = ['READ', 'WRITE'];

        if($this->accesTelegram()) $permissions['TELEGRAM'] = ['READ'];
        if($this->accesWriteTelegram()) $permissions['TELEGRAM'] = ['READ', 'WRITE'];

        return $permissions;
    }

    /**
     * Acces functions, bit ontroler
     * 1st bit[0]; acces control 1 ok, 0 no acces
     * 2nd bit[1]; accesAdmin(), 1 ok, 0 no
     * 3rs, 4th bit[2],[3]; accesColla() 1sr read, 2nd write
     * 4th, 5th bit[4],[5]; accesBBDD() 1sr read, 2nd write
     * 6th, 7th bit[6],[7]; accesEvents() 1sr read, 2nd write
     */

    /** to know user has acces */
    public function hasAcces()
    {
        $acces = (int) str_split(decbin($this->type))[0];

        return $acces === 1;

    }

    /** to know this user is admin */
    public function accesAdmin()
    {
        if(!$this->hasAcces()) return false;

        if($this->role!='ADMIN') return false;

        $acces = (int) str_split(decbin($this->type))[1];
        ;
        return $acces;
    }

    /** to know this user is admin */
    public function accesColla()
    {
        if(!$this->hasAcces()) return false;

        $acces = (int) str_split(decbin($this->type))[2];

        return $acces === 1;
    }

    /** to know this user has acces to WRITE Colla */
    public function accesWriteColla()
    {
        if(!$this->hasAcces()) return false;
        if(!$this->accesColla()) return false;

        $acces = (int) str_split(decbin($this->type))[3];

        return $acces === 1;
    }

    /** to know this user has acces to Castellers */
    public function accesBBDD()
    {
        if(!$this->hasAcces()) return false;

        $acces = (int) str_split(decbin($this->type))[4];

        return $acces === 1;
    }

    /** to know this user has acces to WRITE Castellers */
    public function accesWriteBBDD()
    {
        if(!$this->hasAcces()) return false;
        if(!$this->accesBBDD()) return false;

        $acces = (int) str_split(decbin($this->type))[5];

        return $acces === 1;
    }

    /** to know this user has acces to Events */
    public function accesEvents()
    {
        if(!$this->hasAcces()) return false;

        $acces = (int) str_split(decbin($this->type))[6];

        return $acces === 1;
    }

    /** to know this user has acces to WRITE Events */
    public function accesWriteEvents()
    {
        if(!$this->hasAcces()) return false;
        if(!$this->accesEvents()) return false;

        $acces = (int) str_split(decbin($this->type))[7];

        return $acces === 1;
    }

    /** to know this user has acces to Pinyes */
    public function accesBoards()
    {
        if(!$this->hasAcces()) return false;

        $acces = (int) str_split(decbin($this->type))[8];

        return $acces === 1;
    }

    /** to know this user has acces to WRITE Pinyes */
    public function accesWriteBoards()
    {
        if(!$this->hasAcces()) return false;
        if(!$this->accesBoards()) return false;

        $acces = (int) str_split(decbin($this->type))[9];

        return $acces === 1;
    }

    /** to know this user has acces to Telegram */
    public function accesTelegram()
    {
        if(!$this->hasAcces()) return false;

        $acces = (int) str_split(decbin($this->type))[10];

        return $acces === 1;
    }

    /** to know this user has acces to WRITE Telegram */
    public function accesWriteTelegram()
    {
        if(!$this->hasAcces()) return false;
        if(!$this->accesTelegram()) return false;

        $acces = (int) str_split(decbin($this->type))[11];

        return $acces === 1;
    }

    /** relations */

    /** get user Colla  */
    public function colla()
    {
        return $this->belongsTo('App\Colla', 'colla_id', 'id_colla');
    }
}
