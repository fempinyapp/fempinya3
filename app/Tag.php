<?php

namespace App;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Tag extends Model
{
    protected $table = 'tags';
    protected $primaryKey = "id_tag";
    public $timestamps = true;

    /** return true/false if tag is used  */
    public function isUsed() : bool
    {
        if($this->type=='CASTELLERS' || $this->type=='POSITIONS')
        {
            $count = DB::table('casteller_tag')->where('tag_id', $this->id_tag)->count();
        }
        elseif($this->type=='EVENTS')
        {
            $count = DB::table('event_tag')->where('tag_id', $this->id_tag)->count();
        }
        elseif($this->type=='ATTENDANCE')
        {
            $count = DB::table('event_attendance_tag')->where('tag_id', $this->id_tag)->count();
        }
        elseif($this->type=='BOARDS')
        {
            $count = DB::table('board_tags')->where('tag_id', $this->id_tag)->count();
        }

        if($count>0) return true;

        return false;
    }

    /** get Tags from Colla
     * @param string $type
     * @param null $colla
     * @return Collection
     */
    public static function currentTags(string $type = 'CASTELLERS', $colla = null) : Collection
    {
        if(is_null($colla) || empty($colla))
        {
            $colla = Colla::getCurrent();
        }

        return Tag::where('colla_id', $colla->id_colla)->where('type', $type)->get();
    }

    /** get groups Tags from Colla
     * @param string $type
     * @param null $colla
     * @return Collection
     */
    public static function groups($type = 'CASTELLERS', $colla = null) : Collection
    {
        if(is_null($colla) || empty($colla))
        {
            $colla = Colla::getCurrent();
        }

        return Tag::select('group')
                    ->where('colla_id', $colla->id_colla)->where('type', $type)
                    ->orderBy('group', 'asc')
                    ->distinct()
                    ->get();
    }

    public static function validName($name)
    {

        if ((empty($name)) || is_null($name) || $name==" " || $name=="null")
        {
            return false;
        }

        //No symbols, special characters
        $rexSafety = "/[\^<,\"@\/\{\}\(\)\*\$%\?=>:\|;#]+/i";

        if (preg_match($rexSafety, $name))
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public static function validNameAttendance($name)
    {

        if ((empty($name)) || is_null($name) || $name==" " || $name=="null")
        {
            return false;
        }

        //No symbols, special characters
        $rexSafety = "/[\^<\"@\/\{\}\(\)\*\$%\?=>:\|;#]+/i";

        if (preg_match($rexSafety, $name))
        {
            return false;
        }
        else
        {
            return true;
        }
    }

}
