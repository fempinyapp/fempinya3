<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attendance extends Model
{
    protected $table = 'attendance';
    protected $primaryKey = "id_attendance";
    
    

    /** set status of a Member / Event attendance
     * @param int $id_casteller
     * @param int $id_event
     * @param string $status
     * @param int $companions
     * @param string $source
     * @param array $options
     * @return array
     */
    public static function setStatus(int $id_casteller, int $id_event, string $status = null, $companions = null, $source = 'WEB', $options = array()) : array
    {
        //value to return
        $register['original'] = null;
        $register['new'] = null;

        $count = Attendance::where('casteller_id', $id_casteller)->where('event_id', $id_event)->count();

        if($count > 0)
        {
            $attendance = Attendance::where('casteller_id', $id_casteller)->where('event_id', $id_event)->first();
            $register['original'] = $attendance;
        }
        else
        {
            $attendance = new Attendance;
        }

        if($status == 'UNKNOWN' || $status == 'NO')
        {
            $attendance->companions = 0;
            $options = [];
        }
        else
        {
            if(!is_null($companions)) $attendance->companions = $companions;
        }

        $attendance->status = $status;
        $attendance->casteller_id = $id_casteller;
        $attendance->event_id = $id_event;
        $attendance->source = $source;
        $attendance->options = json_encode($options);
        $attendance->save();

        $register['new'] = $attendance;

        return $register;
    }

    /** set verified status of a Member / Event attendance
     * @param int $id_casteller
     * @param int $id_event
     * @param string $status
     * @return array
     */
    public static function setVerifiedStatus(int $id_casteller, int $id_event, string $status) : array
    {
        //value to return
        $register['original'] = null;
        $register['new'] = null;

        $count = Attendance::where('casteller_id', $id_casteller)->where('event_id', $id_event)->count();

        if($count > 0)
        {
            $attendance = Attendance::where('casteller_id', $id_casteller)->where('event_id', $id_event)->first();
            $register['original'] = $attendance;
        }
        else
        {
            $attendance = new Attendance;
        }

        if($status == 'UNKNOWN' && $attendance->status_verified == 'UNKNOWN')
        {
            Attendance::where('id_attendance', $attendance->id_attendance)->delete();
        }
        else
        {
            $attendance->status_verified = $status;
            $attendance->casteller_id = $id_casteller;
            $attendance->event_id = $id_event;
            $attendance->save();
        }

        $register['new'] = $attendance;

        return $register;
    }

    /** set attendance answers Event / Casteller via AJAX
     * @param int $id_casteller
     * @param int $id_event
     * @param array $answers
     * @return string
     */
    public static function setAnswers(int $id_casteller, int $id_event, array $answers) : bool
    {
        $count = Attendance::where('casteller_id', $id_casteller)->where('event_id', $id_event)->count();

        if($count > 0)
        {
            $attendance = Attendance::where('casteller_id', $id_casteller)->where('event_id', $id_event)->first();

            $attendance->options = json_encode($answers);

            $attendance->save();

            return 'true';
        }
        else
        {
            return 'false';
        }
    }

    /** set Companions Event / Casteller via AJAX */
    public static function setCompanions(int $id_casteller, int $id_event, int $companions) : bool
    {
        $count = Attendance::where('casteller_id', $id_casteller)->where('event_id', $id_event)->count();

        if($count > 0)
        {
            $attendance = Attendance::where('casteller_id', $id_casteller)->where('event_id', $id_event)->first();

            $attendance->companions = $companions;

            $attendance->save();

            return 'true';
        }
        else
        {
            return 'false';
        }
    }

    /** get Attendance of Casteller / Event
     * @param int $id_casteller
     * @param int $id_event
     * @return \stdClass
     */
    public static function getAttendance(int $id_casteller, int $id_event)
    {
        $attendance = Attendance::where('casteller_id', $id_casteller)->where('event_id', $id_event)->first();

        if(is_null($attendance))
        {
            $attendance = new \stdClass();
            $attendance->options = '[]';
            $attendance->companions = 0;
            $attendance->status = '';
            $attendance->status_verified = '';
            $attendance->comments = '';
            $attendance->updated_at = '';
        }

        return $attendance;
    }
}
