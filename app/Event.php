<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Event extends Model
{
    protected $table = 'events';
    protected $primaryKey = "id_event";
    public $timestamps = true;

    /** get upcoming Events form Colla
     * @param string $tags
     * @param string $search_type
     * @param null $colla
     * @return Builder
     */
    public static function getUpcommingEventsByTags($tags = 'all', $search_type = 'OR', $colla = null) : Builder
    {
        if (is_null($colla))
        {
            $colla = Colla::getCurrent();
        }

        if(is_array($tags) && in_array("all", $tags))
        {
            $tags = 'all';
        }

        if(empty($tags) || is_null($tags))
        {
            $tags = 'all';
        }

        if($tags != 'all')
        {
            if(is_array($tags))
            {

                if($search_type == 'OR')
                {
                    $events = Event::join('event_tag', 'events.id_event', '=', 'event_tag.event_id')
                        ->join('tags', 'event_tag.tag_id', '=', 'tags.id_tag')
                        ->select('events.*')
                        ->distinct()
                        ->where('events.colla_id', $colla->id_colla)
                        ->whereDate('start_date', '>', Carbon::now())
                        ->where(function ($query) use ($tags){
                            foreach ($tags as $tag)
                            {
                                $query->orWhere('tags.value', $tag);
                            }
                        });
                }
                elseif($search_type == 'AND')
                {
                    $events = Event::leftJoin('event_tag', 'events.id_event', '=', 'event_tag.event_id')
                        ->leftJoin('tags', 'event_tag.tag_id', '=', 'tags.id_tag')
                        ->select(DB::raw('events.*, group_concat(tags.value) as tags'))
                        ->where('events.colla_id', $colla->id_colla)
                        ->whereDate('start_date', '>', Carbon::now())
                        ->groupBy('events.id_event');

                    foreach ($tags as $tag)
                    {
                        $events->having(DB::raw("Find_In_Set('".$tag."',tags)"), '>', 0);
                    }
                }
            }
            else
            {
                $events = Event::join('event_tag', 'events.id_event', '=', 'event_tag.event_id')
                    ->join('tags', 'event_tag.tag_id', '=', 'tags.id_tag')
                    ->select('events.*')
                    ->where('events.colla_id', $colla->id_colla)
                    ->whereDate('start_date', '>', Carbon::now())
                    ->where('tags.value', $tags);
            }
        }
        else
        {
            $events = Event::where('colla_id', $colla->id_colla)->whereDate('start_date', '>', Carbon::now());
        }

        return $events;
    }

    /** get past Events form Colla
     * @param string $tags
     * @param string $search_type
     * @param null $colla
     * @return Builder
     */
    public static function getPastEventsByTags($tags = 'all', $search_type = 'OR', $colla = null) : Builder
    {
        if (is_null($colla))
        {
            $colla = Colla::getCurrent();
        }

        if(empty($tags) || is_null($tags))
        {
            $tags = 'all';
        }

        if(is_array($tags) && in_array("all", $tags))
        {
            $tags = 'all';
        }

        if($tags != 'all')
        {
            if(is_array($tags))
            {

                if($search_type == 'OR')
                {
                    $events = Event::join('event_tag', 'events.id_event', '=', 'event_tag.event_id')
                        ->join('tags', 'event_tag.tag_id', '=', 'tags.id_tag')
                        ->select('events.*')
                        ->distinct()
                        ->where('events.colla_id', $colla->id_colla)
                        ->whereDate('start_date', '<', Carbon::now())
                        ->where(function ($query) use ($tags){
                            foreach ($tags as $tag)
                            {
                                $query->orWhere('tags.value', $tag);
                            }
                        });
                }
                elseif($search_type == 'AND')
                {
                    $events = Event::leftJoin('event_tag', 'events.id_event', '=', 'event_tag.event_id')
                        ->leftJoin('tags', 'event_tag.tag_id', '=', 'tags.id_tag')
                        ->select(DB::raw('events.*, group_concat(tags.value) as tags'))
                        ->where('events.colla_id', $colla->id_colla)
                        ->whereDate('start_date', '<', Carbon::now())
                        ->groupBy('events.id_event');

                    foreach ($tags as $tag)
                    {
                        $events->having(DB::raw("Find_In_Set('".$tag."',tags)"), '>', 0);
                    }
                }
            }
            else
            {
                $events = Event::join('event_tag', 'events.id_event', '=', 'event_tag.event_id')
                    ->join('tags', 'event_tag.tag_id', '=', 'tags.id_tag')
                    ->select('events.*')
                    ->where('events.colla_id', $colla->id_colla)
                    ->whereDate('start_date', '<', Carbon::now())
                    ->where('tags.value', $tags);
            }
        }
        else
        {
            $events = Event::where('colla_id', $colla->id_colla)->whereDate('start_date', '<', Carbon::now());
        }

        return $events;
    }

    /** get a Event of open and close data  */
    public function prova()
    {
    dd($this);
    }
    public function isOpen($date = null) : bool
    {
        if(is_null($date)) $date = Carbon::now();

        if($this->open_date < $date && $this->close_date > $date)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /** add Tag to Event
     * @param Tag $tag
     * @return bool
     */
    public function addTag(Tag $tag) : bool
    {
        if($tag->type=='EVENTS' && $tag->colla_id == $this->colla_id && !$this->hasTag($tag->value))
        {
            DB::table('event_tag')->insert(['event_id' => $this->id_event, 'tag_id' => $tag->id_tag]);
            return true;
        }
        else
        {
            return false;
        }
    }

    /** get tags to Event */
    public function tags() : Collection
    {
        return Tag::join('event_tag', 'tags.id_tag', 'event_tag.tag_id')
            ->join('events', 'event_tag.event_id', 'events.id_event')
            ->where('events.id_event', $this->id_event)
            ->where('tags.type', 'EVENTS')
            ->select('tags.*')
            ->get();
    }

    /** get Array tags (only name or value) to Event
     * @param string $return_type
     * @return array
     */
    public function tagsArray($return_type = 'NAME') : array
    {
        $array = [];

        foreach($this->tags() as $tag)
        {
            if($return_type=='NAME')
            {
                array_push($array, $tag->name);
            }
            elseif($return_type=='VALUE')
            {
                array_push($array, $tag->value);
            }
        }

        return $array;
    }

    /** remove tag form Event
     * @param Tag $tag
     * @return bool
     */
    public function removeTag(Tag $tag) : bool
    {
        if($this->hasTag($tag->value) && $tag->colla_id == $this->colla_id)
        {
            if(DB::table('event_tag')->where('event_id', $this->id_event)->where('tag_id', $tag->id_tag)->delete())
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }

    /** return true if Event has ha tag
     * @param $tag_value
     * @return bool
     */
    public function hasTag($tag_value) : bool
    {
        if(in_array($tag_value, $this->tagsArray('VALUE')))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /** add personalise attendance answer to Event
     * @param Tag $tag
     * @return bool
     */
    public function addAttendanceAnswer(Tag $tag) : bool
    {
        if($tag->type=='ATTENDANCE' && $tag->colla_id == $this->colla_id && !$this->hasAttendanceAnswer($tag->value))
        {
            DB::table('event_attendance_tag')->insert(['event_id' => $this->id_event, 'tag_id' => $tag->id_tag]);
            return true;
        }
        else
        {
            return false;
        }
    }


    /** remove attendance personalize answer
     * @param Tag $tag
     * @return bool
     */
    public function removeAttendanceAnswer(Tag $tag) : bool
    {
        if($this->hasAttendanceAnswer($tag->value) && $tag->colla_id == $this->colla_id)
        {
            if(DB::table('event_attendance_tag')->where('event_id', $this->id_event)->where('tag_id', $tag->id_tag)->delete())
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }

    /** return is Event has a attendance personalize answer
     * @param $tag_value
     * @return bool
     */
    public function hasAttendanceAnswer($tag_value) : bool
    {
        if(in_array($tag_value, $this->answersArray('VALUE')))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /** get attendance answer to Event */
    public function attendanceAnswers() : Collection
    {
        return Tag::join('event_attendance_tag', 'tags.id_tag', 'event_attendance_tag.tag_id')
            ->join('events', 'event_attendance_tag.event_id', 'events.id_event')
            ->where('events.id_event', $this->id_event)
            ->where('tags.type', 'ATTENDANCE')
            ->select('tags.*')
            ->get();
    }



    /** get Array personalise answers (only name or value) to Event
     * @param string $return_type
     * @return array
     */
    public function answersArray($return_type = 'NAME') : array
    {
        $array = [];

        foreach($this->attendanceAnswers() as $answer)
        {
            if($return_type=='NAME')
            {
                array_push($array, $answer->name);
            }
            elseif($return_type=='VALUE')
            {
                array_push($array, $answer->value);
            }
            elseif($return_type=='ID')
            {
                array_push($array, $answer->id_tag);
            }
        }

        return $array;
    }

    /** get a count of confirmed attendance to event */
    public function countAttenders() : array
    {
        $attendance = [];

        $attendance['YES'] = Attendance::where('event_id', $this->id_event)->where('status', 'YES')->count();
        $attendance['NO'] = Attendance::where('event_id', $this->id_event)->where('status', 'NO')->count();
        $attendance['UNKNOWN'] = Attendance::where('event_id', $this->id_event)
                                            ->where(function($q){
                                                $q->orWhere('status', 'UNKNOWN');
                                                $q->orWhereNull('status');
                                            })
                                            ->count();

        $attendance['companions'] = Attendance::where('event_id', $this->id_event)->where('status', 'YES')->sum('companions');

        return $attendance;
    }
}
