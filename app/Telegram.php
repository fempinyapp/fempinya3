<?php
 
namespace App;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use phpDocumentor\Reflection\Types\Collection;


class Telegram extends Model
{


    protected $table = 'telegrams';
    protected $primaryKey = "id_telegram";
    

    // update language to user id_telegram
    
    public static function updateLanguage($id_telegram, $language)
    {
        $telegram = Telegram::find($id_telegram);
        $telegram->language = $language;
        $telegram->save();

        return true;
    }

    // insert telegram to anonyus
    // public static function 
    public static function setVisitor(int $id_telegram, int $id_casteller, int $id_colla, int $status = 0, $casteller_multi_id = 0, string $name = null, string $language = 'ca') : array
    {
        //value to return
        $register['original'] = null;
        $register['new'] = null;

        $count = Visitor::where('id_telegram', $id_telegram)->count();

        if($count > 0)
        {
            $visitor = Visitor::where('casteller_id', $id_casteller)->where('colla_id', $id_colla)->first();
            $register['original'] = $visitor;
        }
        else
        {
            $visitor = new Visitor;
        }

        if($status == '1')
        {
            $visitor->status = 0;
        }
        else
        {
            $visitor->status = $status;
        }

        $visitor->id_telegram = $id_telegram;
        $visitor->status = $status;
        $visitor->casteller_id = $id_casteller;
        $visitor->colla_id = $id_colla;
        $visitor->casteller_multi_id = $casteller_multi_id;
        $visitor->name = $name;
        $visitor->language = $language;
        $visitor->save();

        $register['new'] = $telegram;

        //return true;
        return $register;
    }
    

    // insert telegram to casteller
    // public static function 
    public static function setTelegram(int $id_telegram, int $id_casteller, int $id_colla, int $status = 0, $casteller_multi_id = 0, string $name = null, string $language = 'ca') : array
    {
        //value to return
        $register['original'] = null;
        $register['new'] = null;

        $count = Telegram::where('id_telegram', $id_telegram)->count();

        if($count > 0)
        {
            $telegram = Telegram::where('casteller_id', $id_casteller)->where('colla_id', $id_colla)->first();
            $register['original'] = $telegram;
        }
        else
        {
            $telegram = new Telegram;
        }

        if($status == '1')
        {
            $telegram->status = 0;
        }
        else
        {
            $telegram->status = $status;
        }

        $telegram->id_telegram = $id_telegram;
        $telegram->status = $status;
        $telegram->casteller_id = $id_casteller;
        $telegram->colla_id = $id_colla;
        $telegram->casteller_multi_id = $casteller_multi_id;
        $telegram->name = $name;
        $telegram->language = $language;
        $telegram->save();

        $register['new'] = $telegram;

        //return true;
        return $register;
    }
    
// delete user id_telegram
   public static function DestroyeTelegram($id_telegram)
   {
        $telegram = Telegram::findOrFail($id_telegram);
        $telegram->delete();
        return true; //$telegram; 
    }

// get telegram_id to user id_telegram
    protected static $current_telegram = null;
    public static function getTelegram($casteller_id){
        if(!isset(static::$current_telegram))
        {
           $telegrams = Telegram::where('casteller_id', $casteller_id)->get();
           foreach ($telegrams as $telegram)
            {
                return $telegram->id_telegram;
            }
        } else{
            return 0;
        }
    }

    // get telegram_id to multiuser
    // insert telegram to casteller
    // public static function 

      public static function updateMultiuser($id_telegram, $casteller_multi_id = 0)
      {
          $telegram = Telegram::find($id_telegram);
          $telegram->casteller_multi_id = $casteller_multi_id;
          $telegram->save();
  
          return true;
      }

    // get value row to id_casteller
    public static function getNameCasteller($casteller_id, $row = 'name'){
        if(!isset(static::$current_telegram))
        {
           $castellers = Casteller::where('id_casteller', $casteller_id)->get();
           foreach ($castellers as $casteller)
            {
                if( $row == 'alias') {
                    return $casteller->alias;
                } else {
                    return $casteller->name;
                }

            }
        } else{
            return 0;
        }
    }


    

}
