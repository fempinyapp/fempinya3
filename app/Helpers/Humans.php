<?php

namespace App\Helpers;


use App\Casteller;
use App\Event;
use Carbon\Carbon;

class Humans
{
    public static function readEventColumn($id_event, $column, $align='right')
    {
        $event = Event::find($id_event);

        if($column=="tags")
        {
            $tags = null;

            foreach ($event->tagsArray('NAME') as $tag) //filterd tag -- paint it blue https://open.spotify.com/track/63T7DJ1AFDD6Bn8VzG6JE8?si=QfAzbKC3Sl2sNxmBj12crA
            {
                $tags .= '<span class="badge badge-primary pull-'.$align.'" style="margin-left: 3px; margin-bottom: 3px;">'.$tag.'</span>';
            }

            return $tags;
        }
        elseif($column=="start_date")
        {
            setlocale(LC_ALL, 'ca_ES');
            if (empty($event->start_date)) return $event->start_date;
            $start_date = Carbon::parse($event->start_date);
            return $start_date->isoFormat('dddd, D MMMM \d\e OY \a \l\e\s HH:mm');
        }
        else
        {
            return $event->{$column};
        }
    }

    /** Read for humans fields date, age...
     * @param $id_casteller
     * @param $column
     * @param string $align
     * @return false|string|null
     */
    public static function readCastellerColumn($id_casteller, $column, $align='right')
    {
        $casteller = Casteller::find($id_casteller);

        if ($column=="age")
        {
            $age = $casteller->age();

            if(is_null($casteller->birthdate)) return '';

            if ($age > 0)
            {
                return $age;
            }
            else
            {
                return '0';
            }
        }
        elseif($column=="birthdate")
        {
            if (empty($casteller->birthdate)) return $casteller->birthdate;
            return date('d/m/Y', strtotime($casteller->birthdate));
        }
        elseif($column=="subscription_date")
        {
            if (empty($casteller->subscription_date)) return $casteller->subscription_date;
            return date('d/m/Y', strtotime($casteller->subscription_date));
        }
        elseif($column=="gender")
        {
            if (is_null($casteller->gender))
            {
                return '<span class="fa fa-genderless"></span>';
            }
            elseif($casteller->gender==1)
            {
                return '<span style="color: blue;" class="si si-symbol-male"></span>';
            }
            else
            {
                return '<span style="color: deeppink;" class="si si-symbol-female"></span>';
            }

        }
        elseif($column=="tags")
        {
            $tags = null;

            foreach ($casteller->tagsArray('NAME') as $tag) //filterd tag -- paint it blue https://open.spotify.com/track/63T7DJ1AFDD6Bn8VzG6JE8?si=QfAzbKC3Sl2sNxmBj12crA
            {
                $tags .= '<span class="badge badge-primary pull-'.$align.'" style="margin-left: 3px; margin-bottom: 3px;">'.$tag.'</span>';
            }

            return $tags;
        }
        else
        {
            return $casteller->{$column};
        }
    }

    /** Replace special characters from a string */
    public static function replaceSpecialCharacters($string)
    {
        $unwanted_array = array('Š'=>'S', 'š'=>'s', 'Ž'=>'Z', 'ž'=>'z', 'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A', 'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E',
            'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U',
            'Ú'=>'U', 'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss', 'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a', 'æ'=>'a', 'ç'=>'c',
            'è'=>'e', 'é'=>'e', 'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o', 'ô'=>'o', 'õ'=>'o',
            'ö'=>'o', 'ø'=>'o', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'þ'=>'b', 'ÿ'=>'y');

        $string = strtr( $string, $unwanted_array);

        return preg_replace('/[^a-zA-Z0-9 ]+/', '', $string);
    }

    //private static $days_ca = ['dilluns', 'dimarts', 'dimecres', 'dijous', 'divendres', 'dissabte', 'diumenge'];

    //private static $months_ca = ['de gener', ''];
}


