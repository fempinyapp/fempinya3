<?php

namespace App\Providers;

use App\Attendance;
use App\Casteller;
use App\Event;
use App\Policies\BoardPolicy;
use App\Policies\CastellerPolicy;
use App\Policies\EventPolicy;
use App\Policies\TagPolicy;
use App\Tag;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
        Casteller::class => CastellerPolicy::class,
        Tag::class => TagPolicy::class,
        Event::class => EventPolicy::class,
        Board::class => BoardPolicy::class
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
    }
}
