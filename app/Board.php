<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Board extends Model
{
    protected $table = 'boards';
    protected $primaryKey = "id_board";
    public $timestamps = true;

    private static $colors = ['f44336', '9c27b0', '3f51b5', '2196f3', '009688', '8bc34a', 'ffeb3b', 'ffc107', '607d8b', '795548'];

    /** get num of rows (rengles) of BASE/BOARD */
    public function getArrayBasesRows() : array
    {
        $array = [];

        foreach(json_decode($this->data, true) as $name => $base)
        {
            $array[$name] = is_null($base) ? null : array_keys($base['structure']);
        }

        return $array;
    }

    /** get array colors */
    public static function getColors()
    {
        return self::$colors;
    }

    /** get base (tags) to Board */
    public function tags() : ? \Illuminate\Database\Eloquent\Collection
    {
        return Tag::join('board_tags', 'tags.id_tag', 'board_tags.tag_id')
            ->join('boards', 'board_tags.board_id', 'boards.id_board')
            ->where('id_board', $this->id_board)
            ->where('tags.type', 'BOARDS')
            ->select('tags.*')
            ->get();
    }

    /** true/false board has folre */
    public function hasFolre() : bool
    {
        if($this->type==='FOLRE' || $this->type==='MANILLES' || $this->type==='PUNTALS')
        {
            return true;
        }
        return false;
    }

    /** true/false board has mailles */
    public function hasManilles() : bool
    {
        if($this->type==='MANILLES' || $this->type==='PUNTALS')
        {
            return true;
        }
        return false;
    }

    /** true/false board has puntals */
    public function hasPuntals() : bool
    {
        if($this->type==='PUNTALS')
        {
            return true;
        }
        return false;
    }
}
