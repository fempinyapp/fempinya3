<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Jenssegers\Date\Date;

class Language
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if (!empty(Auth::user()->language))
        {
            App::setLocale(Auth::user()->language);
            Date::setLocale(Auth::user()->language);
            setlocale(LC_MONETARY, 'es_ES');
        }

        return $next($request);
    }
}
