<?php

namespace App\Http\Controllers;

use App\Colla;
use App\User;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Illuminate\View\View;
use Intervention\Image\ImageManagerStatic as Image;

class ProfileController extends Controller
{
    /**
     * Edit Profile User
     */
    public function getSetupUser()
    {
        $user = Auth::user();

        if($user->accesAdmin())
        {
            $data_content['colles'] = Colla::select('id_colla', 'name')->get();
        }

        $data_content['users'] = $user;
        $data_content['colla'] = Colla::getCurrent();
        return view('profile.user', $data_content);

    }

    /**
     * Update user profile.
     *
     * @param Request $request
     * @return RedirectResponse|Redirector
     */
    public function postUpdateUserProfile(Request $request)
    {
        $request->validate([
            'name' => 'required|max:255|min:3',
            'language' => 'required|size:2',
            'photo'=>'sometimes|file|image|mimes:jpeg,png|max:2048'
        ]);

        $user = User::find(Auth::user()->id_user);

        if($user->accesAdmin())
        {
            $user->colla_id = $request->input('select_colla');
        }

        $user->name = $request->input('name');
        $user->language = $request->input('language');

        $user->save();

        //exist file, upload
        if($request->file('photo'))
        {
            //if photo, delete photos
            if($user->photo)
            {
                unlink(public_path('media/image/users').'/'.$user->photo);

                $start = strpos($user->photo, '_')+1;
                $photo_name = substr($user->photo, $start, 10);
                unlink(public_path('media/image/users').'/'.$user->id_user.'_'.$photo_name.'-xs.jpg');
                unlink(public_path('media/image/users').'/'.$user->id_user.'_'.$photo_name.'-med.jpg');

            }

            $random_str = Str::random(10);
            $image_input = $request->file('photo');


            $imatge_xs = Image::make($image_input);
            $imatge_xs->fit(32);
            $imatge_xs->encode('jpg');
            $imatge_xs->save(public_path('media/image/users/').$user->id_user.'_'.$random_str.'-xs'.'.jpg');

            $imatge_med = Image::make($image_input);
            $imatge_med->fit(128);
            $imatge_med->encode('jpg');
            $imatge_med->save(public_path('media/image/users/').$user->id_user.'_'.$random_str.'-med'.'.jpg');


            $user->photo = $user->id_user.'_'.$random_str;
            $user->save();
        }

        Session::flash('status_ok', trans('user.user_updated'));
        return redirect(route('profile.user'));
    }

    /** update user password
     * @param Request $request
     * @return RedirectResponse|Redirector
     */
    public function postUpdateUserPassword(Request $request)
    {
        $request->validate([
            'confirm_password' => [
                'required',
                'string',
                'min:8',             // must be at least 8 characters in length
                'regex:/[a-z]/',      // must contain at least one lowercase letter
                'regex:/[0-9]/',      // must contain at least one digit
            ],
            'new_password' => [
                'required',
                'string',
                'min:8',             // must be at least 8 characters in length
                'regex:/[a-z]/',      // must contain at least one lowercase letter
                'regex:/[0-9]/',      // must contain at least one digit
            ],
        ]);

        $user = User::find(Auth::user()->id_user);

        if(!Hash::check($request->password, $user->password))
        {
            Session::flash('status_ko', trans('user.user_password_not_correct'));
            return redirect(route('profile.user'));
        }

        if ($request->new_password == $request->confirm_password)
        {
            $user->setPassword($request->new_password);

            Session::flash('status_ok', trans('user.user_password_updated'));
            return redirect(route('profile.user'));
        }
        else
        {
            Session::flash('status_ko', trans('user.user_password_not_equals'));
            return redirect(route('profile.user'));
        }
    }

    public function postAddUser(Request $request)
    {
        if(!Auth::user()->accesWriteColla()) abort(403);

        $colla = Colla::getCurrent();

        $request->validate([
            'email' => 'required|email:rfc|unique:users,email',
            'name' => 'required|max:255|min:3',
            'language' => 'required|size:2',
            'confirm_password' => [
                'required',
                'string',
                'min:8',             // must be at least 8 characters in length
                'regex:/[a-z]/',      // must contain at least one lowercase letter
                'regex:/[0-9]/',      // must contain at least one digit
            ],
            'password' => [
                'required',
                'string',
                'min:8',             // must be at least 8 characters in length
                'regex:/[a-z]/',      // must contain at least one lowercase letter
                'regex:/[0-9]/',      // must contain at least one digit
            ],
        ]);

        if ($request->password != $request->confirm_password)
        {
            if(!Auth::user()->accesColla()) abort(403);

            $data_content['colla'] = $colla;
            $data_content['users'] = User::where('colla_id', $colla->id_colla)->where('role', '!=', 'ADMIN')->get();
            $data_content['active'] = ['li_profile' => '', 'div_profile' => '', 'li_users' => 'active', 'div_users' => 'show active'];

            Session::flash('status_ko', trans('user.user_password_not_equals'));
            return view('profile.colla', $data_content);
        }


        $user = new User();
        $user->colla_id = $colla->id_colla;
        $user->role = 'COLLA';
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->language = $request->input('language');
        $user->password = Hash::make($request->input('password'));

        //PERMISSIONS
        $permission[0] = 1;
        $permission[1] = 0;

        //permission colla
        switch ($request->input('permission_colla'))
        {
            case 'DENIED':
                $permission[2] = 0;
                $permission[3] = 0;
                break;
            case 'READ':
                $permission[2] = 1;
                $permission[3] = 0;
                break;
            case 'WRITE':
                $permission[2] = 1;
                $permission[3] = 1;
                break;
        }

        //permission castellers
        switch ($request->input('permission_castellers'))
        {
            case 'DENIED':
                $permission[4] = 0;
                $permission[5] = 0;
                break;
            case 'READ':
                $permission[4] = 1;
                $permission[5] = 0;
                break;
            case 'WRITE':
                $permission[4] = 1;
                $permission[5] = 1;
                break;
        }

        //permission events
        switch ($request->input('permission_events'))
        {
            case 'DENIED':
                $permission[6] = 0;
                $permission[7] = 0;
                break;
            case 'READ':
                $permission[6] = 1;
                $permission[7] = 0;
                break;
            case 'WRITE':
                $permission[6] = 1;
                $permission[7] = 1;
                break;
        }

        //permission boards
        switch ($request->input('permission_pinyes'))
        {
            case 'DENIED':
                $permission[8] = 0;
                $permission[9] = 0;
                break;
            case 'READ':
                $permission[8] = 1;
                $permission[9] = 0;
                break;
            case 'WRITE':
                $permission[8] = 1;
                $permission[9] = 1;
                break;
        }

        //permission telegram
        switch ($request->input('permission_telegram'))
        {
            case 'DENIED':
                $permission[10] = 0;
                $permission[11] = 0;
                break;
            case 'READ':
                $permission[10] = 1;
                $permission[11] = 0;
                break;
            case 'WRITE':
                $permission[10] = 1;
                $permission[11] = 1;
                break;
        }

        $user->type = bindec(implode($permission));
        $user->save();

        Session::flash('status_ok', trans('user.user_added'));
        return redirect(route('profile.colla'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param User $user
     * @return Factory|View
     */
    public function getEditUserModalAjax(User $user)
    {
        $colla = Colla::getCurrent();
        if(!Auth::user()->accesColla() || $user->colla_id!=$colla->id_colla) return view('errors.403');
        $data_content['user'] = $user;

        return view('profile.modal-update-user', $data_content);
    }

    /** update user, profile and permissions
     * @param Request $request
     * @param User $user
     * @return RedirectResponse|Redirector
     */
    public function postUpdateUser(Request $request, User $user)
    {
        $colla = Colla::getCurrent();

        if(!Auth::user()->accesWriteColla() || $user->colla_id!=$colla->id_colla) abort(403);

        $request->validate([
            'name' => 'required|max:255|min:3',
            'language' => 'required|size:2',
            'permission_colla' => 'required|max:6|min:4',
            'permission_castellers' => 'required|max:6|min:4',
            'permission_events' => 'required|max:6|min:4',
            'permission_pinyes' => 'required|max:6|min:4',
            'permission_telegram' => 'required|max:6|min:4',
        ]);

        $user->name = $request->input('name');
        $user->language = $request->input('language');

        if($request->has('permission_admin') && $request->input('permission_admin')==='ADMIN')
        {
            $user->role = 'ADMIN';
            $user->type = 4095;
        }
        else
        {
            //PERMISSIONS
            $permission[0] = 1;
            $permission[1] = 0;

            //permission colla
            switch ($request->input('permission_colla'))
            {
                case 'DENIED':
                    $permission[2] = 0;
                    $permission[3] = 0;
                    break;
                case 'READ':
                    $permission[2] = 1;
                    $permission[3] = 0;
                    break;
                case 'WRITE':
                    $permission[2] = 1;
                    $permission[3] = 1;
                    break;
            }

            //permission castellers
            switch ($request->input('permission_castellers'))
            {
                case 'DENIED':
                    $permission[4] = 0;
                    $permission[5] = 0;
                    break;
                case 'READ':
                    $permission[4] = 1;
                    $permission[5] = 0;
                    break;
                case 'WRITE':
                    $permission[4] = 1;
                    $permission[5] = 1;
                    break;
            }

            //permission events
            switch ($request->input('permission_events'))
            {
                case 'DENIED':
                    $permission[6] = 0;
                    $permission[7] = 0;
                    break;
                case 'READ':
                    $permission[6] = 1;
                    $permission[7] = 0;
                    break;
                case 'WRITE':
                    $permission[6] = 1;
                    $permission[7] = 1;
                    break;
            }

            //permission boards
            switch ($request->input('permission_pinyes'))
            {
                case 'DENIED':
                    $permission[8] = 0;
                    $permission[9] = 0;
                    break;
                case 'READ':
                    $permission[8] = 1;
                    $permission[9] = 0;
                    break;
                case 'WRITE':
                    $permission[8] = 1;
                    $permission[9] = 1;
                    break;
            }

            //permission telegram
            switch ($request->input('permission_telegram'))
            {
                case 'DENIED':
                    $permission[10] = 0;
                    $permission[11] = 0;
                    break;
                case 'READ':
                    $permission[10] = 1;
                    $permission[11] = 0;
                    break;
                case 'WRITE':
                    $permission[10] = 1;
                    $permission[11] = 1;
                    break;
            }

            $user->role = 'COLLA';
            $user->type = bindec(implode($permission));
        }

        $user->save();

        if(strpos($request->headers->get('referer'), 'admin/users'))
        {
            Session::flash('status_ok', trans('user.user_updated'));
            return redirect(route('admin.users'));
        }
        else
        {
            Session::flash('status_ok', trans('user.user_updated'));
            return redirect(route('profile.colla'));
        }

    }

    /** destroy user form colla profile
     * @param User $user
     * @return RedirectResponse|Redirector
     * @throws \Exception
     */
    public function postDestroyUser(User $user)
    {
        $colla = Colla::getCurrent();

        if(!Auth::user()->accesWriteColla() || $user->colla_id!=$colla->id_colla) return view('errors.403');

        $user->delete();

        $users = User::where('colla_id', $colla->id_colla)->where('role', '!=', 'ADMIN')->get();

        $data_content['colla'] = $colla;
        $data_content['users'] = $users;

        Session::flash('status_ok', trans('admin.user_destroyed'));
        return redirect(route('profile.colla'));
    }


    /**
     * get colla for profile.
     *
     * @return Factory|View
     * @throws AuthorizationException
     */
    public function getSetupColla()
    {
        if(!Auth::user()->accesColla()) abort(403);

        $colla = Colla::getCurrent();

        $data_content['colla'] = $colla;
        $data_content['users'] = User::where('colla_id', $colla->id_colla)->where('role', '!=', 'ADMIN')->get();
        $data_content['active'] = ['li_profile' => 'active', 'div_profile' => 'show active', 'li_users' => '', 'div_users' => ''];

        return view('profile.colla', $data_content);

    }
    /** update colla form edit colla profile
     * @param Request $request
     * @return RedirectResponse|Redirector
     */
    public function postUpdateColla(Request $request)
    {
        if(!Auth::user()->accesWriteColla()) abort(403);

        $colla = Colla::getCurrent();

        $request->validate([
            'name' => 'required|max:50|min:3',
            'email' => 'required|email:rfc',
            'phone' => 'required|numeric',
            'country' => 'required|max:100|min:3',
            'city' => 'required|max:100|min:3',
            'logo'=>'sometimes|file|image|mimes:jpeg,png|max:2048'
        ]);

        $colla->name = $request->input('name');
        $colla->email = $request->input('email');
        $colla->phone = $request->input('phone');
        $colla->country = $request->input('country');
        $colla->city = $request->input('city');
        $colla->color = $request->input('color');

        $colla->save();

        //exist file, upload
        if($request->file('logo'))
        {
            //Has logo, destroy logo
            if($colla->logo)
            {
                unlink(public_path('media/image/colles/'.$colla->shortname).'/'.$colla->logo);
            }

            $file = $request->file('logo');
            $file_name = Str::random(10).'.'.$request->logo->extension();
            $file->move(public_path('media/image/colles/'.$colla->shortname), $file_name);

            $colla->logo = $file_name;
            $colla->save();
        }

        $data_content['active'] = ['li_profile' => '', 'div_profile' => '', 'li_users' => 'active', 'div_users' => 'show active'];

        Session::flash('status_ok', trans('admin.colla_updated'));
        return redirect(route('profile.colla'));
    }
}
