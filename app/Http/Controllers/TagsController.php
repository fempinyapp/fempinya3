<?php

namespace App\Http\Controllers;

use App\Colla;
use App\Tag;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Illuminate\View\View;

class TagsController extends Controller
{
    /**
     * List Tags type='CASTELLERS'.
     *
     * @return Factory|View
     */
    public function getListTags()
    {
        if(!Auth::user()->accesBBDD()) abort(403);

        $data_content['colla'] = Colla::getCurrent();
        $data_content['tags'] = Tag::currentTags();
        $data_content['tags_groups'] = Tag::groups();
        $data_content['positions'] = Tag::currentTags('POSITIONS');
        $data_content['type'] = 'CASTELLERS';

        return view('tags.list', $data_content);
    }

    /**
     * List Tags type='EVENTS'.
     *
     * @return Factory|View
     */
    public function getListEvents()
    {
        if(!Auth::user()->accesEvents()) abort(403);

        $data_content['colla'] = Colla::getCurrent();
        $data_content['tags'] = Tag::currentTags('EVENTS');
        $data_content['type'] = 'EVENTS';

        return view('tags.list', $data_content);
    }

    /** List Tags (bases) type='BOARDS' */
    public function getListBoards()
    {
        if(!Auth::user()->accesEvents()) abort(403);

        $data_content['colla'] = Colla::getCurrent();
        $data_content['tags'] = Tag::currentTags('BOARDS');
        $data_content['type'] = 'BOARDS';

        return view('tags.list', $data_content);
    }

    /** List Tags type='ATTENDANCE' */
    public function getListAttendance()
    {
        if(!Auth::user()->accesEvents()) abort(403);

        $data_content['colla'] = Colla::getCurrent();
        $data_content['tags'] = Tag::currentTags('ATTENDANCE');
        $data_content['type'] = 'ATTENDANCE';

        return view('tags.list', $data_content);
    }

    /**
     * get Modal add Tag via AJAX
     *
     * @param string $type
     * @return Factory|View
     */
    public function getAddTagModalAjax($type = 'CASTELLERS')
    {
        if(!Auth::user()->accesWriteBBDD()) abort(403);

        if($type=='CASTELLERS') $data_content['tags_groups'] = Tag::groups();
        if($type=='POSITIONS') $data_content['tags_groups'] = Tag::groups('POSITIONS');

        $data_content['type'] = $type;

        return view('tags.modal-add', $data_content);
    }

    /** toggle Tag group via AJAX
     * @param Tag $tag
     * @param $group
     * @return string
     * @throws AuthorizationException
     */
    public function getToggleGroupAjax(Tag $tag, $group) : string
    {
        if(!Auth::user()->accesWriteBBDD()) abort(403);
        $this->authorize('getTag', $tag);

        $tag->group = $group;

        $tag->save();

        return 'ok';
    }

    /**
     * Add Casteller Tag
     *
     * @param Request $request
     * @return RedirectResponse|Redirector
     */
    public function postAddCastellerTag(Request $request)
    {
        if(!Auth::user()->accesWriteBBDD()) abort(403);

        $request->validate([
            'name' => 'required|max:150|min:2'
        ]);

        $colla = Colla::getCurrent();

        //Check vàlid name
        if(!Tag::validName($request->input('name')))
        {
            Session::flash('status_ko', trans('tag.invalid_name'));
            return redirect(route('castellers.tags'));
        }

        //Check if exist
        $value = Str::slug($request->input('name'));
        $tags_same = Tag::where('colla_id', $colla->id_colla)->where('type', 'CASTELLERS')->where('value',  $value)->count();

        if($tags_same>=1)
        {
            Session::flash('status_ko', trans('tag.tag_exist'));
            return redirect(route('castellers.tags'));
        }

        $tag = new Tag();

        $tag->colla_id = $colla->id_colla;
        $tag->type = 'CASTELLERS';
        $tag->name = $request->input('name');
        $tag->value = $value;
        $tag->group = $request->input('group');

        $tag->save();

        Session::flash('status_ok', trans('tag.tag_added'));
        return redirect(route('castellers.tags'));
    }

    public function postAddPositionTag(Request $request)
    {
        if(!Auth::user()->accesWriteBBDD()) abort(403);

        $request->validate([
            'name' => 'required|max:150|min:2'
        ]);

        $colla = Colla::getCurrent();

        //Check vàlid name
        if(!Tag::validName($request->input('name')))
        {
            Session::flash('status_ko', trans('tag.invalid_name'));
            return redirect(route('castellers.tags'));
        }

        //Check if exist
        $value = Str::slug($request->input('name'));
        $tags_same = Tag::where('colla_id', $colla->id_colla)->where('type', 'POSITIONS')->where('value',  $value)->count();

        if($tags_same>=1)
        {
            Session::flash('status_ko', trans('tag.tag_exist'));
            return redirect(route('castellers.tags'));
        }

        $tag = new Tag();

        $tag->colla_id = $colla->id_colla;
        $tag->type = 'POSITIONS';
        $tag->name = $request->input('name');
        $tag->value = $value;

        $tag->save();

        Session::flash('status_ok', trans('tag.tag_added'));
        return redirect(route('castellers.tags'));
    }

    public function postAddBoardTag(Request $request)
    {
        if(!Auth::user()->accesWriteBoards()) abort(403);

        $request->validate([
            'name' => 'required|max:150|min:2'
        ]);

        $colla = Colla::getCurrent();

        //Check vàlid name
        if(!Tag::validName($request->input('name')))
        {
            Session::flash('status_ko', trans('tag.invalid_name'));
            return redirect(route('castellers.tags'));
        }

        //Check if exist
        $value = Str::slug($request->input('name'));
        $tags_same = Tag::where('colla_id', $colla->id_colla)->where('type', 'BOARDS')->where('value',  $value)->count();

        if($tags_same>=1)
        {
            Session::flash('status_ko', trans('tag.tag_exist'));
            return redirect(route('castellers.tags'));
        }

        $tag = new Tag();

        $tag->colla_id = $colla->id_colla;
        $tag->type = 'BOARDS';
        $tag->name = $request->input('name');
        $tag->value = $value;
        $tag->group = $request->input('group');

        $tag->save();

        Session::flash('status_ok', trans('boards.base_added'));
        return redirect(route('boards.tags'));
    }

    /** Add Event Tag
     * @param Request $request
     * @return RedirectResponse|Redirector
     */
    public function postAddEventTag(Request $request)
    {
        if(!Auth::user()->accesWriteEvents()) abort(403);

        $request->validate([
            'name' => 'required|max:150|min:2'
        ]);

        $colla = Colla::getCurrent();

        //Check vàlid name
        if(!Tag::validName($request->input('name')))
        {
            Session::flash('status_ko', trans('tag.invalid_name'));
            return redirect(route('events.tags'));
        }

        //Check if exist
        $value = Str::slug($request->input('name'));
        $tags_same = Tag::where('colla_id', $colla->id_colla)->where('type', 'EVENTS')->where('value',  $value)->count();

        if($tags_same>=1)
        {
            Session::flash('status_ko', trans('tag.tag_exist'));
            return redirect(route('events.tags'));
        }

        $tag = new Tag();

        $tag->colla_id = $colla->id_colla;
        $tag->type = 'EVENTS';
        $tag->name = $request->input('name');
        $tag->value = $value;

        $tag->save();

        Session::flash('status_ok', trans('tag.tag_added'));
        return redirect(route('events.tags'));
    }

    /** ADD Atendance Tag
     * @param Request $request
     * @return RedirectResponse|Redirector
     */
    public function postAddAttendanceTag(Request $request)
    {
        if(!Auth::user()->accesWriteEvents()) abort(403);

        $request->validate([
            'name' => 'required|max:150|min:2'
        ]);

        $colla = Colla::getCurrent();

        //Check vàlid name
        if(!Tag::validNameAttendance($request->input('name')))
        {
            Session::flash('status_ko', trans('tag.invalid_name'));
            return redirect(route('events.answers'));
        }

        //Check if exist
        $value = Str::slug($request->input('name'));
        $tags_same = Tag::where('colla_id', $colla->id_colla)->where('type', 'ATTENDANCE')->where('value',  $value)->count();

        if($tags_same>=1)
        {
            Session::flash('status_ko', trans('tag.tag_exist'));
            return redirect(route('events.answers'));
        }

        $tag = new Tag();

        $tag->colla_id = $colla->id_colla;
        $tag->type = 'ATTENDANCE';
        $tag->name = $request->input('name');
        $tag->value = $value;

        $tag->save();

        Session::flash('status_ok', trans('tag.tag_added'));
        return redirect(route('events.answers'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Tag $tag
     * @return Factory|View
     */
    public function getEditTagsModalAjax(Tag $tag)
    {
        if($tag->type==='CASTELLERS')
        {
            if(!Auth::user()->accesWriteBBDD()) abort(403);
            $data_content['tags_groups'] = Tag::groups();
        }
        elseif($tag->type==='EVENTS')
        {
            if(!Auth::user()->accesWriteEvents()) abort(403);
        }
        elseif($tag->type==='ATTENDANCE')
        {
            if(!Auth::user()->accesWriteEvents()) abort(403);
        }

        $data_content['type'] = $tag->type;
        $data_content['tag'] = $tag;

        return view('tags.modal-add', $data_content);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Tag $tag
     * @return RedirectResponse|Redirector
     * @throws AuthorizationException
     */
    public function postUpdateTag(Request $request, Tag $tag)
    {
        if($tag->type==='CASTELLERS' || $tag->type==='POSITIONS' )
        {
            if(!Auth::user()->accesWriteBBDD()) abort(403);
        }
        elseif($tag->type==='EVENTS' || $tag->type==='ATTENDANCE')
        {
            if(!Auth::user()->accesWriteEvents()) abort(403);
        }
        elseif($tag->type==='BOARDS')
        {
            if(!Auth::user()->accesWriteBoards()) abort(403);
        }

        $this->authorize('getTag', $tag);

        $request->validate([
            'name' => 'required|max:150|min:2'
        ]);

        //Check valid name
        if($tag->type==='EVENTS' || $tag->type==='CASTELLERS' || $tag->type==='POSITIONS')
        {
            if(!Tag::validName($request->input('name')))
            {
                Session::flash('status_ko', trans('tag.invalid_name'));

                if($tag->type=='CASTELLERS')
                {
                    return redirect(route('castellers.tags'));
                }
                elseif($tag->type=='EVENTS')
                {
                    return redirect(route('events.tags'));
                }


            }
        }
        elseif($tag->type=='ATTENDANCE')
        {
            if(!Tag::validNameAttendance($request->input('name')))
            {
                return redirect(route('events.answers'));
            }
        }
        elseif($tag->type=='BOARDS')
        {
            if(!Tag::validName($request->input('name')))
            {
                return redirect(route('boards.tags'));
            }
        }

        $value = Str::slug($request->input('name'));

        $tag->name = $request->input('name');
        $tag->value = $value;

        if($tag->type=='CASTELLERS')
        {
            $tag->group = $request->input('group');
        }

        $tag->save();

        Session::flash('status_ok', trans('tag.tag_updated'));
        if($tag->type=='CASTELLERS')
        {
            return redirect(route('castellers.tags'));
        }
        elseif($tag->type=='EVENTS')
        {
            return redirect(route('events.tags'));
        }
        elseif($tag->type=='ATTENDANCE')
        {
            return redirect(route('events.answers'));
        }
        elseif($tag->type=='BOARDS')
        {
            return redirect(route('boards.tags'));
        }
    }

    /**
     * Destroy tag
     *
     * @param Tag $tag
     * @return RedirectResponse|Redirector
     * @throws AuthorizationException
     */
    public function postDestroyTag(Tag $tag)
    {
        if($tag->type==='CASTELLERS' || $tag->type==='POSITIONS')
        {
            if(!Auth::user()->accesWriteBBDD()) abort(403);
        }
        elseif($tag->type==='EVENTS' || $tag->type==='ATTENDANCE')
        {
            if(!Auth::user()->accesWriteEvents()) abort(403);
        }
        elseif($tag->type==='BOARDS')
        {
            if(!Auth::user()->accesWriteBoards()) abort(403);
        }

        $type = $tag->type;

        $this->authorize('getTag', $tag);

        if($tag->isUsed())
        {
            Session::flash('status_ko', trans('tag.btn_delete_dissabled'));
            return redirect(route('castellers.tags'));
        }

        $tag->delete();

        if($type==='POSITIONS')
        {
            Session::flash('status_ok', trans('tag.position_deleted'));
        }
        else
        {
            Session::flash('status_ok', trans('tag.tag_deleted'));
        }

        if($type==='CASTELLERS' || $type==='POSITIONS')
        {
            return redirect(route('castellers.tags'));
        }
        elseif($type==='EVENTS')
        {
            return redirect(route('events.tags'));
        }
        elseif($type==='ATTENDANCE')
        {
            return redirect(route('events.answers'));
        }
        elseif($type==='BOARDS')
        {
            return redirect(route('boards.tags'));
        }
    }
}
