<?php

namespace App\Http\Controllers;

use App\Colla;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Illuminate\View\View;

class CollesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Factory|View
     */
    public function getList()
    {
        $colles = Colla::get();

        $data_content['colles'] = $colles;

        return view('admin.colles.list', $data_content);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Factory|View
     */
    public function getAddCollaModal()
    {
        return view('admin.colles.modal-add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return RedirectResponse|Redirector
     */
    public function postStoreColla(Request $request)
    {
        $request->validate([
            'name' => 'required|max:50|min:3',
            'email' => 'required|email:rfc',
            'phone' => 'required|numeric',
            'country' => 'required|max:100|min:3',
            'city' => 'required|max:100|min:3',
            'shortname' => 'required|max:20|min:3|unique:colles,shortname',
            'logo'=>'sometimes|file|image|mimes:jpeg,png|max:2048'
        ]);

        $shortname = $request->input('shortname');

        $colles_shortname = Colla::where('shortname', $shortname)->get();

        if(count($colles_shortname) > 0)
        {
            Session::flash('status_ko', trans('admin.error_same_shortname'));
            return redirect(route('admin.colles'));
        }
        else
        {
            $colla = new Colla();

            $colla->name = $request->input('name');
            $colla->email = $request->input('email');
            $colla->phone = $request->input('phone');
            $colla->country = $request->input('country');
            $colla->city = $request->input('city');
            $colla->color = $request->input('color');
            $colla->shortname = $shortname;
            $colla->save();

            //dreate directories
            mkdir(public_path('media/colles/'.$colla->shortname));
            mkdir(public_path('media/colles/'.$colla->shortname.'/castellers'));
            mkdir(public_path('media/colles/'.$colla->shortname.'/svg'));

            //exist file, upload
            if($request->file('logo'))
            {
                $file = $request->file('logo');
                $file_name = Str::random(10).'.'.$request->logo->extension();
                $file->move(public_path('media/colles/'.$colla->shortname), $file_name);

                $colla->logo = $file_name;
                $colla->save();
            }




            Session::flash('status_ok', trans('admin.colla_added'));
            return redirect(route('admin.colles'));
        }
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\colla  $colla
     * @return Factory|View
     */
    public function getEditCollaModalAjax(colla $colla)
    {
        $data_content['colla'] = $colla;

        return view('admin.colles.modal-add', $data_content);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\colla  $colla
     * @return RedirectResponse|Redirector
     */
    public function postUpdateColla(Request $request, colla $colla)
    {
        $request->validate([
            'name' => 'required|max:50|min:3',
            'email' => 'required|email:rfc',
            'phone' => 'required|numeric',
            'country' => 'required|max:100|min:3',
            'city' => 'required|max:100|min:3',
            'logo'=>'sometimes|file|image|mimes:jpeg,png|max:2048'
        ]);

        $colla->name = $request->input('name');
        $colla->email = $request->input('email');
        $colla->phone = $request->input('phone');
        $colla->country = $request->input('country');
        $colla->city = $request->input('city');
        $colla->color = $request->input('color');


        $colla->save();

        //exist file, upload
        if($request->file('logo'))
        {
            //Has logo, destroy logo
            if($colla->logo)
            {
                unlink(public_path('media/colles/'.$colla->shortname).'/'.$colla->logo);
            }

            $file = $request->file('logo');
            $file_name = Str::random(10).'.'.$request->logo->extension();
            $file->move(public_path('media/colles/'.$colla->shortname), $file_name);

            $colla->logo = $file_name;
            $colla->save();
        }


        Session::flash('status_ok', trans('admin.colla_updated'));
        return redirect(route('admin.colles'));

    }



    /**
     * Remove the specified resource from storage.
     *
     * @param \App\colla $colla
     * @return RedirectResponse|Redirector
     * @throws \Exception
     */
    public function postDestroyColla(colla $colla)
    {
        //if colla admin, not delete
        if($colla->id_colla===1)
        {
            Session::flash('status_ko', trans('admin.colla_cant_destroyed'));
            return redirect(route('admin.colles'));
        }

        //Colla is the own user, not delete colla
        if(Auth::user()->role!='ADMIN' || Auth::user()->colla_id===$colla->id_colla)
        {
            abort(403);
        }

        //Has logo, destroy logo
        if($colla->logo)
        {
            unlink(public_path('media/colles/'.$colla->shortname).'/'.$colla->logo);
            //TODO: delete all files
        }

        $colla->delete();

        Session::flash('status_ok', trans('admin.colla_destroyed'));
        return redirect(route('admin.colles'));
    }
}
