<?php

namespace App\Http\Controllers;

use App\Colla;
use App\Event;
use App\Helpers\Humans;
use App\Tag;
use Carbon\Carbon;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request as RequestInput;
use Illuminate\Support\Facades\Session;
use Illuminate\View\View;
use PHPUnit\Util\Json;

class EventsController extends Controller
{
    /**
     * get list of Events
     *
     * @return Factory|View
     */
    public function getList()
    {
        if(!Auth::user()->accesEvents()) abort(403);

        $data_content['tags'] = Tag::currentTags('EVENTS');

        return view('events.list', $data_content);
    }

    /** get $time = upcoming/past events via AJAX
     * @param string $time
     */
    public function postListAjax(string $time = 'upcoming')
    {
        if(!Auth::user()->accesEvents()) abort(403);

        //params
        $limit = RequestInput::input('length');
        $skip = RequestInput::input('start');

        //order
        //$column_num = intval(Request::input('order')[0]['column']);
        $dir = RequestInput::input('order')[0]['dir'];
        $column_order = RequestInput::input('columns')[intval(RequestInput::input('order')[0]['column'])]['name'];

        //tags
        $tags = RequestInput::input('tags');
        $tags_search_type = RequestInput::input('filter_search_type'); //AND or OR

        if($time=='upcoming')
        {
            $events = Event::getUpcommingEventsByTags($tags, $tags_search_type);
        }
        elseif($time=='past')
        {
            $events = Event::getPastEventsByTags($tags, $tags_search_type);
        }

        $data = new \stdClass();
        $data->data = array();

        if (!empty($_POST['search']['value']))
        {
            $events->where(function($q){
                $q->orWhere('events.name' , 'LIKE' , '%'.$_POST['search']['value'].'%');
            });
        }

        $all_events = $events;
        $data->recordsTotal = count($all_events->get());
        $data->recordsFiltered = count($all_events->get());

        $events = $events->take($limit) ->skip($skip);
        $events = $events->orderBy($column_order, $dir)->get();

        foreach ($events as $event)
        {
            $array_event = [];

            $array_event['name'] = $event->name.' <span class="text-success" style="font-size: 11px;"><i class="fa fa-check"></i>'.$event->countAttenders()['YES'].'</span>
                                                    <span class="text-danger" style="font-size: 11px;"><i class="fa fa-close"></i>'.$event->countAttenders()['NO'].'</span>
                                                    <span class="text-warning" style="font-size: 11px;"><i class="fa fa-question"></i>'.$event->countAttenders()['UNKNOWN'].'</span>';

            $array_event['tags'] = Humans::readEventColumn($event->id_event, 'tags', 'right');
            $array_event['start_date'] = Humans::readEventColumn($event->id_event, 'start_date');
            $array_event['buttons'] = '<a href="'.route('event.attendance', $event->id_event).'" class="btn btn-info"><i class="fa fa-list-ul"></i></a> <a href="'.route('events.edit', $event->id_event).'" class="btn btn-warning"><i class="fa fa-pencil"></i></a>';
            if(Auth::user()->accesBoards())
            {
                $array_event['buttons'] .= '<a href="" class="btn btn-outline-primary"><i class="fa fa-first-order"></i></a>';
            }
            array_push($data->data, $array_event);
        }

        echo json_encode($data);
    }

    /**
     * form create one event
     *
     * @return Factory|View
     */
    public function getCreate()
    {
        if(!Auth::user()->accesWriteEvents()) abort(403);

        $data_content['type_add'] = 'ONE';
        $data_content['tags'] = Tag::currentTags('EVENTS');
        $data_content['attendance_answers'] = Tag::currentTags('ATTENDANCE');

        return view('events.create', $data_content);
    }

    /**
     * form create group of events
     *
     * @return Factory|View
     */
    public function getCreateGroup()
    {
        if(!Auth::user()->accesWriteEvents()) abort(403);

        $data_content['type_add'] = 'GROUP';
        $data_content['tags'] = Tag::currentTags('EVENTS');
        $data_content['attendance_answers'] = Tag::currentTags('ATTENDANCE');

        return view('events.create', $data_content);
    }

    /**
     * Store group of Events
     *
     * @param  \Illuminate\Http\Request  $request
     * @return RedirectResponse|Redirector
     */
    public function postStoreEventGroup(Request $request)
    {
        if(!Auth::user()->accesWriteEvents()) abort(403);

        $request->validate([
            'name' => 'required|max:100|min:3',
            'address' => 'nullable|max:255|min:3',
            'duration' => 'required|numeric',
            'hour' => 'required|numeric',
            'min' => 'required|numeric',
            'visiblity' => 'required',
        ]);

        $colla = Colla::getCurrent();

        $array_dates = explode(',', $request->start_dates);

        foreach ($array_dates as $date)
        {
            $date = substr($date, 0, strpos($date, '('));
            $date =  Carbon::parse($date);
            $date = $date->format('Y-m-d').' '.$request->hour.':'.$request->min.':00';

            $event = new Event();

            $event->colla_id = $colla->id_colla;
            //$event->type = null;
            $event->name = $request->name;
            $event->address = $request->address;
            $event->duration = $request->duration;
            $event->start_date = $date;

            //open date
            if($request->open_date_select==='now')
            {
                $event->open_date = Carbon::now()->toDateTimeString();
            }
            else
            {
                switch ($request->open_date_mode)
                {
                    case 'months':
                        $open_date = Carbon::parse($date)->subMonths(intval($request->open_date_time));
                        break;
                    case 'weeks':
                        $open_date = Carbon::parse($date)->subWeekdays(intval($request->open_date_time));
                        break;
                    case 'days':
                        $open_date = Carbon::parse($date)->subDays(intval($request->open_date_time));
                        break;
                    case 'hours':
                        $open_date = Carbon::parse($date)->subHours(intval($request->open_date_time));
                        break;
                }

                $event->open_date = $open_date->toDateTimeString();
            }

            //close date
            if($request->close_date_select==='now')
            {
                $event->close_date = Carbon::now()->toDateTimeString();
            }
            else
            {
                switch ($request->close_date_mode)
                {
                    case 'months':
                        $close_date = Carbon::parse($date)->subMonths(intval($request->close_date_time));
                        break;
                    case 'weeks':
                        $close_date = Carbon::parse($date)->subWeekdays(intval($request->close_date_time));
                        break;
                    case 'days':
                        $close_date = Carbon::parse($date)->subDays(intval($request->close_date_time));
                        break;
                    case 'hours':
                        $close_date = Carbon::parse($date)->subHours(intval($request->close_date_time));
                        break;
                }

                $event->close_date = $close_date->toDateTimeString();
            }

            $event->companions = $request->companions;
            $event->visible = $request->visiblity;

            $event->save();

            //tags
            foreach($request->input('tags') as $tag)
            {
                $tag = Tag::where('colla_id', $colla->id_colla)->where('value', $tag)->first();
                $event->addTag($tag);
            }

            //Attendance personalise answers
            if(!is_null($request->input('answers')))
            {
                foreach($request->input('answers') as $answer)
                {
                    $answer = Tag::where('colla_id', $colla->id_colla)->where('value', $answer)->first();
                    $event->addAttendanceAnswer($answer);
                }
            }

        }

        Session::flash('status_ok', trans('event.group_events_added'));
        return redirect(route('events.list'));
    }

    /** Store a single Event
     * @param Request $request
     * @return RedirectResponse|Redirector
     */
    public function postStoreEvent(Request $request)
    {
        if(!Auth::user()->accesWriteEvents()) abort(403);

        $request->validate([
            'name' => 'required|max:100|min:3',
            'address' => 'nullable|max:255|min:3',
            'duration' => 'required|numeric',
            'start_date' => 'required',
            'hour' => 'required|numeric',
            'min' => 'required|numeric',
            'visiblity' => 'required',
        ]);

        $colla = Colla::getCurrent();

        $date =  str_replace('/', '-', $request->start_date);
        $date = date('Y-m-d', strtotime($date)).' '.$request->hour.':'.$request->min.':00';

        $event = new Event();

        $event->colla_id = $colla->id_colla;
        //$event->type = null;
        $event->name = $request->name;
        $event->address = $request->address;
        $event->duration = $request->duration;
        $event->start_date = $date;

        //open date
        if($request->open_date_mode==='now')
        {
            $event->open_date = Carbon::now()->toDateTimeString();
        }
        else
        {
            $open_date =  str_replace('/', '-', $request->open_date);
            $open_date = date('Y-m-d', strtotime($open_date)).' '.$request->hour_open_date.':'.$request->min_open_date.':00';

            $event->open_date = $open_date;
        }

        //close date
        if($request->close_date_mode==='now')
        {
            $event->close_date = Carbon::now()->toDateTimeString();
        }
        else
        {
            $close_date =  str_replace('/', '-', $request->close_date);
            $close_date = date('Y-m-d', strtotime($close_date)).' '.$request->hour_close_date.':'.$request->min_close_date.':00';

            $event->close_date = $close_date;
        }

        $event->companions = $request->companions;
        $event->visible = $request->visiblity;

        $event->save();

        //tags
        foreach($request->input('tags') as $tag)
        {
            $tag = Tag::where('colla_id', $colla->id_colla)->where('value', $tag)->first();
            $event->addTag($tag);
        }

        //Attendance personalise answers
        if(!is_null($request->input('answers')))
        {
            foreach($request->input('answers') as $answer)
            {
                $answer = Tag::where('colla_id', $colla->id_colla)->where('value', $answer)->first();
                $event->addAttendanceAnswer($answer);
            }
        }


        Session::flash('status_ok', trans('event.event_added'));
        return redirect(route('events.list'));
    }


    /**
     * Edit Event.
     *
     * @param Event $event
     * @return Factory|View
     * @throws AuthorizationException
     */
    public function getEditEvent(Event $event)
    {
        $this->authorize('getEvent', $event);
        if(!Auth::user()->accesWriteEvents()) abort(403);

        $data_content['event'] = $event;
        $data_content['tags'] = Tag::currentTags('EVENTS');
        $data_content['attendance_answers'] = Tag::currentTags('ATTENDANCE');

        return view('events.create', $data_content);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param Event $event
     * @return RedirectResponse|Redirector
     * @throws AuthorizationException
     */
    public function postUpdateEvent(Request $request, Event $event)
    {
        if(!Auth::user()->accesWriteEvents()) abort(403);
        $this->authorize('getEvent', $event);

        $request->validate([
            'name' => 'required|max:100|min:3',
            'address' => 'nullable|max:255|min:3',
            'duration' => 'required|numeric',
            'start_date' => 'required',
            'hour' => 'required|numeric',
            'min' => 'required|numeric',
            'visiblity' => 'required',
        ]);

        $colla = Colla::getCurrent();

        $date =  str_replace('/', '-', $request->start_date);
        $date = date('Y-m-d', strtotime($date)).' '.$request->hour.':'.$request->min.':00';

        //$event->type = null;
        $event->name = $request->name;
        $event->address = $request->address;
        $event->duration = $request->duration;
        $event->start_date = $date;

        //open date
        if($request->open_date_mode==='now')
        {
            $event->open_date = Carbon::now()->toDateTimeString();
        }
        else
        {
            $open_date =  str_replace('/', '-', $request->open_date);
            $open_date = date('Y-m-d', strtotime($open_date)).' '.$request->hour_open_date.':'.$request->min_open_date.':00';

            $event->open_date = $open_date;
        }

        //close date
        if($request->close_date_mode==='now')
        {
            $event->close_date = Carbon::now()->toDateTimeString();
        }
        else
        {
            $close_date =  str_replace('/', '-', $request->close_date);
            $close_date = date('Y-m-d', strtotime($close_date)).' '.$request->hour_close_date.':'.$request->min_close_date.':00';

            $event->close_date = $close_date;
        }

        $event->companions = $request->companions;
        $event->visible = $request->visiblity;

        $event->save();

        //tags
        foreach($event->tags() as $tag)
        {
            $event->removeTag($tag);
        }

        foreach($request->input('tags') as $tag)
        {
            $tag = Tag::where('colla_id', $colla->id_colla)->where('value', $tag)->first();
            $event->addTag($tag);
        }

        //Attendance personalise answers
        foreach($event->attendanceAnswers() as $tag)
        {
            $event->removeAttendanceAnswer($tag);
        }

        if(!is_null($request->input('answers')))
        {
            foreach($request->input('answers') as $answer)
            {
                $answer = Tag::where('colla_id', $colla->id_colla)->where('value', $answer)->first();
                $event->addAttendanceAnswer($answer);
            }
        }


        Session::flash('status_ok', trans('event.event_updated'));
        return redirect(route('events.list'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Event $event
     * @return RedirectResponse|Redirector
     * @throws Exception
     */
    public function postDestroyEvent(Event $event)
    {
        if(!Auth::user()->accesWriteEvents()) abort(403);
        $this->authorize('getEvent', $event);

        $event->delete();

        Session::flash('status_ok', trans('event.event_destroyed'));
        return redirect(route('events.list'));
    }
}
