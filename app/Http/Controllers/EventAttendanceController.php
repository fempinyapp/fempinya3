<?php

namespace App\Http\Controllers;

use App\Colla;
use App\Helpers\Humans;
use App\Tag;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use App\Event;
use App\Attendance;
use App\Casteller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request as RequestInput;
use Illuminate\View\View;

class EventAttendanceController extends Controller
{
    /** List Castellers attenders at event
     * @param Event $event
     * @return Factory|View
     * @throws AuthorizationException
     */
    public function getIndex(Event $event)
    {
        if(!Auth::user()->accesEvents()) abort(403);
        $this->authorize('getEvent', $event);

        $data_content['event'] = $event;
        $data_content['tags'] = Tag::currentTags();
        $data_content['positions'] = Tag::currentTags('POSITION');

        return view('events.attendance.list', $data_content);
    }

    /** List Castellers attenders at event for datatable via AJAX
     * @param Event $event
     * @throws AuthorizationException
     */
    public function postListAttendersAjax(Event $event)
    {
        if(!Auth::user()->accesEvents()) abort(403);
        $this->authorize('getEvent', $event);

        $answers = $event->attendanceAnswers();

        //params
        $limit = RequestInput::input('length');
        $skip = RequestInput::input('start');

        //order
        $dir = RequestInput::input('order')[0]['dir'];
        $column_order = RequestInput::input('columns')[intval(RequestInput::input('order')[0]['column'])]['name'];

        //tags
        $tags = RequestInput::input('tags');
        $tags_search_type = RequestInput::input('filter_search_type'); //AND or OR

        $castellers = Casteller::filterCastellersByTags($tags, $tags_search_type);

        if(!is_null(RequestInput::input('status')))
        {
            $castellers = $castellers->leftJoin('attendance', 'castellers.id_casteller', 'attendance.casteller_id')
                ->where('attendance.event_id', $event->id_event)
                ->where(function($q)
                {
                    foreach (RequestInput::input('status') as $status)
                    {
                        $q->orWhere('attendance.status', $status);
                    }
                });
        }

        $data = new \stdClass();
        $data->data = array();

        if (!empty($_POST['search']['value']))
        {
            $castellers->where(function($q){
                $q->orWhere('castellers.name' , 'LIKE' , '%'.$_POST['search']['value'].'%');
                $q->orWhere('castellers.last_name' , 'LIKE' , '%'.$_POST['search']['value'].'%');
                $q->orWhere('castellers.alias', 'LIKE', '%'.$_POST['search']['value'].'%');
                $q->orWhere('castellers.email', 'LIKE', '%'.$_POST['search']['value'].'%');
                $q->orWhere('castellers.email2', 'LIKE', '%'.$_POST['search']['value'].'%');
            });
        }

        $all_castellers = $castellers;
        $data->recordsTotal = count($all_castellers->get());
        $data->recordsFiltered = count($all_castellers->get());

        $castellers = $castellers->take($limit) ->skip($skip);
        $castellers = $castellers->orderBy($column_order, $dir)->get();

        foreach($castellers as $casteller)
        {
            $attendance = Attendance::getAttendance($casteller->id_casteller, $event->id_event);
            $array_attender = [];

            $array_attender['name'] = '<a href="'.route('castellers.edit', $casteller->id_casteller).'" class="btn btn-info" style="margin-right: 6px;"><i class="fa fa-vcard-o"></i></a>';
            $array_attender['name'] .= (empty($casteller->alias)) ? $casteller->name.' '.$casteller->last_name : $casteller->name.' '.$casteller->last_name.' ('.$casteller->alias.')';

            switch($attendance->status)
            {
                case 'YES':
                    $array_attender['status'] ='<button class="btn btn-success btn-status" data-id_casteller="'.$casteller->id_casteller.'"><i class="fa fa-check"></i></button>';
                    break;
                case 'NO':
                    $array_attender['status'] ='<button class="btn btn-danger btn-status" data-id_casteller="'.$casteller->id_casteller.'"><i class="fa fa-close"></i></button>';
                    break;
                case 'UNKNOWN':
                    $array_attender['status'] ='<button class="btn btn-outline-warning btn-status" data-id_casteller="'.$casteller->id_casteller.'"><i class="fa fa-question"></i></button>';
                    break;
                default:
                    $array_attender['status'] ='<button class="btn btn-secondary btn-status" data-id_casteller="'.$casteller->id_casteller.'"><i class="fa fa-question"></i></button>';
                    break;
            }

            switch($attendance->status_verified)
            {
                case 'YES':
                    $array_attender['status_verified'] ='<button class="btn btn-success btn-status-verified" data-id_casteller="'.$casteller->id_casteller.'"><i class="fa fa-check"></i></button>';
                    break;
                case 'NO':
                    $array_attender['status_verified'] ='<button class="btn btn-danger btn-status-verified" data-id_casteller="'.$casteller->id_casteller.'"><i class="fa fa-close"></i></button>';
                    break;
                default:
                    $array_attender['status_verified'] ='<button class="btn btn-secondary btn-status-verified" data-id_casteller="'.$casteller->id_casteller.'"><i class="fa fa-question"></i></button>';
                    break;
            }

            if(count($answers)<1)
            {
                $array_attender['attendance_answers'] = '<select class="selectize2 form-control answers" name="answers[]" style="width: 100%" multiple disabled>';

                $array_attender['attendance_answers'] .= '</select>';
            }
            else
            {
                if(is_null($attendance->options)) $attendance->options = [];

                if($attendance->status=='YES')
                {
                    $array_attender['attendance_answers'] = '<select class="selectize2 form-control answers" name="answers[]" style="width: 100%" multiple>';
                }
                else
                {
                    $array_attender['attendance_answers'] = '<select class="selectize2 form-control answers" name="answers[]" style="width: 100%" multiple disabled>';
                }

                foreach($answers as $answer)
                {
                    if(in_array($answer->id_tag, json_decode($attendance->options)))
                    {
                        $array_attender['attendance_answers'] .= '<option value="'.$answer->id_tag.'" selected>'.$answer->name.'</option>';
                    }
                    else
                    {
                        $array_attender['attendance_answers'] .= '<option value="'.$answer->id_tag.'">'.$answer->name.'</option>';
                    }
                }
                $array_attender['attendance_answers'] .= '</select>';
            }


            if($attendance->status=='YES')
            {
                $array_attender['companions'] = '<input class="form-control companions" name="companions" type="number" data-id_casteller="'.$casteller->id_casteller.'" value="'.$attendance->companions.'">';
            }
            else
            {
                $array_attender['companions'] = '<input class="form-control companions" name="companions" type="number" data-id_casteller="'.$casteller->id_casteller.'" value="0" disabled>';
            }

            if(is_null($attendance->updated_at) || empty($attendance->updated_at))
            {
                $array_attender['last_update'] = '';
            }
            else
            {
                $array_attender['last_update'] = date('d/m/Y H:i:s', strtotime($attendance->updated_at));
            }


            array_push($data->data, $array_attender);
        }

        echo json_encode($data);
    }

    /** set attendance status Event / Casteller via AJAX
     * @param Request $request
     * @return array|mixed
     * @throws AuthorizationException
     */
    public function postSetStatusAjax(Request $request)
    {
        if(!Auth::user()->accesWriteEvents()) abort(403);
        $event = Event::find($request->id_event);
        $this->authorize('getEvent', $event);

        $id_casteller = $request->id_casteller;
        $id_event = $request->id_event;
        $status = $request->status;
        $companions = $request->companions;
        $source = 'WEB';

        if(is_null($request->answers))
        {
            $answers = [];
        }
        else
        {
            $answers = $request->answers;
        }

        $status = Attendance::setStatus($id_casteller, $id_event, $status, $companions, $source, $answers);

        return $status;
    }

    /** set attendance status verified Event / Casteller via AJAX
     * @param Request $request
     * @return array
     * @throws AuthorizationException
     */
    public function postSetStatusVerifiedAjax(Request $request)
    {
        if(!Auth::user()->accesWriteEvents()) abort(403);
        $event = Event::find($request->id_event);
        $this->authorize('getEvent', $event);

        $id_casteller = $request->id_casteller;
        $id_event = $request->id_event;
        $status_verified = $request->status;

        $status_verified = Attendance::setVerifiedStatus($id_casteller, $id_event, $status_verified);

        return $status_verified;
    }

    /** set attendance answers Event / Casteller via AJAX
     * @param Request $request
     * @throws AuthorizationException
     */
    public function postSetAnswersAjax(Request $request)
    {
        if(!Auth::user()->accesWriteEvents()) abort(403);
        $event = Event::find($request->id_event);
        $this->authorize('getEvent', $event);

        $id_casteller = $request->id_casteller;
        $id_event = $request->id_event;

        if(is_null($request->answers))
        {
            $answers = [];
        }
        else
        {
            $answers = $request->answers;
        }

        Attendance::setAnswers($id_casteller, $id_event, $answers);
    }

    /** set attendance answers Event / Casteller via AJAX
     * @param Request $request
     * @throws AuthorizationException
     */
    public function postSetCompanionsAjax(Request $request)
    {
        if(!Auth::user()->accesWriteEvents()) abort(403);
        $event = Event::find($request->id_event);
        $this->authorize('getEvent', $event);

        $id_casteller = $request->id_casteller;
        $id_event = $request->id_event;
        $companions = $request->companions;

        Attendance::setCompanions($id_casteller, $id_event, $companions);
    }

    /** get list attendance by blocks
     * @param Event $event
     * @return Factory|View
     * @throws AuthorizationException
     */
    public function getListBlocks(Event $event)
    {
        if(!Auth::user()->accesEvents()) abort(403);
        $this->authorize('getEvent', $event);

        /*$tags = Tag::where('colla_id', $event->colla_id)->where('type', 'CASTELLERS')
                    ->where('group', '1')
                    ->distinct()
                    ->get();*/

        $positions = Tag::currentTags('POSITION');

        $castellers = [];

        foreach($positions as $position)
        {
            $castellers[$position->value] = Casteller::filterCastellersByTags($position->value, 'AND')->get();

            foreach($castellers[$position->value] as $casteller)
            {
                $attendance = Attendance::getAttendance($casteller->id_casteller, $event->id_event);

                switch($attendance->status)
                {
                    case 'YES':
                        $casteller->status ='<button class="btn btn-success btn-status" data-id_casteller="'.$casteller->id_casteller.'"><i class="fa fa-check"></i></button>';
                        break;
                    case 'NO':
                        $casteller->status ='<button class="btn btn-danger btn-status" data-id_casteller="'.$casteller->id_casteller.'"><i class="fa fa-close"></i></button>';
                        break;
                    case 'UNKNOWN':
                        $casteller->status ='<button class="btn btn-outline-warning btn-status" data-id_casteller="'.$casteller->id_casteller.'"><i class="fa fa-question"></i></button>';
                        break;
                    default:
                        $casteller->status ='<button class="btn btn-secondary btn-status" data-id_casteller="'.$casteller->id_casteller.'"><i class="fa fa-question"></i></button>';
                        break;
                }

                switch($attendance->status_verified)
                {
                    case 'YES':
                        $casteller->status_verified ='<button class="btn btn-success btn-status-verified" data-id_casteller="'.$casteller->id_casteller.'"><i class="fa fa-check"></i></button>';
                        break;
                    case 'NO':
                        $casteller->status_verified ='<button class="btn btn-danger btn-status-verified" data-id_casteller="'.$casteller->id_casteller.'"><i class="fa fa-close"></i></button>';
                        break;
                    default:
                        $casteller->status_verified ='<button class="btn btn-secondary btn-status-verified" data-id_casteller="'.$casteller->id_casteller.'"><i class="fa fa-question"></i></button>';
                        break;
                }

            }
        }

        $data_content['castellers'] = $castellers;
        $data_content['event'] = $event;
        //$data_content['tags'] = $tags;
        $data_content['positions'] = $positions;

        return view('events.attendance.list-blocks', $data_content);
    }
}
