<?php

namespace App\Http\Controllers;

//use Illuminate\Http\Response;


use App\News;
use App\Colla;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Str;
//use Illuminate\Database\Eloquent\Collection;
//use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Input;
use Illuminate\Contracts\View\Factory;
use Illuminate\Support\Facades\Session;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Support\Facades\Request as RequestInput;

class NewsController extends Controller
{
    //

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function index(): Collection{
    public function index(){

        $colla = Colla::getCurrent();

        if(!Auth::user()->accesBBDD()) abort(403);

        // pendent d'ordenar per new_date
        //$dir = RequestInput::input('order')[0]['dir'];
        //$column_order = RequestInput::input('columns')[intval(RequestInput::input('order')[0]['column'])]['name'];

        //$news = News::where('colla_id', $colla->id_colla)->whereNotNull('id_new')->paginate(10)->orderBy('new_date', 'desc');
        $news = News::where('colla_id', $colla->id_colla)->orderBy('new_date', 'desc')->paginate(10);

        $data_content['news'] = $news;

        return view('news.llista',compact('news'));
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('news.crear');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $noticia = new News();
         $noticia->title = $request->title;
         $noticia->body = $request->body;
         $noticia->visiblity = $request->visiblity;
         $noticia->colla_id = auth()->user()->colla_id;
         $noticia->user_id = auth()->user()->id_user;
         //new date
         if(!isset($request->new_date))
        {
            $noticia->new_date = Carbon::now()->toDateTimeString();
        }
        else
        {
            $new_date =  str_replace('/', '-', $request->new_date);
            $noticia->new_date = date('Y-m-d', strtotime($new_date)).' '.$request->hour_new_date.':'.$request->min_new_date.':00';
        }
        $noticia->save();

    return back()->with('mensaje', trans('news.fet'));
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function details($id)
    {
        //Aquí valida si existeix sino redirigeix al 404
        $new = News::findOrFail($id);
        return view('news.details', compact('new'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editar($id)
    {

        //Aquí valida si existeix sino redirigeix al 404
        $new = News::findOrFail($id);
        $data_content['new'] = News::findOrFail($id);
        $data_content['redactor'] = User::findOrFail($new->user_id);

        return view('news.editar', $data_content);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $noticiaActualizada = News::find($id);
        $noticiaActualizada->title = $request->title;
        $noticiaActualizada->body = $request->body;
        $noticiaActualizada->colla_id = $request->colla_id;
        $noticiaActualizada->user_id = $request->user_id;
        $noticiaActualizada->visiblity = $request->visiblity;
        //new date
        if(!isset($request->new_date))
        {
            $noticiaActualizada->new_date = Carbon::now()->toDateTimeString();
        }
        else
        {
            $new_date =  str_replace('/', '-', $request->new_date);
            $noticiaActualizada->new_date = date('Y-m-d', strtotime($new_date)).' '.$request->hour_new_date.':'.$request->min_new_date.':00';
        }

        $noticiaActualizada->save();
        return back()->with('mensaje', trans('news.editada')  );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function eliminar($id)
    {
        //
        $noticiaEliminar = News::findOrFail($id);
        $noticiaEliminar->delete();
        return back()->with('mensaje', trans('news.deleted') );


    }
}
