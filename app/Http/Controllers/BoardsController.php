<?php

namespace App\Http\Controllers;

use App\Board;
use App\Colla;
use App\Event;
use App\Tag;
use DOMXPath;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Request as RequestInput;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\View\View;
use mysql_xdevapi\Exception;
use RecursiveArrayIterator;
use RecursiveIteratorIterator;

class BoardsController extends Controller
{
    /** get Boards list */
    public function getList()
    {
        if(!Auth::user()->accesBoards()) abort(403);
        $colla = Colla::getCurrent();

        $data_content['bases'] = Tag::currentTags('BOARDS');
        $data_content['boards'] = Board::where('colla_id', $colla->id_colla)->get();

        return view('boards.list', $data_content);
    }

    /** get Modal Add Board */
    public function getAddBoardModalAjax()
    {
        if(!Auth::user()->accesWriteBoards()) abort(403);

        $data_content['bases'] = Tag::currentTags('BOARDS');

        return view('boards.modal-add', $data_content);
    }


    /**
     * Form add new new board
     *
     * @return Application|Factory|View
     */
    /*
    public function getAddBoard()
    {
        if(!Auth::user()->accesWriteBoards()) abort(403);

        $data_content['bases'] = Tag::currentTags('BOARDS');

        return view('boards.import.1-add', $data_content);
    }*/

    /**
     * Add board
     *
     * @param  \Illuminate\Http\Request  $request
     * @return Application|Factory|View
     */
    public function postAddBoard(Request $request)
    {
        if(!Auth::user()->accesWriteBoards()) abort(403);

        $request->validate([
            'type' => 'required',
            'base' => 'required|numeric',
            'name' => 'required|min:3|max:100',
            'type_map' => 'required'
        ]);
        $colla = Colla::getCurrent();

        switch ($request->input('type'))
        {
            case 'PINYA':
                $data_json = ['pinya' => null];
                break;
            case 'FOLRE':
                $data_json = ['pinya' => null, 'folre' => null];
                break;
            case 'MANILLES':
                $data_json = ['pinya' => null, 'folre' => null, 'manilles' => null];
                break;
            case 'PUNTALS':
                $data_json = ['pinya' => null, 'folre' => null, 'manilles' => null, 'puntals' => null];
                break;
        }

        $board = new Board();

        $board->colla_id = $colla->id_colla;
        $board->name = $request->input('name');
        $board->type = $request->input('type');
        $board->data = json_encode($data_json);

        $board->save();

        DB::table('board_tags')->insert(['tag_id' => (int) $request->input('base'), 'board_id' => $board->id_board]);

        $data_content['bases'] = Tag::currentTags('BOARDS');
        $data_content['board'] = $board;
        $data_content['type_map'] = $request->input('type_map');

        return view('boards.import.1-upload-svg', $data_content);
    }

    /** Upload SVG && make data content (PINYA/FOLRE/MANILLES/PUNTALS) on Board via AJAX
     * @param Board $board
     * @param Request $request
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function postUploadSvg(Board $board, Request $request)
    {
        if(!Auth::user()->accesWriteBoards()) abort(403);
        $this->authorize('getBoard', $board);

        $colla = Colla::getCurrent();
        $data = json_decode($board->data, true);

        $file = $request->file('svg');
        $type_map = $request->input('type_map');
        $html = $request->input('html');

        $file_name = $board->id_board.'_'.$type_map.'.svg';

        switch ($type_map)
        {
            case 'PINYA':
                $board->html_pinya = $html;
                $data['pinya']['svg'] = $file_name;
                $data['pinya']['structure'] = null;
                $type_map = 'FOLRE';
                break;
            case 'FOLRE':
                $board->html_folre = $html;
                $data['folre']['svg'] = $file_name;
                $data['folre']['structure'] = null;
                $type_map = 'MANILLES';
                break;
            case 'MANILLES':
                $board->html_pinya = $html;
                $data['manilles']['svg'] = $file_name;
                $data['manilles']['structure'] = null;
                $type_map = 'PUNTALS';
                break;
            case 'PUNTALS':
                $board->html_pinya = $html;
                $data['puntals']['svg'] = $file_name;
                $data['puntals']['structure'] = null;
                $type_map = null;
                break;
        }

        $board->data = json_encode($data);
        $board->data_code = json_encode($data);
        $board->save();

        $file->move(public_path('media/colles/'.$colla->shortname.'/svg'), $file_name);

        $data_content['board'] = $board;
        $data_content['type_map'] = $type_map;

        return 'true';
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Board $board
     * @param string $map
     * @return Application|Factory|View
     */
    public function getTagRowMap(Board $board, string $map)
    {
        if(!Auth::user()->accesWriteBoards()) abort(403);
        $this->authorize('getBoard', $board);

        $data_content['board'] = $board;
        $data_content['type_map'] = $map;

        return view('boards.import.2-tag-row-map', $data_content);
    }

    /** Tag position from Board and map via AJAX
     * @param Board $board
     * @param string $map
     * @param Request $request
     * @return string
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function postTagPosition(Board $board, string $map, Request $request)
    {
        if(!Auth::user()->accesWriteBoards()) abort(403);
        $this->authorize('getBoard', $board);

        $type_map = strtolower($map);

        $data = json_decode($board->data, true);
        $data_code = json_decode($board->data_code, true);

        if(isset($request->name))
        {
            $name = $request->name;
            $id_row = $request->id_row;

            $data[$type_map]['structure'][$name]['baix'] = $id_row;
            $data_code[$type_map]['structure'][ $id_row] = [$name, 'baix'];
            $data_code[$type_map]['structure'][ $id_row] = ['row' => $name, 'position' => 'baix'];
        }
        else
        {
            $id_row = $request->id_row;
            $position = $request->position;
            $cord = (int) $request->cord;
            $core = $request->core;
            $row = $request->row;
            $side = $request->side;

            $data_code[$type_map]['structure'][ $id_row] = ['row' => $row, 'position' => $position, 'side' => $side, 'cord' => $cord];

            if($core==="true")
            {
                if(is_null($side))
                {
                    $data[$type_map]['structure'][$row][$position] = $id_row;
                }
                else
                {
                    $data[$type_map]['structure'][$row][$position][$side] = $id_row;
                }
            }
            elseif($core==="false")
            {
                if(is_null($side))
                {
                    $data[$type_map]['structure'][$row][$position][(int)$cord] = $id_row;
                }
                else
                {
                    if(isset($data[$type_map]['structure'][$row][$position][$side][(int)$cord]))
                    {
                        if(!in_array($id_row, $data[$type_map]['structure'][$row][$position][$side][(int)$cord]))
                        {
                            array_push($data[$type_map]['structure'][$row][$position][$side][$cord], $id_row);
                        }
                    }
                    else
                    {
                        $data[$type_map]['structure'][$row][$position][$side][(int)$cord] = [$id_row];
                    }
                }
            }
        }

        $board->data = json_encode($data);
        $board->data_code = json_encode($data_code);
        $board->save();

        return 'true';
    }

    /** post delete position via AJAX
     * @param Board $board
     * @param string $map
     * @param Request $request
     */
    public function postDeletePosition(Board $board, string $map, Request $request)
    {
        if(!Auth::user()->accesWriteBoards()) abort(403);
        $this->authorize('getBoard', $board);

        $type_map = strtolower($map);

        $data = json_decode($board->data, true);

        if($request->cord==0)
        {
            if($request->side=="false")
            {
                unset($data[$type_map]['structure'][$request->row][$request->name]);
            }
            else
            {
                unset($data[$type_map]['structure'][$request->row][$request->name][$request->side]);
            }
        }
        else
        {
            if($request->side=="false")
            {
                unset($data[$type_map]['structure'][$request->row][$request->name][$request->cord]);
            }
            else
            {
                if (($key = array_search($request->id_row, $data[$type_map]['structure'][$request->row][$request->name][$request->side][$request->cord])) !== false)
                {
                    unset($data[$type_map]['structure'][$request->row][$request->name][$request->side][$request->cord][$key]);
                }
            }
        }

        $data_code = json_decode($board->data_code, true);
        try{
            unset($data_code[$type_map]['structure'][$request->id_row]);
        }
        catch (Exception $e)
        {
            return 'false';
        }

        $board->data = json_encode($data);
        $board->data_code = json_encode($data_code);

        $board->save();

        return 'true';
    }

    /** Tag ALL positions into map
     * @param Board $board
     * @param string $map
     * @return Application|Factory|View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function getTagAllMap(Board $board, string $map)
    {
        if(!Auth::user()->accesWriteBoards()) abort(403);
        $this->authorize('getBoard', $board);

        $colors = Board::getColors();

        $row_color = [];

        $map_rows = $board->getArrayBasesRows()[strtolower($map)];

        foreach($map_rows as $k => $row)
        {
            $row_color[$row] =  $colors[$k];
        }

        $data_content['map_rows'] = $map_rows;
        $data_content['row_color'] = $row_color;
        $data_content['board'] = $board;
        $data_content['type_map'] = $map;
        $data_content['positions'] = Tag::currentTags('POSITIONS');

        return view('boards.import.3-tag-all-map', $data_content);
    }

    /** styles rows map
     * @param Board $board
     * @param string $map
     * @return Application|Factory|View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function getStyleMap(Board $board, string $map)
    {
        if(!Auth::user()->accesWriteBoards()) abort(403);
        $this->authorize('getBoard', $board);

        $data_content['board'] = $board;
        $data_content['type_map'] = $map;
        $data_content['positions'] = Tag::currentTags('POSITIONS');

        return view('boards.import.4-style-map', $data_content);
    }

    /** change style map bia AJAX
     * @param Board $board
     * @param string $map
     * @param Request $request
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function postStyleMapAjax(Board $board, string $map, Request $request)
    {
        if(!Auth::user()->accesWriteBoards()) abort(403);
        $this->authorize('getBoard', $board);

        $html = 'html_'.strtolower($map);

        $board->{$html} = $request->html;

        $board->save();

        return 'true';
    }

    /** add map on board
     * @param Board $board
     * @param string $map
     */
    public function getAddMapp(Board $board, string $map)
    {
        if(!Auth::user()->accesWriteBoards()) abort(403);
        $this->authorize('getBoard', $board);

        $data_content['bases'] = Tag::currentTags('BOARDS');
        $data_content['board'] = $board;
        $data_content['type_map'] = $map;

        return view('boards.import.1-upload-svg', $data_content);
    }

    /** get modal preview Board
     * @param Board $board
     */
    public function getModalPreviewBoard(Board $board)
    {
        if(!Auth::user()->accesBoards()) abort(403);
        $this->authorize('getBoard', $board);

        $board->data = json_decode($board->data, true);
        $board->data_code =json_decode($board->data_code, true);

        $data_content['colla'] = Colla::getCurrent();
        $data_content['board'] = $board;

        return view('boards.modals.modal-view', $data_content);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Board $board
     * @return void
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy(Board $board)
    {
        if(!Auth::user()->accesWriteBoards()) abort(403);
        $this->authorize('getBoard', $board);

        $colla = Colla::getCurrent();

        foreach(json_decode($board->data, true) as $base)
        {
            unlink(public_path('media/colles/'.$colla->shortname).'/svg/'.$base['svg']);
        }

        $board->delete();
    }
}
