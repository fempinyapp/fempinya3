<?php
namespace App\Http\Controllers;

use App\Tag;
use App\Colla;
use App\Event;
use Exception;
use App\Casteller;
use App\Attendance;
use Carbon\Carbon;
use App\Helpers\Humans;
use App\Telegram;
use Illuminate\Support\Facades\Log;
use Telegram\Bot\Laravel\Facades\Telegram as TelegramBot;
use Telegram\Bot\FileUpload\InputFile;
use Jenssegers\Date\Date;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Str;


class TelegramController extends Controller
{
    /**
     * This method will create a new keyboard from a button array
     *
     * @param array $buttons
     * @param integer  $countByRow
     * @return array
     */
    private static function keyboardFormatter(array $buttons, $countByRow = 1)
    {
        // Empty Keyboard
        $keyboard = [];

        // validating if buttons is setted
        if (count($buttons) > 0){

            // empty row
            $rows = [];

            // iterating button array
            foreach ($buttons as $btn) {

                // validate row's length for each iteration
                if (count($rows) < $countByRow ) {
                    // pushing button into row
                    $rows[] = $btn;
                } else {
                    // Pushing row into main keyboard
                    $keyboard[] = $rows;
                    // unsetting row
                    unset($rows);
                    // Adding button to new row
                    $rows[] = $btn;
                } 
            }

            // pushing last row into keyboard
            if (isset($rows)) {
                $keyboard[] = $rows;
                // unsetting row
                unset($rows);
            }
        }

        // return keyboard
        return $keyboard;
    }


    public function index()
    {
    try {
            // ESTRUCTURA/FLUIX ENTRE TELEGRAM-Function-TELEGRAM
            // 1. REBEM  les peticions del bot
            // en desenvolupament
            // en Produccio
            // 2. OBTENIM imput de telegram
            // definim varibles per users no registrats
            // Consultem BBDD
            // Assignem valors a varibles per users registrats
            // convertim impunt en Command
            // si procedeix de callback_query
            // RE convertim impunt en $command_callback
            // analitzem parametres
            // decidim segons switch ($command_callback)
            // si NO procedeix de callback_query
            // dicidim tipus usuaris
            //- usuaris que no son castellers/colla registats
            //-- switch ($command)
            //-- command possibles: help, codi, fempinya, contactar
            //- User registrer castellers sense rol = null
            //-- switch ($command)
            //-- command possibles: help, perfil, agenda
            //- User registrer castellers amb rol != null
            //-- switch ($command)
            //-- command possibles: help, photo, !castellers, !assistencia
            // 3. ENVIEM resposta al usuari ouput al telegram



            // en Desenvolupament
 //           $update = collect(TelegramBot::getUpdates())->last(); //ultim
//            $update = TelegramBot::getUpdates();  // tots
//            dd($update);
            // Salida final
            $finalOutput = [
                'chat_id'       => null,
                'text'          => null,
                'parse_mode'    => "HTML",
                // tipo keyboard
                'reply_markup' => null,
                'keyboard' => null,
                // Banderas para manipulacion de mensaje
                'edit' => false,
                'delete' => false,

                // Bandera para el tipo de mensaje
                'photo' => false,
            ];
            
            
            $chatIdDev=257202166;  // telegram_id de Pepe
            //$telegram_debug = 0;
            $telegram_debug = env('TELEGRAM_DEBUG', '');
            if($telegram_debug == 1){
                // en Produccio
                $update = TelegramBot::getWebHookUpdate();
            } elseif($telegram_debug == 0){
                // en Desenvolupament
                $update = collect(TelegramBot::getUpdates())->last(); //ultim
            }
            else {
                // en Manteniment
                /////////////////////////////////////////////////////////////////
                $up = TelegramBot::sendMessage([
                'chat_id' => $chatIdDev,
                'text'    => "Fem manteniment. "
                ]);
                exit(200); // Aquesta linia finalitza
                /////////////////////////////////////////////////////////////////
            }
            $callback_id = 0;
            $keyboard_tipus = null;
            $status = null;



            // si procedeix de callback_query
            if($update->get('callback_query')!=null) {
                $callback_data = $update->get('callback_query')->get('data');
                $callback_id = $update->get('callback_query')->getMessage()->get('message_id'); 
                
                // proba de tilde //dd($update->get('callback_query'));
                //$callback_chat_id = $update->get('callback_query')->getMessage()->get('chat')->get('id'); // ['message']['chat']['id'];
                //dd($callback_chat_id);
                //$callback_id = collect($update)->last()->get['callback_query']['message']['message_id'];
                
                // Resposta al usuari
                //dd( $update->getChat()->get('id'));
                //$finalOutput['chat_id'] = $update->get('callback_query')->getChat()->get('id');
               // $finalOutput['chat_id'] = $update->getChat()->get('id');
                $finalOutput['text'] =  "Rebut id ".$callback_id. "  data ".$callback_data;
                
                // $finalOutput['message_id'] = $callback_id;
                // Per a editar missatge
               //  $finalOutput['edit'] = true;
                // $finalOutput['text'] .=  "\nfinal de rebut id ".$callback_id." data ".$callback_data;
            }

            $finalOutput['chat_id'] = $update->getChat()->get('id');
            $finalOutput['message_id']  = $update->getMessage()->get('message_id');
            $finalOutput['text'] = utf8_decode($update->getMessage()->get('text'));
            $first_name = $update->getChat()->get('first_name');
            
           

            $finalOutput['chat_id'] = $update->getChat()->get('id');
            $finalOutput['message_id']  = $update->getMessage()->get('message_id');
           // $finalOutput['text'] = $update->getMessage()->get('text');
            $finalOutput['text'] = utf8_decode($update->getMessage()->get('text'));

            $first_name = $update->getChat()->get('first_name');
            
            
            $response = "Hola este es el mensaje de respuesta a tu peticion \n\n ".$finalOutput['text'] ."\n".$finalOutput['chat_id']." ";
            // self::sendNotification($finalOutput);
            // exit(200);
            

            $finalOutput['chat_id'] = $update->getChat()->get('id');
            $finalOutput['message_id']  = $update->getMessage()->get('message_id');
            $finalOutput['text'] = $update->getMessage()->get('text');
            $first_name = $update->getChat()->get('first_name');
            
            $response = trans('telegram.salutacio')."\n\n ";
            $response .=  "⚠️  No se que fer amb: <b>".$finalOutput['text']."</b> ";
            $response .= "\n\n🆘 ".trans('telegram.pots_demanar_ajuda')."\n\n "; 


            // Consultem BBDD
            $data_content['telegrams'] = Telegram::find($finalOutput['chat_id']);  // user telegram
            $status = $data_content['telegrams']['status'];
            $casteller_id = $data_content['telegrams']['casteller_id'] ? $data_content['telegrams']['casteller_id'] : 0;
            $family = $casteller_id ;  //family de user per interactuar entre multiuser
            $casteller_multi_id = $data_content['telegrams']['casteller_multi_id'] ? $data_content['telegrams']['casteller_multi_id'] : 0;
            $colla_id = $data_content['telegrams']['colla_id'];

            // comprobem si es perteny a family
            if( Casteller::where('family', $casteller_id)->count() >= 1 and $casteller_id>=1  ) { 
                $tinc_multiuser =""; 
                $castellers = Casteller::where('family', $casteller_id)->where('family_head', 0)->get();
                $recordsTotalFamily = count($castellers);
                // $tinc_multiuser = "👥 Family: ".$recordsTotalFamily." "; 
                foreach ($castellers as $casteller)
                    {
                        // reasignem casteller_id i colla_id al multiuser actiu = casteller_multi_id
                        
                        if( $casteller->id_casteller == $casteller_multi_id) {
                            $activat = "";
                            $casteller_id = $casteller->id_casteller; 
                            $colla_id = $casteller->colla_id; 
                            $tinc_multiuser  = $casteller->name;
                            $activat = "✅";
                        } 
                        
                      //  $tinc_multiuser  .= " ". $casteller->name;
                    }
            } else {
                $tinc_multiuser =""; 
            }

            $data_content['colles'] = Colla::find($colla_id);  // per casteller de varies colles  family ha de coincidir
            $lang = $data_content['telegrams']['language'];     //language en telegram
            if($lang==null ) { $lang = 'ca';}
            App::setLocale($lang);
            //Date::setLocale($colla->language)  // pendent time_zone
            //$shortname_colla =  $data_content['colles']['shortname']; // nom curt de la colla
            $name_colla =  $data_content['colles']['name']; // nom de la colla
            $data_content['castellers'] = Casteller::find($casteller_id); // reasignem castellers al multiuser activat
            $alias =  $data_content['castellers']['alias'];
            $rol = null; 


            // convertim impunt en Command
            // minuscules  utf8 Note que "alfabético" está determinado por la ubicación actual. 
            // Esto significa que por ejemplo, en la localidad "C" predeterminada, los caracteres como la diéresis-A (Ä) no serán convertidos.
            $get_text = explode(' ', strtolower(trim($finalOutput['text']))); 
            //$get_text = explode(' ', trim($finalOutput['text']));
            $command = $get_text[0];

            $num_parametres = count($get_text);
             if($num_parametres>1 ) {$primer_Parametre = $get_text[1]; } else { $primer_Parametre="";}

            // si procedeix de callback_query
            // si hem_rebut callback_data
            if( $callback_id != 0 ) {
                $keyboard_tipus = 'teclado_inline'; // 'teclado_inline' o 'keyboard' 
                $get_callback = explode(' ', trim($callback_data));
                $command_callback = $get_callback[0];

                $parametres_arguments = explode('#', trim($get_callback[1]));
                $num_parametres_arguments = count($parametres_arguments);
                //  dd("En 123  en callback_id ".$callback_id,  "num_parametres_arguments: ".$num_parametres_arguments);
                // $aixo = "+++";
                // for ($m = 1; $m < $num_parametres_arguments; $m++) {
                //     $aixo .= $m." " .$parametres_arguments[$m];
                // }
                // dd("En 131  en callback_id ".$callback_id,  "num_parametres_arguments: ".$num_parametres_arguments,  "arra: " .$get_callback[0], "parametres_arguments: ".$parametres_arguments[1]);
                $response =  "En 113 Rebem ".$callback_id;
                $response .=  "\ncommand_callback ".$command_callback;
                $response .=  "\narguments ".$parametres_arguments[1];

                if($num_parametres_arguments>=1) {$response .=  "\narguments ".$parametres_arguments[1];}

                switch ($command_callback) {
                    case 'attendance':
                        $response = "🤖" .$command_callback; // $response = trans('telegram.has_demanat'). ": <b>".trans('telegram.attendance')."</b>";
                        $status = $parametres_arguments[2]; // | '0' ;
                        $status_send = $parametres_arguments[5] | '0' ;
                        $my_attende = Attendance::getAttendance($casteller_id, $parametres_arguments[1]);
                        $count_my_attende = Attendance::where('event_id', $parametres_arguments[1])->where('casteller_id' , $casteller_id)->count();
                        if($parametres_arguments[1]>=1)
                            {
                            // CARACTERISTIQUES DE L'ENVENT
                            $events = Event::where('id_event', $parametres_arguments[1])->get();
                            $companions = 0;
                            foreach($events as $event)
                                {
                                    $answers = $event->attendanceAnswers();
                                    $companions = $event->companions;
                                }
                            
                            // PER DEFECTE // proces attendance standar
    
                            $status_display_yes = ""; $status_display_no = "";  $status_display_unknown = ""; 
                            if ($my_attende->status=='YES') {  $status_display_yes = "✅ "; }
                            if ($my_attende->status=='NO') {  $status_display_no = "❌ "; }
                            if ($my_attende->status=='UNKNOWN') {  $status_display_unknown = "❓ "; } 
                            if ($my_attende->companions>=1) {  
                                $status_display_compa = "✅ ".$my_attende->companions; 
                                $my_companions = $my_attende->companions;
                            } else {
                                $status_display_compa = "❔ ";
                                $my_companions = 0 ;
                            } 

                            $teclat[1] = ['text' => 'YES '.$status_display_yes, 'callback_data' => 'attendance #'.$parametres_arguments[1]."#YES#0#0#1#".$callback_id."" ];      // Boton
                            $teclat[2] = ['text' => 'NO'.$status_display_no, 'callback_data' => 'attendance #'.$parametres_arguments[1]."#NO#0#0#1#".$callback_id."" ];
                            $teclat[3] = ['text' => 'UNKNOWN'.$status_display_unknown, 'callback_data' => 'attendance #'.$parametres_arguments[1]."#UNKNOWN#0#0#1#".$callback_id."" ];
                            // INTERACCIO status YES and companions
                            $response .= "\n  companions: <b>".$companions."</b> ";
                            if($companions==1 and $my_attende->status=='YES'){
                                if($my_attende->companions>=1){ 
                                    $status_display_compa = "✅ ".$my_attende->companions." "; 
                                    $my_companions = $my_attende->companions;
                                }
                                $status_display = "✅ :(".$parametres_arguments[3].") "; 
                                $teclat[4] = ['text' =>  $status_display_compa.' ACOMPANYANTS', 'callback_data' => 'attendance_companions #'.$parametres_arguments[1]."#YES#".$my_companions."#0#null#".$callback_id."" ];     
                            }
                        }
                        if($status_send==1){
                            if($status!='YES') { $my_companions= 0; }
                                $update_statuts = Attendance::setStatus($casteller_id, $parametres_arguments[1], $status, $my_companions, 'TG', null);
                                $response .= "\n ".trans('telegram.attendance_status_done').":" ;
                                $my_attende = Attendance::getAttendance($casteller_id, $parametres_arguments[1]);
                                $count_my_attende = Attendance::where('event_id', $parametres_arguments[1])->where('casteller_id' , $casteller_id)->count();
        
                                if($count_my_attende>=1) {
                                    $status_display_yes = ""; $status_display_no = "";  $status_display_unknown = ""; 
                                    if ($parametres_arguments[2]=='YES') {  $status_display_yes = "✅ "; }
                                    if ($parametres_arguments[2]=='NO') {  $status_display_no = "❌ "; }
                                    if ($parametres_arguments[2]=='UNKNOWN') {  $status_display_unknown = "❓ "; } 
                                    $teclat[1] = ['text' => 'YES '.$status_display_yes, 'callback_data' => 'attendance #'.$parametres_arguments[1]."#YES#0#0#1#".$callback_id."" ];      // Boton
                                    $teclat[2] = ['text' => 'NO'.$status_display_no, 'callback_data' => 'attendance #'.$parametres_arguments[1]."#NO#0#0#1#".$callback_id."" ];
                                    $teclat[3] = ['text' => 'UNKNOWN'.$status_display_unknown, 'callback_data' => 'attendance #'.$parametres_arguments[1]."#UNKNOWN#0#0#1#".$callback_id."" ];
                                    // INTERACCIO status YES and companions
                                    if($companions==1 and $parametres_arguments[2]=='YES'){
                                            $status_display_compa = "!✅ ".$parametres_arguments[3]." "; 
                                            $my_companions = $parametres_arguments[3];
                                            if($my_attende->companions>=1){ 
                                                $status_display_compa = "com✅ ".$my_attende->companions." "; 
                                                $my_companions = $my_attende->companions;
                                            } 
                                            $teclat[4] = ['text' =>$my_companions."::". $status_display_compa.' ACOMPANYANTS', 'callback_data' => 'attendance_companions #'.$parametres_arguments[1]."#YES#".$my_companions."#0#2#".$callback_id."" ];     
                                    } 

                                    if($status!='YES') {
                                        unset($teclat[4]);   
                                    }
                                }
                                
                        }

                        $keyboard_tipus = true; // "teclado_inline";
                        if(count($teclat) >=1 ){
                            if($count_my_attende>=1) {
                                $status_display_yes = ""; $status_display_no = "";  $status_display_unknown = ""; 
                                if ($parametres_arguments[2]=='YES') {  $status_display_yes = "✅ "; }
                                if ($parametres_arguments[2]=='NO') {  $status_display_no = "❌ "; }
                                if ($parametres_arguments[2]=='UNKNOWN') {  $status_display_unknown = "❓ "; } 
                                $teclat[1] = ['text' => 'YES '.$status_display_yes, 'callback_data' => 'attendance #'.$parametres_arguments[1]."#YES#0#0#1#".$callback_id."" ];      // Boton
                                $teclat[2] = ['text' => 'NO'.$status_display_no, 'callback_data' => 'attendance #'.$parametres_arguments[1]."#NO#0#0#1#".$callback_id."" ];
                                $teclat[3] = ['text' => 'UNKNOWN'.$status_display_unknown, 'callback_data' => 'attendance #'.$parametres_arguments[1]."#UNKNOWN#0#0#1#".$callback_id."" ];
                                // INTERACCIO status YES and companions
                                   // if($companions==1 and $parametres_arguments[2]=='YES'){
                                    if($companions==1 and  $status=='YES') {  
                                        $status_display_compa = "❔ "; // "par✅ ".$parametres_arguments[3]." "; 
                                        $my_companions = $parametres_arguments[3];
                                        if($my_attende->companions>=1){ 
                                            $status_display_compa = "✅ ".$my_attende->companions." "; 
                                            $my_companions = $my_attende->companions;
                                        } else { }
                                        $teclat[4] = ['text' =>  $status_display_compa.' ACOMPANYANTS', 'callback_data' => 'attendance_companions #'.$parametres_arguments[1]."#YES#".$my_companions."#0#2#".$callback_id."" ];     
                                    } 
                              //  }
                                if($companions==1 and $parametres_arguments[2]=='YES'){
                                    if($status=='NO') {  $status_display = "✅ ".$parametres_arguments[3]." "; 
                                        //zz pasar $my_companions
                                        $teclat[4] = ['text' => 'error lina 388', 'callback_data' => 'attendance_companions_zzz #'.$parametres_arguments[1]."#YES#0#0#2#".$callback_id."" ];     
                                    } 
                                }
                            }
                            if($callback_id!=0){         
                                //  $finalOutput['delete'] =  $callback_id ; //true;
                                //  $finalOutput['edit'] = $parametres_arguments[6] ; // $callback_id ; //true;
                            }
                            $finalOutput['reply_markup'] = json_encode([
                                    // Recorda aquest element es un array d'un array, es  a dir una matriu
                                    // 'inline_keyboard' => $teclado_inline,
                                    'inline_keyboard' => self::keyboardFormatter($teclat,2),
                                    'resize_keyboard' => true,
                                    'one_time_keyboard' => false
                                    
                            ]);
                            $response = "\n ".trans('telegram.attendance_status_done')."" ;
                            //$response .= "\n callback_data: <b>".$callback_data."</b> ";
                            $response .= "\n callback_id :  <b>".$callback_id."</b> ";
    
                        }

                    break;

                    case 'attendance_companions':
                        $response = "🤖" .$command_callback; // $response = trans('telegram.has_demanat'). ":  <b>".trans('telegram.attendance_status')."</b>".$callback_id;
                        //$response .= "\n En 384 arguments 0: callback_data <b>".$callback_data."</b> ";
                        $my_attende = Attendance::getAttendance($casteller_id, $parametres_arguments[1]);
                        $count_my_attende = Attendance::where('event_id', $parametres_arguments[1])->where('casteller_id' , $casteller_id)->count();
                        if ($my_attende->companions>=1) {  
                           // $status_display_compa = "✅ ".$my_attende->companions; 
                            $my_companions = $my_attende->companions;
                        } else {
                            $status_display_compa = "❔ ";
                            $my_companions = 0 ;
                        } 
                        // CARACTERISTIQUES DE L'ENVENT
                        $events = Event::where('id_event', $parametres_arguments[1])->get();
                        foreach($events as $event)
                            {
                                $answers = $event->attendanceAnswers();
                                $companions = $event->companions;
                            }
                        
                        if($companions==1){
                                $j=100; $k=0;
                                while ($k < 10) {
                                    if($my_companions == $k) {  
                                        $status_display = "✅ "; 
                                    } else { 
                                        $status_display = ""; 
                                    }
                                    $teclat[$j] = ['text' =>   ' '.$status_display.' '.$k.' ACOMPANYANTS', 'callback_data' => 'attendance_companions #'.$parametres_arguments[1]."#YES#".$k."#1#3#".$callback_id."" ];      // Boton
                                    $j++; $k++;
                                }

                               //     
                        }
                        $status_send = $parametres_arguments[5] | 0;
                        if($status_send==3){
                            //$update_statuts = Attendance::setStatus($id_casteller, $id_event, $status, $companions, 'TG', $answers);
                              $update_statuts = Attendance::setStatus($casteller_id, $parametres_arguments[1], 'YES', $parametres_arguments[3], 'TG', null);
                              $response .= "\n ".trans('telegram.attendance_status_done').":".$callback_id ;
                              $j=100; $k=0;
                                while ($k < 10) {
                                    if($parametres_arguments[3] == $k) {  $status_display = "✅ "; 
                                    } else { $status_display = ""; }
                                    $teclat[$j] = ['text' =>  $status_display.' '.$k.' ACOMPANYANTS', 'callback_data' => 'attendance_companions #'.$parametres_arguments[1]."#YES#".$k."#1#3#".$callback_id."" ];      // Boton
                                    $j++; $k++;
                                }
                        ///// aqui delete si funciona                    
                        ///  $finalOutput['delete'] = $parametres_arguments[6]; // true;
                                
                                if($parametres_arguments[6]>=1) {
                                //    $finalOutput['delete'] = $parametres_arguments[6]; // true;
                                }
                                //if($parametres_arguments[6]>$callback_id ) $finalOutput['edit'] =  $parametres_arguments[6] ; //true;
                         }
                      // $finalOutput['edit'] =  $parametres_arguments[6] ; //true;
                      if($callback_id>=1){ 
                        
                       // $finalOutput['edit'] = $callback_id; // true;

                        $finalOutput['reply_markup'] = json_encode([
                            // Recorda aquest element es un array d'un array, es  a dir una matriu
                            // 'inline_keyboard' => $teclado_inline,
                            'inline_keyboard' => self::keyboardFormatter($teclat,2),
                            'resize_keyboard' => true,
                            'one_time_keyboard' => false,
                            'edit' =>  $callback_id
                        ]);
                    } 
                    else {
                        $keyboard_tipus = true; // "teclado_inline";
                      if(count($teclat) >=1 ){
                          $finalOutput['reply_markup'] = json_encode([
                              // Recorda aquest element es un array d'un array, es  a dir una matriu
                              // 'inline_keyboard' => $teclado_inline,
                              'inline_keyboard' => self::keyboardFormatter($teclat,2),
                              'resize_keyboard' => true,
                              'one_time_keyboard' => false,
                            //  'edit' =>  $parametres_arguments[6]
                          ]);

                    }

                      }
                    break;

                    case 'attendance_status':
                        $response = "🤖"  .$command_callback; // $response = trans('telegram.has_demanat'). ":  <b>".trans('telegram.attendance_status')."</b>";
                        $response .= "en 356 t: <b>".$command_callback."</b> ".$parametres_arguments[1]." ".$parametres_arguments[2]." ".$parametres_arguments[3]." ".$parametres_arguments[4];
                        $response .= "en 493 PENDENT CAL DEFINIR LES DADES QUE ES GUARDEN AMB EL MATEIX FORMAT QUE JONSON DE LA WEB ";
                        exit(200); // Aquesta linia finalitza

                        $events = Event::where('id_event', $parametres_arguments[1])->get();

                        foreach($events as $event)
                                    {
                                        $answers = $event->attendanceAnswers();
                                        $companions = $event->companions;
                                    }
                                $status_answer = count($answers);
                                $n=3;
                                if($status_answer>=1){
                                    foreach($answers as $answer)
                                        {
                                        $n++;
                                        $teclat[$n] = ['text' => '🟡 '.$answer->name, 'callback_data' => 'attendance_status #'.$parametres_arguments[1]."#YES#0"."#'".$answer->id_tag."'#" ];      // Boton
                                        }
                                }
                                if($companions!=0 ){
                                      $teclat[$n+1] = ['text' => 'acomp. ', 'callback_data' => 'attendance_status #'.$parametres_arguments[1]."#YES#0"."#'".$answer->id_tag."'" ];      // Boton
                                 }

                      //  $response .= "\n\n answers: <b>".$answers."</b> ";
                        $response .= "\n\n companions: <b>".$companions."</b> ";
                        $id_casteller = intval($casteller_id);
                        $id_event = intval($parametres_arguments[1]);
                        $status = $parametres_arguments[2];
                        $companions = $parametres_arguments[3] ; //| 0;
                        $answers  =  $parametres_arguments[4] | '0';

                       // $response .= "\n id_casteller: <b>".$id_casteller."</b> ";
                       // $response .= "\n id_event: <b>".$id_event."</b> ";
                        $response .= "\n ".trans('telegram.attendance_status')." <b>".$status."</b><b>".$answers."</b> ";
                        $response .= "\n companions: <b>".$companions."</b> ";
                        $update_statuts = Attendance::setStatus($id_casteller, $id_event, $status, $companions, 'TG', $answers);
                        $response .= "\n ".trans('telegram.attendance_status_done').":" ;
                        $my_attende = Attendance::getAttendance($casteller_id, $parametres_arguments[1]);
                        
                        $response .= "\n status_verifiedus: <b>".$my_attende->status_verified."</b> "; 
                        $response .= "\n Has companions: <b>".$my_attende->companions."</b> "; 
                        $response .= "\n Has options: <b>".$my_attende->options."</b> "; 
                       // $response .= "\n Has status: <b>".$my_attende->status."</b> "; 
                       // if($my_attende->status=='NO') $teclat[2] = ['text' => '❌ NO', 'callback_data' => 'attendance_status #'.$parametres_arguments[1]."#YES#0#0#" ];      // Boton
                       // if($my_attende->status=='UNKNOWN') $teclat[30] = ['text' => '❓ UNKNOWN', 'callback_data' => 'attendance_status #'.$parametres_arguments[1]."#YES#0#0#" ];      // Boton
                       // if($my_attende->status=='YES') $teclat[1] = ['text' => '✅ YES', 'callback_data' => 'attendance_status #'.$parametres_arguments[1]."#YES#0#0#" ];      // Boton
                        
                        if($my_attende->status=='YES' AND $my_attende->companions>0){
                        $teclat[1] = ['text' => '✅ YES', 'callback_data' => 'attendance_status #'.$parametres_arguments[1]."#YES#0#0#" ];      // Boton
                            // Editem missatge
                            TelegramBot::sendMessage([
                                'chat_id' => $finalOutput['chat_id'],
                                'text' =>'🔄 Següent pas',
                                'parse_mode' => "HTML",
                                'reply_markup' => 
                                json_encode([
                                    // Recorda aquest element es un array d'un array, es  a dir una matriu
                                    'inline_keyboard' => self::keyboardFormatter($teclat, 2),
                                    'resize_keyboard' => true,
                                    'one_time_keyboard' => false
                                    ])
                            ]);
                        }

                    break;

                    case 'recordar' :
                        if($command_callback=='recordar'){
                            $response = "🤖" .$command_callback; // $response = trans('telegram.has_demanat'). ":  <b>".trans('telegram.recordar')."</b>";
                           // $response .= "\nen 338 Has demanat: <b>".$command_callback."</b> ".$parametres_arguments[1]." ".$parametres_arguments[2]." ".$parametres_arguments[3];
                           // $botons[] = ['text' => trans('telegram.preparar_missatge_per_a').' '.$parametres_arguments[2].' '.$parametres_arguments[1], 'url' => 'https://www.fempinya.cat'] ;
                            
                           $colla = Colla::find($colla_id);
                           $limit =3;  // Nomès les 3 primeres. Haurien d'estar reservades
                           //$avui =  Carbon::now();
                           $tags = Tag::currentTags('EVENTS', $colla)->take($limit);
                           
                           $recordsTotal = count($tags);
                            if( $recordsTotal >= 1){
                                $comptador=0;
                                foreach ($tags as $tag)
                                {
                                    $comptador++;
                                    if($comptador==1){ 
                                        $opcio_1 = $tag->value; $opcio_text_1 = $tag->name;
                                        $botons[] = ['text' => trans('telegram.recordatori_expres').' '. $opcio_text_1.' ', 'callback_data' => 'recordatoriexpres #'.$parametres_arguments[1]."#".$opcio_1."#".$parametres_arguments[3]."#" ] ;
                                    }
                                    if($comptador==2) {
                                        $opcio_2 = $tag->value; $opcio_text_2 = $tag->name;
                                        $limit = 1; $skip =0; $column_order = 'start_date'; $dir = 'ASC';
                                        $future_events = Event::getUpcommingEventsByTags($opcio_2, 'OR', $colla)->where('visible', '=', 1)
                                        ->whereDate('open_date', '<', Carbon::now())->whereDate('close_date', '>', Carbon::now())
                                        ->take($limit)->skip($skip)
                                        ->orderBy($column_order, $dir)
                                        ->get();
                                        $botons[] = ['text' => trans('telegram.recordatori_expres').' '. $opcio_text_2.' ', 'callback_data' => 'recordatoriexpres #'.$parametres_arguments[1]."#".$opcio_2."#".$parametres_arguments[3]."#" ] ;
                                    }
                                    if($comptador==3) {
                                        $opcio_3 = $tag->value; $opcio_text_3 = $tag->name;
                                        $botons[] = ['text' => trans('telegram.recordatori_expres').' '. $opcio_text_3.' ', 'callback_data' => 'recordatoriexpres #'.$parametres_arguments[1]."#".$opcio_3."#".$parametres_arguments[3]."#" ] ;
                                    }
                                }

                                $keyboard_tipus = 'teclado_inline'; // 'teclado_inline' o 'keyboard' 
                                $finalOutput['reply_markup'] = json_encode([
                                    // Recorda aquest element es un array d'un array, es  a dir una matriu
                                    'inline_keyboard' => self::keyboardFormatter($botons, 1), 
                                    'resize_keyboard' => true,
                                    'one_time_keyboard' => false
                                ]);
                            }   else {
                                $response .= "\nError 597 : <b>".$command_callback."</b> ".$parametres_arguments[1]." ".$parametres_arguments[2]." ".$parametres_arguments[3];
                            }                         
                        }

                        $finalOutput['text'] = $response;
                    break;

                    case 'recordatoriexpres' :
                            $response = "🤖" .$command_callback; // $response = trans('telegram.has_demanat'). ":  <b>".trans('telegram.recordatoriexpres')."</b>";
                            // $response .= trans('telegram.error')." ".trans('telegram.noparametres');
                            // $parametres_arguments[1] corresponent al tag tipus de castellers
                            // $parametres_arguments[2] corresponent a activitats, assajos o actuacions
                            // $parametres_arguments[3] corresponent a skip
                            $colla = Colla::find($colla_id);
                            $event_proper = 0; $tex_botons = "";
                            $recordsTotalCastellers = 0;
                            $compta_tramesos = 0;
                            $response_ok_trames = 0;
                            $compta_ok_trames= 0;
                            $response_no_trames= 0;
                            $compta_no_trames= 0;
                            $response_ko = 0;
                            $compta_ko = 0;

                            
                            $limit = 1; $skip =0; $column_order = 'start_date'; $dir = 'ASC';
                            $future_events = Event::getUpcommingEventsByTags($parametres_arguments[2], 'OR', $colla)->where('visible', '=', 1)
                            ->whereDate('open_date', '<', Carbon::now())->whereDate('close_date', '>', Carbon::now())
                           ->take($limit)->skip($skip)
                            ->orderBy($column_order, $dir)
                            ->get();


                            $events = $future_events;
                            $data = new \stdClass();
                            $data->data = array();
                            $recordsTotal = count($future_events);
                            $comptador = 0;  
                            // $response .= "🤖" .$parametres_arguments[2] ." ".$colla; 
                            if($recordsTotal>=1) {
                                $response_icono = "\n\n🟢 ";
                                $response .= $response_icono." <b>".$recordsTotal." ".trans('telegram.events')."</b> ";
                                foreach ($events as $event)
                                {
                                    $response .= "\n📅 ".Humans::readEventColumn($event->id_event, 'start_date');
                                    $tex_botons = Humans::readEventColumn($event->id_event, 'start_date');
                                    $event_proper = $event->id_event;
                                }
                            }
                            if($recordsTotal>=1) { 
                                // Fem consulta amb filtre $parametres_arguments[1]
                                $castellers = Casteller::filterCastellersByTags($parametres_arguments[1], 'OR', $colla)->get();
                                $recordsTotal = count($castellers);  
                                
                                $response_icono = "\n🔴  ";
                                $data = new \stdClass();
                                $data->data = array();
                                $recordsTotalCastellers = count($castellers);
                                $compta_tramesos = 0;  
                                if($recordsTotalCastellers>=1) {
                                    $response_icono = "\n🟢 ";
                                    $response .= $response_icono." ". trans('telegram.trobats').": <b> ".$recordsTotalCastellers. " ".trans('telegram.castellers')."</b> ";
                                    $response_ok_trames = " ok <b>".trans('telegram.done')."</b>";
                                    $response_no_trames = " no trames <b>".trans('telegram.done')."</b>";
                                    $response_ko = " ko <b>".trans('telegram.caution')."</b>";
                                    $compta_ok_trames = 0; $compta_ko = 0; $compta_no_trames = 0;
                                    foreach ($castellers as $casteller)
                                    {
                                        if(Telegram::getTelegram($casteller->id_casteller) ){
                                            $compta_tramesos++;

                                            // consultem estat assistencia del proper event            
                                            $ha_confirmat = 0;
                                            $attendances = Attendance::where('casteller_id', $casteller->id_casteller)->where('event_id', '=', $event_proper)->get();
                                            $status = "❔";
                                        
                                            foreach ($attendances as $attendance)
                                                {
                                                    if($attendance->status=='YES') {$status = "✅ ";  $attendance_status = $attendance->status; $ha_confirmat = 1;}
                                                    if($attendance->status=='NO') {$status =  "❌ ";  $attendance_status = $attendance->status; $ha_confirmat = 1;}
                                                    if($attendance->status=='UNKNOWN') {$status = "❓";  $attendance_status = $attendance->status; }
                                                }
                                            $boto_recorda[] = ['text' => $status." ".$tex_botons, 'callback_data' => 'attendance #'.$event_proper."#ATTENDANCE"];
                                            
                                            if($ha_confirmat == 0 ){ // si no ha confirmat assistencia
                                                $compta_ok_trames++;
                                                // enviem missatge a  $chatIdDesti 
                                                $response_ok_trames .= " + <b>".$casteller->alias."</b> ";
                                                $response_recordatori ="ℹ️ Recordatori expres";
                                                $response_recordatori .= "Encara no Has confirmat (".$ha_confirmat.")";
                                                $response_recordatori .= $parametres_arguments[2];
                                                $response_recordatori .= " ".$tex_botons;
                                                $chatIdDesti = Telegram::getTelegram($casteller->id_casteller);
                                                TelegramBot::sendMessage([
                                                    'chat_id' => $chatIdDesti,
                                                    'text' => $response_recordatori,
                                                    'parse_mode' => "HTML",
                                                    'reply_markup' => 
                                                    json_encode([
                                                        // Recorda aquest element es un array d'un array, es  a dir una matriu
                                                        'inline_keyboard' => self::keyboardFormatter($boto_recorda, 1),
                                                        'resize_keyboard' => true,
                                                        'one_time_keyboard' => false
                                                        ])
                                                ]);
                                            } else { 
                                                $compta_no_trames++; 
                                                $response_no_trames .= " - <b>".$casteller->alias."</b>";
                                            }
                                            unset($boto_recorda);  // buidem boto_recorda
                                        } else {
                                            $compta_ko++;
                                        // $response .= "\nℹ️ NO Telegram ";
                                            $response_ko .= " - <b>".$casteller->alias."</b>";
                                        }
                                    }

                                }                            
                            }
                            $response_avis ='';
                            if($event_proper==0) { $response_avis .= "\n⚠️ no hi ha cap esdeveniment "; }
                            if($recordsTotalCastellers==0) { $response_avis .= "\n⚠️ no hi ha cap casteller "; }
                            $response .= "\n ".$response_avis;
                            $response .= "\n\n📊 Total: ".$recordsTotalCastellers ." tramesos ".$compta_tramesos;
                            $response .= "\n ".$response_ok_trames." <b>".$compta_ok_trames."</b>";
                            $response .= "\n ".$response_no_trames." <b>".$compta_no_trames."</b>";
                            $response .= "\n ".$response_ko." <b>".$compta_ko."</b>";
                         
                    break;


                    case 'tag': 
                        $response = "🤖"; // $response = trans('telegram.has_demanat'). " <b> ".$parametres_arguments[1]." ".$parametres_arguments[2]."</b> ";
                        $response .= "🤖"; // $response = trans('telegram.has_demanat'). ": ".$command_callback."<b> ".trans('telegram.tag')."</b>";



                        $colla = Colla::find($colla_id);
                        
                        if( $parametres_arguments[2] =='EVENTS' )
                        {
                            $response = "🤖"; // $response = trans('telegram.has_demanat'). ": <b> ".trans('telegram.events')."</b>";
                           $response .= "\nEn 354 Has demanat: (".$parametres_arguments[1].") ".$parametres_arguments[2];

                            // Fem recompte total sense limit
                            $future_events = Event::getUpcommingEventsByTags($parametres_arguments[1], 'OR', $colla)->where('visible', '=', 1)
                            ->whereDate('open_date', '<', Carbon::now())->whereDate('close_date', '>', Carbon::now())
                            ->get();
                            $recordsTotals = count($future_events);  

                            $limit = 5; $skip =0; $column_order = 'start_date'; $dir = 'ASC'; 
                            $skip =  $parametres_arguments[3] | 0 ; 
                            // Fem recompte parcial amb limit
                            $future_events = Event::getUpcommingEventsByTags($parametres_arguments[1], 'OR', $colla)->where('visible', '=', 1) 
                            ->whereDate('open_date', '<', Carbon::now())->whereDate('close_date', '>', Carbon::now())
                            ->take($limit)->skip($skip)
                            ->orderBy($column_order, $dir)->get();
                            $response_icono = "\n🔴  ";
                            $events = $future_events;
                            $data = new \stdClass();
                            $data->data = array();
                            $recordsTotal = count($future_events);
                            $comptador = 0;  
                            if($recordsTotal>=1) {
                                $response_icono = "\n🟢 ";
                                
                                $response .= $response_icono." ". trans('telegram.trobats').": <b>".$recordsTotal." / ".$recordsTotals."</b> ";
                                foreach ($events as $event)
                                {
                                    $comptador++; 
                                  //   $response .= "\nℹ️ ". Humans::readEventColumn($event->id_event, 'start_date');
                                    $tex_botons = Humans::readEventColumn($event->id_event, 'start_date');
                                    $attendances = Attendance::where('casteller_id', $casteller_id)->where('event_id', '=', $event->id_event)->get();
                                    $status = "❔";
                                    $attendance_status = 0;
                                    if (count($attendances)>=1){
                                        foreach ($attendances as $attendance)
                                        {
                                            if($attendance->status=='YES') {$status = "✅ ";  $attendance_status = $attendance->status; }
                                            if($attendance->status=='NO') {$status =  "❌ ";  $attendance_status = $attendance->status; }
                                            if($attendance->status=='UNKNOWN') {$status = "❓";  $attendance_status = $attendance->status; }
                                        }
                                    }
                                    $response .= "\n ".$status." ".$tex_botons;
                                    //  crea array per a convertir en json_encode
                                    //$botons[] = ['text' => "+".$status." ".$tex_botons, 'callback_data' => 'attendance #'.$event->id_event."#".$attendance_status];

                                }
                                // if($recordsTotal>1) {
                                if($recordsTotal>$comptador) {
                                        // $skip_anterior = 1; 
                                   // $botons[] = ['text' => $limit.'⏪ '.$skip_anterior.' ('.$parametres_arguments[3].')', 'callback_data' => 'tag #'.$parametres_arguments[1]."#$parametres_arguments[2]#".$skip_anterior."#" ] ;

                                    if($skip>$limit) {  // boto anteriors
                                        $skip_anterior = $parametres_arguments[3] - $limit; 
                                        $botons[] = ['text' => '⏪ '.$skip_anterior, 'callback_data' => 'tag #'.$parametres_arguments[1]."#$parametres_arguments[2]#".$skip_anterior."#" ] ;
                                    }

                                    $skip = $limit; 
                                    if($comptador==$limit) {  // boto seguents
                                        $comptador = 0;
                                        $skip_seguent = $parametres_arguments[3] + $limit; 
                                        $botons[] = ['text' => '⏩ '.$skip_seguent, 'callback_data' => 'tag #'.$parametres_arguments[1]."#$parametres_arguments[2]#".$skip_seguent."#" ] ;
                                    }
                                // }                               
                                //if( )    
                                $keyboard_tipus = 'teclado_inline'; // 'teclado_inline' o 'keyboard' 
                                    $finalOutput['reply_markup'] = json_encode([
                                        // Recorda aquest element es un array d'un array, es  a dir una matriu
                                        'inline_keyboard' => self::keyboardFormatter($botons, 1), 
                                        'resize_keyboard' => true,
                                        'one_time_keyboard' => false
                                    ]);
                                }
                            } else {
                                $response .= " ".trans('telegram.no_registres')." ";
                            }
                        }
                        if( $parametres_arguments[2] =='CASTELLERS' )
                        {
                            $response = "🤖"; // $response = trans('telegram.has_demanat'). ": ".$command_callback."<b> ".trans('telegram.castellers')."</b>";
                            $response = "🤖"; // $response = trans('telegram.has_demanat').": <b>".$parametres_arguments[2]." ".$parametres_arguments[1]. "</b> "; //.$parametres_arguments[3];
                            // Fem recompte total sense limit
                            $castellers = Casteller::filterCastellersByTags($parametres_arguments[1], 'OR', $colla)->get();
                            $recordsTotals = count($castellers);  
                            // Fem recompte parcial amb limit
                            $limit = 2; $skip =  $parametres_arguments[3] | 0 ;
                            $castellers = Casteller::filterCastellersByTags($parametres_arguments[1], 'OR', $colla)->take($limit)->skip($skip)->get();
                            // $castellersss = Casteller::filterCastellersByTags($parametres_arguments[1], 'OR', $colla)->take($limit)->skip($skip)->toSql();
                            // dd($castellers);
                            // $response .= "\n consulta es (".$castellersss.") ";

                            $response_icono = "\n🔴  ";
                            $data = new \stdClass();
                            $data->data = array();
                            $recordsTotal = count($castellers);
                            $comptador = 0;  $pagines = $recordsTotals / $recordsTotal;
                            if($recordsTotal>=1) {
                                $response_icono = "\n🟢 ";
                                $response .= $response_icono." ". trans('telegram.trobats').": <b> ".$recordsTotals."</b> ";
                                
                                foreach ($castellers as $casteller)
                                {
                                    $comptador++; 
                                    $response .= "\nℹ️  ".$casteller->name;
                                        $response .= " ".$casteller->last_name;
                                        $response .= " <b>".$casteller->alias."</b>";
                                       // $response .= " ".$comptador;
                                }
                                
                                if($recordsTotal>=1) {
                                    if($skip>=$limit) {  // boto anteriors
                                        $skip_anterior = $skip - $limit; 
                                        $botons[] = ['text' => '⏪ ', 'callback_data' => 'tag #'.$parametres_arguments[1]."#$parametres_arguments[2]#".$skip_anterior."#" ] ;
                                    }

                                    $skip = $limit; 
                                    if($comptador==$limit) {  // boto seguents
                                        $comptador = 0;
                                        $botons[] = ['text' => '⏩ ', 'callback_data' => 'tag #'.$parametres_arguments[1]."#$parametres_arguments[2]#".$skip."#" ] ;
                                        $keyboard_tipus = 'teclado_inline'; // 'teclado_inline' o 'keyboard' 
                                        $finalOutput['reply_markup'] = json_encode([
                                            // Recorda aquest element es un array d'un array, es  a dir una matriu
                                            'inline_keyboard' => self::keyboardFormatter($botons, 2), 
                                            'resize_keyboard' => true,
                                            'one_time_keyboard' => false
                                        ]);
                                    }
                                }
                                $finalOutput['text'] = $response ;
                            } else {
                                $response .= "\n⚠️ ".trans('telegram.error')." " ;
                                $response .= " ".trans('telegram.no_registres')." ".$recordsTotal;
                            }
                        }
                       // $finalOutput['text'] = 'En 500  '.$response;
                    break;

                    case 'tag_assistencia':
                        // $response = "en 513: <b>".$command_callback."</b> ".$parametres_arguments[1]." ".$parametres_arguments[2]." ".$parametres_arguments[3];
                        // $response = "🤖"; // $response = trans('telegram.has_demanat'). ": <b>".$parametres_arguments[2]." ".$parametres_arguments[1]."</b>";
                        $response = "🤖"; // $response = trans('telegram.has_demanat'). ": <b>".$parametres_arguments[2]." </b>";
                        $colla = Colla::find($colla_id);
                        $limit = 5; $skip = $parametres_arguments[3] | 0 ;  $column_order = 'start_date'; $dir = 'ASC'; 
                        
                        if( $parametres_arguments[2] =='EVENTS' )
                        {
                            // Fem recompte total sense limit
                            $future_events = Event::getUpcommingEventsByTags($parametres_arguments[1], 'OR', $colla)
                            ->whereDate('open_date', '<', Carbon::now())->whereDate('close_date', '>', Carbon::now())
                            ->orderBy($column_order, $dir)->get();
                            $recordsTotals = count($future_events);
                            // Fem recompte parcial amb limit
                            $future_events = Event::getUpcommingEventsByTags($parametres_arguments[1], 'OR', $colla)
                            ->whereDate('open_date', '<', Carbon::now())->whereDate('close_date', '>', Carbon::now())
                            ->take($limit)->skip($skip)
                            ->orderBy($column_order, $dir)->get();
                            $response_icono = "\n🔴  ";
                            $events = $future_events;
                            $data = new \stdClass();
                            $data->data = array();
                            $recordsTotal = count($future_events);
                            $comptador = 0;  
                            if($recordsTotal>=1) {
                                $response_icono = "\n🟢 ";
                            }
                            $response .= $response_icono." ". trans('telegram.trobats').": <b>".$recordsTotal."/".$recordsTotals."</b> ";
                            foreach ($events as $event)
                            {
                                $tex_botons = Humans::readEventColumn($event->id_event, 'start_date');
                                $attendances = Attendance::where('casteller_id', $casteller_id)->where('event_id', '=', $event->id_event)->get();
                                $count = Attendance::where('event_id', $event->id_event)->count();
                                $count_yes = Attendance::where('event_id', $event->id_event)->where('status', '=', 'YES')->count();
                                $count_no = Attendance::where('event_id', $event->id_event)->where('status', '=', 'NO')->count();
                                $count_unkonwn = Attendance::where('event_id', $event->id_event)->where('status', '=', 'UNKNOWN')->count();
                                // $response .= "\n".$count."🕴 ".$count_yes."✅ ".$count_no."❌ ".$count_unkonwn."❓";
                                $count_att = "🕴".$count." ✅".$count_yes." ❌".$count_no;
                                if($event->isOpen())
                                {
                                  //  $response .= ' <b>obert</b>';
                                  $comptador++; 
                                  //  crea array per a convertir en json_encode
                                  $botons[] = ['text' => $count_att." ".$tex_botons, 'callback_data' => 'tag_assistencia #'.$event->id_event."#ATTENDANCE#0"];
                                } 
                                $attendance_status = 0;
                            }
                            
                            if($recordsTotal>=1){
                                if($skip >= $limit) {  // boto anteriors
                                $skip_anterior =  $parametres_arguments[3]  - $limit; 
                                $botons[] = ['text' => '⏪ ', 'callback_data' => 'tag_assistencia #'.$parametres_arguments[1]."#$parametres_arguments[2]#".$skip_anterior."#" ] ;
                                }

                                $skip = $limit; 
                                if($comptador <=  $recordsTotals) {  // boto seguents
                                    $skip_seguent =  $parametres_arguments[3] + $limit; 
                                      if( $skip_seguent < $recordsTotals){
                                        $botons[] = ['text' => '⏩ ' , 'callback_data' => 'tag_assistencia #'.$parametres_arguments[1]."#$parametres_arguments[2]#".$skip_seguent."#" ] ;
                                      }
                                }
                                $keyboard_tipus = 'teclado_inline'; // 'teclado_inline' o 'keyboard' 
                                $finalOutput['reply_markup'] = json_encode([
                                    // Recorda aquest element es un array d'un array, es  a dir una matriu
                                    'inline_keyboard' => self::keyboardFormatter($botons),
                                    'resize_keyboard' => true,
                                    'one_time_keyboard' => false
                                ]);
                            }

                        }
                        
                        if( $parametres_arguments[2] =='ATTENDANCE' )
                        {
                            $limit = 2; $column_order = 'start_date'; $dir = 'ASC';
                            $skip =  $parametres_arguments[3] | 0 ; 
                            // Fem recompte total sense limit
                            $attendances = Attendance::where('event_id', '=', $parametres_arguments[1])->get();
                            $recordsTotals = count($attendances);

                            // Fem recompte parcial amb limit
                            $attendances = Attendance::where('event_id', '=', $parametres_arguments[1])
                            ->take($limit)->skip($skip)
                            ->get();
                            $response_icono = "\n🔴 pp ";
                            $data = new \stdClass();
                            $data->data = array();
                            $recordsTotal = count($attendances);
                            
                            if($recordsTotal>=1) {
                                $response_icono = "\n🟢 ";
                            }

                            $response .= $response_icono." ". trans('telegram.trobats').": <b>".$recordsTotal."</b> ";
                            $comptador = 0;
                            foreach ($attendances as $attendance)
                            {
                                $comptador++;
                                if($comptador<=$limit) {
                                   // $response .= "\nℹ️";

                                    $status = "❔";
                                    if($attendance->status=='YES') {$status = "✅ "; }
                                    if($attendance->status=='NO') {$status =  "❌ "; }
                                    if($attendance->status=='UNKNOWN') {$status = "❓"; }

                                    $response .= "\n ".$status;  
                                    if(Telegram::getNameCasteller($attendance->casteller_id))
                                    {
                                        $response .= " ".Telegram::getNameCasteller($attendance->casteller_id, 'alias')." ";
                                    }

                                    if( $attendance->companions >0 ){
                                        $response .= " ".$attendance->companions;
                                    }
                                } else {
                                    if(Telegram::getNameCasteller($attendance->casteller_id))
                                    {
                                        $response .= ">> ".Telegram::getNameCasteller($attendance->casteller_id, 'alias')." ";
                                    }
                                }
                                $response .= " ".$comptador;
                            }
                            $response .= "\n\n>>".$recordsTotals." :".$comptador." ::".$skip ;

                            $botons = [];
                            
                            // if($recordsTotal>=$limit){
                                if($skip >= $limit) {  // boto anteriors
                                $skip_anterior =  $parametres_arguments[3]  - $limit; 
                                $botons[] = ['text' => '⏪ '.$skip_anterior, 'callback_data' => 'tag_assistencia #'.$parametres_arguments[1]."#$parametres_arguments[2]#".$skip_anterior."#" ] ;
                                }

                                $skip = $limit; 
                            //    if($comptador <=  $recordsTotals) {  // boto seguents
                                    $skip_seguent =  $parametres_arguments[3] + $limit; 
                                      if( $skip_seguent < $recordsTotals){
                                        $botons[] = ['text' => '⏩ '.$skip_seguent  , 'callback_data' => 'tag_assistencia #'.$parametres_arguments[1]."#$parametres_arguments[2]#".$skip_seguent."#" ] ;
                                      }
                            //    }
                                if(count($botons)>=1){
                                    $keyboard_tipus = 'teclado_inline'; // 'teclado_inline' o 'keyboard' 
                                    $finalOutput['reply_markup'] = json_encode([
                                        // Recorda aquest element es un array d'un array, es  a dir una matriu
                                        'inline_keyboard' => self::keyboardFormatter($botons, 2),
                                        'resize_keyboard' => true,
                                        'one_time_keyboard' => false
                                    ]);
                                }
                          //  }
                            // $finalOutput['text'] = $response ;
                        } 
                        
                        //$finalOutput['text'] = 'En 608 Esta es la ayuda de mi bot...  '.$response;
                    break;

                    case 'aplicar_ok':
                        $keyboard_tipus = 'keyboard'; // 'teclado_inline' o 'keyboard'                        
                        //$response = "🤖"; // $response = trans('telegram.has_demanat'). ": ".$command."<b> ".trans('telegram.help')."</b> ".trans('telegram.castellers');
                        $response = "\n".trans('telegram.command_aplicar')." ";
                        $response = "\n⚠️ ".trans('telegram.done')." " ;
                    break;

                    case 'language':
                        $response = "🤖"; // $response = trans('telegram.has_demanat'). ": <b>".trans('telegram.language')."</b>";

                        $id_telegram = $finalOutput['chat_id'];
                        $lang = $parametres_arguments[1];

                        if(Telegram::updateLanguage($id_telegram, $lang))
                        {
                            //$response .= "\n⚠️ ".trans('telegram.done')." " ;
                            $response .=  trans('telegram.per_aplicar')." ".$lang;
                        }
                       // $response .= " callback_id ".$callback_id."";
                        $finalOutput['message_id'] = $callback_id;
                         
                        $lang_cat ='';  $lang_fr ='';  $lang_en =''; $lang_es ='';
                        if( $lang == 'ca') { $lang_cat =' ✅'; }
                        if( $lang == 'fr') { $lang_fr =' ✅'; }
                        if( $lang == 'en') { $lang_en =' ✅'; }
                        if( $lang == 'es') { $lang_es =' ✅'; }

                        $botons[] = ['text' =>  $lang_cat.'Català', 'callback_data' => 'language #ca#'];
                        $botons[] = ['text' =>  $lang_fr.'Français', 'callback_data' => 'language #fr#'];
                        $botons[] = ['text' =>  $lang_en.'English', 'callback_data' => 'language #en#'];
                        $botons[] = ['text' =>  $lang_es.'Castellano', 'callback_data' => 'language #es#'];
                        $botons[] = ['text' =>  trans('telegram.command_aplicar') .' '.$lang, 'callback_data' => 'aplicar_ok #ok#'];

                        $keyboard_tipus = 'teclado_inline'; // 'teclado_inline' o 'keyboard' 
                        $finalOutput['reply_markup'] = json_encode([
                            // Recorda aquest element es un array d'un array, es  a dir una matriu
                            'inline_keyboard' => self::keyboardFormatter($botons, 2),
                            'resize_keyboard' => true,
                            'one_time_keyboard' => false
                        ]);

                    break;

                    case 'multiuser_on':
                        $response = "🤖"; // $response = trans('telegram.has_demanat'). ": <b>".trans('telegram.multiuser')."</b>";

                        $id_telegram = $finalOutput['chat_id'];
                        $id_telegram_multi = $parametres_arguments[1];
                        $response .=  trans('telegram.multiuser')." ".$id_telegram_multi;
                                //   updateMultiuser($id_telegram, $casteller_multi_id = 0)
                        if(Telegram::updateMultiuser($id_telegram, $id_telegram_multi))
                        {
                            $response .= "\n⚠️ ".trans('telegram.done')." " ;
                            $response .=  trans('telegram.multiuser')." ".Telegram::getNameCasteller($id_telegram_multi);
                            $casteller_id  = $id_telegram_multi;
                            $rol = '';
                            $botons[] = ['text' =>  trans('telegram.command_aplicar') .' ', 'callback_data' => 'aplicar_ok #ok#'];
                            $keyboard_tipus = 'teclado_inline'; // 'teclado_inline' o 'keyboard' 
                            $finalOutput['reply_markup'] = json_encode([
                                // Recorda aquest element es un array d'un array, es  a dir una matriu
                                'inline_keyboard' => self::keyboardFormatter($botons, 2),
                                'resize_keyboard' => true,
                                'one_time_keyboard' => false
                            ]);

                        } else {
                            $response .= "\n⚠️ ".trans('telegram.error')." " ;
                        }

                    break;
                    
                }  // end switch ($command_callback)
            }  // end // si hem_rebut callback_data

            // $message = substr(strstr($text," "), 1);

            $response_nivell = "";
            $response_nivell .= "<b>".$command."</b> <code>".$num_parametres."</code>";
          //  $casteller_id = 0;  $rol = null;
            if($casteller_id <= 0 ) {  // usuaris que no son castellers/colla registats
                // test
                //     if(Telegram::setVisitor($finalOutput['chat_id'], 0, 0, 1, 0,  $first_name, 'ca' ) ){ 
                //         $response .= "\n⚠️ ".trans('telegram.done')." " ;
                //     }
                $finalOutput['text'] = $response;
                $finalOutput['text'] = $response_nivell;

               
                // $tinc = "";
                $botons[] =  ['/help', '/codi', 'contactar', 'ℹ️']; //menu USER UnRegistrer
                $keyboard_tipus = 'keyboard'; // 'teclado_inline' o 'keyboard' 
                 $finalOutput['reply_markup'] = json_encode([
                     // Recorda aquest element es un array d'un array, // 'keyboard' o 'inline_keyboard'
                     // 'inline_keyboard' => self::keyboardFormatter($botons),
                     'keyboard' => $botons,
                     'resize_keyboard' => true,
                     'one_time_keyboard' => false
                 ]);
                    
                $response_nivell .= "<code>trans('telegram.no_registrer')</code> ";
                switch ($command) {

                    case 'aplicar_ok':
                        $keyboard_tipus = 'keyboard'; // 'teclado_inline' o 'keyboard'                        
                        //$response = "🤖"; // $response = trans('telegram.has_demanat'). ": ".$command."<b> ".trans('telegram.help')."</b> ".trans('telegram.castellers');
                        $response = "\n".trans('telegram.command_aplicar')." ";
                        $response = "\n⚠️ ".trans('telegram.done')." " ;
                    break;
                    
                    case '/help': case trans('telegram.command_help'): case '🆘':

                        $keyboard_tipus = 'keyboard'; // 'teclado_inline' o 'keyboard' 
                        $response = "🤖" .trans('telegram.command_help'); // $response = trans('telegram.has_demanat'). ": <b>".trans('telegram.command_help')."</b> ".trans('telegram.no_registrer');
                        $response .= "\n\n➡️ ".trans('telegram.text_help_u')."";
                        $response .= "\n➡️ ".trans('telegram.text_codi_u')."";
                        $response .= "\n➡️ ".trans('telegram.text_quisom_u')."";
                        $response .= "\n➡️ ".trans('telegram.text_contactar_u')."";
                    break;

                    case '/codi': case trans('telegram.command_alta'):
                    // provisionalment s'ha de reforçar la validació amb dues dades
                        $response = "🤖" .trans('telegram.command_alta'); // $response = trans('telegram.has_demanat'). ": <b>".trans('telegram.codi')."</b> ".trans('telegram.no_registrer');
                        
                        $response .= "\n\nℹ️<b>".trans('telegram.Has_demanat_registre')."</b> ";

                        $response .= "\n\nℹ️<b>".trans('telegram.aceptes_condicions')."</b> ";
                        $response .= "\n".$num_parametres;
                        
                       
                        if($num_parametres>=2 ) {
                            // $castellers = Casteller::where('alias', $get_text[1])->where('email', $get_text[2])->toSql();
                            //  $response.= $castellers;
                            if($num_parametres==2 ) {
                                $castellers = Casteller::where('national_id_number', $get_text[1])->orWhere('alias', $get_text[1])->orWhere('email', $get_text[1])->get();
                            }
                            if($num_parametres==3 ) {
                                $castellers = Casteller::where('alias', $get_text[1])->where('email', $get_text[2])->get();
                            }
                            $recordsTotal = count($castellers);
                            $response_icono = "\n🔴  ";
                            
                            if($recordsTotal<=0) {
                                $response_icono = "\n🔴 ";
                                $response .= $response_icono." ".trans('telegram.error')." ";
                                $response .= " <b>".$get_text[1]."</b> o <b>".$get_text[2]."</b> ".trans('telegram.incorrecte');
                            }
                            
                            if($recordsTotal>1) {
                                $response_icono = "\n⚠️ ";
                                $response .= $response_icono." ".trans('telegram.error')." <b>".$primer_Parametre."</b> ".trans('telegram.repetit')."";
                            }

                            
                            if($recordsTotal==1) { 
                                $response .= " 🟢 ".trans('telegram.done')." ";
                                $data = new \stdClass();
                                $data->data = array();
                               
                                foreach ($castellers as $casteller)
                                {
                                   // $tinc = $casteller->hasTag('tecnica');
                                    //                       (int $id_telegram, int $id_casteller, int $id_colla, int $status = 0, $casteller_multi_id = 0, string $name = null, string $language = 'ca')
                                    if(Telegram::setTelegram($finalOutput['chat_id'], $casteller->id_casteller, $casteller->colla_id, 1, 0,  $first_name, 'ca' ) ){ 
                                        $response .= "\n⚠️ ".trans('telegram.done')." " ;
                                        $response .= "\nℹ️  ".$casteller->name;
                                        $response .= " ".$casteller->last_name;
                                        $response .= "\n ".$casteller->alias;
                                        
                                        $response_nivell = "🚸";
                                        $response_nivell .= "<code>".trans('telegram.casteller')." </code>";
                                        //$response_nivell .= " <b>(".$tinc.") </b>";

                                        $botonets[]  = [trans('telegram.command_help')." 🆘", trans('telegram.command_agenda')." 📅", trans('telegram.command_perfil')." ⚙️"]; //menu castellers
                                        // $botonets[]  = ["🥇", "🥈", "🥉"]; //menu castellers
                                        $colla = Colla::find($colla_id);
                                        $limit =3;
                                        $tags = Tag::currentTags('EVENTS', $colla)->take($limit);
                                        $recordsTotal = count($tags);
                                        $opcio_1 = trans('telegram.command_actuacions');
                                        $opcio_2 = trans('telegram.command_assajos');
                                        $opcio_3 = trans('telegram.command_activitats');
                                        if( $recordsTotal >= 1){
                                            $comptador=0;
                                            foreach ($tags as $tag)
                                            {
                                                $comptador++;
                                                if($comptador==1) $opcio_1 = $tag->value;
                                                if($comptador==2) $opcio_2 = $tag->name;
                                                if($comptador==3) $opcio_3 = $tag->name;
                                            }
                                        }
                                        $botonets[]  = [$opcio_1." 🥇", $opcio_2." 🥈", $opcio_3." 🥉"]; //menu castellers

                                        
                                        if($casteller->hasTag('tecnica')) {  
                                            $botonets[] =  ["Tecnica", "!castellers", "!assistencia",  "!recordatoris"];
                                            $rol = 'tecnica';
                                            $response_nivell .= "<code>".trans('telegram.tecnica')." </code>";
                                        }                                        
                                        
                                        
                                        $keyboard_tipus = 'keyboard'; // 'teclado_inline' o 'keyboard' 
                                         $finalOutput['reply_markup'] = json_encode([
                                             // Recorda aquest element es un array d'un array, // 'keyboard' o 'inline_keyboard'
                                             // 'inline_keyboard' => self::keyboardFormatter($botons),
                                             'keyboard' => $botonets,
                                             'resize_keyboard' => true,
                                             'one_time_keyboard' => false
                                         ]);
            
                                    } else {
                                        $response .= "\n⚠️ ".trans('telegram.error')." " ;
                                    }
                                }
                            }
                        } else {
                            $response .= "\n<b>".trans('telegram.no_parametres')."</b>:";
                            $response .= "\n\n⚠️ ".trans('telegram.text_per_donar_alta_1')." ";         
                            $response .= "\n\n⚠️ ".trans('telegram.text_per_donar_alta_2')." ";         
                            $response .= "\nℹ️ ".trans('telegram.si_continues')." <b> ".trans('telegram.aceptes_condicions')."</b> ";
                            $response .= "\n\n➡️ <b>".trans('telegram.mes_informacio')." </b> /help ";
                        }
                        
                    break;

                    case 'ℹ️': case 'fempinya':
                        $response = "🤖"; // $response = trans('telegram.has_demanat'). ": ℹ️<b> ".trans('telegram.fempinya')."</b> ".trans('telegram.no_registrer');

                        $foto = "https://fempinya.cat/img/photo_FP3.png" ;
                        $url = "https://fempinya.cat";
                        $response .= "<b>FemPinya:</b>  ".$foto." ".$url." gestió de colles castelleres" ;
                        
                        $response .= "\n\n".trans('telegram.es_un_bot_de_fempinya')."";
                        $response .= "\n".trans('telegram.norma_1')."";
                        $response .= "\n".trans('telegram.norma_2')."";
                        $response .= "\n".trans('telegram.norma_3')."";
                        $response .= "\n".trans('telegram.norma_4')." ";
                        //$finalOutput['text'] = $response;
                    break;

                    case 'contactar':
                        if($primer_Parametre!="") {
                            $finalOutput['text'] = substr_replace($finalOutput['text'], '', 0, 10);
                            $response = "🤖"; // $response = trans('telegram.has_demanat').": <b> ".trans('telegram.missatge_enviat')."</b> ".trans('telegram.no_registrer');
                            //$response = "\n".trans('telegram.missatge_enviat')."";
                            $response = "\n\n<b>".$finalOutput['text']."</b>";
                            $response2 =  'Missatge des de contactar: ('.$first_name.' '.$finalOutput['chat_id'].')  '.$finalOutput['text'];
                            // enviem missatge a  $chatIdDev
                            TelegramBot::sendMessage([
                                'chat_id' => $chatIdDev,
                                'text' => $response2,
                                'parse_mode' => "HTML"
                            ]);

                        } else {
                            $response = "🤖"; // $response = trans('telegram.has_demanat').": <b> ".trans('telegram.contactar')."</b> ".trans('telegram.no_registrer');
                            $response .= "\n⚠️<i>".trans('telegram.escriu_contactar_seguit_missatge')."</i>";
                            $response .= "\n⚠️<b>".trans('telegram.important_teu_correu')."</b> ";
                            $response .= "\n\n<i>trans('telegram.exemple'): \n</i>trans('telegram.contactar_bla_bla_bla')";
                        }
                       
                        $finalOutput['text'] = $response;
                    break;
                }  // end switch
            } // end usuaris que no son castellers/colla registats

            $finalOutput['text'] = "(En 1256) ".$response;
 
            
            
            // User registrer castellers sense rol = null
            if($casteller_id >= 1 ) {
                //dd("En linea 178 Ayuda seteada en el array de salida", $finalOutput);
                $response_nivell .= " <code> ".trans('telegram.casteller')." </code>";
                
                if($tinc_multiuser!="") {
                    
                    $botonets[]  = [trans('telegram.command_help')." 🆘", trans('telegram.command_agenda')." 📅",  $tinc_multiuser." ⚙️"]; //menu castellers

                } else { 
                    $botonets[]  = [trans('telegram.command_help')." 🆘", trans('telegram.command_agenda')." 📅", trans('telegram.command_perfil')." ⚙️" ]; //menu castellers 
                }
                

                $colla = Colla::find($colla_id);
                $limit =3;
                $tags = Tag::currentTags('EVENTS', $colla)->take($limit);
                $recordsTotal = count($tags);
                $opcio_1 = trans('telegram.command_actuacions');
                $opcio_2 = trans('telegram.command_assajos');
                $opcio_3 = trans('telegram.command_activitats');
                // if( $recordsTotal >= 1){
                //     $comptador=0;
                //     foreach ($tags as $tag)
                //     {
                //         $comptador++;
                //         if($comptador==1) $opcio_1 = $tag->value;
                //         if($comptador==2) $opcio_2 = $tag->name;
                //         if($comptador==3) $opcio_3 = $tag->name;
                //     }
                // }
                $botonets[]  = [$opcio_1." 🥇", $opcio_2." 🥈", $opcio_3." 🥉"]; //menu castellers
               // $botonets[]  = [ trans('telegram.command_language')." 🏳️", trans('telegram.command_multiuser')." 👥", trans('telegram.command_baixa')." 🚮" ]; //menu perfil
               
                $recordsTotal = count($tags);
                $casteller = $data_content['castellers'];  
                if($casteller->hasTag('tecnica')) 
                {
                    $botonets[] =  [ "!".trans('telegram.command_cercar')."" , "!castellers", "!assistencia",  "!recordatoris"];
                    $rol = 'tecnica'; 
                }
                $response_nivell .= " <code>".trans('telegram.tecnica')." </code>";


                if($command=='perfil' || $command=='⚙️') $botonets[]  = [ trans('telegram.command_language')." 🏳️", trans('telegram.command_multiuser')." 👥", trans('telegram.command_baixa')." 🚮" ]; //menu perfil

                if( $keyboard_tipus != 'teclado_inline'){
                   // $keyboard_tipus = 'keyboard'; // 'teclado_inline' o 'keyboard' 
                    $finalOutput['reply_markup'] = json_encode([
                        // Recorda aquest element es un array d'un array, // 'keyboard' o 'inline_keyboard'
                        // 'inline_keyboard' => self::keyboardFormatter($botons),
                        'keyboard' => $botonets,
                        'resize_keyboard' => true,
                        'one_time_keyboard' => false
                    ]); 
                }
                switch ($command) {
                    case '/fet': // 
                        $keyboard_tipus = 'keyboard'; // 'teclado_inline' o 'keyboard' 
                        $response = trans('telegram.fet'). ": ".$command."<b> ".trans('telegram.fet')."</b> ".trans('telegram.castellers');
                       
                                                
                    break;
                    case '/help': case  trans('telegram.command_help'): case '🆘':
                        $keyboard_tipus = 'keyboard'; // 'teclado_inline' o 'keyboard'                        
                        $response = "🤖"; // $response = trans('telegram.has_demanat'). ": ".$command."<b> ".trans('telegram.help')."</b> ".trans('telegram.castellers');
                        $response = "\n".trans('telegram.command_help')." ";
                        $response .= "\n⚙️ ".trans('telegram.text_perfil')." ";
                        $response .= "\n📅 ".trans('telegram.text_agenda')." ";
                        $response .= "\n🥇 ".trans('telegram.text_actuacions')." ";
                        $response .= "\n🥈 ".trans('telegram.text_assajos')." ";
                        $response .= "\n🥉 ".trans('telegram.text_activitats')." ";
                        $response .= "\n🚮 ".trans('telegram.text_baixa')." ";
                                                
                    break;

                    case '/baixa': case  trans('telegram.command_baixa'): case '🚮': 
                        $response = "🤖" .trans('telegram.baixa'); // $response = trans('telegram.has_demanat').": <b>".trans('telegram.baixa')."</b> ".trans('telegram.castellers');
                        $response .= "\n".trans('telegram.text_baixa') ;
                        if($command == trans('telegram.command_baixa') || $command == "🚮") { 
                            $response .= "\n" ;
                            $response .= "\n➡️ ".trans('telegram.confirmar_baixa')." ";

                        } else if ($command == '/baixa' ){
                            if(Telegram::DestroyeTelegram($finalOutput['chat_id']) ){ 
                                $response .= "\n⚠️ ".$command." " ;
                                $response .= "\n ".trans('telegram.done_baixa')." ";
                                $response_nivell = "<b>🚸 </b>";
                                $response_nivell .= "<b>".trans('telegram.no_registrer')."</b> ";
                                $botons[] =  ['/help', trans('telegram.command_alta'), 'contactar', 'ℹ️']; //menu USER UnRegistrer
                                $keyboard_tipus = 'keyboard'; // 'teclado_inline' o 'keyboard' 
                                $finalOutput['reply_markup'] = json_encode([
                                    // Recorda aquest element es un array d'un array, // 'keyboard' o 'inline_keyboard'
                                    // 'inline_keyboard' => self::keyboardFormatter($botons),
                                    'keyboard' => $botons,
                                    'resize_keyboard' => true,
                                    'one_time_keyboard' => false
                                ]);
                            } else {
                                $response .= "\n⚠️ ".trans('telegram.error')." " ;
                            }
                        }
                    break;

                    case 'perfil': case $tinc_multiuser : case  trans('telegram.command_perfil'): case '⚙️':
                        $response = "🤖" .trans('telegram.perfil'); // $response = trans('telegram.has_demanat').": <b> ".trans('telegram.perfil')."</b> ".trans('telegram.castellers');
                        $keyboard_tipus = 'keyboard'; // 'teclado_inline' o 'keyboard' 
                        
                        //$response .= "\n<b>".trans('telegram.command_perfil')."</b>:";
                        // $response .= "\n<b>".$tinc_multiuser."</b>:";
                        $casteller = $data_content['castellers'];
                            $foto = "" ;

                            if(!empty($casteller->num_soci) || !is_null($casteller->num_soci)) $response .= "\n🎫  ".$casteller->num_soci;
                            if(!empty($casteller->national_id_number) || !is_null($casteller->national_id_number)) $response .= "\n🎫  ".$casteller->national_id_number;
                            if(!empty($casteller->getProfileImage()) || !is_null(getProfileImage())) $foto =  $casteller->getProfileImage(); 
                            if(!empty($casteller->gender) || !is_null($casteller->gender)){ 
                                if ($casteller->gender == 1) {
                                    $response .= "\n🧔  "; 
                                }
                                else
                                {
                                    $response .= "\n👩‍🦰  ";                                   
                                }
                            }
                            if (is_null($casteller->gender))
                            {
                                     $response .= "\nℹ️ ";    
                            }
                            if(!empty($casteller->name) || !is_null($casteller->name)) $response .= "  ".$casteller->name;
                            if(!empty($casteller->last_name) || !is_null($casteller->last_name))  $response .= " ".$casteller->last_name;
                            if(!empty($casteller->alias) || !is_null($casteller->alias)) $response .= "\n👤 ".$casteller->alias;
                            if(!empty($casteller->birthdate) || !is_null($casteller->birthdate)) $response .= "\n🎂 ".$casteller->birthdate;
                            if(!empty($casteller->family) || !is_null($casteller->family)) $response .= "\n👨‍👦 ".$casteller->family;
                            if(!empty($casteller->family_head) || !is_null($casteller->family_head)) $response .= "\n👨‍👩‍👦‍👦 ".$casteller->family_head;
                            if(!empty($casteller->email) || !is_null($casteller->email)) $response .= "\n📧 ".$casteller->email;
                            if(!empty($casteller->email2) || !is_null($casteller->email2)) $response .= "\n✉️ ".$casteller->email2;
                            if(!empty($casteller->phone) || !is_null($casteller->phone)) $response .= "\n☎️ ".$casteller->phone;
                            if(!empty($casteller->mobile_phone) || !is_null($casteller->mobile_phone)) $response .= "\n📱 ".$casteller->mobile_phone;
                            if(!empty($casteller->emergency_phone) || !is_null($casteller->emergency_phone)) $response .= "\n📱👩‍🚒 ".$casteller->emergency_phone;
                            if(!empty($casteller->address) || !is_null($casteller->address)) $response .= "\n📬 ".$casteller->address;
                            if(!empty($casteller->postal_code) || !is_null($casteller->postal_code)) $response .= "\n      ".$casteller->postal_code." - ";
                            if(!empty($casteller->city) || !is_null($casteller->city)) $response .= "\n      ".$casteller->city;
                            if(!empty($casteller->comarca) || !is_null($casteller->comarca)) $response .= "\n      ".$casteller->comarca;
                            if(!empty($casteller->province) || !is_null($casteller->province)) $response .= "\n      ".$casteller->province;
                            if(!empty($casteller->country) || !is_null($casteller->country)) $response .= "\n      ".$casteller->country;
                            if(!empty($casteller->height) || !is_null($casteller->height)) $response .= "\n<b>".trans('telegram.height')."</b> :".$casteller->height;
                            if(!empty($casteller->weight) || !is_null($casteller->weight)) $response .= "\n<b>".trans('telegram.weight')."</b> :".$casteller->weight;
                            if(!empty($casteller->shoulder_height) || !is_null($casteller->shoulder_height)) $response .= "\n<b>".trans('telegram.shoulder_height')."</b> :".$casteller->shoulder_height;
                            // $response .= " ".$castellers->getProfileImage(); 
                            $response .= "\n\n<b>avatar:</b>  ".$foto." " ;
                    break;


                    case '👥': case 'multiuser': // 
                        $response = "🤖"; // $response = trans('telegram.has_demanat').": <b> ".trans('telegram.multiuser')."</b> ".trans('telegram.castellers');
                        $response .= "\n\nℹ️<b>".trans('telegram.multiuser')."</b> ";

                        $response .= "\n\nℹ️<b>".trans('telegram.multiuser_condicions')."</b> ";

                      //  $castellers = Casteller::where('family', $casteller_id)->where('family_head', 0)->get();
                        $castellers = Casteller::where('family', $family)->get();
                        $recordsTotalFamily = count($castellers);
                        $response2 = "\n\n👥 ".$recordsTotalFamily." membres"; 
                        if($recordsTotalFamily>=1){
                            foreach ($castellers as $casteller)
                            {
                                $activat = "";
                                if( $casteller->id_casteller == $casteller_multi_id) {$activat = "✅";   }

                                $response2  .= "\n🚻  " ;
                                $response2 .= " ".  $casteller->alias ." ->".$casteller->name;
                                $response2 .= " ".$activat;
                                
                                $boto_multiuser[] = ['text' => $activat.' '.$casteller->alias, 'callback_data' => 'multiuser_on #'.$casteller->id_casteller."#on"];
                            }
                            //if($recordsTotalFamily>=1){
                            $keyboard_tipus = 'teclado_inline'; // 'teclado_inline' o 'keyboard' 
                            $finalOutput['reply_markup'] = json_encode([
                                // Recorda aquest element es un array d'un array, es  a dir una matriu
                                'inline_keyboard' => self::keyboardFormatter($boto_multiuser, 2), 
                                'resize_keyboard' => true,
                                'one_time_keyboard' => false
                            ]);
                        }
                        // $response .= "\n".$num_parametres;
                        if($num_parametres==3 ) {
                            // $castellers = Casteller::where('alias', $get_text[1])->where('email', $get_text[2])->toSql();
                            //  $response.= $castellers;
                            //$castellers = Casteller::where('national_id_number', $primer_Parametre)->orWhere('alias', $primer_Parametre)->orWhere('email', $primer_Parametre)->get();
                            $castellers = Casteller::where('alias', $get_text[1])->where('birthdate', $get_text[2])->get();
                            $recordsTotal = count($castellers);
                            $response_icono = "\n🔴  ";
                            
                            if($recordsTotal<=0) {
                                $response_icono = "\n🔴 ";
                                $response .= $response_icono." ".trans('telegram.error')." ";
                                $response .= " <b>".$get_text[1]."</b> o <b>".$get_text[2]."</b> ".trans('telegram.incorrecte');
                            }
                            
                            if($recordsTotal>1) {
                                $response_icono = "\n⚠️ ";
                                $response .= $response_icono." ".trans('telegram.error')." <b>".$primer_Parametre."</b> ".trans('telegram.repetit')."";
                            }

                            
                            if($recordsTotal==1) { 
                                $response .= " 🟢 ".trans('telegram.done')." ";
                                $data = new \stdClass();
                                $data->data = array();
                               
                                foreach ($castellers as $casteller)
                                {
                                    $response .= "\n\n⚠️ Pendent de programar " ;
                                    $response .= $casteller->name;
                                    $response .= "\n⚠️ s'ha de relacionar aquest casteller amb la seva familia " ;
                                    $response .= "\n⚠️ i decidir si un casteller pot perteny a una familia d'unaltre colla " ;


                                }
                            }
                        } else {
                            $response .= "\n<b>".trans('telegram.no_parametres')."</b>:";
                            $response .= "\n\n⚠️ ".trans('telegram.text_per_donar_alta_1')." ";         
                            $response .= "\n\n⚠️ ".trans('telegram.text_per_donar_alta_2')." ";         
                            $response .= "\nℹ️ ".trans('telegram.si_continues')." <b> ".trans('telegram.aceptes_condicions')."</b> ";
                            $response .= "\nℹ️  ".$response2;
                            $response .= "\n\n➡️ <b>".trans('telegram.mes_informacio')." </b> /help ";
                        }
                        
                    break;


                    case 'language';  case trans('telegram.command_language'): case '🏳️': ; // case '🇦🇩': case '🇫🇷': case '🇬🇧': case '🇪🇸':

                        $response = "🤖"; // $response = trans('telegram.has_demanat').": <b> ".trans('telegram.language')."</b> ";
                        $response .= trans('telegram.tria_una_opcio');
                        $lang_cat ='';  $lang_fr ='';  $lang_en =''; $lang_es ='';
                        if( $lang == 'ca') { $lang_cat ='✅ '; }
                        if( $lang == 'fr') { $lang_fr ='✅ '; }
                        if( $lang == 'en') { $lang_en ='✅ '; }
                        if( $lang == 'es') { $lang_es ='✅ '; }

                        $botons[] = ['text' =>  $lang_cat.'Català', 'callback_data' => 'language #ca#'];
                        $botons[] = ['text' =>  $lang_fr.'Français', 'callback_data' => 'language #fr#'];
                        $botons[] = ['text' =>  $lang_en.'English', 'callback_data' => 'language #en#'];
                        $botons[] = ['text' =>  $lang_es.'Castellano', 'callback_data' => 'language #es#'];
                        $keyboard_tipus = 'teclado_inline'; // 'teclado_inline' o 'keyboard' 
                        $finalOutput['reply_markup'] = json_encode([
                            // Recorda aquest element es un array d'un array, es  a dir una matriu
                            'inline_keyboard' => self::keyboardFormatter($botons, 2), 
                            'resize_keyboard' => true,
                            'one_time_keyboard' => false
                        ]);

                    break;

                    case 'actuacions':  case 'assajos':   case 'activitats':  
                    case 'actuacio':  case 'assaig':   case 'activitat':  
                    case  trans('telegram.command_actuacions'):
                    case trans('telegram.command_assajos'):
                    case trans('telegram.command_activitats'):
                        
                        $response = "🤖"; // $response = trans('telegram.has_demanat'). ": ".trans('telegram.tria_una_opcio')." ";

                        $colla = Colla::find($colla_id);
                        $limit =3;  // Nomès les 3 primeres. Haurien d'estar reservades
                        $avui =  Carbon::now();
                        $tags = Tag::currentTags('EVENTS', $colla)->take($limit);
                        $recordsTotal = count($tags);
                        $opcio_1 = trans('telegram.command_actuacions');
                        $opcio_2 = trans('telegram.command_assajos');
                        $opcio_3 = trans('telegram.command_activitats');
                        if( $recordsTotal >= 1){
                            $comptador=0;
                            foreach ($tags as $tag)
                            {
                                $comptador++;
                                if($comptador==1) $opcio_1 = $tag->value;
                                if($comptador==2) $opcio_2 = $tag->name;
                                if($comptador==3) $opcio_3 = $tag->name;
                            }
                        }
                        if( $command =='actuacions' or  $command == 'actuacio' or $command == $opcio_1 or $command == trans('telegram.command_actucions') )
                        {
                            $response .= "".trans('telegram.command_actuacions')." ";
                            //$limit = 10; $skip =0; 
                            $column_order = 'start_date'; $dir = 'ASC';
                            $future_events = Event::getUpcommingEventsByTags($opcio_1, 'OR', $colla)->where('visible', '=', 1)
                            ->where('open_date', '<', $avui)->where('close_date', '>', $avui)
                           // ->take($limit)->skip($skip)
                            ->orderBy($column_order, $dir)->get();

                        }
                        if( $command =='assajos' or  $command =='assaig'  or $command == $opcio_2 or $command == trans('telegram.command_assajos') )
                        {
                            $response .= "".trans('telegram.command_assajos')." ";
                            $limit = 10; $skip =0; $column_order = 'start_date'; $dir = 'ASC'; 
                            
                            $future_events = Event::getUpcommingEventsByTags($opcio_2, 'OR', $colla)->where('visible', '=', 1)
                            ->where('open_date', '<', $avui)->where('close_date', '>', $avui)
                            ->take($limit)->skip($skip)->orderBy($column_order, $dir)->get();

                        }
                        if( $command =='activitats' or $command == 'activitat' or $command == $opcio_3 or $command == trans('telegram.command_activitats') )
                        {
                            $response .= "".trans('telegram.command_activitats')." ";
                            $limit = 10; $skip =0; $column_order = 'start_date'; $dir = 'ASC';
                            
                            $future_events = Event::getUpcommingEventsByTags($opcio_3, 'OR', $colla)->where('visible', '=', 1)
                            ->where('open_date', '<', $avui)->where('close_date', '>', $avui)
                            ->take($limit)->skip($skip)->orderBy($column_order, $dir)->get();
                            

                        }
                        $response_icono = "\n🔴  ";
                        $events = $future_events;
                        $data = new \stdClass();
                        $data->data = array();
                        $recordsTotal = count($future_events);
                        
                        $answers = Tag::currentTags('ATTENDANCE', $colla);


                        if($recordsTotal>=1) {
                                $response_icono = "\n🟢 ";
                            
                                $response .= $response_icono." ".trans('telegram.trobats').": <b>".$recordsTotal."</b> ";
                                foreach ($events as $event)
                                {
                                    $tex_botons = Humans::readEventColumn($event->id_event, 'start_date');
                                    // $response .= ' <b>'.$tex_botons.'</b>';
                                    $attendances = Attendance::where('casteller_id', $casteller_id)->where('event_id', '=', $event->id_event)->get();
                                    $status = "❔";
                                    $status_options = ""; $attendance_options = 0; $attendance_companions = $event->companions;
                                    $attendance_status = 0; $status_send=0;
                                    foreach ($attendances as $attendance)
                                        {
                                            $status_send=1;
                                            if($attendance->status=='YES') {$status = "✅ ";  $attendance_status = $attendance->status; }
                                            if($attendance->status=='NO') {$status =  "❌ ";  $attendance_status = $attendance->status; }
                                            if($attendance->status=='UNKNOWN') {$status = "❓";  $attendance_status = $attendance->status; }
                                            $attendance_options = $attendance->options;
                                            $attendance_companions = $attendance->companions;

                                        }
                                    if($event->isOpen())
                                    {
                                        //  crea array per a convertir en json_encode
                                        $botons[] = ['text' => $status." ".$tex_botons."", 'callback_data' => 'attendance #'.$event->id_event."#".$attendance_status."#0#".$attendance_options."#".$status_send."#".$callback_id."" ];
                                    }
                                   
                                }
                                $keyboard_tipus = 'teclado_inline'; // 'teclado_inline' o 'keyboard' 
                                $finalOutput['reply_markup'] = json_encode([
                                    // Recorda aquest element es un array d'un array, es  a dir una matriu
                                    'inline_keyboard' => self::keyboardFormatter($botons),
                                    'resize_keyboard' => true,
                                    'one_time_keyboard' => false
                                ]);
                        } else {
                            $response .= $response_icono." ". trans('telegram.trobats').": <b>".$recordsTotal."</b> ";
                        }

                    break;

                    case 'agenda': case trans('telegram.command_agenda'): case '📅':  
                        $response = "🤖" .trans('telegram.command_agenda'); // $response = trans('telegram.has_demanat').": <b> ".trans('telegram.agenda')."</b>";
                        $colla = Colla::find($colla_id);
                        $tags = Tag::currentTags('EVENTS', $colla);
                        $recordsTotal = count($tags);
                        if( $recordsTotal >= 1){
                            $response .= "\n\n".trans('telegram.tria_una_opcio')." ".$recordsTotal;
                            $botons = [];
                            foreach ($tags as $tag)
                            {
                                $count = 0;
                                $column_order = 'start_date'; $dir = 'ASC';
                                $count = Event::getUpcommingEventsByTags($tag->value, 'OR', $colla)->where('visible', '=', 1)
                               // ->whereDate('open_date', '<', Carbon::now())->whereDate('close_date', '>', Carbon::now())
                               //->take($limit)->skip($skip)
                                ->orderBy($column_order, $dir)
                                ->count();
                                $response .= "\n📅 ".$count." ".$tag->name;
                                if ($count>=1){
                                    $botons[] = ['text' => "📅 ".$count." ".$tag->name, 'callback_data' => 'tag #'.$tag->value."#".$tag->type."#0#" ] ;
                                }
                            }

                            $keyboard_tipus = 'teclado_inline'; // 'teclado_inline' o 'keyboard' 
                            $finalOutput['reply_markup'] = json_encode([
                                // Recorda aquest element es un array d'un array, es  a dir una matriu
                                'inline_keyboard' => self::keyboardFormatter($botons),
                                'resize_keyboard' => true,
                                'one_time_keyboard' => false
                            ]);
                        } else {
                            $response .= " ".trans('telegram.no_registres')." ";
                        }

                    break;
                }  // end switch
            }   // end User registrer castellers sense rol = null

            
            //- User registrer castellers amb rol != null
            if($rol == 'tecnica' ) {
               // $response_nivell .= "<b>".trans('telegram.tecnica')."</b> ";

                switch ($command) {
                   
                    case '/help': case  trans('telegram.command_help'): case '🆘':
                        $response = "🤖" .trans('telegram.command_help'); 
                        $keyboard_tipus = 'keyboard'; // 'teclado_inline' o 'keyboard' 
                        $response .=  "<b> ".trans('telegram.tecnica')."</b> ";
                        $response .= "\n➡️ ".trans('telegram.text_tecnica')." ";
                        $response .= "\n➡️ ".trans('telegram.text_llistat_castellers')." ";
                        $response .= "\n➡️ ".trans('telegram.text_llistat_asssitencia')." ";
                        $response .= "\n➡️ ".trans('telegram.text_recordatori')." ";
                        $response .= "\n➡️ ".trans('telegram.text_cercar')." ";
                    break;

                    case '!cercar':
                        $response = "🤖" .trans('telegram.cercar'); // $response = trans('telegram.has_demanat').": <b> ".trans('telegram.cercar')."</b>";
                        
                        if($primer_Parametre!="") {
                            $colla = Colla::find($colla_id);
                            $response .= "\n".trans('telegram.hem_rebut').": <b>".$primer_Parametre."</b>";
                            $castellers = Casteller::where('id_casteller', '=', $primer_Parametre,'OR', $colla )
                            ->orWhere('alias', '=', $primer_Parametre,'OR', $colla )
                            ->orWhere('name', '=', $primer_Parametre,'OR', $colla )
                            ->orWhere('last_name', '=', $primer_Parametre,'OR', $colla )
                            ->get();
                            //$casteller = Casteller::where('id_casteller', '=', $primer_Parametre)->toSql();
                            $response_icono = "\n🔴  ";
                            $data = new \stdClass();
                            $data->data = array();
                            $recordsTotal = count($castellers);
                            if($recordsTotal>=1) {
                                $response_icono = "\n🟢 ";
                                $response .= $response_icono." ".trans('telegram.trobats').": <b>".$recordsTotal."</b> ";
                                
                                foreach ($castellers as $casteller)
                                {
                                        $response .= "\nℹ️  ".$casteller->name;
                                        $response .= " ".$casteller->last_name;
                                        $response .= " <b>".$casteller->alias."</b>";
                                }
                            } else {
                                $response .= "\n⚠️ ".trans('telegram.error')." " ;
                                $response .= " ".trans('telegram.no_registres')." ".$recordsTotal;
                            }
                            $finalOutput['text'] = $response ;
                        } else {
                            $response .= "\n<b>".trans('telegram.no_parametres')."</b>:";
                            $response .= "\n<b>".trans('telegram.text_cercar')."</b>:";
                            
                        }
                    break;

                    case '/photo':
                        $response = "🤖" .trans('telegram.photo'); // $response = trans('telegram.has_demanat').": <b> ".trans('telegram.photo')."</b> ".trans('telegram.tecnica');
                        if($primer_Parametre!="" and $num_parametres>=3) {
                            $response .= "\n".trans('telegram.hem_rebut').": <b>".$primer_Parametre."</b>";
                            /************************************************************************************************************
                            *                       PRUEBA DE ENVIO DE IMAGENES AL CHAT DESDE PRODUCCION                                *
                            ************************************************************************************************************/
                            $aixo = "+++";
                            for ($m = 1; $m < $num_parametres; $m++) {
                                $aixo .= "\n".$m.": " .$get_text[$m];
                            }
                            $caption = "".$aixo."";

                            // URL DE LA IMATGE A INTERNET
                            $foto =  'https://fempinya.cat/img/photo_FP3.png';
                            // $foto_name = "photo_FP3.jpg";
                            $foto_name = basename($foto);
                            $foto_ext = explode(".", basename($foto));
                            $caption .= " ext: ".$foto_ext[1]; //
                            $ext_valid = 'jpg|png|gif|svg|';
                            // $foto_extension = extension($foto);
                            //$caption .= " ".$foto_name."";
                            if($ext_valid) {
                                $caption .= " ext_valid: ". $ext_valid;
                            } else {
                                $caption .= " NO ext_valid: ". $ext_valid;
                            }

                            // RUTA per guardar la foto
                            $foto_path = '/var/www/fempinya.eu/public/media/image/';

                            // RUTA complerta per trobar la foto
                            $foto_full_path = $foto_path . $foto_name;

                            // UPLOAD la foto al servidor LOCAL
                            $var_img = file_put_contents($foto_full_path, file_get_contents($foto) );

                            TelegramBot::sendPhoto([
                                'chat_id' => $update->getChat()->get('id'), 	// id del usuario final
                                'caption' => $caption,
                                'parse_mode' => "HTML",
                                'photo' => InputFile::create($foto_full_path)
                            ]);

                            // ELIMINANT foto del servidor LOCAL
                            unlink($foto_path);

                            // FINALITZA EL PROCES i envia flujo
                            //die();
                            // COMENTAR O ELIMINAR LA LINEA ANTERIOR 
                        } else {
                            $response .= "\n<b>".trans('telegram.no_parametres').":</b>";
                            $response .= "\n<b>".trans('telegram.text_photo')."</b>";
                            $response .= "\n<b>".trans('telegram.text_photo_format')."</b>";

                            $finalOutput['text'] = $response;
                        }

                    break;

                    case '🕹': case 'tecnica':
                        $response = "🤖" .trans('telegram.tecnica'); // $response = trans('telegram.has_demanat').": <b> ".trans('telegram.tecnica')."</b> ".trans('telegram.tecnica');
                        $response .= trans('telegram.opcions_rol_tecnica');
                        $response .= "\n ".trans('telegram.text_llistat_castellers')." ";
                        $response .= "\n ".trans('telegram.text_llistat_asssitencia')." ";
                        $response .= "\n ".trans('telegram.text_recordatori')." ";
                        if($primer_Parametre!="") {
                            $response .= "\n".trans('telegram.hem_rebut').": <b>".$primer_Parametre."</b>";
                            
                        } else {
                            $response .= "\n<b>".trans('telegram.no_parametres').":</b>";
                            
                        }
                        $foto = "https://fempinya.cat/img/photo_FP3.png" ;
                        $finalOutput['photo'] = $foto; //'fempinya.cat/img/photo_FP3.png';
                       // $finalOutput['text'] = $response;
                    break;

                    case '👬': case '!castellers':
                        $response = "🤖" .$command; // $response = trans('telegram.has_demanat'). ": ".$command."<b> ".trans('telegram.tecnica')."</b> ".trans('telegram.tecnica');
                        $response .= "\n". trans('telegram.opcions_rol_tecnica');

                        if($primer_Parametre!="") {
                             $response .= "\n".trans('telegram.hem_rebut').": <b>".$primer_Parametre."</b>";
                        } else {
                            $response .= "\nNo hem_rebut <b>".trans('telegram.no_parametres').":</b>";
                            $colla = Colla::find($colla_id);
                            $tags = Tag::currentTags('CASTELLERS', $colla);
                            $recordsTotal = count($tags);
                            if($recordsTotal>=1){
                                $response .= "\n\n".trans('telegram.tria_una_opcio')." ".$recordsTotal;
                                $skip = 0;
                                foreach ($tags as $tag)
                                {
                                    $count = 0;
                                    $castellers = Casteller::filterCastellersByTags($tag->value, 'OR', $colla)->count(); 
                                    $count = ($castellers);
                                    $response .= "\n 🕴 ".$count.' '.$tag->name;
                                    // $skip++ ;  // 0
                                    if($count >=1 ){
                                        $botons[] = ['text' => $tag->name.' 🕴'.$count, 'callback_data' => 'tag #'.$tag->value."#".$tag->type."#".$skip."#" ] ;
                                    }
                                }

                                $keyboard_tipus = 'teclado_inline'; // 'teclado_inline' o 'keyboard' 
                                $finalOutput['reply_markup'] = json_encode([
                                    // Recorda aquest element es un array d'un array, es  a dir una matriu
                                    'inline_keyboard' => self::keyboardFormatter($botons, 2), 
                                    'resize_keyboard' => true,
                                    'one_time_keyboard' => false
                                ]);
                            } else {
                                $response_icono = "\n🔴  ";
                                $response .= $response_icono." ". trans('telegram.trobats').": <b>".$recordsTotal."</b> ";
                            }
                        } // end if
                    break;

                    case '☑️': case '!assistencia':
                        $response = "🤖" .$command; // $response = trans('telegram.has_demanat'). ": ".$command."<b> ".trans('telegram.assistencia')."</b> ".trans('telegram.tecnica');
                        $response .= trans('telegram.opcions_rol_tecnica');

                        $colla = Colla::find($colla_id);
                        $tags = Tag::currentTags('EVENTS', $colla);
                        $recordsTotal = count($tags);
                        $response .= "\n\n".trans('telegram.tria_una_opcio')." ".$recordsTotal;
                        $botons = [];
                        foreach ($tags as $tag)
                        {
                            $response .= "\n🔹 ".$tag->name;
                            $botons[] = ['text' => $tag->name, 'callback_data' => 'tag_assistencia #'.$tag->value."#".$tag->type."#0#" ] ;
                        }
                        $keyboard_tipus = 'teclado_inline'; // 'teclado_inline' o 'keyboard' 
                        $finalOutput['reply_markup'] = json_encode([
                            // Recorda aquest element es un array d'un array, es  a dir una matriu
                            'inline_keyboard' => self::keyboardFormatter($botons,2),
                            'resize_keyboard' => true,
                            'one_time_keyboard' => false
                        ]);
                    break;

                    case '!recordatoris' : // case 'recordatoriexpres':
                        $response = "🤖" .$command; // $response = trans('telegram.has_demanat'). ": ".$command."<b> ".trans('telegram.recordatoris')."</b> ".trans('telegram.tecnica');
                        
                        if($primer_Parametre != "") {
                            $response = "🤖" .$command; // $response = trans('telegram.has_demanat'). ": ".$command."<b> ".trans('telegram.recordatoris')."</b> ".trans('telegram.tecnica');
                            $response .= trans('telegram.error')." ".trans('telegram.noparametres');

                        } 
                        else {
                            $response = "🤖" .$command; // $response = trans('telegram.has_demanat'). ": ".$command."<b> ".trans('telegram.recordatoris')."</b> ".trans('telegram.tecnica');
                            $response .= "\n".trans('telegram.tria_una_opcio');
                            $colla = Colla::find($colla_id);
                            $tags = Tag::currentTags('CASTELLERS', $colla);
                            $recordsTotal = count($tags);
                            if($recordsTotal>=1){
                                $response .= "\n\n".trans('telegram.tria_grup_destinatari')." ".$recordsTotal;
                                $skip = 0;
                                foreach ($tags as $tag)
                                {
                                    $count = 0;
                                    $castellers = Casteller::filterCastellersByTags($tag->value, 'OR', $colla)->count();
                                    $count = ($castellers);
                                    if($count >=1 ){
                                        $botons[] = ['text' => ''.$tag->name.' 🕴'.$count, 'callback_data' => 'recordar #'.$tag->value."#".$tag->type."#".$skip."#" ] ;
                                    }
                                }
                            }
                            $keyboard_tipus = 'teclado_inline'; // 'teclado_inline' o 'keyboard' 
                                $finalOutput['reply_markup'] = json_encode([
                                    // Recorda aquest element es un array d'un array, es  a dir una matriu
                                    'inline_keyboard' => self::keyboardFormatter($botons, 2), 
                                    'resize_keyboard' => true,
                                    'one_time_keyboard' => false
                                ]);
                        }
                        $finalOutput['text'] = $response;
                        
                    break;

                } //end switch
            } // end if 
            
            $finalOutput['text'] = $response;

            $cas = self::sendNotification($finalOutput);

        } catch (Exception $exception) {
            Log::error( "/n".$exception->getLine(). " | " . $exception->getCode() . " | " . $exception->getMessage());
            Log::error( "/n".$exception->getTraceAsString() );
             
            die(500);
        }

        dd($cas);

        return true;
    }


    private function sendNotification(array $params){

        // Editar un mensaje
        // NO FUNCIONA
        if ($params['edit']){
           //  dd($params['edit']);
            //return TelegramBot::editMessage($params);
            return TelegramBot::sendMessage($params);
        }

        // Eliminar un missatge
        if ($params['delete']) {
            return TelegramBot::deleteMessage($params);
        }

        // Enviar una foto
        if ($params['photo']){

            $params['photo'] =  InputFile::create($params['photo'], "Titol Foto");
            return TelegramBot::sendPhoto($params);
            
        }

        // Enviamen de text tradicional
        if (!$params['photo'] && !$params['delete'] && !$params['edit'] ){
            return TelegramBot::sendMessage($params);
        }
    }
}

