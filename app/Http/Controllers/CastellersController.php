<?php
namespace App\Http\Controllers;

use App\Casteller;
use App\Colla;
use App\Helpers\Humans;
use App\Tag;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Request as RequestInput;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Intervention\Image\ImageManagerStatic as Image;
use Symfony\Component\HttpFoundation\ParameterBag;


class CastellersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Factory|\Illuminate\View\View
     */
    public function getList()
    {
        if(!Auth::user()->accesBBDD()) abort(403);

        $data_content['colla'] = Colla::getCurrent();
        $data_content['tags'] = Tag::currentTags();
        $data_content['positions'] = Tag::currentTags('POSITION');
        $data_content['families'] = Colla::getFamilies();
        $data_content['tags_groups'] = Tag::groups();

        return view('castellers.list', $data_content);
    }

    /** get Castellers filtered list via AJAX */
    public function postListAjax()
    {
        if(!Auth::user()->accesBBDD()) abort(403);

        $colla = Colla::getCurrent();

        //params
        $limit = RequestInput::input('length');
        $skip = RequestInput::input('start');

        //order
        //$column_num = intval(Request::input('order')[0]['column']);
        $dir = RequestInput::input('order')[0]['dir'];
        $column_order = RequestInput::input('columns')[intval(RequestInput::input('order')[0]['column'])]['name'];

        //tags
        $tags = RequestInput::input('tags');
        $tags_search_type = RequestInput::input('filter_search_type'); //AND or OR

        $castellers = Casteller::filterCastellersByTags($tags, $tags_search_type);

        $data = new \stdClass();
        $data->data = array();

        if (!empty($_POST['search']['value']))
        {
            $castellers->where(function($q){
                $q->orWhere('castellers.name' , 'LIKE' , '%'.$_POST['search']['value'].'%');
                $q->orWhere('castellers.last_name' , 'LIKE' , '%'.$_POST['search']['value'].'%');
                $q->orWhere('castellers.alias', 'LIKE', '%'.$_POST['search']['value'].'%');
                $q->orWhere('castellers.email', 'LIKE', '%'.$_POST['search']['value'].'%');
                $q->orWhere('castellers.email2', 'LIKE', '%'.$_POST['search']['value'].'%');
            });
        }

        $all_castellers = $castellers;
        $data->recordsTotal = count($all_castellers->get());
        $data->recordsFiltered = count($all_castellers->get());

        $castellers = $castellers->take($limit) ->skip($skip);
        $castellers = $castellers->orderBy($column_order, $dir)->get();

        foreach($castellers as $casteller)
        {
            $array_casteller = [];

            $array_casteller['photo'] = '<img src="'.$casteller->getProfileImage().'" class="img-avatar img-avatar32" alt="">';
            $array_casteller['name'] = (empty($casteller->alias)) ? $casteller->name.' '.$casteller->last_name : $casteller->name.' '.$casteller->last_name.' ('.$casteller->alias.')';
            $array_casteller['tags'] = Humans::readCastellerColumn($casteller->id_casteller, 'tags', 'right');
            $array_casteller['gender'] = Humans::readCastellerColumn($casteller->id_casteller, 'gender');
            $array_casteller['birthdate'] = Humans::readCastellerColumn($casteller->id_casteller, 'birthdate');
            $array_casteller['position'] = is_null($casteller->position()) ? '' :$casteller->position()->name;
            $array_casteller['buttons'] = '<a href="'.route('castellers.edit', $casteller->id_casteller).'" class="btn btn-info"><i class="fa fa-vcard-o"></i></a>';

            array_push($data->data, $array_casteller);
        }

        echo json_encode($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return RedirectResponse|Redirector
     */
    public function postAddCasteller(Request $request)
    {
        if(!Auth::user()->accesWriteBBDD()) abort(403);

        $colla = Colla::getCurrent();

        $this->types = ['dni', 'nie', 'passport'];

        $request->validate([
            'num_soci' => 'nullable|numeric',
            'national_id_number' => 'nullable|max:50|min:7',
            'national_id_type' => 'nullable|max:8|min:3|in:' . implode(',', $this->types),
            'name' => 'required|max:150|min:3',
            'last_name' => 'required|max:150|min:3',
            'alias' => 'required|max:150|min:3',
            'birthdate' => 'nullable|date_format:d/m/Y',
            'subscription_date' => 'nullable|date_format:d/m/Y',
            'family' => 'nullable|max:150|min:2',
            'email' => 'nullable|email:rfc',
            'email2' => 'nullable|email:rfc',
            'phone' => 'nullable|max:20|min:7',
            'emergency_phone' => 'nullable|max:20|min:7',
            'mobile_phone' => 'nullable|max:20|min:7',
            'address' => 'nullable|max:255|min:3',
            'country' => 'nullable|max:100|min:3',
            'city' => 'nullable|max:100|min:3',
            'comarca' => 'nullable|max:100|min:3',
            'height' => 'nullable|numeric',
            'weight' => 'nullable|numeric',
            'shoulder_height' => 'nullable|numeric',
            'photo'=>'nullable|file|image|mimes:jpeg,png|max:2048'
        ]);

        $attributes = new ParameterBag($request->except('_token'));
        $casteller = Casteller::newCasteller($attributes, $colla);

        Session::flash('status_ok', trans('casteller.casteller_added'));
        return redirect(route('castellers.edit', $casteller->id_casteller));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param Casteller $casteller
     * @return Factory|\Illuminate\View\View
     * @throws AuthorizationException
     */
    public function getCardCasteller(Casteller $casteller)
    {
        $this->authorize('getCasteller', $casteller);
        if(!Auth::user()->accesBBDD()) abort(403);

        $data_content['casteller'] = $casteller;

        return view('castellers.card', $data_content);
    }

    /** get edit-card casteller via Ajax
     * @param Casteller $casteller
     * @return Factory|\Illuminate\View\View
     * @throws AuthorizationException
     */
    public function getCardEditCastellerAjax(Casteller $casteller)
    {
        $this->authorize('getCasteller', $casteller);
        if(!Auth::user()->accesWriteBBDD()) return view('errors.403');

        $data_content['casteller'] = $casteller;
        $data_content['tags'] = Tag::currentTags();
        $data_content['positions'] = Tag::currentTags('POSITION');
        $data_content['tags_groups'] = Tag::groups();
        $data_content['families'] = Colla::getFamilies();

        return view('castellers.ajax.card-edit', $data_content);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Casteller $casteller
     * @return RedirectResponse|Redirector
     * @throws AuthorizationException
     */
    public function postUpdateCasteller(Request $request, Casteller $casteller)
    {
        $this->authorize('getCasteller', $casteller);
        if(!Auth::user()->accesWriteBBDD()) abort(403);

        $colla = Colla::getCurrent();

        $this->types = ['dni', 'nie', 'passport'];

        $request->validate([
            'num_soci' => 'nullable|numeric',
            'national_id_number' => 'nullable|max:50|min:7',
            'national_id_type' => 'nullable|max:8|min:3|in:' . implode(',', $this->types),
            'name' => 'required|max:150|min:3',
            'last_name' => 'required|max:150|min:3',
            'alias' => 'required|max:150|min:3',
            'birthdate' => 'nullable|date_format:d/m/Y',
            'subscription_date' => 'nullable|date_format:d/m/Y',
            'family' => 'nullable|max:150|min:2',
            'email' => 'nullable|email:rfc',
            'email2' => 'nullable|email:rfc',
            'phone' => 'nullable|max:20|min:7',
            'emergency_phone' => 'nullable|max:20|min:7',
            'mobile_phone' => 'nullable|max:20|min:7',
            'address' => 'nullable|max:255|min:3',
            'country' => 'nullable|max:100|min:3',
            'city' => 'nullable|max:100|min:3',
            'comarca' => 'nullable|max:100|min:3',
            'height' => 'nullable|numeric',
            'weight' => 'nullable|numeric',
            'shoulder_height' => 'nullable|numeric',
            'photo'=>'nullable|file|image|mimes:jpeg,png|max:2048'
        ]);

        $casteller->num_soci = $request->input('num_soci');
        $casteller->national_id_number = $request->input('national_id_number');
        $casteller->national_id_type = $request->input('national_id_type');
        $casteller->name = $request->input('name');
        $casteller->last_name = $request->input('last_name');
        $casteller->alias = $request->input('alias');
        $casteller->family = $request->input('family');

        //gender
        if($request->input('gender')=='1')
        {
            $casteller->gender = 1;
        }
        elseif($request->input('gender')=='0')
        {
            $casteller->gender = 0;
        }
        else
        {
            $casteller->gender = null;
        }

        //birthdate
        if (is_null($request->input('birthdate')) || empty($request->input('birthdate')))
        {
            $casteller->birthdate = null;
        }
        else
        {
            $casteller->birthdate = date_format(date_create_from_format("d/m/Y", $request->input('birthdate')), "Y-m-d");
        }

        //subscription_date
        if (is_null($request->input('subscription_date')) || empty($request->input('subscription_date')))
        {
            $casteller->subscription_date = null;
        }
        else
        {
            $casteller->subscription_date = date_format(date_create_from_format("d/m/Y", $request->input('subscription_date')), "Y-m-d");
        }

        $casteller->email = $request->input('email');
        $casteller->email2 = $request->input('email2');
        $casteller->phone = $request->input('phone');
        $casteller->mobile_phone = $request->input('mobile_phone');
        $casteller->emergency_phone = $request->input('emergency_phone');
        $casteller->address = $request->input('address');
        $casteller->postal_code = $request->input('postal_code');
        $casteller->city = $request->input('city');
        $casteller->comarca = $request->input('comarca');
        $casteller->province = $request->input('province');
        $casteller->country = $request->input('country');
        $casteller->comments = $request->input('comments');
        $casteller->height = (float) $request->input('height');
        $casteller->weight = (float) $request->input('weight');
        $casteller->shoulder_height = (float) $request->input('shoulder_height');

        //tags
        foreach($casteller->tags() as $tag)
        {
            $casteller->removeTag($tag);
        }

        if(!is_null($request->input('tags')))
        {
            foreach($request->input('tags') as $tag)
            {
                $tag = Tag::where('colla_id', $colla->id_colla)->where('value', $tag)->first();
                $casteller->addTag($tag);
            }
        }

        //position
        if(!is_null($casteller->position()))
        {
            DB::table('casteller_tag')->where('casteller_id', $casteller->id_casteller)->where('tag_id', $casteller->position()->id_tag)->delete();
        }

        if(!is_null($request->input('position')))
        {
            $position = Tag::where('colla_id', $colla->id_colla)->where('value', $request->input('position'))->where('type', 'POSITION')->first();
            DB::table('casteller_tag')->insert(['casteller_id' => $casteller->id_casteller, 'tag_id' => $position->id_tag]);
        }

        $casteller->save();

        //exist file, upload
        if($request->file('photo'))
        {
            //if photo, delete photos
            if($casteller->photo)
            {
                unlink(public_path('media/colles/'.$colla->shortname.'/castellers').'/'.$casteller->photo.'-xs.jpg');
                unlink(public_path('media/colles/'.$colla->shortname.'/castellers').'/'.$casteller->photo.'-med.jpg');
                unlink(public_path('media/colles/'.$colla->shortname.'/castellers').'/'.$casteller->photo.'-xl.jpg');
            }

            $random_str = Str::random(10);
            $image_input = $request->file('photo');


            $imatge_xs = Image::make($image_input);
            $imatge_xs->fit(32);
            $imatge_xs->encode('jpg');
            $imatge_xs->save(public_path('media/colles/'.$colla->shortname.'/castellers/').$casteller->id_casteller.'_'.$random_str.'-xs'.'.jpg');

            $imatge_med = Image::make($image_input);
            $imatge_med->fit(128);
            $imatge_med->encode('jpg');
            $imatge_med->save(public_path('media/colles/'.$colla->shortname.'/castellers/').$casteller->id_casteller.'_'.$random_str.'-med'.'.jpg');

            $imatge_xl = Image::make($image_input);
            $imatge_xl->fit(1024);
            $imatge_xl->encode('jpg');
            $imatge_xl->save(public_path('media/colles/'.$colla->shortname.'/castellers/').$casteller->id_casteller.'_'.$random_str.'-xl'.'.jpg');


            $casteller->photo = $casteller->id_casteller.'_'.$random_str;
            $casteller->save();
        }


        Session::flash('status_ok', trans('casteller.casteller_updated'));
        return redirect(route('castellers.edit', $casteller->id_casteller));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Casteller $casteller
     * @return RedirectResponse|Redirector
     * @throws AuthorizationException
     */
    public function postDestroyCasteller(Casteller $casteller)
    {
        $this->authorize('getCasteller', $casteller);
        if(!Auth::user()->accesWriteBBDD()) abort(403);

        $colla = Colla::getCurrent();

        //if photo, delete photos
        if($casteller->photo)
        {
            unlink(public_path('media/colles/'.$colla->shortname.'/castellers').'/'.$casteller->photo.'-xs.jpg');
            unlink(public_path('media/colles/'.$colla->shortname.'/castellers').'/'.$casteller->photo.'-med.jpg');
            unlink(public_path('media/colles/'.$colla->shortname.'/castellers').'/'.$casteller->photo.'-xl.jpg');
        }

        $casteller->delete();

        Session::flash('status_ok', trans('casteller.casteller_destroyed'));
        return redirect(route('castellers.list'));
    }
}
