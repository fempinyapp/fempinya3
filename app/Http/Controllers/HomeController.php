<?php

namespace App\Http\Controllers;

use App\Colla;
use App\Event;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function index()
    {
        return redirect('dashboard')->send();
    }

    public function getHome()
    {
        return view('template.partials.home');
    }

    /** Show the application dashboard to the user.  */
    public function dashboard()
    {
        $data_content['colla'] = Colla::getCurrent();

        return view('dashboard.dashboard', $data_content);
    }
}
