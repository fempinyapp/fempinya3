<?php

namespace App\Policies;

use App\Casteller;
use App\Tag;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class CastellerPolicy
{
    use HandlesAuthorization;

    /** Only get Casteller own Colla
     * @param User $auth
     * @param Casteller $casteller
     * @return bool
     */
    public function getCasteller(User $auth, Casteller $casteller)
    {
        return $auth->colla_id===$casteller->colla_id;
    }
}
