<?php

namespace App\Console\Commands;

use App\Colla;
use App\Helpers\Humans;
use App\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Hash;

class CreateColla extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fempinya:create-colla {name} {email} {password}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create colla with user superadmin';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $name = $this->arguments()['name'];
        $email = $this->arguments()['email'];
        $password = $this->arguments()['password'];

        $colla = new Colla();
        $colla->name = $name;
        $colla->shortname = Humans::replaceSpecialCharacters($name);
        $colla->email = $email;
        $colla->phone = 666;
        $colla->save();

        //create directories
        if(!is_dir(public_path('media/image'))) mkdir(public_path('media/image'));
        if(!is_dir(public_path('media/image/users'))) mkdir(public_path('media/image/users'));
        if(!is_dir(public_path('media/colles'))) mkdir(public_path('media/colles'));
        if(!is_dir(public_path('media/colles/'.$colla->shortname))) mkdir(public_path('media/colles/'.$colla->shortname));
        if(!is_dir(public_path('media/colles/'.$colla->shortname.'/castellers'))) mkdir(public_path('media/colles/'.$colla->shortname.'/castellers'));
        if(!is_dir(public_path('media/colles/'.$colla->shortname.'/svg'))) mkdir(public_path('media/colles/'.$colla->shortname.'/svg'));

        $user = new User();
        $user->colla_id = $colla->id_colla;
        $user->role = 'ADMIN';
        $user->type = 4095;
        $user->name = $name;
        $user->email = $email;
        $user->language = 'ca';
        $user->password = Hash::make($password);

        $user->save();

        echo 'ok';
    }
}
