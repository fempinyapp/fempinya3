<?php

namespace App\Console\Commands;

use App\Casteller;
use App\Colla;
use App\Tag;
use Illuminate\Console\Command;
use Faker\Factory;
use Illuminate\Support\Facades\DB;

class CreateDemo extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fempinya:create-demo {id_colla}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create demo content';

    protected $translation = [
        'ca' => [
            'faker' => 'es_ES',
            'habitual' => ['habitual', 'Habitual'],
            'esporadic' => ['esporadic', 'Esporàdic'],
            'actiu' => ['actiu', 'Actiu'],
            'passiu' => ['passiu', 'Passiu'],
            'soci' => ['soci-protector', 'Soci protector'],
            'graller' => ['graller', 'Graller'],
            'timbaler' => ['timbaler', 'Timbaler'],
            'professions' => ['Estudiant', 'Arquitecte', 'Jardiner', 'Fuster', 'Psicòleg', 'Pensionista', 'Administratiu', 'Informàtic'],
            'tipus_socis' => ['Individual', 'Familiar', 'Jubilat', 'Estudiant'],
            'family' => 'Família',
            'events_tags' => ['actuació', 'assaig'],
            'attendance_tags' => ['arribo tard', 'marxo abans'],
        ]

    ];
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $id_colla = $this->arguments()['id_colla'];

        $this->castellers($id_colla, 'ca');
        $this->tags($id_colla, 'ca');
    }

    /** create castellers demo */
    private function castellers($id_colla, $lang)
    {
        $faker = Factory::create($this->translation[$lang]['faker']);

        $cities = array('Barcelona', 'Badalona', 'Hospitalet de Llobregat', 'Sabadell', 'Terrassa', 'Valls');
        $provinces = array('Barcelona', 'Tarragona', 'Lleida', 'Girona');
        $comarques = ['Barcelonés', 'Baix Llobregat', 'l\'Alt Camp', 'Tarragonès', 'l\'Alt Penedès'];

        for ($i = 1; $i <= 985; $i++)
        {
            $casteller = new Casteller();

            $casteller->colla_id = $id_colla;
            $casteller->gender = $faker->numberBetween(0, 1);
            if ($casteller->gender > 0) {
                $casteller->name = $faker->firstName('male');
                //$casteller->photo =  'avatarBM.jpg';
            }
            else
            {
                $casteller->name = $faker->firstName('female');
                //$casteller->photo =  'avatar.jpg';
            }
            $casteller->last_name = $faker->lastName() . ' ' . $faker->lastName();
            $casteller->email = $faker->safeEmail();
            $casteller->email2 = null;
            $casteller->national_id_type = 'dni';
            $casteller->national_id_number = $faker->dni();
            $casteller->birthdate = $faker->dateTimeThisCentury();
            $casteller->alias = null;
            $casteller->mobile_phone = $faker->phoneNumber();
            $casteller->phone = $faker->phoneNumber();
            $casteller->emergency_phone = $faker->phoneNumber();
            $casteller->address = $faker->streetAddress();
            $casteller->city = $faker->randomElement($cities);
            $casteller->postal_code = '080' . $faker->numberBetween(10, 42);
            $casteller->province = $faker->randomElement($provinces);
            $casteller->comarca = $faker->randomElement($comarques);
            $casteller->country = null;
            $casteller->comments = null;
            $casteller->height = $faker->numberBetween(160, 200);
            $casteller->weight = $faker->numberBetween(50, 100);
            $casteller->height = $faker->numberBetween(130, 170);

            $casteller->save();
        }

    }

    /** put tag in castellers demo */
    private function tags($id_colla, $lang)
    {
        $faker = Factory::create($this->translation[$lang]['faker']);
        $colla = Colla::find($id_colla);
        //create new tags
        $new_tags[1] = array(
            $this->translation[$lang]['habitual'][0] => $this->translation[$lang]['habitual'][1],
            $this->translation[$lang]['esporadic'][0] => $this->translation[$lang]['esporadic'][1],
        );
        $new_tags[2] = array(
            $this->translation[$lang]['actiu'][0] => $this->translation[$lang]['actiu'][1],
            $this->translation[$lang]['passiu'][0] => $this->translation[$lang]['passiu'][1],
            $this->translation[$lang]['soci'][0] => $this->translation[$lang]['soci'][1],
            $this->translation[$lang]['graller'][0] => $this->translation[$lang]['graller'][1],
            $this->translation[$lang]['timbaler'][0] => $this->translation[$lang]['timbaler'][1],
        );

        foreach ($new_tags as $id_group => $group)
        {
            foreach ($new_tags[$id_group] as $k => $v) {
                $tag = new Tag();
                $tag->name = $v;
                $tag->value = $k;
                $tag->type = 'CASTELLERS';
                $tag->group = $id_group;
                $tag->colla_id = $id_colla;
                $tag->save();
            }
        }

        $tags = Tag::where('colla_id', $id_colla)->where('type', 'CASTELLERS')
                    ->where('value', '!=', 'esporadic')
                    ->where('value', '!=', 'habitual')
                    ->get();

        $esporadics = Casteller::orderByRaw("RAND()")->take(596)->get();

        $tag_espoadic = Tag::where('colla_id', $id_colla)->where('value', 'esporadic')->first();

        foreach ($esporadics as $esporadic)
        {
            $esporadic->addTag($tag_espoadic);
        }

        $habituals = Casteller::join('casteller_tag', 'castellers.id_casteller', '=', 'casteller_tag.casteller_id')
            ->join('tags', 'casteller_tag.tag_id', '=', 'tags.id_tag')
            ->select('castellers.*')
            ->where('castellers.colla_id', $colla->id_colla)
            ->where('tags.value', '!=', 'esporadic')
            ->orderByRaw("RAND()")->take(352);

        $tag_habitual = Tag::where('colla_id', $id_colla)->where('value', 'habitual')->first();

        foreach ($habituals as $habitual)
        {
            $habitual->addTag($tag_habitual);
        }

        foreach ($tags as $tag)
        {
            $castellers = Casteller::orderByRaw("RAND()")->take($faker->numberBetween(70, 135))->get();

            foreach ($castellers as $casteller)
            {
                $casteller->addTag($tag);
            }
        }

        $positions = ['lateral' => ['lateral', 'Lateral'],
            'vent' => ['vent', 'Vent'],
            'mans' => ['mans', 'Mans'],
            'agulla' => ['agulla', 'Agulla'],
            'crossa' => ['crossa', 'Crossa'],
            'contrafort' => ['contrafort', 'Contrafort'],
            'tronc' => ['tronc', 'Tronc'],
            'canalla' => ['canalla', 'Canalla']];

        foreach ($positions as $position)
        {
            $tag = new Tag();
            $tag->name = $position[1];
            $tag->value = $position[0];
            $tag->type = 'POSITION';
            $tag->group = null;
            $tag->colla_id = $id_colla;
            $tag->save();
        }

        $castellers = Casteller::where('colla_id', $id_colla)->get();
        $positions = Tag::where('type', 'POSITION')->where('colla_id', $id_colla)->get()->toArray();

        foreach($castellers as $casteller)
        {
            $key = array_rand($positions, 1);

            DB::table('casteller_tag')->insert(['casteller_id' => $casteller->id_casteller, 'tag_id' => $positions[$key]['id_tag']]);
        }

    }
}
