<style>
    .custom-file-input ~ .custom-file-label::after {
        content: "{!! trans('admin.btn_input_file_text') !!}";
    }
</style>
<div class="block block-themed block-transparent mb-0" id="profile">
    <div class="block-header bg-primary-dark">
        <h3 class="block-title">{!! trans('casteller.add_casteller') !!}</h3>
        <div class="block-options">
            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                <i class="si si-close"></i>
            </button>
        </div>
    </div>

    {!! Form::open(array('id' => 'FormAddCasteller', 'url' => route('castellers.add'), 'method' => 'POST', 'class' => '', 'enctype' => 'multipart/form-data')) !!}

    <div class="block-content">
        <div class="row form-group">
            <div class="col-md-4">
                <label class="control-label">{!! trans('casteller.name') !!}</label>
                <input type="text" class="form-control" id="name" name="name" value="" required>
            </div>
            <div class="col-md-8">
                <label class="control-label">{!! trans('casteller.last_name') !!}</label>
                <input type="text" class="form-control" id="last_name" name="last_name" value="" required>
            </div>
        </div>
        <div class="row form-group">
            <div class="col-md-4">
                <label class="control-label">{!! trans('casteller.alias') !!}</label>
                <input type="text" class="form-control" id="alias" name="alias" value="">
            </div>
            <div class="col-md-4">
                <label class="control-label">{!! trans('casteller.national_id_type') !!}</label>
                <select class="form-control" name="national_id_type" id="national_id_type">
                    <option value="dni">{!! trans('casteller.dni') !!}</option>
                    <option value="nie">{!! trans('casteller.nie') !!}</option>
                    <option value="passport">{!! trans('casteller.passport') !!}</option>
                </select>
            </div>
            <div class="col-md-4">
                <label class="control-label">{!! trans('casteller.national_id_number') !!}</label>
                <input type="text" class="form-control" id="national_id_number" name="national_id_number" value="">
            </div>
        </div>
        <div class="row form-group">
            <div class="col-md-4">
                <label class="control-label">{!! trans('casteller.gender') !!}</label>
                <select class="form-control" name="gender" id="gender">
                    <option value="0">{!! trans('casteller.gender_female') !!}</option>
                    <option value="1">{!! trans('casteller.gender_male') !!}</option>
                    <option value="2">{!! trans('casteller.gender_nobinary') !!}</option>
                    <option value="3">{!! trans('casteller.gender_nsnc') !!}</option>
                </select>
            </div>
            <div class="col-md-4">
                <label class="control-label">{!! trans('casteller.birthdate') !!}</label>
                <input type="text" class="form-control" name="birthdate" id="birthdate" pattern="(0[1-9]|1[0-9]|2[0-9]|3[01])/(0[1-9]|1[012])/[0-9]{4}">
            </div>
            <div class="col-md-4">
                <label class="control-label">{!! trans('casteller.subscription_date') !!}</label>
                <input type="text" class="form-control" name="subscription_date" id="subscription_date" pattern="(0[1-9]|1[0-9]|2[0-9]|3[01])/(0[1-9]|1[012])/[0-9]{4}">
            </div>
        </div>
        <div class="row form-group">
            <div class="col-md-4">
                <label class="control-label">{!! trans('casteller.family') !!}</label>
                <select class="form-control family" name="family" id="family"style="width: 100%;">
                    <option value="" selected>&nbsp;&nbsp;</option>
                    @foreach($families as $family)
                        <option value="{!! $family !!}">{!! $family !!}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-3">
                <label class="control-label">{!! trans('casteller.num_soci') !!}</label>
                <input type="number" class="form-control" name="num_soci" id="num_soci" value="">
            </div>
            <div class="col-md-5">
                <label class="control-label">{!! trans('casteller.photo') !!}</label>
                <div class="custom-file">
                    <input type="file" class="custom-file-input" name="photo" id="photo" accept="image/png, image/jpeg">
                    <label class="custom-file-label" for="photo">{!! trans('general.select_file') !!}</label>
                </div>
            </div>

        </div>
        <div class="row form-group">
            <div class="col-md-4">
                <label class="control-label">{!! trans('general.phone') !!}</label>
                <input type="text" class="form-control" id="phone" name="phone" value="">
            </div>
            <div class="col-md-4">
                <label class="control-label">{!! trans('casteller.mobile_phone') !!}</label>
                <input type="text" class="form-control" id="phone" name="mobile_phone" value="">
            </div>
            <div class="col-md-4">
                <label class="control-label">{!! trans('casteller.emergency_phone') !!}</label>
                <input type="text" class="form-control" id="emergency_phone" name="emergency_phone" value="">
            </div>
        </div>


        <div class="row form-group">
            <div class="col-md-6">
                <label class="control-label">{!! trans('general.email') !!}</label>
                <input type="email" class="form-control" id="email" name="email" value="">
            </div>
            <div class="col-md-6">
                <label class="control-label">{!! trans('general.email') !!} 2</label>
                <input type="email" class="form-control" id="email2" name="email2" value="">
            </div>

        </div>
        <div class="row form-group">
            <div class="col-md-9">
                <label class="control-label">{!! trans('casteller.address') !!}</label>
                <input type="text" class="form-control" id="address" name="address" value="">
            </div>
            <div class="col-md-3">
                <label class="control-label">{!! trans('casteller.postal_code') !!}</label>
                <input type="text" class="form-control" id="postal_code" name="postal_code" value="">
            </div>
        </div>
        <div class="row form-group">
            <div class="col-md-3">
                <label class="control-label">{!! trans('casteller.city') !!}</label>
                <input type="text" class="form-control" id="city" name="city" value="">
            </div>
            <div class="col-md-3">
                <label class="control-label">{!! trans('casteller.comarca') !!}</label>
                <input type="text" class="form-control" id="comarca" name="comarca" value="">
            </div>
            <div class="col-md-3">
                <label class="control-label">{!! trans('casteller.province') !!}</label>
                <input type="text" class="form-control" id="province" name="province" value="">
            </div>
            <div class="col-md-3">
                <label class="control-label">{!! trans('casteller.country') !!}</label>
                <input type="text" class="form-control" id="country" name="country" value="">
            </div>
        </div>
        <div class="row form-group">
            <div class="col-md-4">
                <label class="control-label">{!! trans('casteller.height') !!} (cm)</label>
                <input type="number" class="form-control" id="height" name="height" value="" min="0" max="250">
            </div>
            <div class="col-md-4">
                <label class="control-label">{!! trans('casteller.weight') !!} (Kg)</label>
                <input type="number" class="form-control" id="weight" name="weight" value="" min="0" max="200" step=".01">
            </div>
            <div class="col-md-4">
                <label class="control-label">{!! trans('casteller.shoulder_height') !!} (cm)</label>
                <input type="number" class="form-control" id="province" name="shoulder_height" value="" min="0" max="200">
            </div>
        </div>
        <div class="row form-group">
            <div class="col-md-12">
                <label class="control-label">{!! trans('casteller.comments') !!}</label>
                <textarea class="form-control" name="comments" id="comments" cols="30" rows="5"></textarea>
            </div>
        </div>

        <h3 class="block-title">{!!  trans('general.tags') !!}</h3>

        <div class="row form-group">
            @if (count($tags_groups) == 1)
                {{-- START - ONLY ONE TAG GROUP --}}
                <div class="col-md-12">

                    <select class="form-control tags_add" placeholder="{!! trans('castellers.without_tags') !!}" name="tags[]" multiple>
                        @foreach ($tags as $tag)
                            <option value="{{ $tag->value }}">{{ $tag->name }}</option>
                        @endforeach
                    </select>

                </div>
                {{-- END - ONLY ONE TAG GROUP --}}
            @else
                {{-- START - MULTIPLE TAG GROUPS --}}
                @foreach ($tags_groups as $tag_group)
                    <div class="col-md-6">
                        <label class="control-label">
                            @if ($tag_group->group == null)
                                {!! trans('casteller.no_group') !!}
                            @else
                                @if (in_array($tag_group->group,[1,2,3,4] ))
                                    {!! trans('casteller.group').' '.$tag_group->group !!}
                                @else
                                    {!! $tag_group->group !!}
                                @endif

                            @endif
                        </label>

                        <select class="form-control tags_add" style="width: 100%;" placeholder="{!! trans('casteller.without_tags') !!}" name="tags[]" multiple>
                            @foreach ($tags as $tag)
                                @if ($tag->group == $tag_group->group)
                                    <option value="{{ $tag->value }}">{{ $tag->name }}</option>
                                @endif
                            @endforeach
                        </select>

                    </div>
                @endforeach
                {{-- END - MULTIPLE TAG GROUPS --}}
            @endif
        </div>
    </div>
</div>
<div class="modal-footer">
    @if(Auth::user()->accesWriteBBDD())
        <button type="submit" form="FormAddCasteller" class="btn btn-alt-primary"><i class="fa fa-save"></i> {!! trans('general.save') !!}</button>
    @endif
    <button type="button" class="btn  btn-alt-secondary" data-dismiss="modal">{!! trans('general.close') !!}</button>
</div>
{!! Form::close() !!}
