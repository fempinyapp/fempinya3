@extends('template.main')

@section('title', trans('general.bbdd'))
@section('css_before')
    <link rel="stylesheet" href="{!! asset('js/plugins/select2/css/select2.min.css') !!}">
    <link rel="stylesheet" href="{{ asset('js/plugins/datatables/dataTables.bootstrap4.css') }}">
    <link rel="stylesheet" href="{{ asset('js/plugins/datatables/buttons-bs4/buttons.bootstrap4.css') }}">
    <link rel="stylesheet" href="{!! asset('js/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css') !!}">
@endsection

@section('content')

<div class="block">

    <div class="block-header block-header-default">

        <div class="block-title">
            <h3 class="block-title"><b>{!! trans('general.castellers') !!}</b></h3>
        </div>
        <div class="block-options">
            <button class="btn btn-primary btn-add-casteller"><i class="fa fa-user-plus"></i> {!! trans('casteller.add_casteller') !!}</button>
        </div>

    </div>

    <div class="block-content block-content-full">
        <div class="row">
            <div class="col-md-1">
                <label class="control-label" style="padding-top: 5px;">
                    {!! trans('casteller.filter') !!}
                </label>
            </div>

            <div class="col-md-2">
                <select name="filter_search_type" id="filter_search_type" class="selectize2 form-control">
                    <option value="AND">AND</option>
                    <option value="OR" selected>OR</option>
                </select>
            </div>
            <div class="col-md-5">
                <select name="tags[]" id="tags" class="selectize2 form-control" multiple>
                    <option value="all" selected>{!! trans('casteller.everybody') !!}</option>
                    <option value="" disabled>{!! trans('general.tags') !!}</option>
                    @foreach($tags as $tag)
                        <option value="{!! $tag->value !!}">{!! $tag->name !!}</option>
                    @endforeach
                    <option value="" disabled>{!! trans('casteller.positions') !!}</option>
                    @foreach($positions as $position)
                        <option value="{!! $position->value !!}">{!! $position->name !!}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-1 text-left">
                <span id="totalCastellers"></span> <i class="fa fa-users"></i>
            </div>

        </div>
        <div class="row" style="padding-top: 25px;">
            <div class="col-md-12">
                <table class="table table-hover table-bordered table-striped" style="width: 100%;" id="castellers">
                    <thead>
                    <tr>
                        <th></th>
                        <th>{!! trans('general.name_last_name') !!}</th>
                        <th>{!! trans('casteller.gender') !!}</th>
                        <th>{!! trans('casteller.birthdate') !!}</th>
                        <th>{!! trans('casteller.position') !!}</th>
                        <th>{!! trans('general.tags') !!}</th>
                        <th>#</th>
                    </tr>
                    </thead>
                </table>
            </div>

        </div>
    </div>
</div>

<!-- START - Modal Add Colla -->
<div class="modal fade" id="modalAddCasteller" tabindex="-1" role="dialog" aria-labelledby="modal-popin" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-popin" role="document">
        <div class="modal-content" id="modalAddCastellerContent">
            <!-- MODAL CONTENT -->
            @include('castellers.modals.modal-add')
        </div>
    </div>
</div>
<!-- END - Modal Add Colla -->

@endsection

@section('js')
    <script src="{!! asset('js/plugins/select2/js/select2.full.min.js') !!}"></script>
    <!-- Page JS Plugins -->
    <script src="{{ asset('js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/buttons.html5.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons-bs4/buttons.bootstrap4.min.js') }}"></script>

    <script type="text/javascript" src="{!! asset('js/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') !!}"></script>
    @if (Auth()->user()->language=='ca')
        <script type="text/javascript" src="{!! asset('js/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.ca.min.js') !!}"></script>
    @elseif(Auth()->user()->language=='es')
        <script type="text/javascript" src="{!! asset('js/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.es.min.js') !!}"></script>
    @endif

    <!-- Page JS Code -->
    <script type="text/javascript">
        $(function () {
            $.ajaxPrefilter(function(options, originalOptions, xhr) { // this will run before each request
                var token = $('meta[name="csrf-token"]').attr('content'); // or _token, whichever you are using

                if (token) {
                    return xhr.setRequestHeader('X-CSRF-TOKEN', token); // adds directly to the XmlHttpRequest Object
                }
            });
        });
    </script>
    <script>
        $(function ()
        {
            function drawCastellersTable()
            {
                var castellers = $("#castellers").DataTable({
                    "language": {!! trans('datatables.translation') !!},
                    "processing": true,
                    "serverSide": true,
                    "ajax": {
                        "url": "{{ route('castellers.list-ajax') }}",
                        "type": "POST",
                        "data": function ( d ) {
                            return $.extend( {}, d, {
                                "tags": $('#tags').val(),
                                "filter_search_type": $('#filter_search_type').val(),
                            } );
                        }
                    },
                    "ordering":'true',
                    "order": [2, 'asc'],
                    "columns": [
                        { "data": "photo", "name": "photo", "orderable": false },
                        { "data": "name", "name": "name"},
                        { "data": "gender", "name": "gender" },
                        { "data": "birthdate", "name": "birthdate" },
                        { "data": "position", "name": "position", "orderable": false },
                        { "data": "tags", "name": "tags", "orderable": false },
                        { "data": "buttons", "name": "buttons", "orderable": false }
                    ],
                    "columnDefs": [
                        { "width": "35%", "targets": [1] },
                        { "width": "8%", "targets": 6 },
                        { "width": "15%", "targets": 5 },
                        { "width": "5%", "targets": 0 }
                    ],
                    buttons: [
                        {
                            extend: 'copyHtml5',
                            exportOptions: {columns: [0, 1, 2, 3, 4, 5]}
                        },
                        {
                            extend: 'excelHtml5',
                            exportOptions: {columns: [0, 1, 2, 3, 4, 5]}
                        }
                    ],
                    "drawCallback": function( settings ) {
                        var api = this.api();

                        $('#totalCastellers').html(api.page.info().recordsTotal);
                    }
                });
            }

            drawCastellersTable();

            $('#tags, #filter_search_type').change(function(){

                $('#castellers').DataTable().destroy();
                drawCastellersTable();
            });

            $('.selectize2, .tags_add').select2({language: "ca"});

            $("#family").select2({
                tags: true,
                language: "ca"
            });

            $('#photo').on('change',function(){
                //get the file name
                var fieldVal = $(this).val();

                // Change the node's value by removing the fake path (Chrome)
                fieldVal = fieldVal.replace("C:\\fakepath\\", "");

                //replace the "Choose a file" label
                $(this).next('.custom-file-label').html(fieldVal);
            });

            $("#birthdate, #subscription_date").datepicker({
                @if (Auth()->user()->language=='ca')
                language: 'ca',
                @elseif(Auth()->user()->language=='es')
                language: 'es',
                @endif
                format: "dd/mm/yyyy"
            });

            $("#filter_search_type").select2({
                minimumResultsForSearch: -1,
                language: "ca"
            });

            $('.btn-add-casteller').on('click', function (event)
            {
                $('#modalAddCasteller').modal('show');
            });

        });
    </script>
@endsection
