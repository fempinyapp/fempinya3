@extends('template.main')

@section('title', trans('general.bbdd'))
@section('css_before')
    <link rel="stylesheet" href="{!! asset('js/plugins/select2/css/select2.min.css') !!}">
    <link rel="stylesheet" href="{!! asset('js/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css') !!}">
@endsection

@section('content')

<div class="row">
    <div class="col-lg-3" style="padding-right: 7px;">
        <div class="block">

            <div class="block-content">
                <div class="row">
                    <div class="col-md-6">
                        <img class="img-avatar img-avatar128" style="border-radius: 8px;" src="{!! $casteller->getProfileImage() !!}" alt="Avatar: {!! $casteller->name !!} {!! $casteller->last_name !!}">
                    </div>
                    <div class="col-md-6" style="padding-left: 0;">
                        <h3 class="text-primary h5 mb-5">{!! $casteller->name !!} {!! $casteller->last_name !!} {!! \App\Helpers\Humans::readCastellerColumn($casteller->id_casteller, 'gender') !!}</h3>
                        <span class="font-size-sm text-muted">@if(!empty($casteller->alias) || !is_null($casteller->alias)) {!! $casteller->alias !!} @endif</span>
                    </div>

                </div>
                <br>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="row" style="line-height: 2.6">

                            @if(!empty($casteller->birthdate) || !is_null($casteller->birthdate))
                                <div class="col-sm-2 font-w800 text-left"><i class="fa fa-birthday-cake" style="font-size: 20px;"></i></div>
                                <div class="col-sm-10 font-weight-lighter text-right">{!! \App\Helpers\Humans::readCastellerColumn($casteller->id_casteller, 'birthdate') !!} ({!! $casteller->age().' '.trans('casteller.years') !!})</div>
                            @endif

                            @if(!empty($casteller->email) || !is_null($casteller->email))
                                <div class="col-sm-2 font-w800 text-left"><i class="fa fa-envelope-o" style="font-size: 20px;"></i></div>
                                <div class="col-sm-10 font-weight-lighter text-right">{!! $casteller->email !!}</div>
                            @endif

                            @if(!empty($casteller->email2) || !is_null($casteller->email2))
                                <div class="col-sm-2 font-w800 text-left"><i class="fa fa-envelope-o" style="font-size: 20px;"></i></div>
                                <div class="col-sm-10 font-weight-lighter text-right">{!! $casteller->email2 !!}</div>
                            @endif

                            @if(!empty($casteller->mobile_phone) || !is_null($casteller->mobile_phone))
                                <div class="col-sm-2 text-secondary text-left"><i class="fa fa-mobile-phone" style="font-size: 29px;"></i></div>
                                <div class="col-sm-10 font-weight-lighter text-right">{!! $casteller->mobile_phone !!}</div>
                            @endif

                            @if(!empty($casteller->phone) || !is_null($casteller->phone))
                                <div class="col-sm-2 text-secondary text-left"><i class="fa fa-phone" style="font-size: 23px;"></i></div>
                                <div class="col-sm-10 font-weight-lighter text-right">{!! $casteller->phone !!}</div>
                            @endif

                            @if(!empty($casteller->emergency_phone) || !is_null($casteller->emergency_phone))
                                <div class="col-sm-3 text-secondary text-left text-danger"><i class="fa fa-phone" style="font-size: 23px;"></i><sup style="top: -0.9em;"><i class="fa fa-plus" style="font-size: 13px; margin-left: -4px;"></i></sup></div>
                                <div class="col-sm-9 font-weight-lighter text-right">{!! $casteller->emergency_phone !!}</div>
                            @endif

                            @if(!empty($casteller->national_id_number) || !is_null($casteller->national_id_number))
                                <div class="col-sm-2 text-secondary text-left"><i class="fa fa-id-card-o" style="font-size: 23px;"></i></div>
                                <div class="col-sm-10 font-weight-lighter text-right">{!! $casteller->national_id_number !!}</div>
                            @endif

                            @if((!empty($casteller->address) && !empty($casteller->postal_code) && !empty($casteller->city) && !empty($casteller->comarca)) || (!is_null($casteller->address) && !is_null($casteller->postal_code) && !is_null($casteller->city) && !is_null($casteller->comarca)))
                            <div class="col-sm-2 text-secondary text-left"><i class="fa fa-address-book-o" style="font-size: 22px;"></i></div>
                            <div class="col-sm-10 font-weight-lighter text-right">{!! $casteller->address !!}<br></div>
                            <div class="col-sm-12 font-weight-lighter text-right" style="line-height: 9px;">{!! $casteller->postal_code !!}, {!! $casteller->city !!}</div>
                            <div class="col-sm-12 font-weight-lighter text-right">{!! $casteller->comarca !!}</div>
                            @endif

                            @if ($casteller->position())
                            <div class="col-sm-12 text-secondary text-left">
                                <b>{!! trans('casteller.position') !!}</b>

                                <p><span class="badge badge-info">{!! $casteller->position()->name !!}</span></p>
                            </div>
                            @endif

                            @if(!empty(\App\Helpers\Humans::readCastellerColumn($casteller->id_casteller, 'tags', 'left')))
                            <div class="col-md-12 text-secondary text-left"><b>{!! trans('general.tags') !!}</b></div>
                            <div class="col-md-12">
                                {!! \App\Helpers\Humans::readCastellerColumn($casteller->id_casteller, 'tags', 'left'); !!}

                            </div>
                            @endif

                            <div class="col-md-12">
                                <hr>
                            </div>

                            <div class="col-md-12">
                                <ul class="nav nav-pills flex-column list-group" style="padding-bottom: 10px;" id="profileList">
                                    <li class="nav-item">
                                        <a class="nav-link d-flex align-items-center justify-content-between list-group-item-action bg-primary-lighter lnk-profile" href="">
                                            <span><i class="fa fa-pie-chart mr-5"></i> {!! trans('casteller.profile') !!}</span>
                                        </a>
                                    </li>

                                    @if(Auth()->user()->accesBBDD())
                                        <li class="nav-item">
                                            <a class="nav-link d-flex align-items-center justify-content-between list-group-item-action lnk-profile" data-tab="edit" href="">
                                                <span><i class="fa fa-pencil mr-5"></i> {!! trans('general.edit') !!}</span>
                                            </a>
                                        </li>
                                    @endif

                                    <li class="nav-item">
                                        <a class="nav-link d-flex align-items-center justify-content-between list-group-item-action lnk-profile" href="">
                                            <span><i class="fa fa-calendar mr-5"></i> {!! trans('general.attendance') !!}</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link d-flex align-items-center justify-content-between list-group-item-action lnk-profile" href="">
                                            <span><i class="fa fa-th mr-5"></i> {!! trans('general.boards') !!}</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-9" style="padding-left: 7px;">
        <div class="block" id="profileBlock">
            <div class="block-header block-header-default">
                <h3 class="block-title" id="profileTitle"><span><i class="fa fa-pie-chart mr-5"></i> {!! trans('casteller.profile') !!}</span></h3>
            </div>
            <div class="block-content" id="profileContent">
                <p>With header background..</p>
            </div>
        </div>
    </div>
</div>

<!-- START - MODAL DELETE -->
<div class="modal fade" id="modalDelCasteller" tabindex="-1" role="dialog" aria-labelledby="modal-popin" aria-hidden="true">
    <div class="modal-dialog modal-sm modal-dialog-popin" role="document">
        <div class="modal-content">
            {!! Form::open(array('url' => route('castellers.destroy', $casteller->id_casteller), 'method' => 'POST', 'class' => 'form-horizontal form-bordered', 'id' => 'fromDelCasteller')) !!}
            <div class="block block-themed block-transparent mb-0">
                <div class="block-header bg-primary-dark">
                    <h3 class="block-title">{!! trans('casteller.del_casteller') !!}</h3>
                    <div class="block-options">
                        <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                            <i class="si si-close"></i>
                        </button>
                    </div>
                </div>

                <div class="block-content text-center">
                    <i class="fa fa-warning" style="font-size: 46px;"></i>
                    <h3 class="semibold modal-title text-danger">{!! trans('general.caution') !!}</h3>
                    <p class="text-muted">{!! trans('casteller.del_casteller_warning') !!}</p>
                </div>
            </div>
            <div class="modal-footer">
                {!! Form::submit(trans('general.delete') , array('class' => 'btn btn-danger')) !!}
                <button type="button" class="btn  btn-alt-secondary" data-dismiss="modal">{!! trans('general.close') !!}</button>
            </div>
            {!! Form::close() !!}

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<!--/ END - MODAL DELETE -->
@endsection

@section('js')
    <script src="{!! asset('js/plugins/select2/js/select2.full.min.js') !!}"></script>

    <script type="text/javascript" src="{!! asset('js/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') !!}"></script>
    @if (Auth()->user()->language=='ca')
        <script type="text/javascript" src="{!! asset('js/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.ca.min.js') !!}"></script>
    @elseif(Auth()->user()->language=='es')
        <script type="text/javascript" src="{!! asset('js/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.es.min.js') !!}"></script>
    @endif
    <script type="text/javascript">
        $(function () {
            $.ajaxPrefilter(function(options, originalOptions, xhr) { // this will run before each request
                var token = $('meta[name="csrf-token"]').attr('content'); // or _token, whichever you are using

                if (token) {
                    return xhr.setRequestHeader('X-CSRF-TOKEN', token); // adds directly to the XmlHttpRequest Object
                }
            });
        });
    </script>
    <script>
        $(function ()
        {
            $('#profileList').on('click', '.lnk-profile', function(e)
            {
                e.preventDefault();
                $('#profileBlock').addClass('block-mode-loading');
                var title =  $(this).html();
                $('#profileList .bg-primary-lighter').removeClass('bg-primary-lighter');
                $(this).addClass('bg-primary-lighter');
                $('#profileTitle').html(title);

                var tab = $(this).data().tab;
                var url;
                if(tab=='edit')
                {
                    url = "{{ route('castellers.edit.card-edit', $casteller->id_casteller) }}";
                }
                else
                {
                    url = null;
                }

                if(url)
                {
                    $.get( url, function() {

                    }).then(function(result){
                        $('#profileBlock').removeClass('block-mode-loading');
                        $('#profileContent').html( result );
                    });
                }
            });

            $("#profileContent").on('click', '.btn-delete-casteller', function (e)
            {
                e.preventDefault();
                $('#modalDelCasteller').modal('show');
            });
        });
    </script>

@endsection
