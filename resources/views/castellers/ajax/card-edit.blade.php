{!! Form::open(array('id' => 'FormUpdateCasteller', 'url' => route('castellers.update', $casteller->id_casteller), 'method' => 'POST', 'class' => '', 'enctype' => 'multipart/form-data')) !!}
<div class="row form-group">
    <div class="col-md-4">
        <label class="control-label">{!! trans('casteller.name') !!}</label>
        <input type="text" class="form-control" id="name" name="name" value="@if(isset($casteller)){!! $casteller->name !!}@endif" required>
    </div>
    <div class="col-md-8">
        <label class="control-label">{!! trans('casteller.last_name') !!}</label>
        <input type="text" class="form-control" id="last_name" name="last_name" value="@if(isset($casteller)){!! $casteller->last_name !!}@endif" required>
    </div>
</div>
<div class="row form-group">
    <div class="col-md-4">
        <label class="control-label">{!! trans('casteller.alias') !!}</label>
        <input type="text" class="form-control" id="alias" name="alias" value="@if(isset($casteller)){!! $casteller->alias !!}@endif">
    </div>
    <div class="col-md-4">
        <label class="control-label">{!! trans('casteller.national_id_type') !!}</label>
        <select class="form-control" name="national_id_type" id="national_id_type">
            <option value="dni" @if(isset($casteller) && $casteller->national_id_type=='dni') selected @endif>{!! trans('casteller.dni') !!}</option>
            <option value="nie" @if(isset($casteller) && $casteller->national_id_type=='nie') selected @endif>{!! trans('casteller.nie') !!}</option>
            <option value="passport" @if(isset($casteller) && $casteller->national_id_type=='passport') selected @endif>{!! trans('casteller.passport') !!}</option>
        </select>
    </div>
    <div class="col-md-4">
        <label class="control-label">{!! trans('casteller.national_id_number') !!}</label>
        <input type="text" class="form-control" id="national_id_number" name="national_id_number" value="@if(isset($casteller)){!! $casteller->national_id_number !!}@endif">
    </div>
</div>
<div class="row form-group">
    <div class="col-md-4">
        <label class="control-label">{!! trans('casteller.gender') !!}</label>
        <select class="form-control" name="gender" id="gender">
            <option value="1" @if(isset($casteller) && $casteller->gender==0) selected @endif>{!! trans('casteller.gender_female') !!}</option>
            <option value="0" @if(isset($casteller) && $casteller->gender==1) selected @endif>{!! trans('casteller.gender_male') !!}</option>
            <option value="" @if(isset($casteller) && $casteller->gender==2) selected @endif>{!! trans('casteller.gender_nobinary') !!}</option>
            <option value="" @if(isset($casteller) && $casteller->gender==3) selected @endif>{!! trans('casteller.gender_nsnc') !!}</option>
        </select>
    </div>
    <div class="col-md-4">
        <label class="control-label">{!! trans('casteller.birthdate') !!}</label>
        <input type="text" class="form-control" name="birthdate" id="birthdate" value="@if(isset($casteller)){!! \App\Helpers\Humans::readCastellerColumn($casteller->id_casteller, 'birthdate') !!}@endif" pattern="(0[1-9]|1[0-9]|2[0-9]|3[01])/(0[1-9]|1[012])/[0-9]{4}">
    </div>
    <div class="col-md-4">
        <label class="control-label">{!! trans('casteller.subscription_date') !!}</label>
        <input type="text" class="form-control" name="subscription_date" id="subscription_date" value="@if(isset($casteller)){!! \App\Helpers\Humans::readCastellerColumn($casteller->id_casteller, 'subscription_date') !!}@endif" pattern="(0[1-9]|1[0-9]|2[0-9]|3[01])/(0[1-9]|1[012])/[0-9]{4}">
    </div>
</div>
<div class="row form-group">
    <div class="col-md-4">
        <label class="control-label">{!! trans('casteller.family') !!}</label>
        <select class="form-control family" name="family" id="family">
            @if(!empty($casteller->family) || !is_null($casteller->family))
                <option value="{!! $casteller->family !!}">{!! $casteller->family !!}</option>
            @else
                <option value="" selected>&nbsp;&nbsp;&nbsp;</option>
            @endif
            <option value="">&nbsp;&nbsp;&nbsp;</option>
            @foreach($families as $family)
                <option value="{!! $family !!}">{!! $family !!}</option>
            @endforeach
        </select>
    </div>
    <div class="col-md-3">
        <label class="control-label">{!! trans('casteller.num_soci') !!}</label>
        <input type="number" class="form-control" name="num_soci" id="num_soci" value="@if(isset($casteller)){!! $casteller->num_soci !!}@endif">
    </div>
    <div class="col-md-5">
        <label class="control-label">{!! trans('casteller.photo') !!}</label>
        <div class="custom-file">
            <input type="file" class="custom-file-input" name="photo" id="photo" accept="image/png, image/jpeg">
            <label class="custom-file-label" for="photo">{!! trans('general.select_file') !!}</label>
        </div>
    </div>

</div>
<div class="row form-group">
    <div class="col-md-4">
        <label class="control-label">{!! trans('general.phone') !!}</label>
        <input type="text" class="form-control" id="phone" name="phone" value="@if(isset($casteller)){!! $casteller->phone !!}@endif">
    </div>
    <div class="col-md-4">
        <label class="control-label">{!! trans('casteller.mobile_phone') !!}</label>
        <input type="text" class="form-control" id="phone" name="mobile_phone" value="@if(isset($casteller)){!! $casteller->mobile_phone !!}@endif">
    </div>
    <div class="col-md-4">
        <label class="control-label">{!! trans('casteller.emergency_phone') !!}</label>
        <input type="text" class="form-control" id="emergency_phone" name="emergency_phone" value="@if(isset($casteller)){!! $casteller->emergency_phone !!}@endif">
    </div>
</div>


<div class="row form-group">
    <div class="col-md-6">
        <label class="control-label">{!! trans('general.email') !!}</label>
        <input type="email" class="form-control" id="email" name="email" value="@if(isset($casteller)){!! $casteller->email !!}@endif">
    </div>
    <div class="col-md-6">
        <label class="control-label">{!! trans('general.email') !!} 2</label>
        <input type="email" class="form-control" id="email2" name="email2" value="@if(isset($casteller)){!! $casteller->email2 !!}@endif">
    </div>

</div>
<div class="row form-group">
    <div class="col-md-9">
        <label class="control-label">{!! trans('casteller.address') !!}</label>
        <input type="text" class="form-control" id="address" name="address" value="@if(isset($casteller)){!! $casteller->address !!}@endif">
    </div>
    <div class="col-md-3">
        <label class="control-label">{!! trans('casteller.postal_code') !!}</label>
        <input type="text" class="form-control" id="postal_code" name="postal_code" value="@if(isset($casteller)){!! $casteller->postal_code !!}@endif">
    </div>
</div>
<div class="row form-group">
    <div class="col-md-3">
        <label class="control-label">{!! trans('casteller.city') !!}</label>
        <input type="text" class="form-control" id="city" name="city" value="@if(isset($casteller)){!! $casteller->city !!}@endif">
    </div>
    <div class="col-md-3">
        <label class="control-label">{!! trans('casteller.comarca') !!}</label>
        <input type="text" class="form-control" id="comarca" name="comarca" value="@if(isset($casteller)){!! $casteller->comarca !!}@endif">
    </div>
    <div class="col-md-3">
        <label class="control-label">{!! trans('casteller.province') !!}</label>
        <input type="text" class="form-control" id="province" name="province" value="@if(isset($casteller)){!! $casteller->province !!}@endif">
    </div>
    <div class="col-md-3">
        <label class="control-label">{!! trans('casteller.country') !!}</label>
        <input type="text" class="form-control" id="country" name="country" value="@if(isset($casteller)){!! $casteller->country !!}@endif">
    </div>
</div>
<div class="row form-group">
    <div class="col-md-4">
        <label class="control-label">{!! trans('casteller.height') !!} (cm)</label>
        <input type="number" class="form-control" id="height" name="height" value="@if(isset($casteller)){!! $casteller->height !!}@endif" min="0" max="250">
    </div>
    <div class="col-md-4">
        <label class="control-label">{!! trans('casteller.weight') !!} (Kg)</label>
        <input type="number" class="form-control" id="weight" name="weight" value="@if(isset($casteller)){!! $casteller->weight !!}@endif" min="0" max="200" step=".01">
    </div>
    <div class="col-md-4">
        <label class="control-label">{!! trans('casteller.shoulder_height') !!} (cm)</label>
        <input type="number" class="form-control" id="province" name="shoulder_height" value="@if(isset($casteller)){!! $casteller->shoulder_height !!}@endif" min="0" max="200">
    </div>
</div>
<div class="row form-group">
    <div class="col-md-12">
        <label class="control-label">{!! trans('casteller.comments') !!}</label>
        <textarea class="form-control" name="comments" id="comments" cols="30" rows="5">@if(isset($casteller)){!! $casteller->comments !!}@endif</textarea>
    </div>
</div>
<hr>
<div class="row form-group">
    <div class="col-md-12">
        <label class="control-label">{!! trans('casteller.position') !!}</label>

        <select class="form-control" name="position" id="position">
            <option value="">{!! trans('casteller.select_position') !!}</option>
            @foreach($positions as $position)
                @if(!is_null($casteller->position()) && $casteller->position()->value===$position->value)
                    <option value="{!! $position->value !!}" selected>{!! $position->name !!}</option>
                @else
                    <option value="{!! $position->value !!}">{!! $position->name !!}</option>
                @endif

            @endforeach
        </select>
    </div>
</div>
<h3 class="block-title">{!!  trans('general.tags') !!}</h3>

<div class="row form-group">
    @if (count($tags_groups) == 1)
        {{-- START - ONLY ONE TAG GROUP --}}
        <div class="col-md-12">

            <select class="tags form-control" placeholder="{!! trans('castellers.without_tags') !!}" name="tags[]" multiple>
                @foreach ($tags as $tag)
                    @if (in_array($tag->value, $casteller->tagsArray('VALUE')))
                        <option value="{{ $tag->value }}" selected>{{ $tag->name }}</option>
                    @else
                        <option value="{{ $tag->value }}">{{ $tag->name }}</option>
                    @endif
                @endforeach
            </select>

        </div>
        {{-- END - ONLY ONE TAG GROUP --}}
    @else
        {{-- START - MULTIPLE TAG GROUPS --}}
        @foreach ($tags_groups as $tag_group)
            <div class="col-md-6">
                <label class="control-label">
                    @if ($tag_group->group == null)
                        {!! trans('casteller.no_group') !!}
                    @else
                        @if (in_array($tag_group->group,[1,2] ))
                            {!! trans('casteller.group').' '.$tag_group->group !!}
                        @else
                            {!! $tag_group->group !!}
                        @endif

                    @endif
                </label>

                <select class="tags form-control" placeholder="{!! trans('casteller.without_tags') !!}" name="tags[]" multiple>
                    @foreach ($tags as $tag)
                        @if ($tag->group == $tag_group->group)
                            @if (in_array($tag->value, $casteller->tagsArray('VALUE')))
                                <option value="{{ $tag->value }}" selected>{{ $tag->name }}</option>
                            @else
                                <option value="{{ $tag->value }}">{{ $tag->name }}</option>
                            @endif
                        @endif
                    @endforeach
                </select>

            </div>
        @endforeach
        {{-- END - MULTIPLE TAG GROUPS --}}
    @endif
</div>

<div class="row form-group">
    <div class="col-md-6 text-left">
        <button class="btn btn-danger btn-delete-casteller"><i class="fa fa-trash-o"></i> {!! trans('general.delete') !!}</button>
    </div>
    <div class="col-md-6 text-right">
        <button form="FormUpdateCasteller" class="btn btn-primary"><i class="fa fa-save"></i> {!! trans('general.save') !!}</button>
    </div>
</div>
{!! Form::close() !!}

<script>
    $(function()
    {
        $('#profileContent .tags').select2({language: "ca"});


        $("#profileContent .family").select2({
            tags: true,
            language: "ca"
        });

        $('#photo').on('change',function(){
            //get the file name
            var fieldVal = $(this).val();

            // Change the node's value by removing the fake path (Chrome)
            fieldVal = fieldVal.replace("C:\\fakepath\\", "");

            //replace the "Choose a file" label
            $(this).next('.custom-file-label').html(fieldVal);
        });

        $("#birthdate, #subscription_date").datepicker({
            @if (Auth()->user()->language=='ca')
            language: 'ca',
            @elseif(Auth()->user()->language=='es')
            language: 'es',
            @endif
            format: "dd/mm/yyyy"
        });

    });
</script>
