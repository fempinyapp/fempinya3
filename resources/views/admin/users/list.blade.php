@extends('template.main')

@section('title', 'Administració - Llistat de colles')
@section('css_before')
    <!-- Page JS Plugins CSS -->
    <link rel="stylesheet" href="{{ asset('js/plugins/datatables/dataTables.bootstrap4.css') }}">
@endsection
@section('css_after')
@endsection

@section('content')

<div class="block">
    <div class="block-header block-header-default">
        <h3 class="block-title"><b>{!! trans('general.users') !!}</b></h3>
    </div>
    <div class="block-content block-content-full">
        <div class="row">

            <div class="col-md-12">
                <table class="table table-bordered table-striped" id="users">
                    <thead>
                    <tr>
                        <th>{!! trans('general.name') !!}</th>
                        <th>{!! trans('general.email') !!}</th>
                        <th>{!! trans('user.permissions') !!}</th>
                        <th>{!! trans('admin.last_login') !!}</th>
                        <th width="5%">#</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($users as $user)
                        <tr>
                            <td>{!! $user->name !!}</td>
                            <td>{!! $user->email !!}</td>
                            <td>
                                @if($user->role==='ADMIN')
                                    <span class="badge badge-danger">{!! trans('user.permission_admin') !!}</span>
                                @else
                                    @foreach($user->getPermissions() as $key => $permission)
                                        <span class="badge badge-success" style="margin-bottom: 4px;">{!! trans('user.permission_'.strtolower($key)) !!}
                                            @if(is_array($permission))
                                                @foreach($permission as $type)
                                                    <span class="badge badge-pill badge-secondary">{!! trans('user.permission_'.strtolower($type)) !!}</span>
                                                @endforeach
                                            @else
                                                <span class="badge badge-pill badge-secondary">{!! trans('user.permission_'.strtolower($permission)) !!}</span>
                                            @endif
                                    </span>
                                    @endforeach
                                @endif

                            </td>
                            <td>
                                @if(is_null($user->last_access_at))
                                    {!! trans('user.not_logging') !!}
                                @else
                                    {!! \Carbon\Carbon::parse($user->last_access_at)->format('d/m/Y h:m:s') !!}
                                @endif
                            </td>

                            <td>
                                <button class="btn btn-sm btn-warning btn-update-user" data-id_user="{!! $user->id_user !!}"><i class="fa fa-pencil"></i></button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- START - Modal Update User -->
<div class="modal fade" id="modalUpdateUser" tabindex="-1" role="dialog" aria-labelledby="modal-popin" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-popin" role="document">
        <div class="modal-content" id="modalUpdateUserContent">
            <!-- MODAL CONTENT -->
        </div>
    </div>
</div>
<!-- END - Modal Update User -->

@endsection


@section('js')
    <!-- Page JS Plugins -->
    <script src="{{ asset('js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>

    <!-- Page JS Code -->
    <script src="{{ asset('js/pages/tables_datatables.js') }}"></script>

    <script>
        $(function ()
        {
            $("#users").DataTable({"language": {!! trans('datatables.translation') !!}});

            $('.btn-add-colla').on('click', function (event)
            {
                $('#modalAddColla').modal('show');

                $('#modalAddCollaContent').html('<div class="col-md-12 text-center"><div class="spinner-border" role="status"><span class="sr-only">Loading...</span></div></div>');

                $.get( "{{ route('admin.colles.add-colla-modal') }}", function( data ) {
                    $('#modalAddCollaContent').html( data );
                });
            });

            $("#users").on('click', '.btn-update-user', function (event)
            {
                var id_user = $(this).data().id_user;

                $('#modalUpdateUser').modal('show');

                $('#modalUpdateUserContent').html('<div class="col-md-12 text-center"><div class="spinner-border" role="status"><span class="sr-only">Loading...</span></div></div>');

                var url = "{{ route('profile.colla.edit-user-modal', ':id_user') }}";
                url = url.replace(':id_user',id_user);

                $.get( url, function( data ) {
                    $('#modalUpdateUserContent').html( data );
                });
            });

        });
    </script>
@endsection
