@extends('template.main')

@section('title', trans('general.events'))
@section('css_before')
    <link rel="stylesheet" href="{!! asset('js/plugins/select2/css/select2.min.css') !!}">
    <link rel="stylesheet" href="{{ asset('js/plugins/datatables/dataTables.bootstrap4.css') }}">
    <link rel="stylesheet" href="{{ asset('js/plugins/datatables/buttons-bs4/buttons.bootstrap4.css') }}">
    <link rel="stylesheet" href="{!! asset('js/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css') !!}">
@endsection

@section('content')

<div class="block">

    <div class="block-header block-header-default">
        <div class="block-title">
            <h3 class="block-title"><b>{!! trans('general.events') !!}</b></h3>
        </div>
        <div class="block-options">

            <a class="btn btn-outline-primary" href="{!! route('events.answers') !!}"><i class="fa fa-calendar-check-o"></i> {!! trans('attendance.attendance_answers') !!}</a>

            <a class="btn btn-outline-primary" href="{!! route('events.tags') !!}"><i class="fa fa-tags"></i> {!! trans('general.tags') !!}</a>

            <div class="btn-group" role="group">
                <button type="button" class="btn btn-primary dropdown-toggle" id="btnGroupDrop1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-calendar-plus-o"></i> {!! trans('event.add_events') !!}</button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                    <a class="dropdown-item" href="{!! route('events.create') !!}">
                        {!! trans('event.add_event') !!}
                    </a>
                    <a class="dropdown-item" href="{!! route('events.create-group') !!}">
                        {!! trans('event.add_multiple_events') !!}
                    </a>
                </div>
            </div>

        </div>
    </div>

    <div class="block-content block-content-full">
        <div class="row">
            <div class="col-md-2">
                <label class="control-label" style="padding-top: 5px;">
                    {!! trans('event.events_type') !!}
                </label>
            </div>

            <div class="col-md-2">
                <select name="filter_search_type" id="filter_search_type" class="selectize2 form-control">
                    <option value="AND">AND</option>
                    <option value="OR" selected>OR</option>
                </select>
            </div>
            <div class="col-md-5">
                <select name="tags[]" id="tags" class="selectize2 form-control" multiple>
                    <option value="all" selected>{!! trans('event.all') !!}</option>
                    @foreach($tags as $tag)
                        <option value="{!! $tag->value !!}">{!! $tag->name !!}</option>
                    @endforeach
                </select>
            </div>

        </div>
    </div>
</div>

<div class="block">
    <div class="block-header ">
        <div class="block-title">
            <h3 class="block-title">{!! trans('event.upcoming_events') !!}</h3>
        </div>
    </div>
    <div class="block-content">
        <div class="row">
            <div class="col-md-12">
                <table class="table table-hover table-bordered table-striped" style="width: 100%;" id="events_upcoming">
                    <thead>
                    <tr>
                        <th>{!! trans('general.name') !!}</th>
                        <th>{!! trans('general.tags') !!}</th>
                        <th>{!! trans('general.date') !!}</th>
                        <th>#</th>
                    </tr>
                    </thead>
                </table>
            </div>

        </div>
    </div>
</div>

<div class="block">
    <div class="block-header ">
        <div class="block-title">
            <h3 class="block-title">{!! trans('event.past_events') !!}</h3>
        </div>
    </div>
    <div class="block-content">
        <div class="row">
            <div class="col-sm-12">
                <table class="table table-hover table-bordered table-striped" style="width: 100%;" id="events_past">
                    <thead>
                    <tr>
                        <th>{!! trans('general.name') !!}</th>
                        <th>{!! trans('general.tags') !!}</th>
                        <th>{!! trans('general.date') !!}</th>
                        <th>#</th>
                    </tr>
                    </thead>
                </table>
            </div>

        </div>
    </div>
</div>
@endsection

@section('js')
    <script src="{!! asset('js/plugins/select2/js/select2.full.min.js') !!}"></script>
    <!-- Page JS Plugins -->
    <script src="{{ asset('js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/buttons.html5.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons-bs4/buttons.bootstrap4.min.js') }}"></script>

    <script type="text/javascript" src="{!! asset('js/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') !!}"></script>
    @if (Auth()->user()->language=='ca')
        <script type="text/javascript" src="{!! asset('js/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.ca.min.js') !!}"></script>
    @elseif(Auth()->user()->language=='es')
        <script type="text/javascript" src="{!! asset('js/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.es.min.js') !!}"></script>
    @endif

    <!-- Page JS Code -->
    <script type="text/javascript">
        $(function () {
            $.ajaxPrefilter(function(options, originalOptions, xhr) { // this will run before each request
                var token = $('meta[name="csrf-token"]').attr('content'); // or _token, whichever you are using

                if (token) {
                    return xhr.setRequestHeader('X-CSRF-TOKEN', token); // adds directly to the XmlHttpRequest Object
                }
            });
        });
    </script>
    <script>
        $(function ()
        {
            function drawEventsTable()
            {
                var events_upcoming = $("#events_upcoming").DataTable({
                    "language": {!! trans('datatables.translation') !!},
                    "processing": true,
                    "serverSide": true,
                    "ajax": {
                        "url": "{{ route('events.list-ajax', 'upcoming') }}",
                        "type": "POST",
                        "data": function ( d ) {
                            return $.extend( {}, d, {
                                "tags": $('#tags').val(),
                                "filter_search_type": $('#filter_search_type').val(),
                            } );
                        }
                    },
                    "ordering":'true',
                    "order": [2, 'asc'],
                    "columns": [
                        { "data": "name", "name": "name"},
                        { "data": "tags", "name": "tags", "orderable": false },
                        { "data": "start_date", "name": "start_date" },
                        { "data": "buttons", "name": "buttons", "orderable": false }
                    ],
                    "columnDefs": [
                        { "width": "40%", "targets": 0 },
                        { "width": "20%", "targets": 1 },
                        { "width": "27%", "targets": 2 },
                        { "width": "13%", "targets": 3 },
                    ],
                    buttons: [
                        {
                            extend: 'copyHtml5',
                            exportOptions: {columns: [0, 1, 2, 3, 4]}
                        },
                        {
                            extend: 'excelHtml5',
                            exportOptions: {columns: [0, 1, 2, 3, 4]}
                        }
                    ]
                });

                var events_past = $("#events_past").DataTable({
                    "language": {!! trans('datatables.translation') !!},
                    "processing": true,
                    "serverSide": true,
                    "ajax": {
                        "url": "{{ route('events.list-ajax', 'past') }}",
                        "type": "POST",
                        "data": function ( d ) {
                            return $.extend( {}, d, {
                                "tags": $('#tags').val(),
                                "filter_search_type": $('#filter_search_type').val(),
                            } );
                        }
                    },
                    "ordering":'true',
                    "order": [2, 'asc'],
                    "columns": [
                        { "data": "name", "name": "name"},
                        { "data": "tags", "name": "tags", "orderable": false },
                        { "data": "start_date", "name": "start_date" },
                        { "data": "buttons", "name": "buttons", "orderable": false }
                    ],
                    "columnDefs": [
                        { "width": "40%", "targets": 0 },
                        { "width": "20%", "targets": 1 },
                        { "width": "27%", "targets": 2 },
                        { "width": "13%", "targets": 3 },
                    ],
                    buttons: [
                        {
                            extend: 'copyHtml5',
                            exportOptions: {columns: [0, 1, 2, 3, 4, 5]}
                        },
                        {
                            extend: 'excelHtml5',
                            exportOptions: {columns: [0, 1, 2, 3, 4, 5]}
                        }
                    ]
                });
            }

            drawEventsTable();

            $('#tags, #filter_search_type').change(function(){

                $('#events_upcoming').DataTable().destroy();
                $('#events_past').DataTable().destroy();
                drawEventsTable();
            });

            $('.selectize2').select2({language: "ca"});

            $("#filter_search_type").select2({
                minimumResultsForSearch: -1,
                language: "ca"
            });

        });
    </script>
@endsection
