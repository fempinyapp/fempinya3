@extends('template.main')

@section('title', trans('general.bbdd'))
@section('css_before')
    <link rel="stylesheet" href="{!! asset('js/plugins/select2/css/select2.min.css') !!}">
    <link rel="stylesheet" href="{{ asset('js/plugins/datatables/dataTables.bootstrap4.css') }}">
    <link rel="stylesheet" href="{{ asset('js/plugins/datatables/buttons-bs4/buttons.bootstrap4.css') }}">
    <link rel="stylesheet" href="{!! asset('js/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css') !!}">
    <style type="text/css">
        .companions {
            width: 65px !important;
        }
    </style>
@endsection

@section('content')

<div class="block">

    <div class="block-header block-header-default">

        <div class="block-title">
            <h3 class="block-title"><b>{!! trans('attendance.attendance') !!}:</b> {!! $event->name !!} <span class="text-muted">({!! \App\Helpers\Humans::readEventColumn($event->id_event, 'start_date'); !!})</span></h3>
        </div>
        <div class="block-options">
            <a class="btn btn-outline-primary" href="{!! route('event.attendance', $event->id_event) !!}"><i class="fa fa-list-ul"></i></a>
        </div>

    </div>

    <div class="block-content block-content-full">
        <div class="row text-right">
            <div class="offset-9 col-3">
                <div class="row">
                    <div class="col-3 text-success h4"><i class="fa fa-check"></i></div>
                    <div class="col-3 text-danger h4"><i class="fa fa-close"></i></div>
                    <div class="col-3 text-warning h4"><i class="fa fa-question"></i></div>
                    <div class="col-3 text-secondary h4"><i class="si si-users"></i></div>
                    <div class="col-3 text-success h4" id="YES">{!! $event->countAttenders()['YES'] !!}</div>
                    <div class="col-3 text-danger h4" id="NO">{!! $event->countAttenders()['NO'] !!}</div>
                    <div class="col-3 text-warning h4" id="UNKNOWN">{!! $event->countAttenders()['UNKNOWN'] !!}</div>
                    <div class="col-3 text-secondary h4">{!! $event->countAttenders()['companions'] !!}</div>
                </div>
            </div>
        </div>

        @foreach($positions as $position)
            <?php $i = 1; ?>
            <div class="row" style="padding-top: 25px;">

                @foreach($castellers[$position->value] as $casteller)

                    @if(($i==1))
                        <div class="col-md-3">
                            <table class="table table-hover table-bordered table-striped table-sm">
                                <thead>
                                <tr>
                                    <th colspan="3" class="text-center">{!! $position->name !!}</th>
                                </tr>
                                <tr>
                                    <th>{!! trans('general.name') !!}</th>
                                    <th><i class="fa fa-check" style="font-size: 22px;"></i></th>
                                    <th><img src="{!! asset('media/img/check-double-solid.svg') !!}" style="width: 20px;" alt="double check"></th>

                                </tr>
                                </thead>
                                <tbody>
                    @endif

                    <tr>
                        <td>{!! $casteller->name.' '.$casteller->last_name !!}</td>
                        <td>{!! $casteller->status !!}</td>
                        <td>{!! $casteller->status_verified !!}</td>
                    </tr>

                    @if(($i == round(count($castellers[$position->value]) / 4)) || ($i == round(count($castellers[$position->value]) / 4)*2) || ($i == round(count($castellers[$position->value]) / 4)*3))
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-3">
                            <table class="table table-hover table-bordered table-striped table-sm">
                                <thead>
                                <tr>
                                    <th colspan="3" class="text-center">{!! $position->name !!}</th>
                                </tr>
                                <tr>
                                    <th>{!! trans('general.name') !!}</th>
                                    <th><i class="fa fa-check" style="font-size: 22px;"></i></th>
                                    <th><img src="{!! asset('media/img/check-double-solid.svg') !!}" style="width: 20px;" alt="double check"></th>
                                </tr>
                                </thead>
                                <tbody>
                    @endif
                    <?php $i++; ?>
                @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

        @endforeach

    </div>
</div>

@endsection

@section('js')
    <script src="{!! asset('js/plugins/select2/js/select2.full.min.js') !!}"></script>
    <!-- Page JS Plugins -->
    <script src="{{ asset('js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/buttons.html5.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons-bs4/buttons.bootstrap4.min.js') }}"></script>

    <script type="text/javascript" src="{!! asset('js/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') !!}"></script>
    @if (Auth()->user()->language=='ca')
        <script type="text/javascript" src="{!! asset('js/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.ca.min.js') !!}"></script>
    @elseif(Auth()->user()->language=='es')
        <script type="text/javascript" src="{!! asset('js/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.es.min.js') !!}"></script>
    @endif

    <!-- Page JS Code -->
    <script type="text/javascript">
        $(function () {
            $.ajaxPrefilter(function(options, originalOptions, xhr) { // this will run before each request
                var token = $('meta[name="csrf-token"]').attr('content'); // or _token, whichever you are using

                if (token) {
                    return xhr.setRequestHeader('X-CSRF-TOKEN', token); // adds directly to the XmlHttpRequest Object
                }
            });
        });
    </script>
    <script>
        $(function ()
        {



            @if(Auth::user()->accesWriteEvents())

                function setStatus(id_casteller, status, companions, answers)
                {
                    $.post( "{{ route('event.attendance.set-status') }}",
                        { 'id_casteller': id_casteller,
                            'id_event' : {!! $event->id_event !!},
                            'status': status });
                }

                function setStatusVerified(id_casteller, status)
                {
                    $.post( "{{ route('event.attendance.set-status-verified') }}",
                        { 'id_casteller': id_casteller,
                            'id_event' : {!! $event->id_event !!},
                            'status': status });
                }

                $('.btn-status').on('click', function ()
                {
                    var status;
                    var id_casteller = $(this).data().id_casteller;

                    var YES = parseInt($('#YES').html());
                    var NO = parseInt($('#NO').html());
                    var UNKNOWN = parseInt($('#UNKNOWN').html());

                    if($(this).hasClass('btn-success'))
                    {
                        status = 'NO';
                        $(this).removeClass( "btn-success" );
                        $(this).addClass( "btn-danger" );
                        $(this).html('<i class="fa fa-close"></i>');
                        YES--;
                        NO++;
                        $('#YES').html(YES);
                        $('#NO').html(NO);
                    }
                    else if($(this).hasClass('btn-danger'))
                    {
                        status = 'UNKNOWN';
                        $(this).removeClass( "btn-danger" );
                        $(this).addClass( "btn-outline-warning" );
                        $(this).html('<i class="fa fa-question"></i>');
                        NO--;
                        UNKNOWN++;
                        $('#UNKNOWN').html(UNKNOWN);
                        $('#NO').html(NO);
                    }
                    else if($(this).hasClass('btn-secondary'))
                    {
                        status = 'YES';
                        $(this).removeClass( "btn-secondary" );
                        $(this).addClass( "btn-success" );
                        $(this).html('<i class="fa fa-check"></i>');
                        YES++;
                        UNKNOWN--;
                        $('#UNKNOWN').html(UNKNOWN);
                        $('#YES').html(YES);
                    }

                    setStatus(id_casteller, status);
                });

                $('.btn-status-verified').on('click', function ()
                {
                    var status;
                    var id_casteller = $(this).data().id_casteller;

                    if($(this).hasClass('btn-success'))
                    {
                        status = 'NO';
                        $(this).removeClass( "btn-success" );
                        $(this).addClass( "btn-danger" );
                        $(this).html('<i class="fa fa-close"></i>');
                    }
                    else if($(this).hasClass('btn-danger'))
                    {
                        status = 'UNKNOWN';
                        $(this).removeClass( "btn-danger" );
                        $(this).addClass( "btn-secondary" );
                        $(this).html('<i class="fa fa-question"></i>');
                    }
                    else if($(this).hasClass('btn-secondary'))
                    {
                        status = 'YES';
                        $(this).removeClass( "btn-secondary" );
                        $(this).addClass( "btn-success" );
                        $(this).html('<i class="fa fa-check"></i>');
                    }

                    setStatusVerified(id_casteller, status);
                });


            @endif


        });
    </script>
@endsection
