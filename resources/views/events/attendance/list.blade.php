@extends('template.main')

@section('title', trans('general.bbdd'))
@section('css_before')
    <link rel="stylesheet" href="{!! asset('js/plugins/select2/css/select2.min.css') !!}">
    <link rel="stylesheet" href="{{ asset('js/plugins/datatables/dataTables.bootstrap4.css') }}">
    <link rel="stylesheet" href="{{ asset('js/plugins/datatables/buttons-bs4/buttons.bootstrap4.css') }}">
    <link rel="stylesheet" href="{!! asset('js/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css') !!}">
    <style type="text/css">
        .companions {
            width: 65px !important;
        }
    </style>
@endsection

@section('content')

<div class="block">

    <div class="block-header block-header-default">

        <div class="block-title">
            <h3 class="block-title"><b>{!! trans('attendance.attendance') !!}:</b> {!! $event->name !!} <span class="text-muted">({!! \App\Helpers\Humans::readEventColumn($event->id_event, 'start_date'); !!})</span></h3>
        </div>
        <div class="block-options">
            <a class="btn btn-outline-primary" href="{!! route('event.attendance.list-block', $event->id_event) !!}"><!--i class="fa fa-th"></i--><i class="fa fa-columns"></i></a>
        </div>

    </div>

    <div class="block-content block-content-full">
        <div class="row">
            <div class="col-md-2">
                <label class="control-label" style="padding-top: 5px;">
                    {!! trans('casteller.filter_castellers') !!}
                </label>
            </div>
        </div>
        <div class="row">
            <div class="col-md-2">
                <select name="filter_search_type" id="filter_search_type" class="selectize2 form-control">
                    <option value="AND">AND</option>
                    <option value="OR" selected>OR</option>
                </select>
            </div>
            <div class="col-md-5">
                <select name="tags[]" id="tags" class="selectize2 form-control" style="width: 100%;" multiple>
                    <option value="all" selected>{!! trans('casteller.everybody') !!}</option>
                    @foreach($tags as $tag)
                        <option value="{!! $tag->value !!}">{!! $tag->name !!}</option>
                    @endforeach
                    <option value="" disabled>{!! trans('casteller.positions') !!}</option>
                    @foreach($positions as $position)
                        <option value="{!! $position->value !!}">{!! $position->name !!}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-1 text-left">
                <span id="totalCastellers"></span> <i class="fa fa-users"></i>
            </div>
            <div class="offset-md-2 col-md-2">
                <div class="row">
                    <div class="col-3 text-success h4"><i class="fa fa-check"></i></div>
                    <div class="col-3 text-danger h4"><i class="fa fa-close"></i></div>
                    <div class="col-3 text-warning h4"><i class="fa fa-question"></i></div>
                    <div class="col-3 text-secondary h4"><i class="si si-users"></i></div>
                    <div class="col-3 text-success h4" id="YES">{!! $event->countAttenders()['YES'] !!}</div>
                    <div class="col-3 text-danger h4" id="NO">{!! $event->countAttenders()['NO'] !!}</div>
                    <div class="col-3 text-warning h4" id="UNKNOWN">{!! $event->countAttenders()['UNKNOWN'] !!}</div>
                    <div class="col-3 text-secondary h4" id="companions">{!! $event->countAttenders()['companions'] !!}</div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-2">
                <label class="control-label" style="padding-top: 5px;">
                    {!! trans('attendance.filter_attendance_status') !!}
                </label>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <select name="status[]" id="status" class="selectize2 form-control" multiple>
                    <option value="YES">{!! trans('general.yes') !!}</option>
                    <option value="NO">{!! trans('general.no') !!}</option>
                </select>
            </div>
        </div>
        <div class="row" style="padding-top: 25px;">
            <div class="col-md-12">
                <table class="table table-hover table-bordered table-striped" style="width: 100%;" id="attenders">
                    <thead>
                    <tr>
                        <th>{!! trans('general.name') !!}</th>
                        <th><i class="fa fa-check" style="font-size: 22px;"></i></th>
                        <th><img src="{!! asset('media/img/check-double-solid.svg') !!}" style="width: 20px;" alt="double check"></th>
                        <th>{!! trans('attendance.attendance_answers') !!}</th>
                        <th>{!! trans('attendance.companions') !!}</th>
                        <th>{!! trans('general.last_update') !!}</th>
                    </tr>
                    </thead>
                </table>
            </div>

        </div>
    </div>
</div>

@endsection

@section('js')
    <script src="{!! asset('js/plugins/select2/js/select2.full.min.js') !!}"></script>
    <!-- Page JS Plugins -->
    <script src="{{ asset('js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/buttons.html5.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons-bs4/buttons.bootstrap4.min.js') }}"></script>

    <script type="text/javascript" src="{!! asset('js/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') !!}"></script>
    @if (Auth()->user()->language=='ca')
        <script type="text/javascript" src="{!! asset('js/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.ca.min.js') !!}"></script>
    @elseif(Auth()->user()->language=='es')
        <script type="text/javascript" src="{!! asset('js/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.es.min.js') !!}"></script>
    @endif

    <!-- Page JS Code -->
    <script type="text/javascript">
        $(function () {
            $.ajaxPrefilter(function(options, originalOptions, xhr) { // this will run before each request
                var token = $('meta[name="csrf-token"]').attr('content'); // or _token, whichever you are using

                if (token) {
                    return xhr.setRequestHeader('X-CSRF-TOKEN', token); // adds directly to the XmlHttpRequest Object
                }
            });
        });
    </script>
    <script>
        $(function ()
        {
            var attenders;
            var personalize_answers = {!! count($event->attendanceAnswers()) !!};
            console.log(personalize_answers);

            function drawAttendersTable()
            {
                attenders = $("#attenders").DataTable({
                    "language": {!! trans('datatables.translation') !!},
                    "processing": true,
                    "serverSide": true,
                    "pageLength": 50,
                    "ajax": {
                        "url": "{{ route('event.attendance.list-attenders', $event->id_event) }}",
                        "type": "POST",
                        "data": function ( d ) {
                            return $.extend( {}, d, {
                                "tags": $('#tags').val(),
                                "filter_search_type": $('#filter_search_type').val(),
                                "status": $('#status').val(),
                            } );
                        }
                    },
                    "ordering":'true',
                    "order": [0, 'asc'],
                    "columns": [
                        { "data": "name", "name": "name"},
                        { "data": "status", "name": "status", "orderable": false},
                        { "data": "status_verified", "name": "status_verified", "orderable": false},
                        { "data": "attendance_answers", "name": "attendance_answers", "orderable": false},
                        { "data": "companions", "name": "companions", "orderable": false},
                        { "data": "last_update", "name": "last_update", "orderable": false}
                    ],
                    "columnDefs": [
                        { "width": "35%", "targets": 0 },
                        { "width": "7%", "targets": [1, 2] },
                        { "width": "8%", "targets": 4 },
                        { "width": "20%", "targets": 3 },
                        { "width": "5%", "targets": 5 }
                    ],
                    buttons: [
                        {
                            extend: 'copyHtml5',
                            exportOptions: {columns: [0, 1, 2, 3, 4, 5]}
                        },
                        {
                            extend: 'excelHtml5',
                            exportOptions: {columns: [0, 1, 2, 3, 4, 5]}
                        }
                    ],
                    "drawCallback": function( settings ) {
                        var api = this.api();

                        $('#totalCastellers').html(api.page.info().recordsTotal);
                    }
                });
            }

            drawAttendersTable();

            attenders.on( 'draw', function () {
                $('.selectize2').select2({language: "ca"});
            });

            $('#tags, #filter_search_type, #status').change(function(){

                $('#attenders').DataTable().destroy();
                drawAttendersTable();
            });

            $('.selectize2').select2({language: "ca"});

            $("#filter_search_type").select2({
                minimumResultsForSearch: -1,
                language: "ca"
            });

            @if(Auth::user()->accesWriteEvents())

                function setStatus(id_casteller, status, companions, answers)
                {
                    $.post( "{{ route('event.attendance.set-status') }}",
                        { 'id_casteller': id_casteller,
                            'id_event' : {!! $event->id_event !!},
                            'status': status,
                            'companions': companions,
                            'answers': answers });
                }

                function setStatusVerified(id_casteller, status)
                {
                    $.post( "{{ route('event.attendance.set-status-verified') }}",
                        { 'id_casteller': id_casteller,
                            'id_event' : {!! $event->id_event !!},
                            'status': status });
                }

                $('#attenders').on('click', '.btn-status', function ()
                {
                    var status;
                    var id_casteller = $(this).data().id_casteller;
                    var companions = $(this).parents('tr').find('.companions').val();
                    var answers = $(this).parents('tr').find('.answers').val();

                    var YES = parseInt($('#YES').html());
                    var NO = parseInt($('#NO').html());
                    var UNKNOWN = parseInt($('#UNKNOWN').html());


                    if($(this).hasClass('btn-success'))
                    {
                        status = 'NO';
                        $(this).removeClass( "btn-success" );
                        $(this).addClass( "btn-danger" );
                        $(this).html('<i class="fa fa-close"></i>');
                        $(this).parents('tr').find('.companions').prop( "disabled", true);
                        $(this).parents('tr').find('.answers').prop( "disabled", true);
                        YES--;
                        NO++;
                        $('#YES').html(YES);
                        $('#NO').html(NO);
                    }
                    else if($(this).hasClass('btn-danger'))
                    {
                        status = 'UNKNOWN';
                        $(this).removeClass( "btn-danger" );
                        $(this).addClass( "btn-outline-warning" );
                        $(this).html('<i class="fa fa-question"></i>');
                        $(this).parents('tr').find('.companions').prop( "disabled", true);
                        $(this).parents('tr').find('.answers').prop( "disabled", true);
                        NO--;
                        UNKNOWN++;
                        $('#UNKNOWN').html(UNKNOWN);
                        $('#NO').html(NO);
                    }
                    else if($(this).hasClass('btn-secondary') || $(this).hasClass('btn-outline-warning'))
                    {
                        status = 'YES';
                        $(this).removeClass( "btn-secondary" );
                        $(this).removeClass('btn-outline-warning')
                        $(this).addClass( "btn-success" );
                        $(this).html('<i class="fa fa-check"></i>');

                        if(personalize_answers<1)
                        {
                            $(this).parents('tr').find('.answers').prop( "disabled", true);
                        }
                        else
                        {
                            $(this).parents('tr').find('.answers').prop( "disabled", false);
                        }

                        $(this).parents('tr').find('.companions').prop( "disabled", false);

                        YES++;
                        UNKNOWN--;
                        $('#UNKNOWN').html(UNKNOWN);
                        $('#YES').html(YES);
                    }

                    setStatus(id_casteller, status, companions, answers);
                });

                $('#attenders').on('click', '.btn-status-verified', function ()
                {
                    var status;
                    var id_casteller = $(this).data().id_casteller;

                    if($(this).hasClass('btn-success'))
                    {
                        status = 'NO';
                        $(this).removeClass( "btn-success" );
                        $(this).addClass( "btn-danger" );
                        $(this).html('<i class="fa fa-close"></i>');
                    }
                    else if($(this).hasClass('btn-danger'))
                    {
                        status = 'UNKNOWN';
                        $(this).removeClass( "btn-danger" );
                        $(this).addClass( "btn-secondary" );
                        $(this).html('<i class="fa fa-question"></i>');
                    }
                    else if($(this).hasClass('btn-secondary'))
                    {
                        status = 'YES';
                        $(this).removeClass( "btn-secondary" );
                        $(this).addClass( "btn-success" );
                        $(this).html('<i class="fa fa-check"></i>');
                    }

                    setStatusVerified(id_casteller, status);
                });

                $('#attenders').on('change', '.answers', function ()
                {
                    var id_casteller = $(this).parents('tr').find('.btn-status').data().id_casteller;
                    var answers = $(this).val();

                    $.post( "{{ route('event.attendance.set-answers') }}",
                        { 'id_casteller': id_casteller,
                            'id_event' : {!! $event->id_event !!},
                            'answers': answers });
                });

                $('#attenders').on('change', '.companions', function ()
                {
                    var id_casteller = $(this).parents('tr').find('.btn-status').data().id_casteller;
                    var companions = $(this).val();

                    $.post( "{{ route('event.attendance.set-companions') }}",
                        { 'id_casteller': id_casteller,
                            'id_event' : {!! $event->id_event !!},
                            'companions': companions });
                });

            @endif


        });
    </script>
@endsection
