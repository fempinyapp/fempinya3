@extends('template.main')

@section('title', trans('news.new'))
@section('content')
<div class="row">
    <div class="col-lg-12" style="padding-right: 7px;">
        <div class="block">
            <div class="block-content">
                <div class="row">                       
                        <div class="col-sm-2 font-w800 text-left  text-corporate-light">
                            {!! trans('news.new') !!}  
                        </div>
                        <div class="col-sm-8 font-weight-lighter text-left">
                            {{ $new->id }} 
                        </div>
                        <div class="col-sm-2 font-weight-lighter text-left">
                            <a href="/news" class="btn btn-primary btn-sm">{{ trans('news.tornar') }}</a>
                        </div>
                </div>
                <div class="row">  
                    <div class="col-sm-12">
                        <div class="row" style="line-height: 2.6">
                            <div class="col-sm-2 font-w800 text-left  text-corporate-light">{!! trans('news.data') !!}</div>
                            <div class="col-sm-10 font-weight-lighter text-left">{{ date('d-m-Y', strtotime($new->start_date)) }}</div>          
                        </div>
                        <div class="row" style="line-height: 2.6">
                            <div class="col-sm-2 font-w800 text-left  text-corporate-light">{!! trans('news.visiblity') !!} </div>
                            <div class="col-sm-10 font-weight-lighter text-left">@if($new->visiblity==1)  {!! trans('general.yes') !!} @else {!! trans('general.no') !!} @endif</div>
                        </div>
                        <div class="row" style="line-height: 2.6">
                            <div class="col-sm-2 font-w800 text-left  text-corporate-light">{!! trans('news.open_date') !!}</div>
                            <div class="col-sm-10 font-weight-lighter text-left">{{ date('d-m-Y H:i', strtotime($new->open_date)) }}</div>
                        </div>
                        <div class="row" style="line-height: 2.6">                                   
                            <div class="col-sm-2 font-w800 text-left  text-corporate-light">{!! trans('news.title') !!}</div>
                            <div class="col-sm-10 font-weight-lighter text-left">{{ $new->title }}</div>
                        </div>
                        <div class="row" style="line-height: 2.6">                                    
                            <div class="col-sm-2 font-w800 text-left  text-corporate-light">{!! trans('news.body') !!}</div>
                            <div class="col-sm-10 font-weight-lighter text-left">
                                {{ $new->body }}                 
                            </div>
                        </div>
                        <div class="row" style="line-height: 2.6">                              
                            <div class="col-sm-2 font-w800 text-left text-corporate-light">{!! trans('news.redactor') !!}</div>
                            <div class="col-sm-10 font-weight-lighter text-left">{{ $new->user_id }}</div>
                        </div> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection 
