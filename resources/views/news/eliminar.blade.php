<form action="{{ route('noticies.eliminar', $item) }}" class="d-inline" method="POST">
    @method('DELETE')
    @csrf
    <button type="submit" class="btn btn-danger btn-sm">Eliminar</button>
</form> 