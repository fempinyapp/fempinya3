@extends('template.main')

@section('title', trans('news.news'))
@section('css_before')
    <link rel="stylesheet" href="{!! asset('js/plugins/select2/css/select2.min.css') !!}">
    <link rel="stylesheet" href="{!! asset('js/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css') !!}">
@endsection

@section('content')
@if (Route::has('login'))
@auth
  <div class="container">
    <div class="block">    
      <div class="block-header block-header-default">
        <h3 class="block-title"><b>{!! trans('news.editar') !!}<b> <span>  {{ $new->id }}</span></h3>      
           <a  href="/news" class="btn btn-primary btn-sm">{!! trans('news.tornar') !!}</a>
      </div>
      <div class="block-content">
            @if (session('mensaje'))
                <div class="alert alert-success">
                    {{ session('mensaje') }}
                </div>
            @endif
            
            
            <form action="{{ route('news.update', $new->id_new) }}" method="POST">
              @method('PUT')
              @csrf

                  
              <div class="row form-group" id="div_open_close_date">
                  
                <div class="col-md-6">
                    <label class="control-label">{!! trans('news.new_date') !!}</label>
                    <div class="row">
                        <div class="col-md-4" id="div_new_date">
                          <input type="text" class="form-control" name="new_date" id="new_date" placeholder="{!! trans('general.select_date') !!}" value="@if(isset($new)){!!date('d/m/Y', strtotime($new->new_date)) !!}@endif" pattern="(0[1-9]|1[0-9]|2[0-9]|3[01])/(0[1-9]|1[012])/[0-9]{4}" required>
                        </div>
                        <div class="col-md-4" id="div_open_time">
                            <div class="row">
                                <div class="col-5" style="padding-left: 0; padding-right: 5px;">
                                    <select class="form-control" name="hour_new_date" id="hour_new_date" required>
                                        @for ($i = 0; $i <= 23; $i++)
                                            @if(isset($new) && date('H', strtotime($new->new_date))==str_pad($i, 2, '0', STR_PAD_LEFT))
                                                <option value="{!! str_pad($i, 2, '0', STR_PAD_LEFT) !!}" selected>{!! str_pad($i, 2, '0', STR_PAD_LEFT) !!}</option>
                                            @else
                                                <option value="{!! str_pad($i, 2, '0', STR_PAD_LEFT) !!}">{!! str_pad($i, 2, '0', STR_PAD_LEFT) !!}</option>
                                            @endif
                                        @endfor
                                    </select>
                                </div>
                                <div class="col-sm-1 text-center" style="padding-left: 0; padding-right: 0; max-width: 1%;"><b>:</b></div>
                                <div class="col-5" style="padding-left: 5px; padding-right: 0;">
                                    <select class="form-control" name="min_new_date" id="min_new_date" required>
                                        @for($i = 0; $i < 60; $i+=5)
                                            @if(isset($new) && date('i', strtotime($new->new_date))==str_pad($i, 2, '0', STR_PAD_LEFT))
                                                <option value="{!! str_pad($i, 2, '0', STR_PAD_LEFT) !!}" selected>{!! str_pad($i, 2, '0', STR_PAD_LEFT) !!}</option>
                                            @else
                                                <option value="{!! str_pad($i, 2, '0', STR_PAD_LEFT) !!}">{!! str_pad($i, 2, '0', STR_PAD_LEFT) !!}</option>
                                            @endif
                                        @endfor
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                  <label class="control-label">{!! trans('news.visiblity') !!}</label>
                  <select class="form-control" name="visiblity" id="visiblity" required>
                    @if($new->visiblity == 1))
                    <option value="1"} selected>{!! trans('general.yes') !!}</option>
                    <option value="0">{!! trans('general.no') !!}</option>
                    @else
                    <option value="1"}>{!! trans('general.yes') !!}</option>
                    <option value="0"  selected>{!! trans('general.no') !!}</option>
                    @endif
                  </select>
                </div>

            </div>
            <div class="row form-group" id="div_title">
                  
              <div class="col-md-12">
                  <label class="control-label">{!! trans('news.title') !!}</label>
                  <div class="row">
                    <div class="col-md-12" id="div_title">
          
                      <input
                      type="text"
                      name="title"
                      value="{{ $new->title }}"
                      placeholder="{{ $new->title }}"
                      class="form-control mb-2"
                      />
          
                    </div>
                  </div>
              </div>
            </div>
          
          
            <div class="row form-group" id="div_body">
            
              <div class="col-md-12">
                <div class="row">
                  <div class="col-md-12" id="div_body">
                  <label class="control-label">{!! trans('news.body') !!}</label>
                  <div class="row">
                    <div class="col-md-12" id="div_body">
                      <textarea cols="10" rows="5" name="body" class="form-control" id="body" placeholder="{!! trans('news.body') !!}" required>{{  $new->body }}</textarea>
                    </div>
                  </div>
              </div>
            </div>
          
            <div class="row form-group" id="div_body">

            <div class="col-md-12">
              <div class="row">
                <div class="col-md-12" id="div_body">
                  <label class="control-label">{!! trans('news.redactor') !!}</label>
                  : {{  $redactor->name }} 
                  <input
                      type="hidden"
                      name="colla_id"
                      value="{{ $new->colla_id }}"
                      class="form-control mb-2"
                    />
                    <input 
                      type="hidden" 
                      name="user_id" 
                      placeholder="{!! trans('news.redactor') !!}" 
                      value="{{  $new->user_id }}"
                      class="form-control mb-2" 
                      >                  
                </div>
              </div>
            </div>
          
          
              <button class="btn btn-warning btn-block" type="submit">{!! trans('news.editar') !!}</button>
          </form>
        </div>
        </div> <!-- /container -->
    </div>
</div>
  @else
<div class="alert alert-danger">
  <strong>{!! trans('user.not_logging') !!}</strong> .
</div>
@endauth
@endif
@endsection

@section('js')
    <script src="{!! asset('js/plugins/select2/js/select2.full.min.js') !!}"></script>

    <script type="text/javascript" src="{!! asset('js/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') !!}"></script>
    @if (Auth()->user()->language=='ca')
        <script type="text/javascript" src="{!! asset('js/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.ca.min.js') !!}"></script>
    @elseif(Auth()->user()->language=='es')
        <script type="text/javascript" src="{!! asset('js/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.es.min.js') !!}"></script>
    @endif

    <script>
        $(function ()
        {
            $('#new_date').datepicker({
                @if (Auth()->user()->language=='ca')
                language: 'ca',
                @elseif(Auth()->user()->language=='es')
                language: 'es',
                @endif
                format: "dd/mm/yyyy"
            });
        });
    </script>
@endsection