@extends('template.main')

@section('title', trans('news.news'))
@section('css_before')
    <link rel="stylesheet" href="{!! asset('js/plugins/select2/css/select2.min.css') !!}">
    <link rel="stylesheet" href="{{ asset('js/plugins/datatables/dataTables.bootstrap4.css') }}">
    <link rel="stylesheet" href="{{ asset('js/plugins/datatables/buttons-bs4/buttons.bootstrap4.css') }}">
    <link rel="stylesheet" href="{!! asset('js/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css') !!}">
@endsection
@section('content')

<div class="block">

    <div class="block-header block-header-default">

        <div class="block-title">
            <h3 class="block-title"><b>{!! trans('news.news') !!}</b> {{auth()->user()->name}}</h3>
        </div>
        <div class="block-options">
            <a href="/news/create" class="btn btn-primary btn-sm"><i class="fa fa-file-text"></i> {!! trans('news.add_news') !!}</a>
        </div>

    </div>


    <div class="block-content block-content-full">
        <div class="row"  style="padding-top: 25px;">
            <div class="col-md-12">
                <table  class="table table-hover table-bordered table-striped" style="width: 100%;" id="news">
                    <thead>
                        <tr>
                        <th scope="col">{!! trans('news.title') !!}</th>
                        <th scope="col">{!! trans('news.body') !!}</th>
                        <th scope="col">{!! trans('news.data') !!}</th>
                        <th scope="col">{!! trans('news.visiblity') !!}</th>
                        <th scope="col">#</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($news as $new)
                        <tr>
                            <td>{{ $new->title }} </td>
                            <td>@if(strlen($new->body)>=100) {{ substr($new->body, 0, 100) }} [<a href="{{route('news.details', $new)}}" class="alert-link">{!! trans('news.read_more') !!}</a>]  @else  {{  $new->body  }} @endif</td>
                            <td> {{ date('d-m-Y H:m', strtotime($new->new_date)) }}</td>
                            <td>@if($new->visiblity==1)  {!! trans('general.yes') !!} @else {!! trans('general.no') !!} @endif</td>

                            <td>
                                <a href="{{route('news.editar', $new->id_new)}}" class="btn btn-warning btn-sm"><i class="fa fa-pencil"></i></a>
                                <a class="btn btn- btn-sm delete-new" data-toggle="modal" data-target="#deleteModal" data-id="{{ $new->id_new }}" data-url="{{ url('news.eliminar', $new->id_new) }}">
                                    <i class="fa fa-trash"></i>
                                </a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                {{$news->links()}}
            </div>
        </div>
    </div>
</div>

<!-- The Modal -->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="modal-popin" aria-hidden="true">
    <div class="modal-dialog modal-sm modal-dialog-popin"  role="document">
        <div class="modal-content">
            <div class="block block-themed block-transparent mb-0">
                <div class="block-header bg-primary-dark">
                    <h3 class="block-title">{!! trans('news.del_new') !!}</h3>
                    <div class="block-options">
                        <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                            <i class="si si-close"></i>
                        </button>
                    </div>
                </div>

                <div class="block-content text-center">
                    <i class="fa fa-warning" style="font-size: 46px;"></i>
                    <h3 class="semibold modal-title text-danger">{!! trans('general.caution') !!}</h3>
                    <p class="text-muted">{!! trans('news.del_new_warning') !!}</p>
                </div>
            </div>
            <div class="modal-footer">
                <form id="userForm" action="" method="get">
                    @csrf
                    @method('eliminar')
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{!! trans('general.close') !!}</button>
                    <button type="submit" class="btn btn-danger">{!! trans('general.delete') !!}</button>

                </form>
            </div>
        </div>
    </div>
</div>


@endsection

@section('js')
    <!-- Page JS Plugins -->
    <script src="{{ asset('js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons/buttons.html5.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/buttons-bs4/buttons.bootstrap4.min.js') }}"></script>




$(document).ready(function() {
    $('#news').DataTable( {
        "ordering":'true',
                    "order": [2, 'asc'],
                    "columns": [
                        { "data": "title", "name": "title"},
                        { "data": "body", "name": "body", "orderable": false },
                        { "data": "new_date", "name": "new_date" },
                        { "data": "buttons", "name": "buttons", "orderable": false }
                    ],
    } );
    // For A Delete Record Popup
    $('.delete-new').click(function () {
       // var id = $(this).attr('data-id');
        var url = $(this).attr('data-url');

      //  $("#deleteForm", 'input').val(id);
        $("#deleteForm").attr("action", url);
    });
} );
@endsection
