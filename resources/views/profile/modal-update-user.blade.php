<div class="block block-themed block-transparent mb-0">
    <div class="block-header bg-primary-dark">
        <h3 class="block-title">
            {!! trans('admin.update_user') !!}
        </h3>

            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                <i class="si si-close"></i>
            </button>
        </div>
    </div>
{!! Form::open(array('id' => 'FormUpdateUserPermissions', 'url' => route('profile.colla.update-user', $user->id_user), 'method' => 'POST', 'class' => '')) !!}
    <div class="block-options">
        <div class="block-content">
            <div class="row form-group">
                <div class="col-md-6">
                    <label class="control-label">{!! trans('general.name') !!}</label>
                    <input type="text" class="form-control" id="name" name="name" value="{!! $user->name !!}" required>
                </div>
                <div class="col-md-6">
                    <label class="control-label">{!! trans('general.email') !!}</label>
                    <input type="email" class="form-control" id="email" name="email" value="{!! $user->email !!}" disabled>
                </div>
            </div>
            <div class="row form-group">
                <div class="col-md-3">
                    <label for="profile-settings-email red">{!! trans('user.choose_language') !!}</label>
                    <select name="language" id="language" class="form-control">
                        <option value="ca" @if($user->language=='ca') selected @endif>{!! trans('user.catalan') !!}</option>
                        <option value="en" @if($user->language=='en') selected @endif>{!! trans('user.english') !!}</option>
                        <option value="fr" @if($user->language=='fr') selected @endif>{!! trans('user.french') !!}</option>
                        <option value="es" @if($user->language=='es') selected @endif>{!! trans('user.spanish') !!}</option>
                    </select>
                </div>
                @if(\Illuminate\Support\Facades\Auth::user()->accesAdmin())
                    <div class="col-md-4">
                        <label class="control-label"><span class="text-warning">{!! trans('admin.do_admin') !!}</span></label>
                        <select name="permission_admin" id="permission_admin" class="form-control">
                            <option value="NO_ADMIN">{!! trans('user.colla_user') !!}</option>
                            <option value="ADMIN" @if($user->accesAdmin()) selected @endif>{!! trans('user.permission_admin') !!}</option>
                        </select>
                    </div>
                @endif
            </div>

            <div class="row form-group">

                <div class="col-md-4">
                    <label class="control-label">{!! trans('user.permission_colla') !!}</label>
                    <select name="permission_colla" id="permission_colla" class="form-control">
                        <option value="DENIED" @if(!isset($user->getPermissions()['COLLA'])) selected @endif>{!! trans('user.permission_denied') !!}</option>
                        <option value="READ" @if(isset($user->getPermissions()['COLLA'][0]) && $user->getPermissions()['COLLA'][0]=='READ' && !isset($user->getPermissions()['COLLA'][1])) selected @endif>{!! trans('user.permission_read') !!}</option>
                        <option value="WRITE"@if(isset($user->getPermissions()['COLLA'][1]) && $user->getPermissions()['COLLA'][1]=='WRITE') selected @endif>{!! trans('user.permission_read_write') !!}</option>
                    </select>
                </div>
                <div class="col-md-4">
                    <label class="control-label">{!! trans('user.permission_bbdd') !!}</label>
                    <select name="permission_castellers" id="permission_castellers" class="form-control">
                        <option value="DENIED" @if(!isset($user->getPermissions()['BBDD'])) selected @endif>{!! trans('user.permission_denied') !!}</option>
                        <option value="READ" @if(isset($user->getPermissions()['BBDD'][0]) && $user->getPermissions()['BBDD'][0]=='READ' && !isset($user->getPermissions()['CASTELLERS'][1])) selected @endif>{!! trans('user.permission_read') !!}</option>
                        <option value="WRITE"@if(isset($user->getPermissions()['BBDD'][1]) && $user->getPermissions()['BBDD'][1]=='WRITE') selected @endif>{!! trans('user.permission_read_write') !!}</option>
                    </select>
                </div>
                <div class="col-md-4">
                    <label class="control-label">{!! trans('user.permission_events') !!}</label>
                    <select name="permission_events" id="permission_events" class="form-control">
                        <option value="DENIED" @if(!isset($user->getPermissions()['EVENTS'])) selected @endif>{!! trans('user.permission_denied') !!}</option>
                        <option value="READ" @if(isset($user->getPermissions()['EVENTS'][0]) && $user->getPermissions()['EVENTS'][0]=='READ' && !isset($user->getPermissions()['EVENTS'][1])) selected @endif>{!! trans('user.permission_read') !!}</option>
                        <option value="WRITE"@if(isset($user->getPermissions()['EVENTS'][1]) && $user->getPermissions()['EVENTS'][1]=='WRITE') selected @endif>{!! trans('user.permission_read_write') !!}</option>
                    </select>
                </div>
            </div>

            <div class="row form-group">


                <div class="col-md-4">
                    <label class="control-label">{!! trans('user.permission_pinyes') !!}</label>
                    <select name="permission_pinyes" id="permission_pinyes" class="form-control">
                        <option value="DENIED" @if(!isset($user->getPermissions()['PINYES'])) selected @endif>{!! trans('user.permission_denied') !!}</option>
                        <option value="READ" @if(isset($user->getPermissions()['PINYES'][0]) && $user->getPermissions()['PINYES'][0]=='READ' && !isset($user->getPermissions()['PINYES'][1])) selected @endif>{!! trans('user.permission_read') !!}</option>
                        <option value="WRITE"@if(isset($user->getPermissions()['PINYES'][1]) && $user->getPermissions()['PINYES'][1]=='WRITE') selected @endif>{!! trans('user.permission_read_write') !!}</option>
                    </select>
                </div>
                <div class="col-md-4">
                    <label class="control-label">{!! trans('user.permission_telegram') !!}</label>
                    <select name="permission_telegram" id="permission_telegram" class="form-control">
                        <option value="DENIED" @if(!isset($user->getPermissions()['TELEGRAM'])) selected @endif>{!! trans('user.permission_denied') !!}</option>
                        <option value="READ" @if(isset($user->getPermissions()['TELEGRAM'][0]) && $user->getPermissions()['TELEGRAM'][0]=='READ' && !isset($user->getPermissions()['TELEGRAM'][1])) selected @endif>{!! trans('user.permission_read') !!}</option>
                        <option value="WRITE"@if(isset($user->getPermissions()['TELEGRAM'][1]) && $user->getPermissions()['TELEGRAM'][1]=='WRITE') selected @endif>{!! trans('user.permission_read_write') !!}</option>
                    </select>
                </div>
            </div>


    </div>
    <div class="modal-footer">
        <button type="submit" form="FormUpdateUserPermissions" class="btn btn-alt-primary">{!! trans('general.update') !!}</button>
        <button type="button" class="btn  btn-alt-secondary" data-dismiss="modal">{!! trans('general.close') !!}</button>
    </div>
{!! Form::close() !!}
</div>
<script>
    $(function(){

    });
</script>
