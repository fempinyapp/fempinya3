@extends('template.main')

@section('title', 'Administració - La meva colla')
@section('css_before')
    <!-- Page JS Plugins CSS -->
    <link rel="stylesheet" href="{{ asset('js/plugins/datatables/dataTables.bootstrap4.css') }}">
@endsection
@section('css_after')
@endsection

@section('content')

<div class="row">
    <div class="col-lg-12">
        <h2 class="text-center">{!! trans('general.colla') !!}: <span style="font-weight: 100">{!! $colla->name !!}</span> </h2>
        <!-- Block Tabs Animated Slide Up -->
        <div class="block">

            <ul class="nav nav-tabs nav-tabs-block" role="tablist">
                <li class="nav-item">
                    <a class="nav-link {!! $active['li_profile'] !!}" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="home" aria-selected="true">{!! trans('general.profile') !!}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {!! $active['li_users'] !!}" id="users-tab" data-toggle="tab" href="#users" role="tab" aria-controls="profile" aria-selected="false">{!! trans('general.users') !!}</a>
                </li>
            </ul>
            <div class="tab-content" id="myTabContent">
                <div class="block-content tab-pane fade {!! $active['div_profile'] !!}" id="profile" role="tabpanel" aria-labelledby="home-tab">
                    <h4 class="font-w400">{!! trans('user.config_colla') !!}</h4>
                    {!! Form::open(array('id' => 'FormUpdateColla', 'url' => route('profile.colla.update'), 'method' => 'POST', 'class' => '', 'enctype' => 'multipart/form-data')) !!}
                    <div class="row">
                        <div class="col-lg-3">
                            @if($colla->logo)
                                <img src="{{ asset('media/colles/'.$colla->shortname.'/'.$colla->logo) }}" class="img-fluid" alt="Logo: {!! $colla->name !!}">
                            @else
                                <img src="{{ asset('media/avatars/avatar.jpg') }}" class="img-avatar128" alt="Logo: {!! $colla->name !!}">
                            @endif

                        </div>
                        <div class="col-lg-9">
                            <div class="row form-group">
                                <div class="col-md-9">
                                    <label class="control-label">{!! trans('admin.name_colla') !!}</label>
                                    <input type="text" class="form-control" id="name" name="name" value="{!! $colla->name !!}" required>
                                </div>
                                <div class="col-md-3">
                                    <label class="control-label">{!! trans('admin.shortname') !!}</label>
                                    <input type="text" class="form-control" id="shortname" name="shortname" value="{!! $colla->shortname !!}" required disabled>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-md-7">
                                    <label class="control-label">{!! trans('general.email') !!}</label>
                                    <input type="email" class="form-control" id="email" name="email" value="{!! $colla->email !!}" required>
                                </div>
                                <div class="col-md-5">
                                    <label class="control-label">{!! trans('general.phone') !!}</label>
                                    <input type="text" class="form-control" id="phone" name="phone" value="{!! $colla->phone !!}" required>
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-md-6">
                                    <label class="control-label">{!! trans('general.country') !!}</label>
                                    <input type="text" class="form-control" id="country" name="country" value="{!! $colla->country !!}" required>
                                </div>
                                <div class="col-md-6">
                                    <label class="control-label">{!! trans('general.city') !!}</label>
                                    <input type="text" class="form-control" id="city" name="city" value="{!! $colla->city !!}" required>
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-md-8">

                                    <label class="control-label">{!! trans('general.logo') !!}</label>
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" name="logo" id="logo" accept="image/png, image/jpeg">
                                        <label class="custom-file-label" for="logo">{!! trans('general.select_file') !!}</label>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <label class="control-label">{!! trans('general.color') !!} principal</label>
                                    <div class="col-6" style="padding-left: 0;"><input type="color" class="form-control" id="color" name="color" value="{!! $colla->color !!}" required=""></div>
                                </div>
                                <div class="col-md-2">
                                    <label class="control-label">{!! trans('general.color') !!} secundàri?</label>
                                    <div class="col-6" style="padding-left: 0;"><input type="color" class="form-control" id="color2" name="color2" value="" required=""></div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-12">
                                    <button type="submit" form="FormUpdateColla" class="btn btn-primary">{!! trans('general.update') !!}</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
                <div class="block-content tab-pane fade {!! $active['div_users'] !!}" id="users" role="tabpanel" aria-labelledby="profile-tab">
                    <h4 class="font-w400">{!! trans('general.users') !!}</h4>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>{!! trans('general.name') !!}</th>
                                <th>{!! trans('user.permissions') !!}</th>
                                <th>{!! trans('admin.last_login') !!}</th>
                                <th>#</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($users as $user)
                                <tr>
                                    <td>{!! $user->name !!}</td>
                                    <td>
                                        @if(empty($user->getPermissions()))
                                            <span class="badge badge-secondary">{!! trans('admin.permission_no_acces') !!}</span>
                                        @else

                                            <table class="table table-sm table-bordered">
                                                <tr>
                                                    <td></td>
                                                    @foreach($user->getPermissions() as $key => $permission)
                                                        <td>{!! trans('user.permission_'.strtolower($key)) !!} </td>
                                                    @endforeach
                                                </tr>

                                                <tr>
                                                    <td>{!! trans('user.permission_read') !!}</td>
                                                    @foreach($user->getPermissions() as $key => $permission)
                                                        <td>
                                                            @if(is_array($permission))
                                                                @if($permission[0]=='READ') <span class="fa fa-check-circle"></span> @endif
                                                            @else
                                                                @if($permission=='READ') <span class="fa fa-check-circle"></span> @endif
                                                            @endif
                                                        </td>
                                                    @endforeach
                                                </tr>
                                                <tr>
                                                    <td>{!! trans('user.permission_write') !!}</td>
                                                    @foreach($user->getPermissions() as $key => $permission)
                                                        <td>
                                                            @if(is_array($permission))
                                                                @if(isset($permission[1]) && $permission[1]=='WRITE') <span class="fa fa-check-circle"></span> @endif
                                                            @endif
                                                        </td>
                                                    @endforeach
                                                </tr>
                                            </table>
                                        @endif

                                    </td>
                                    <td>
                                        @if(is_null($user->last_access_at))
                                            {!! trans('user.not_logging') !!}
                                        @else
                                            {!! \Carbon\Carbon::parse($user->last_access_at)->format('d/m/Y h:m:s') !!}
                                        @endif
                                    </td>
                                    <td>
                                        <button class="btn btn-warning btn-update-user" data-id_user="{!! $user->id_user !!}"><i class="fa fa-pencil"></i></button>
                                        <button class="btn btn-danger btn-delete-user" data-id_user="{!! $user->id_user !!}"><i class="fa fa-trash-o"></i></button>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <hr style="padding-top: 15px;">
                    <h4 class="font-w400">{!! trans('user.add_user') !!}</h4>
                    {!! Form::open(array('id' => 'FormAddUser', 'url' => route('profile.colla.add-user'), 'method' => 'POST', 'class' => '')) !!}
                    <div class="row form-group">
                        <div class="col-md-3">
                            <label class="control-label">{!! trans('general.name') !!}</label>
                            <input type="text" class="form-control" id="name" name="name" value="{!! old('name') !!}" required>
                        </div>
                        <div class="col-md-3">
                            <label class="control-label">{!! trans('general.email') !!}</label>
                            <input type="email" class="form-control" id="email" name="email" value="{!! old('email') !!}" required>
                        </div>
                        <div class="col-md-2">
                            <label class="control-label">{!! trans('user.password') !!}</label>
                            <input type="password" class="form-control" id="password" name="password" value="" required>
                        </div>
                        <div class="col-md-2">
                            <label class="control-label">{!! trans('user.confirm_password') !!}</label>
                            <input type="password" class="form-control" id="confirm_password" name="confirm_password" value="" required>
                        </div>
                        <div class="col-md-2">
                            <label for="profile-settings-email">{!! trans('user.choose_language') !!}</label>
                            <select name="language" id="language" class="form-control">
                                <option value="ca" @if(old('language')=='ca') selected @endif>{!! trans('user.catalan') !!}</option>
                                <option value="en" @if(old('language')=='en') selected @endif>{!! trans('user.english') !!}</option>
                                <option value="fr" @if(old('language')=='fr') selected @endif>{!! trans('user.french') !!}</option>
                                <option value="es" @if(old('language')=='es') selected @endif>{!! trans('user.spanish') !!}</option>
                            </select>
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col-md-2">
                            <label class="control-label">{!! trans('user.permission_colla') !!}</label>
                            <select name="permission_colla" id="permission_colla" class="form-control">
                                <option value="DENIED">{!! trans('user.permission_denied') !!}</option>
                                <option value="READ">{!! trans('user.permission_read') !!}</option>
                                <option value="WRITE">{!! trans('user.permission_read_write') !!}</option>
                            </select>
                        </div>
                        <div class="col-md-2">
                            <label class="control-label">{!! trans('user.permission_bbdd') !!}</label>
                            <select name="permission_castellers" id="permission_castellers" class="form-control">
                                <option value="DENIED">{!! trans('user.permission_denied') !!}</option>
                                <option value="READ">{!! trans('user.permission_read') !!}</option>
                                <option value="WRITE">{!! trans('user.permission_read_write') !!}</option>
                            </select>
                        </div>
                        <div class="col-md-2">
                            <label class="control-label">{!! trans('user.permission_events') !!}</label>
                            <select name="permission_events" id="permission_events" class="form-control">
                                <option value="DENIED">{!! trans('user.permission_denied') !!}</option>
                                <option value="READ">{!! trans('user.permission_read') !!}</option>
                                <option value="WRITE">{!! trans('user.permission_read_write') !!}</option>
                            </select>
                        </div>
                        <div class="col-md-2">
                            <label class="control-label">{!! trans('user.permission_pinyes') !!}</label>
                            <select name="permission_pinyes" id="permission_pinyes" class="form-control">
                                <option value="DENIED">{!! trans('user.permission_denied') !!}</option>
                                <option value="READ">{!! trans('user.permission_read') !!}</option>
                                <option value="WRITE">{!! trans('user.permission_read_write') !!}</option>
                            </select>
                        </div>
                        <div class="col-md-2">
                            <label class="control-label">{!! trans('user.permission_telegram') !!}</label>
                            <select name="permission_telegram" id="permission_telegram" class="form-control">
                                <option value="DENIED">{!! trans('user.permission_denied') !!}</option>
                                <option value="READ">{!! trans('user.permission_read') !!}</option>
                                <option value="WRITE">{!! trans('user.permission_read_write') !!}</option>
                            </select>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-12 text-right">
                            <button class="btn btn-primary" form="FormAddUser">{!! trans('general.add') !!}</button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>

            </div>

        </div>
        <!-- END Block Tabs Animated Slide Up -->

    </div>
</div>

<!-- START - Modal Update User -->
<div class="modal fade" id="modalUpdateUser" tabindex="-1" role="dialog" aria-labelledby="modal-popin" aria-hidden="true">
<div class="modal-dialog modal-lg modal-dialog-popin" role="document">
    <div class="modal-content" id="modalUpdateUserContent">
        <!-- MODAL CONTENT -->
    </div>
</div>
</div>
<!-- END - Modal Update User -->

<!-- START - MODAL DELETE -->
<div class="modal fade" id="modalDelUser" tabindex="-1" role="dialog" aria-labelledby="modal-popin" aria-hidden="true">
<div class="modal-dialog modal-sm modal-dialog-popin" role="document">
    <div class="modal-content">
        {!! Form::open(array('url' => 'n', 'method' => 'POST', 'class' => 'form-horizontal form-bordered', 'id' => 'fromDelUser')) !!}
        <div class="block block-themed block-transparent mb-0">
            <div class="block-header bg-primary-dark">
                <h3 class="block-title">{!! trans('admin.del_user') !!}</h3>
                <div class="block-options">
                    <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                        <i class="si si-close"></i>
                    </button>
                </div>
            </div>

            <div class="block-content text-center">
                <i class="fa fa-warning" style="font-size: 46px;"></i>
                <h3 class="semibold modal-title text-danger">{!! trans('general.caution') !!}</h3>
                <p class="text-muted">{!! trans('admin.del_user_warning') !!}</p>
            </div>
        </div>
        <div class="modal-footer">
            {!! Form::submit(trans('general.delete') , array('class' => 'btn btn-danger')) !!}
            <button type="button" class="btn  btn-alt-secondary" data-dismiss="modal">{!! trans('general.close') !!}</button>
        </div>
        {!! Form::close() !!}

    </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
</div>
<!--/ END - MODAL DELETE -->

@endsection


@section('js')
    <script>
        $(function ()
        {
            $('#logo').on('change',function(){
                //get the file name
                var fieldVal = $(this).val();

                // Change the node's value by removing the fake path (Chrome)
                fieldVal = fieldVal.replace("C:\\fakepath\\", "");

                //replace the "Choose a file" label
                $(this).next('.custom-file-label').html(fieldVal);
            });


            $(".btn-delete-user").on('click', function (event)
            {
                var id_user = $(this).data().id_user;
                $('#modalDelUser').modal('show');

                var url = "{{ route('profile.colla.delete-user', ':id_user') }}";
                url = url.replace(':id_user',id_user);

                $('#fromDelUser').attr('action', url);
            });

            $(".btn-update-user").on('click', function (event)
            {
                var id_user = $(this).data().id_user;

                $('#modalUpdateUser').modal('show');

                $('#modalUpdateUserContent').html('<div class="col-md-12 text-center"><div class="spinner-border" role="status"><span class="sr-only">Loading...</span></div></div>');

                var url = "{{ route('profile.colla.edit-user-modal', ':id_user') }}";
                url = url.replace(':id_user',id_user);

                $.get( url, function( data ) {
                    $('#modalUpdateUserContent').html( data );
                });
            });
        });
    </script>
@endsection
