@extends('template.main')

@section('title', trans('boards.add'))
@section('css_before')
    <link rel="stylesheet" href="{!! asset('js/plugins/select2/css/select2.min.css') !!}">
@endsection
@section('css_after')
    <style>
        .result_pinya div {
            position: absolute;
            text-align: center;
            line-height: 28px;
            display: block;
            overflow: hidden;
            font-size: 11.5px;
            width: 72px;
            height: 31.6px;
            font-family: Helvetica, Verdana, sans-serif;
            color: black;
        }

        .radius_1 {border-radius: 5px;}
        .radius_2 {border-radius: 10px;}
        .radius_3 {border-radius: 15px;}

        .border_1{ border: 1px solid grey;}
        .border_2{ border: 2px solid grey;}
        .border_3{ border: 3px solid grey;}
        .border_4{ border: 4px solid grey;}

        .bg_color_1{background-color: #cfd8dc;}
        .bg_color_2{background-color: #c5cae9;}
        .bg_color_3{background-color: #c5e1a5;}
    </style>
@endsection

@section('content')

<div class="block">
    <div class="block-header block-header-default">
       <h3 class="block-title">
           <div class="row">
               <div class="col-md-12"><b>{!! trans('boards.add') !!}:</b></div>
               <div class="col-md-3">{!! trans('boards.import_step_1') !!}</div>
               <div class="col-md-3">{!! trans('boards.import_step_2') !!}</div>
               <div class="col-md-3">{!! trans('boards.import_step_3') !!}</div>
               <div class="col-md-3 text-success">{!! trans('boards.import_step_4') !!}</div>
           </div>
       </h3>
    </div>
    <div class="block-content">
        <div class="row form-group">
            <div class="col-md-12"><h2>{!! $board->name !!}</h2></div>
            <div class="col-md-12">
                <h5 class="text-info">{!! trans('boards.step_style map_txt', ['BASE' => $type_map]) !!}</h5>
            </div>
            <div class="col-md-9">
                {!! trans('boards.step_style_map_explanation') !!}
            </div>
            <div class="col-md-3 text-right">
                <button class="btn btn-success" data-toggle="modal" data-target="#modalFinalStep">{!! trans('boards.done_next_step') !!} <span class="fa fa-chevron-right"></span></button>
            </div>
        </div>

        <div class="row form-group">
            <div class="col-md-1">
                <label class="control-label">{!! trans('boards.borders') !!}</label>
                <div id="border_1" class="border_1 div-class" style="width: 65px; height: 25px;"></div>
                <div id="border_2" class="border_2 div-class" style="width: 65px; height: 25px; margin-top: 6px;"></div>
                <div id="border_3" class="border_3 div-class" style="width: 65px; height: 25px; margin-top: 6px;"></div>
                <div id="border_4" class="border_4 div-class" style="width: 65px; height: 25px; margin-top: 6px;"></div>
            </div>
            <div class="col-md-1">
                <label class="control-label">{!! trans('boards.background') !!}</label>
                <div id="bg_color" class="text-center bg_color div-class" style="width: 65px; height: 25px; border: 1px solid grey;">text</div>
                <div id="bg_color_1" class="text-center bg_color_1 div-class" style="width: 65px; height: 25px; border: 1px solid grey; margin-top: 6px;">text</div>
                <div id="bg_color_2" class="text-center bg_color_2 div-class" style="width: 65px; height: 25px; border: 1px solid grey; margin-top: 6px;">text</div>
                <div id="bg_color_3" class="text-center bg_color_3 div-class" style="width: 65px; height: 25px; border: 1px solid grey; margin-top: 6px;">text</div>
            </div>
            <div class="col-md-1">
                <label class="control-label">{!! trans('boards.corner') !!}</label>
                <div id="radius" class="radius div-class" style="width: 65px; height: 25px; border: 1px solid grey;"></div>
                <div id="radius_1" class="radius_1 div-class" style="width: 65px; height: 25px; border: 1px solid grey; margin-top: 6px;"></div>
                <div id="radius_2" class="radius_2 div-class" style="width: 65px; height: 25px; border: 1px solid grey; margin-top: 6px;"></div>
                <div id="radius_3" class="radius_3 div-class" style="width: 65px; height: 25px; border: 1px solid grey; margin-top: 6px;"></div>
            </div>

            <div class="col-md-1" style="padding-top: 25px;">
                <button class="btn btn-success" id="BtnNameOk">{!! trans('general.add') !!}</button>
            </div>
            <div class="col-md-1" style="padding-top: 25px;">
                <button class="btn btn-danger" id="BtnRemoveRow">{!! trans('general.no') !!}</button>
            </div>
            <div class="col-md-1" style="padding-top: 27px;">
                <div class="spinner-border" role="status" id="spinnerAddClass" style="display: none;"><span class="sr-only">Loading...</span></div>
                <i class="fa fa-check fa-2x text-success" id="divDone" style="display: none;"></i>
            </div>
        </div>

        <div class="row">
            <div class="result_pinya" style="position: relative; height: 2000px;">
                @if($type_map==='PINYA')
                    {!! $board->html_pinya !!}
                @elseif($type_map==='FOLRE')
                    {!! $board->html_folre !!}
                @elseif($type_map==='MANILLES')
                    {!! $board->html_manilles !!}
                @elseif($type_map==='PUNTALS')
                    {!! $board->html_puntals !!}
                @endif
            </div>
        </div>

    </div>
</div>

<!-- START - MODAL FINAL STEP -->
<div class="modal fade" id="modalFinalStep" tabindex="-1" role="dialog" aria-labelledby="modal-popin" aria-hidden="true">
    <div class="modal-dialog modal-dialog-popin" role="document">
        <div class="modal-content">
            {!! Form::open(array('url' => 'n', 'method' => 'POST', 'class' => 'form-horizontal', 'id' => 'fromDelColla')) !!}
            <div class="block block-themed block-transparent mb-0">
                <div class="block-header bg-primary-dark">
                    <h3 class="block-title">{!! trans('boards.base_import_finished') !!}</h3>
                    <div class="block-options">
                        <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                            <i class="si si-close"></i>
                        </button>
                    </div>
                </div>

                <div class="block-content">
                    <p class="text-muted">{!! trans('boards.base_import_finished_txt', ['base' => $type_map,'pinya' => $board->name]) !!}</p>
                    @if(is_null($board->html_pinya) ||
                       (($board->type==='FOLRE' || $board->type==='MANILLES' || $board->type==='PUNTALS') && is_null($board->html_folre)) ||
                       (($board->type==='MANILLES' || $board->type==='PUNTALS') && is_null($board->html_manilles)) ||
                       (($board->type==='PUNTALS') && is_null($board->html_manilles)))
                        <p class="text-muted">{!! trans('boards.bases_not_imported') !!}</p>
                    @endif

                    @if(is_null($board->html_pinya))
                        <a href="{!! route('boards.add-map', ['board' => $board, 'map' => 'PINYA']) !!}" class="btn btn-success mb-10">{!! trans('boards.import_pinya') !!}</a><br>
                    @endif

                    @if(($board->type==='FOLRE' || $board->type==='MANILLES' || $board->type==='PUNTALS') && is_null($board->html_folre))
                        <a href="{!! route('boards.add-map', ['board' => $board, 'map' => 'FOLRE']) !!}" class="btn btn-success mb-10">{!! trans('boards.import_folre') !!}</a><br>
                    @endif

                    @if(($board->type==='MANILLES' || $board->type==='PUNTALS') && is_null($board->html_manilles))
                        <a href="{!! route('boards.add-map', ['board' => $board, 'map' => 'MANILLES']) !!}" class="btn btn-success mb-10">{!! trans('boards.import_manilles') !!}</a><br>
                    @endif

                    @if(($board->type==='PUNTALS') && is_null($board->html_manilles))
                        <a href="{!! route('boards.add-map', ['board' => $board, 'map' => 'PUNTALS']) !!}" class="btn btn-success mb-10">{!! trans('boards.import_puntals') !!}</a><br>
                    @endif

                </div>
            </div>
            <div class="modal-footer">

                    @if(is_null($board->html_pinya) ||
                   (($board->type==='FOLRE' || $board->type==='MANILLES' || $board->type==='PUNTALS') && is_null($board->html_folre)) ||
                   (($board->type==='MANILLES' || $board->type==='PUNTALS') && is_null($board->html_manilles)) ||
                   (($board->type==='PUNTALS') && is_null($board->html_manilles)))
                        <a href="{!! route('boards.list') !!}" class="btn btn-primary mr-auto">{!! trans('boards.not_yet_rtn_boards_list') !!}</a>
                    @else
                        <a href="{!! route('boards.list') !!}" class="btn btn-primary mr-auto">{!! trans('boards.rtn_boards_list') !!}</a>
                    @endif

                    <button type="button" class="btn btn-alt-secondary" data-dismiss="modal">{!! trans('general.close') !!}</button>

                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<!--/ END - MODAL FINAL STEP-->

@endsection

@section('js')
    <script src="{!! asset('js/plugins/select2/js/select2.full.min.js') !!}"></script>
    <script type="text/javascript">
        $(function () {
            $.ajaxPrefilter(function(options, originalOptions, xhr) { // this will run before each request
                var token = $('meta[name="csrf-token"]').attr('content'); // or _token, whichever you are using

                if (token) {
                    return xhr.setRequestHeader('X-CSRF-TOKEN', token); // adds directly to the XmlHttpRequest Object
                }
            });
        });
    </script>
<script>
    $(function ()
    {
        var id_row;
        var addClasses = {border: null, background: null, radius: null};

        $('#result_pinya').on('click', 'div', function ()
        {
            id_row = null;

            id_row = $(this).attr('id');

            $.each( addClasses, function(key, value)
            {
                if(value!=null)
                {
                    $('#'+id_row).addClass(value);
                }
            });

        });


        $('.div-class').on('click', function ()
        {
            if($(this).hasClass('border_1') ||$(this).hasClass('border_2') || $(this).hasClass('border_3') || $(this).hasClass('border_4'))
            {
                addClasses.border =  $(this).attr('id');
                $('#'+id_row).css('border', '');

                $('#'+id_row).removeClass('border_1');
                $('#'+id_row).removeClass('border_2');
                $('#'+id_row).removeClass('border_3');
                $('#'+id_row).removeClass('border_4');

            }

            if($(this).hasClass('radius') || $(this).hasClass('radius_1') || $(this).hasClass('radius_2') || $(this).hasClass('radius_3'))
            {
                if($(this).hasClass('radius'))
                {
                    addClasses.radius =  null;
                }
                else
                {
                    addClasses.radius =  $(this).attr('id');
                }

                $('#'+id_row).removeClass('radius_1');
                $('#'+id_row).removeClass('radius_2');
                $('#'+id_row).removeClass('radius_3');
            }

            if($(this).hasClass('bg_color') || $(this).hasClass('bg_color_1') || $(this).hasClass('bg_color_2') || $(this).hasClass('bg_color_3'))
            {
                if($(this).hasClass('bg_color'))
                {
                    addClasses.background =  null;
                }
                else
                {
                    addClasses.background =  $(this).attr('id');
                }

                $('#'+id_row).removeClass('bg_color_1');
                $('#'+id_row).removeClass('bg_color_2');
                $('#'+id_row).removeClass('bg_color_3');
            }

            $.each( addClasses, function(key, value)
            {
                if(value!=null)
                {
                    $('#'+id_row).addClass(value);
                }
            });

        });


        $('#BtnRemoveRow').on('click', function ()
        {
            $('#'+id_row).addClass('border_1');

            $('#'+id_row).removeClass('bg_color_1');
            $('#'+id_row).removeClass('bg_color_2');
            $('#'+id_row).removeClass('bg_color_3');

            $('#'+id_row).removeClass('radius_1');
            $('#'+id_row).removeClass('radius_2');
            $('#'+id_row).removeClass('radius_3');

            $('#'+id_row).removeClass('border_2');
            $('#'+id_row).removeClass('border_3');
            $('#'+id_row).removeClass('border_4');

            id_row = null;
            addClasses = {border: null, background: null, radius: null};
        });

        $('#BtnNameOk').on('click', function ()
        {
            $('#spinnerAddClass').show();

            var html = $('#result_pinya').html();

            $.post( "{!! route('boards.style-map-ajax', ['board' => $board, 'map' => $type_map]) !!}",
                { html: html })
                .done(function( data ) {
                    if(data==='true')
                    {
                        $('#spinnerAddClass').hide();
                        $('#divDoneAddName').show();
                        $('#row_name').val('');
                        setTimeout(function(){
                            $('#divDone').hide(200);
                        }, 2000)

                    }
                });
        });

        putPositions();
    });
    function putPositions()
    {
        var data = {!! $board->data_code !!};
        var type_map = '{!! strtolower($type_map); !!}';
        var structure = data[type_map].structure;

        $.each(structure, function(i, v)
        {
            if(v.position==='baix')
            {
                $('#'+i).html(v.row);
                $('#'+i).css('border', '4px solid gray');
                $('#'+i).css('line-height', '23px');
                $('#'+i).css('color','black');
            }
            else
            {
                if(v.cord==0)
                {
                    if(v.side==null)
                    {
                        $('#'+i).html(v.position);
                    }
                    else
                    {
                        var CL_side = v.side==='left' ? '{!! trans('general.CL_left') !!}' : '{!! trans('general.CL_right') !!}';
                        $('#'+i).html(v.position+' '+CL_side);

                    }

                }
                else
                {
                    if(v.side==null)
                    {
                        $('#'+i).html(v.position+' '+v.cord);
                    }
                    else
                    {
                        var CL_side = v.side==='left' ? '{!! trans('general.CL_left') !!}' : '{!! trans('general.CL_right') !!}';
                        $('#'+i).html(v.position+' '+v.cord+' '+CL_side);
                    }
                }

                $('#'+i).css('line-height', '28px');
                $('#'+i).addClass('border_1')
                $('#'+i).attr('data-row', v.row);
                $('#'+i).attr('data-position', v.position);
                $('#'+i).attr('data-cord', v.cord);
                $('#'+i).attr('data-side', v.side);
            }
        });
    }
</script>
@endsection
