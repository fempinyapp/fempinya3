@extends('template.main')

@section('title', trans('boards.add'))
@section('css_before')
    <link rel="stylesheet" href="{!! asset('js/plugins/select2/css/select2.min.css') !!}">
@endsection
@section('css_after')
    <style>
        #result_pinya div {
            position: absolute;
            text-align: center;
            line-height: 28px;
            border: 1px solid grey;
            display: block;
            overflow: hidden;
            font-size: 11.5px;
            font-family: Helvetica, Verdana, sans-serif;
        }
    </style>
@endsection

@section('content')

<div class="block">
    <div class="block-header block-header-default">
       <h3 class="block-title">
           <div class="row">
               <div class="col-md-12"><b>{!! trans('boards.add') !!}:</b></div>
               <div class="col-md-3 text-success">{!! trans('boards.import_step_1') !!}</div>
               <div class="col-md-3 text-warning">{!! trans('boards.import_step_2') !!}</div>
               <div class="col-md-3 text-warning">{!! trans('boards.import_step_3') !!}</div>
               <div class="col-md-3 text-warning">{!! trans('boards.import_step_4') !!}</div>
           </div>
       </h3>
    </div>
    <div class="block-content">
        <div class="row">
            <div class="col-md-10">
                <h5 class="text-info">{!! trans('boards.step_upload_svg_txt', ['BASE' => $type_map, 'NAME' => $board->name]) !!}</h5>
            </div>
            <div class="col-md-2 text-right">
                <div class="spinner-border" role="status" id="spinnerLoadSVGFile" style="display: none;"><span class="sr-only">Loading...</span></div>
            </div>
        </div>

        {!! Form::open(array('id' => 'formLoadSVGFile', 'method' => 'POST', 'class' => '', 'enctype' => 'multipart/form-data')) !!}

            <div class="row form-group">
                <div class="col-md-3">
                    <label class="control-label">{!! trans('boards.select_svg_file') !!}</label>
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" name="svg_file" id="svg_file" accept="image/svg+xml" form="formLoadSVGFile" required>
                        <label class="custom-file-label" for="svg_file">{!! trans('general.select_file') !!}</label>
                    </div>
                </div>
                <div class="col-md-2" style="padding-top: 25px;">
                    <button class="btn btn-alt-primary" id="loadSVGFile">{!! trans('boards.start') !!}</button>
                </div>
                <div class="col-md-3 offset-md-4 text-right" id="divUploadSVG" style="padding-top: 25px; display: none;">
                    <label class="control-label">{!! trans('boards.is_correct') !!}</label>
                    <button class="btn btn-success btn-load-svg">{!! trans('general.yes') !!}</button>
                </div>
            </div>

        {!! Form::close() !!}
        <div class="row">
            <div id="result_pinya" style="position: relative; height: 2000px;"></div>
        </div>

    </div>
</div>

@endsection

@section('js')
    <script src="{!! asset('js/plugins/select2/js/select2.full.min.js') !!}"></script>
    <script type="text/javascript">
        $(function () {
            $.ajaxPrefilter(function(options, originalOptions, xhr) { // this will run before each request
                var token = $('meta[name="csrf-token"]').attr('content'); // or _token, whichever you are using

                if (token) {
                    return xhr.setRequestHeader('X-CSRF-TOKEN', token); // adds directly to the XmlHttpRequest Object
                }
            });
        });
    </script>
<script>
    $(function ()
    {
        $('#svg_file').on('change',function(){
            //get the file name
            var fieldVal = $(this).val();

            // Change the node's value by removing the fake path (Chrome)
            fieldVal = fieldVal.replace("C:\\fakepath\\", "");

            //replace the "Choose a file" label
            $(this).next('.custom-file-label').html(fieldVal);
        });

        $("#formLoadSVGFile").on("submit", function(e)
        {
            e.preventDefault()
            $('#spinnerLoadSVGFile').show();
            $('#result_pinya').html('');

            var file = document.getElementById('svg_file').files[0];
            var FR = new FileReader();

            var svg;

            FR.readAsText(file);
            FR.onload = function(data)
            {
                xml = data.target.result;
                xml = $.parseXML(xml);
                svg = $(xml).find('rect');

                var id = 1;

                svg.each(function ()
                {
                    var item = this;

                    var x = item.attributes.x.nodeValue * 2;
                    var y = item.attributes.y.nodeValue * 2;
                    var width = item.attributes.width.nodeValue * 2;
                    var height = item.attributes.height.nodeValue * 2;

                    var div = $('<div></div>');

                    if (item.attributes.transform)
                    {
                        var transform = item.attributes.transform.nodeValue;

                        transform = transform.split(' ');
                        transform[4] = 0;
                        transform[5] = 0;
                        transform = transform.toString() + ')';

                        $(div).css('transform', transform);
                        if(height>width)
                        {
                            $(div).css('top', y+20 + 'px');
                            $(div).css('left', x-20 + 'px');
                            $(div).css('width', height + 'px');
                            $(div).css('height',  width+ 'px');
                            if(y>523)
                            {
                                $(div).css('transform',  transform+'rotate(90deg)');
                            }
                            else
                            {
                                $(div).css('transform',  transform+'rotate(90deg)');
                            }

                        }
                        else
                        {
                            $(div).css('top', y + 'px');
                            $(div).css('left', x + 'px');
                            $(div).css('width', width + 'px');
                            $(div).css('height', height + 'px');
                            $(div).css('transform', transform);
                        }

                    }
                    else
                    {
                        if(height>width)
                        {
                            $(div).css('top', y+20 + 'px');
                            $(div).css('left', x-20 + 'px');
                            $(div).css('width', height + 'px');
                            $(div).css('height',  width+ 'px');
                            if(x>514)
                            {
                                $(div).css('transform',  'rotate(90deg)');
                            }
                            else
                            {
                                $(div).css('transform',  'rotate(270deg)');
                            }

                        }
                        else
                        {
                            $(div).css('top', y + 'px');
                            $(div).css('left', x + 'px');
                            $(div).css('width', width + 'px');
                            $(div).css('height', height + 'px');
                        }
                    }

                    $(div).attr('id', id);

                    id++;

                    $('#result_pinya').append(div);

                });
            }
            $('#spinnerLoadSVGFile').hide();
            $('#divUploadSVG').show();
        });


        $('.btn-load-svg').on('click', function (e)
        {
            e.preventDefault()
            $('#spinnerLoadSVGFile').show();
            var html = $('#result_pinya').html();
            var file = document.getElementById('svg_file').files[0];

            var fd = new FormData();

            fd.append('svg', file);
            fd.append('html', html);
            fd.append('type_map', '{!! $type_map !!}');

            $.ajax({
                url: "{!! route('boards.upload-svg', $board) !!}",
                type: "post",
                data: fd,
                contentType: false,
                processData: false
            })
                .done(function(res)
                {
                    if(res==='true')
                    {
                        window.location.replace("{!! route('boards.tag-row-map', ['board' => $board, 'map' => $type_map]) !!}");
                    }
                });

        });

    });
</script>
@endsection
