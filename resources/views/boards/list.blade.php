@extends('template.main')

@section('title', trans('general.boards'))
@section('css_before')
    <link rel="stylesheet" href="{{ asset('js/plugins/datatables/dataTables.bootstrap4.css') }}">
@endsection
@section('css_after')
    <style>
        .result_pinya div {
            position: absolute;
            text-align: center;
            line-height: 28px;
            display: block;
            overflow: hidden;
            font-size: 11.5px;
            width: 72px;
            height: 31.6px;
            font-family: Helvetica, Verdana, sans-serif;
            color: black;
        }

        .radius_1 {border-radius: 5px;}
        .radius_2 {border-radius: 10px;}
        .radius_3 {border-radius: 15px;}

        .border_1{ border: 1px solid grey;}
        .border_2{ border: 2px solid grey;}
        .border_3{ border: 3px solid grey;}
        .border_4{ border: 4px solid grey;}

        .bg_color_1{background-color: #cfd8dc;}
        .bg_color_2{background-color: #c5cae9;}
        .bg_color_3{background-color: #c5e1a5;}
    </style>
@endsection

@section('content')

<div class="block">
    <div class="block-header block-header-default">
        <div class="block-title">
            <h3 class="block-title"><b>{!! trans('general.boards') !!}</b></h3>
        </div>
        <div class="block-options">

            <a class="btn btn-outline-primary" href="{!! route('boards.tags') !!}"><i class="fa fa-database"></i> {!! trans('boards.bases') !!}</a>

            <button class="btn btn-primary btn-add-board" href="{!! route('boards.add') !!}"><i class="si si-map"></i> {!! trans('boards.add') !!}</button>

        </div>
    </div>
    <div class="block-content block-content-full">
        <div class="row">

            <div class="col-md-12">
                <table class="table table-hover table-striped table-bordered" id="boards" width="100%">
                    <thead>
                        <tr>
                            <th>{!! trans('general.name') !!}</th>
                            <th>{!! trans('boards.type') !!}</th>
                            <th>{!! trans('boards.base') !!}</th>
                            <th>{!! trans('boards.added_to') !!}</th>
                            <th>#</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($boards as $board)
                        <tr>
                            <td>{!! $board->name !!}</td>
                            <td>
                                @if($board->type==='PINYA')
                                    {!! trans('boards.pinya') !!}
                                @elseif($board->type==='FOLRE')
                                    {!! trans('boards.folre') !!}
                                @elseif($board->type==='MANILLES')
                                    {!! trans('boards.manilles') !!}
                                @elseif($board->type==='PUNTALS')
                                    {!! trans('boards.puntals') !!}
                                @endif
                            </td>
                            <td>
                                @foreach ($board->tags() as $tag)
                                    <span style="margin-left: 3px; margin-bottom: 3px;">{!! $tag->name !!}</span>
                                @endforeach
                            </td>
                            <td>{!! date('m/d/Y', strtotime($board->created_at)) !!}</td>
                            <td>
                                <button class="btn btn-info btn-preview" data-board="{{ $board->id_board }}"><i class="fa fa-eye"></i></button>

                                <div class="btn-group" role="group">
                                    <button type="button" class="btn btn-warning dropdown-toggle" id="btnGroupDrop1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-pencil"></i></button>
                                    <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                                        <a class="dropdown-item" href="{{ route('boards.tag-row-map', ['board' => $board->id_board, 'map' => 'PINYA']) }}">
                                            {!! trans('boards.edit_base', ['BASE' => 'PINYA']) !!}
                                        </a>
                                        @if($board->hasFolre())
                                            <a class="dropdown-item" href="{{ route('boards.tag-row-map', ['board' => $board->id_board, 'map' => 'FOLRE']) }}">
                                                {!! trans('boards.edit_base', ['BASE' => 'FOLRE']) !!}
                                            </a>
                                        @endif
                                        @if($board->hasManilles())
                                            <a class="dropdown-item" href="{{ route('boards.tag-row-map', ['board' => $board->id_board, 'map' => 'MANILLES']) }}">
                                                {!! trans('boards.edit_base', ['BASE' => 'MANILLES']) !!}
                                            </a>
                                        @endif
                                        @if($board->hasPuntals())
                                            <a class="dropdown-item" href="{{ route('boards.tag-row-map', ['board' => $board->id_board, 'map' => 'PUNTALS']) }}">
                                                {!! trans('boards.edit_base', ['BASE' => 'PUNTALS']) !!}
                                            </a>
                                        @endif
                                    </div>
                                </div>

                                <button class="btn btn-danger btn-delete-board" data-board="{{ $board->id_board }}"><i class="fa fa-trash-o"></i></button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- START - Modal long -->
<div class="modal fade" id="modalLong" tabindex="-1" role="dialog" aria-labelledby="modal-popin" aria-hidden="true">
    <div class="modal-dialog modal-xl modal-dialog-popin" role="document">
        <div class="modal-content" id="modalLongContent">
            <!-- MODAL CONTENT -->
            <div class="col-md-12 text-center">
                <div class="spinner-border" role="status"><span class="sr-only">Loading...</span></div>
            </div>
        </div>
    </div>
</div>
<!-- END - Modal Add Board -->

<!-- START - Modal long -->
<div class="modal fade" id="modalAddBoard" tabindex="-1" role="dialog" aria-labelledby="modal-popin" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-popin" role="document">
        <div class="modal-content" id="modalAddBoardContent">
            <!-- MODAL CONTENT -->
            @include('boards.modals.modal-add')

        </div>
    </div>
</div>
<!-- END - Modal long -->

<!-- START - MODAL DELETE -->
<div class="modal fade" id="modalDelBoard" tabindex="-1" role="dialog" aria-labelledby="modal-popin" aria-hidden="true">
    <div class="modal-dialog modal-sm modal-dialog-popin" role="document">
        <div class="modal-content">
            {!! Form::open(array('url' => 'n', 'method' => 'POST', 'class' => 'form-horizontal form-bordered', 'id' => 'formDelBoard')) !!}
            <div class="block block-themed block-transparent mb-0">
                <div class="block-header bg-primary-dark">
                    <h3 class="block-title">{!! trans('boards.del_board') !!}</h3>
                    <div class="block-options">
                        <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                            <i class="si si-close"></i>
                        </button>
                    </div>
                </div>
                <div class="block-content text-center">
                    <i class="fa fa-warning" style="font-size: 46px;"></i>
                    <h3 class="semibold modal-title text-danger">{!! trans('general.caution') !!}</h3>
                    <p class="text-muted">{!! trans('boards.del_board_warning') !!}</p>
                </div>
            </div>
            <div class="modal-footer">
                {!! Form::submit(trans('general.delete') , array('class' => 'btn btn-danger')) !!}
                <button type="button" class="btn  btn-alt-secondary" data-dismiss="modal">{!! trans('general.close') !!}</button>
            </div>
            {!! Form::close() !!}

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<!--/ END - MODAL DELETE -->
@endsection

@section('js')
<script src="{{ asset('js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('js/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>

<script type="text/javascript">
    $(function () {
        $.ajaxPrefilter(function(options, originalOptions, xhr) { // this will run before each request
            var token = $('meta[name="csrf-token"]').attr('content'); // or _token, whichever you are using

            if (token) {
                return xhr.setRequestHeader('X-CSRF-TOKEN', token); // adds directly to the XmlHttpRequest Object
            }
        });
    });
</script>
<script>
    $(function ()
    {
        $("#boards").DataTable({
            "language": {!! trans('datatables.translation') !!},
            "ordering":'true',
            "order": [0, 'asc'],
            "columns": [
                { "data": "name", "name": "name" },
                { "data": "type", "name": "type"},
                { "data": "base", "name": "base" },
                { "data": "added", "name": "added" },
                { "data": "btns", "name": "btns", "orderable": false },

            ],

            "columnDefs": [
                { "width": "40%", "targets": 0 },
                { "width": "15%", "targets": 1 },
                { "width": "15%", "targets": 2 },
                { "width": "15%", "targets": 3 },
                { "width": "15%", "targets": 4 }
            ]
        });

        $('.btn-delete-board').on('click', function()
        {
            var id_board = $(this).data().id_board;

            $('#formDelBoard').attr('action', id_board);
            $('#modalDelBoard').modal('show');
        });

        $('.btn-add-board').on('click', function (event)
        {
            $('#modalAddBoard').modal('show');
        });

        $('.btn-preview').on('click', function(event)
        {
            $('#modalLong').modal('show');

            var board = $(this).data().board;
            var url = "{{ route('boards.preview-board-ajax', ['board' => ':board']) }}";
            url = url.replace(':board', board)

            $.get( url, function( data ) {
                $('#modalLongContent').html( data );
            });
        });

        $('.btn-edit-board').on('click', function (e)
        {
            var button = $(this);


            console.log($(this));
        });
    });


</script>
@endsection
