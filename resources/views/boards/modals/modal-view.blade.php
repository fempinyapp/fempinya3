
<div class="block block-themed block-transparent mb-0">
    <div class="block-header bg-primary-dark">
        <h3 class="block-title">
            {!! trans('general.board').': <b>'.$board->name.'</b>' !!}
        </h3>
        <div class="block-options">
            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                <i class="si si-close"></i>
            </button>
        </div>
    </div>

    <div class="block-content">
        <div class="row">
            <div class="col-md-12">
                <div class="block">
                    <ul class="nav nav-tabs nav-tabs-block" data-toggle="tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" href="#tab-pinya" data-toggle="tab">{!! trans('general.pinya') !!}</a>
                        </li>
                        @if($board->hasFolre())
                            <li class="nav-item">
                                <a class="nav-link" href="#tab-folre" data-toggle="tab">{!! trans('general.folre') !!}</a>
                            </li>
                        @endif
                        @if($board->hasManilles())
                            <li class="nav-item">
                                <a class="nav-link" href="#tab-manilles" data-toggle="tab">{!! trans('general.manilles') !!}</a>
                            </li>
                        @endif
                        @if($board->hasPuntals())
                            <li class="nav-item">
                                <a class="nav-link" href="#tab-puntals" data-toggle="tab">{!! trans('general.puntals') !!}</a>
                            </li>
                        @endif
                    </ul>
                    <div class="block-content tab-content">
                        <div class="tab-pane active" id="tab-pinya" role="tabpanel">
                            <div class="row">
                                <div class="result_pinya" id="result-pinya" style="position: relative; height: 1200px;">
                                    {!! $board->html_pinya !!}
                                </div>
                            </div>

                        </div>
                        @if($board->hasFolre())
                            <div class="tab-pane" id="tab-folre" role="tabpanel">
                                <div class="result_pinya" style="position: relative; height: 1200px;">
                                    {!! $board->html_folre !!}
                                </div>
                            </div>
                        @endif

                        @if($board->hasManilles())
                            <div class="tab-pane" id="tab-manilles" role="tabpanel">
                                <div class="result_pinya" style="position: relative; height: 1200px;">
                                    {!! $board->html_manilles !!}
                                </div>
                            </div>
                        @endif

                        @if($board->hasPuntals())
                            <div class="tab-pane" id="tab-puntals" role="tabpanel">
                                <div class="result_pinya" style="position: relative; height: 1200px;">
                                    {!! $board->html_puntals !!}
                                </div>
                            </div>
                        @endif

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal-footer">
        <a type="button" class="btn btn-alt-secondary" data-dismiss="modal">{!! trans('general.close') !!}</a>
    </div>
</div>

