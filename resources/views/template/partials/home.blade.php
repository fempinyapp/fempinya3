<!doctype html>
<html lang="ca">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ mix('js/codebase.app.js') }}"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/codebase.min.css') }}" rel="stylesheet">
</head>

<body>
<!-- Page Container -->
<!--
    Available classes for #page-container:

GENERIC

    'enable-cookies'                            Remembers active color theme between pages (when set through color theme helper Codebase() -> uiHandleTheme())

SIDEBAR & SIDE OVERLAY

    'sidebar-r'                                 Right Sidebar and left Side Overlay (default is left Sidebar and right Side Overlay)
    'sidebar-mini'                              Mini hoverable Sidebar (screen width > 991px)
    'sidebar-o'                                 Visible Sidebar by default (screen width > 991px)
    'sidebar-o-xs'                              Visible Sidebar by default (screen width < 992px)
    'sidebar-inverse'                           Dark themed sidebar

    'side-overlay-hover'                        Hoverable Side Overlay (screen width > 991px)
    'side-overlay-o'                            Visible Side Overlay by default

    'enable-page-overlay'                       Enables a visible clickable Page Overlay (closes Side Overlay on click) when Side Overlay opens

    'side-scroll'                               Enables custom scrolling on Sidebar and Side Overlay instead of native scrolling (screen width > 991px)

HEADER

    ''                                          Static Header if no class is added
    'page-header-fixed'                         Fixed Header

HEADER STYLE

    ''                                          Classic Header style if no class is added
    'page-header-modern'                        Modern Header style
    'page-header-inverse'                       Dark themed Header (works only with classic Header style)
    'page-header-glass'                         Light themed Header with transparency by default
                                                (absolute position, perfect for light images underneath - solid light background on scroll if the Header is also set as fixed)
    'page-header-glass page-header-inverse'     Dark themed Header with transparency by default
                                                (absolute position, perfect for dark images underneath - solid dark background on scroll if the Header is also set as fixed)

MAIN CONTENT LAYOUT

    ''                                          Full width Main Content if no class is added
    'main-content-boxed'                        Full width Main Content with a specific maximum width (screen width > 1200px)
    'main-content-narrow'                       Full width Main Content with a percentage width (screen width > 1200px)
-->
<div id="page-container" class="sidebar-inverse page-header-fixed page-header-inverse main-content-boxed">

    <!-- Header -->
    <header id="page-header">
        <div class="content-header">
            <div class="content-header-section">
                <ul class="nav-main-header nav-main-header-no-icons">
                    <li>
                        <a class="active" href="">
                            <i class="si si-home"></i>Home
                        </a>
                    </li>
                    <li class="nav-main-heading">Heading</li>
                    <li>
                        <a class="nav-submenu" data-toggle="nav-submenu" href="#">
                            <i class="si si-puzzle"></i>Dropdown
                        </a>
                        <ul>
                            <li>
                                <a href="javascript:void(0)">Link #1</a>
                            </li>
                            <li>
                                <a href="javascript:void(0)">Link #2</a>
                            </li>
                            <li>
                                <a class="nav-submenu" data-toggle="nav-submenu" href="#">Dropdown</a>
                                <ul>
                                    <li>
                                        <a href="javascript:void(0)">Link #1</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">Link #2</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-main-heading">Vital</li>
                    <li>
                        <a href="javascript:void(0)">
                            <i class="si si-wrench"></i>Page
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">
                            <i class="si si-wrench"></i>Page
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">
                            <i class="si si-wrench"></i>Page
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </header>
    <!-- END Header -->
    <!-- END Header -->

    <!-- Main Container -->
    <main id="main-container" style="min-height: 210.9px; margin-top: 20px;">


        <div class="row">
            <div class="col-md-4 offset-md-1">

                <a class="block mb-2" href="javascript:void(0)">
                    <div class="block block-bordered mb-2">
                        <div class="block-header">
                            asdasdas
                        </div>
                    </div>
                </a>

                <a class="block mb-2" href="javascript:void(0)">
                    <div class="block block-bordered mb-2">
                        <div class="block-header">
                            asdasdas
                        </div>
                    </div>
                </a>

                <a class="block mb-2" href="javascript:void(0)">
                    <div class="block block-bordered mb-2">
                        <div class="block-header">
                            asdasdas
                        </div>
                    </div>
                </a>

                <a class="block mb-2" href="javascript:void(0)">
                    <div class="block block-bordered mb-2">
                        <div class="block-header">
                            asdasdas
                        </div>
                    </div>
                </a>

                <a class="block mb-2" href="javascript:void(0)">
                    <div class="block block-bordered mb-2">
                        <div class="block-header">
                            asdasdas
                        </div>
                    </div>
                </a>


            </div>
            <div class="col-md-6">

                <div id="accordion2" role="tablist" aria-multiselectable="true">
                    <div class="block block-bordered block-rounded mb-2">
                        <div class="block-header" role="tab" id="accordion2_h1">
                            <a class="font-w600 collapsed" data-toggle="collapse" data-parent="#accordion2" href="#accordion2_q1" aria-expanded="false" aria-controls="accordion2_q1">2.1 Accordion Title</a>
                        </div>
                        <div id="accordion2_q1" class="collapse" role="tabpanel" aria-labelledby="accordion2_h1" style="">
                            <div class="block-content">
                                <p>Dolor posuere proin blandit accumsan senectus netus nullam curae, ornare laoreet adipiscing luctus mauris adipiscing pretium eget fermentum, tristique lobortis est ut metus lobortis tortor tincidunt himenaeos habitant quis dictumst proin odio sagittis purus mi, nec taciti vestibulum quis in sit varius lorem sit metus mi.</p>
                            </div>
                        </div>
                    </div>
                    <div class="block block-bordered block-rounded mb-2">
                        <div class="block-header" role="tab" id="accordion2_h2">
                            <a class="font-w600 collapsed" data-toggle="collapse" data-parent="#accordion2" href="#accordion2_q2" aria-expanded="false" aria-controls="accordion2_q2">2.2 Accordion Title</a>
                        </div>
                        <div id="accordion2_q2" class="collapse" role="tabpanel" aria-labelledby="accordion2_h2" style="">
                            <div class="block-content">
                                <p>Dolor posuere proin blandit accumsan senectus netus nullam curae, ornare laoreet adipiscing luctus mauris adipiscing pretium eget fermentum, tristique lobortis est ut metus lobortis tortor tincidunt himenaeos habitant quis dictumst proin odio sagittis purus mi, nec taciti vestibulum quis in sit varius lorem sit metus mi.</p>
                            </div>
                        </div>
                    </div>
                    <div class="block block-bordered block-rounded mb-2">
                        <div class="block-header" role="tab" id="accordion2_h3">
                            <a class="font-w600 collapsed" data-toggle="collapse" data-parent="#accordion2" href="#accordion2_q3" aria-expanded="false" aria-controls="accordion2_q3">2.3 Accordion Title</a>
                        </div>
                        <div id="accordion2_q3" class="collapse" role="tabpanel" aria-labelledby="accordion2_h3">
                            <div class="block-content">
                                <p>Dolor posuere proin blandit accumsan senectus netus nullam curae, ornare laoreet adipiscing luctus mauris adipiscing pretium eget fermentum, tristique lobortis est ut metus lobortis tortor tincidunt himenaeos habitant quis dictumst proin odio sagittis purus mi, nec taciti vestibulum quis in sit varius lorem sit metus mi.</p>
                            </div>
                        </div>
                    </div>
                    <div class="block block-bordered block-rounded mb-2">
                        <div class="block-header" role="tab" id="accordion2_h4">
                            <a class="font-w600 collapsed" data-toggle="collapse" data-parent="#accordion2" href="#accordion2_q4" aria-expanded="false" aria-controls="accordion2_q4">2.4 Accordion Title</a>
                        </div>
                        <div id="accordion2_q4" class="collapse" role="tabpanel" aria-labelledby="accordion2_h4">
                            <div class="block-content">
                                <p>Dolor posuere proin blandit accumsan senectus netus nullam curae, ornare laoreet adipiscing luctus mauris adipiscing pretium eget fermentum, tristique lobortis est ut metus lobortis tortor tincidunt himenaeos habitant quis dictumst proin odio sagittis purus mi, nec taciti vestibulum quis in sit varius lorem sit metus mi.</p>
                            </div>
                        </div>
                    </div>
                    <div class="block block-bordered block-rounded">
                        <div class="block-header" role="tab" id="accordion2_h5">
                            <a class="font-w600 collapsed" data-toggle="collapse" data-parent="#accordion2" href="#accordion2_q5" aria-expanded="false" aria-controls="accordion2_q5">2.5 Accordion Title</a>
                        </div>
                        <div id="accordion2_q5" class="collapse" role="tabpanel" aria-labelledby="accordion2_h5">
                            <div class="block-content">
                                <p>Dolor posuere proin blandit accumsan senectus netus nullam curae, ornare laoreet adipiscing luctus mauris adipiscing pretium eget fermentum, tristique lobortis est ut metus lobortis tortor tincidunt himenaeos habitant quis dictumst proin odio sagittis purus mi, nec taciti vestibulum quis in sit varius lorem sit metus mi.</p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </main>
    <!-- END Main Container -->

    <!-- Footer -->
    <footer id="page-footer" class="bg-white opacity-0" style="opacity: 1;">
        <div class="content content-full">
            <!-- Footer Navigation -->
            <div class="row items-push-2x mt-30">
                <div class="col-6 col-md-4">
                    <h3 class="h5 font-w700">Heading</h3>
                    <ul class="list list-simple-mini font-size-sm">
                        <li>
                            <a class="link-effect font-w600" href="javascript:void(0)">Link #1</a>
                        </li>
                        <li>
                            <a class="link-effect font-w600" href="javascript:void(0)">Link #2</a>
                        </li>
                        <li>
                            <a class="link-effect font-w600" href="javascript:void(0)">Link #3</a>
                        </li>
                    </ul>
                </div>
                <div class="col-6 col-md-4">
                    <h3 class="h5 font-w700">Heading</h3>
                    <ul class="list list-simple-mini font-size-sm">
                        <li>
                            <a class="link-effect font-w600" href="javascript:void(0)">Link #1</a>
                        </li>
                        <li>
                            <a class="link-effect font-w600" href="javascript:void(0)">Link #2</a>
                        </li>
                        <li>
                            <a class="link-effect font-w600" href="javascript:void(0)">Link #3</a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <h3 class="h5 font-w700">Company LTD</h3>
                    <div class="font-size-sm mb-30">
                        1080 Sunshine Valley, Suite 2563<br>
                        San Francisco, CA 85214<br>
                        <abbr title="Phone">P:</abbr> (123) 456-7890
                    </div>
                </div>
            </div>
            <!-- END Footer Navigation -->

            <!-- Copyright Info -->
            <div class="font-size-sm clearfix border-t pt-20 pb-10">
                <div class="float-right">
                    Crafted with <i class="fa fa-heart text-pulse"></i> by <a class="font-w600" href="https://www.berrly.com" target="_blank">Berrly</a>
                </div>
                <div class="float-left">
                    <a class="font-w600" href="https://1.envato.market/95j" target="_blank">Codebase</a> © <span class="js-year-copy js-year-copy-enabled">2020</span>
                </div>
            </div>
            <!-- END Copyright Info -->
        </div>
    </footer>
    <!-- END Footer -->
</div>
<!-- END Page Container -->



</body>
</html>
