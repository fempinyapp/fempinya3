<!-- Sidebar -->
<nav id="sidebar">
    <!-- Sidebar Content -->
    <div class="sidebar-content">
        <!-- Side Header -->
        <div class="content-header content-header-fullrow px-15 bg-black-op-10">
            <!-- Mini Mode -->
            <div class="content-header-section sidebar-mini-visible-b">
                <!-- Logo -->
                <span class="content-header-item font-w700 font-size-xl float-left animated fadeIn">asd
                                <span class="text-dual-primary-dark">c</span><span class="text-primary">b</span>
                            </span>
                <!-- END Logo -->
            </div>
            <!-- END Mini Mode -->

            <!-- Normal Mode -->
            <div class="content-header-section text-center align-parent sidebar-mini-hidden">
                <!-- Close Sidebar, Visible only on mobile screens -->
                <!-- Layout API, functionality initialized in Template._uiApiLayout() -->
                <button type="button" class="btn btn-circle btn-dual-secondary d-lg-none align-v-r" data-toggle="layout" data-action="sidebar_close">
                    <i class="fa fa-times text-danger"></i>
                </button>
                <!-- END Close Sidebar -->

                <!-- Logo -->

                <div class="content-header-item">
                    <a class=" font-w700" href="{!! route('home') !!}">
                        <img src="{!! asset('media/img/logo.svg') !!}" alt="Logo: Fem pinya" style="padding-bottom: 6px;" width="60">
                        <br>
                        <span class="font-size-xl text-dual-primary-dark">Fem</span><span class="font-size-xl text-warning-light">Pinya</span>
                    </a>
                </div>
                <!-- END Logo -->
            </div>
            <!-- END Normal Mode -->
        </div>
        <!-- END Side Header -->

        <!-- Side Navigation -->
        <div class="content-side content-side-full" style="margin-top: 50px;">
            <ul class="nav-main">

                @if (\Illuminate\Support\Facades\Auth::user()->accesAdmin())
                    {{-- START: ADMIN --}}
                    <li @if(Request::segment(1)=='admin') class="open" @endif>

                        @if(Request::segment(1)=='admin')
                            <a class="active nav-submenu" data-toggle="nav-submenu" href="#">
                        @else
                            <a class="nav-submenu" data-toggle="nav-submenu" href="#">
                        @endif

                            <i class="si si-lock"></i>
                            <span class="sidebar-mini-hide">{!! trans('admin.administrators') !!}</span>
                        </a>
                        <ul>
                            <li>
                                <a @if(Request::segment(2)=='colles') class="active" @endif href="{!! route('admin.colles') !!}">{!! trans('admin.colles') !!}</a>
                            </li>
                            <li>
                                <a @if(Request::segment(2)=='users') class="active" @endif href="{!! route('admin.users') !!}">{!! trans('general.users') !!}</a>
                            </li>
                        </ul>
                    </li>
                    {{-- END: ADMIN --}}
                @endif

                    @if (\Illuminate\Support\Facades\Auth::user()->accesBBDD())
                        {{-- START: CASTELLERS --}}
                        <li @if(Request::segment(1)=='castellers') class="open" @endif>

                            @if(Request::segment(1)=='castellers')
                                <a class="active nav-submenu" data-toggle="nav-submenu" href="{!! route('castellers.list') !!}">
                            @else
                                <a class="nav-submenu" data-toggle="nav-submenu" href="{!! route('castellers.list') !!}">
                            @endif
                                <i class="si si-users"></i>
                                <span class="sidebar-mini-hide">{!! trans('general.bbdd') !!}</span>

                                </a>
                                <ul>
                                    <li>
                                        <a @if(Request::segment(2)=='list' || Request::segment(2)=='edit') class="active" @endif href="{!! route('castellers.list') !!}">{!! trans('general.castellers') !!}</a>
                                    </li>
                                    <li>
                                        <a @if(Request::segment(2)=='tags') class="active" @endif href="{!! route('castellers.tags') !!}">{!! trans('general.tags_and_positions') !!}</a>
                                    </li>
                                </ul>
                                </li>
                        {{-- END: CASTELLRS --}}


                    @endif

                    @if (\Illuminate\Support\Facades\Auth::user()->accesEvents())
                        {{-- START: EVENTS --}}
                        <li @if(Request::segment(1)=='events') class="open" @endif>

                            <a @if(Request::segment(1)=='events') class="active" @endif  href="{!! route('events.list') !!}"><i class="fa fa-calendar"></i><span class="sidebar-mini-hide">{!! trans('general.events') !!}</span></a>

                        </li>
                        {{-- END: EVENTS --}}
                    @endif

                    @if (\Illuminate\Support\Facades\Auth::user()->accesBoards())
                        {{-- START: BOARDS --}}
                        <li @if(Request::segment(1)=='boards') class="open" @endif>

                            <a @if(Request::segment(1)=='boards') class="active" @endif  href="{!! route('boards.list') !!}"><i class="si si-map"></i><span class="sidebar-mini-hide">{!! trans('general.pinyes') !!}</span></a>

                        </li>
                        {{-- END: BOARDS --}}
                    @endif
                    <li>
                        <a href="/news"><i class="fa fa-file-text-o"></i> {!! trans('news.news') !!}</a>
                   </li>
            </ul>
        </div>
        <!-- END Side Navigation -->

    </div>
    <!-- Sidebar Content -->
</nav>
<!-- END Sidebar -->
