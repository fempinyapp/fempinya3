<?php

return [
    'attendance' => 'Assitència',
    'filter_attendance_status' => 'Filtra per assitència prevista',
    'attendance_answers' => 'Respostes personalitzdes',
    'del_attendance_answer_warning' => 'Estas segur que vols eliminar la resposta?',
    'status' => 'estat',
    'status_verified' => 'verificat',
    'companions' => 'Acompanyants',
];
