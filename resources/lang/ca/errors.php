<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'error'   => 'Alguna cosa no ha funcionat com esperaves',
    '400'   => 'Ho sentim, però la sol·licitud conté sintaxi errònia i no es pot complir.',
    '401'   => 'Ho sentim, però no esteu autoritzat per accedir a aquesta pàgina.',
    '403'   => 'Ho sentim, però no teniu permís per accedir a aquesta pàgina.',
    '404'   => 'Ho sentim, però no s\'ha trobat la pàgina que estàs buscant.',
    '419'   => 'Ho sentim, però la sessió ha expirat',
    '500'   => 'Ho sentim, però la sol·licitud conté sintaxi errònia i no es pot complir.',
    '503'   => 'Ho sentim, però actualment el nostre servei no està disponible.',

];
