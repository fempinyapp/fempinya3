<?php

return [
    'tag' => 'Etiqueta',
    'del_tag_warning' => 'Estàs segur que vols elimiar aquesta etiqueta?',
    'add_tag' => 'Afegir una etiqueta',
    'add_attendance_tag' => 'Afegir una resposta personalitzada',
    'update_tag' => 'Edita una etiqeuta',
    'invalid_name' => 'Nom de l\'etiqueta no vàlid',
    'tag_exist' => 'Ja existeix una etiqueta amb aquest nom',
    'tag_added' => 'Etiqueta afegida correctament!',
    'tag_updated' => 'Etiqueta actualitzada correctament!',
    'tag_deleted' => 'Etiqueta eliminada correctament!',
    'btn_delete_dissabled' => 'Aquesta etiqueta està en ús',
    'event_tags' => 'Etiquetes dels esdeveniments',
    'add_tag_event' => 'Afegir una etiqueta d\'esdeveniments',
    'update_event_tag' => 'Edita una etiqueta d\'esdeveniments',
    'update_attendance_tag' => 'Edita una etiqueta d\'assistència',
    'not_castellers_tags' => 'Encara no has afegit cap etiqueta ',
    'not_events_tags' => 'Encara no has afegit cap etiqueta d\'esdeveniments',
    'not_attendance_tags' => 'Encara no has afegit cap resposta personalitzada',
    'not_position_tags' => 'Encara no has afegit cap posició',
];
