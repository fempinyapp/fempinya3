<?php

return [

    'administrators' => 'Administradors',
    'colles' => 'Colles',
    'add_colla' => 'Afegeix una nova colla',

    'last_login' => 'Últim inici de sessió',
    'num_castellers' => 'Núm. castellers',
    'name_colla' => 'Nom de la colla',
    'shortname' => 'Nom curt',
    'error_same_shortname' => 'Error, el nom curt ha de ser únic',
    'colla_added' => 'Colla agefida correctament!',
    'colla_no_users' => 'Encara no té usaris',
    'btn_input_file_text' => 'Navega...',
    'del_colla' => 'Esborra una colla',
    'del_colla_warning' => 'Estàs segur que vols eliminar la colla?<br>També eliminaràs els seus usuaris, castellers i esdeveniments, aquesta acció és irrecuperable!',
    'colla_destroyed' => 'Colla eliminada correctament',
    'colla_cant_destroyed' => 'Aquesta colla no es pot eliminar',
    'update_colla' => 'Actualitza colla',
    'colla_updated' => 'Colla actualitzada correctament!',
    'del_user' => 'Esborra un usuari',
    'del_user_warning' => 'Estàs segur que vols eliminar aquest usuari?',
    'user_destroyed' => 'Usuari eliminat correctament',
    'update_user' => 'Actualitza usuari',
    'do_admin' => 'Fer administrador del sistema',

];
