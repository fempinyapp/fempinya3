<?php

return [
    'events_type' => 'Tipus d\'esdeveniments',
    'all' => 'Tots',
    'upcoming_events' => 'Pròxims esdeveniments',
    'past_events' => 'Esdeveniments passats',
    'add_event' => 'Un esdeveniment',
    'add_events' => 'Afegeix esdeveniments',
    'add_multiple_events' => 'Més d\'un esdeveniment',
    'add_new_event' => 'Afegeix un nou esdeveniment',
    'add_new_group_events' => 'Afegeix un grup  d\'esdeveniments',
    'update_event' => 'Edita l\'esdeveniment',
    'event_tags' => 'Etiquetes de l\'esdeveniment',
    'event_name' => 'Nom de l\'esdeveniment',
    'event_date' => 'Data de l\'esdeveniment',
    'hour' => 'Hora',
    'duration' => 'Durada (min)',
    'visiblity' => 'Visible?',
    'open_date' => 'Data d\'obertura de les inscripcions',
    'close_date' => 'Data de tancament de les inscripcions',
    'num_selected_dates' => 'Núm. dates sel·lecionades',
    'select_multiple_dates' => 'Selecciona les dates que vulguis',
    'months_before' => 'mesos abans',
    'weeks_before' => 'setmanes abans',
    'days_before' => 'dies abans',
    'hours_before' => 'hores abans',
    'immediately' => 'Immediatament',
    'companions' => 'Acompanyants',
    'group_events_added' => 'Esdeveniments afegits correctament!',
    'event_added' => 'Esdeveniment afegit correctament!',
    'event_updated' => 'Esdeveniment actualitzat correctament!',
    'event_destroyed' => 'Esdeveniment eliminat correctament!',
    'del_event_warning' => 'Estàs segur que vols eliminar l\'esdeveniment?<br>També eliminaràs tots els seus registres d\'assistència, aquesta acció és irrecuperable!',
    ];
