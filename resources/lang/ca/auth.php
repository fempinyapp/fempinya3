<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed'   => 'Aquestes credencials no concorden amb els nostres registres.',
    'throttle' => 'Heu superat el nombre màxim d\'intents d\'accés. Per favor, torna a intentar-ho en :seconds segons.',
    'remember_me' => 'Recorda\'m',
    'e-mail' => 'Correu electrònic',
    'password' => 'Contrasenya',
    'confirm_password' => 'Confirma la contrasenya',
    'login' => 'Entra',
    'password_lost' => 'Has oblidat la teva contrasenya?',
    'ups' => 'Ostres!',
    'problems' => 'Hem tingu alguns problemes.',
    'reset_password' => 'Reinicia la contrasenya',
    'password_reset_link' => 'Envia l\'enllaç de restabliment de contrasenya',

];
