<?php

return [

    'news' => 'Noticies',
    'new' => 'Noticia',
    'add_news' => 'Afegir noticia',
    'title' => 'Titol',
    'body' => 'Cos de la noticia',
    'data' => 'Data',
    'open_date' => 'Data d\'obertura de la noticia',
    'hour_open_date' => 'Hora apertura',
    'tornar' => 'Tornar',
    'redactor' => 'Redactor',
    'editar' => 'Editar noticia',
    'visiblity' => 'Visible',
    'fet' => 'Noticia creada!',
    'deleted' => 'Noticia esborrada!',
    'editada' => 'Noticia editada!',
    'read_more' => 'llegir mes',
    'del_new' => 'Esborra una noticia',
    'del_new_warning' => 'Estàs segur que vols eliminar la noticia? Aquesta acció és irrecuperable!',
    
    

];
