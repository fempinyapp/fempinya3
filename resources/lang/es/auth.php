<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed'   => 'Estas credenciales no coinciden con nuestros registros.',
    'throttle' => 'Demasiados intentos de acceso. Por favor intente nuevamente en :seconds segundos.',
    //TODO: translate
    'e-mail' => 'Correu electrònic',
    'password' => 'Contrasenya',
    'confirm_password' => 'Confirma la contrasenya',
    'login' => 'Entra',
    'password_lost' => 'Has oblidat la teva contrasenya?',
    'ups' => 'Ostres!',
    'problems' => 'Hem tingu alguns problemes.',
    'reset_password' => 'Reinicia la contrasenya',
    'password_reset_link' => 'Envia l\'enllaç de restabliment de contrasenya',
];
