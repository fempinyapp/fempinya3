<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'error'   => 'Algo no ha funcionado como esperabas.',
    '400'   => 'Lo sentimos, pero su solicitud contiene una sintaxis incorrecta y no se puede cumplir.',
    '401'   => 'Lo sentimos pero no está autorizado para acceder a esta página.',
    '403'   => 'Lo sentimos pero no tienes permiso para acceder a esta página.',
    '404'   => 'Lo sentimos, pero no se encontró la página que está buscando.',
    '419'   => 'Lo sentimos, pero la sesión ha expirado.',
    '500'   => 'Lo sentimos, pero su solicitud contiene una sintaxis incorrecta y no se puede cumplir.',
    '503'   => 'Lo sentimos, pero nuestro servicio no está disponible actualmente.',
    
];