<?php

return [
    'command_help' => 'ayuda',
    'command_perfil' => 'perfil',
    'command_agenda' => 'agenda',
    'command_actuacions' => 'Actuaciones',
    'command_assajos' => 'Ensayos',
    'command_activitats' => 'Actividades',
    'command_tecnica' => 'técnica',
    'command_language' => 'idioma',
    'command_multiuser' => 'Multiusiario',
    'command_alta' => 'codigo',
    'command_baixa' => 'baja',

    'command_aplicar' => 'Aplicar',
    'per_aplicar' => 'Para aplicar los cambios',
    'canvis_aplicats' => 'Los cambios se han aplicado',
    'command_cercar' => 'Buscar',
    'command_xxx' => 'xxx',
    'command_xxx' => 'xxx',
    'command_xxx' => 'xxx',
    'command_xxx' => 'xxx',


    'command_photo' => 'foto',
    'casteller' => 'casteller',
    'tecnica' => 'Técnica',
    'salutacio' => 'Hola esta es una respuesta a tu peteición', 
    'Has_demanat_registre' => 'has pedido registrar te en FemPinya',
    'attendance' => 'asistencia',
    'attendance_status' => 'estado assistencia',
    'attendance_status_done' => 'asistencia actualizada',
    'tag' => 'etiqueta',
    'tancat' => 'cerrado',
    'no_registres' => 'sin registros',
    'castellers' => 'castellers',
    'error' => 'Error!',
    'tag_assistencia' => 'etiqueta de asistencia',
    'language' => 'idioma',
    'done' => 'Hecho!',
    'help' => 'ayuda',
    'no_registrer' => 'sin registrar',
    'codi' => 'codi',
    'aceptes_condicions' => 'Aceptas las condiciones del servicio',
    'incorrecte' => 'incorrecto',
    'repetit' => 'repetido',
    'agenda' => 'Agenda',
    'no_parametres' => 'Sin parametros',
    'text_per_donar_alta_1' => 'Para registrar te debes escribir <i>codi + AAA</i> AAA es el codigo que te facilitará tu colla',
    'text_per_donar_alta_2' => 'Para registrar te debes escribir <i>codi + alias + correo-e</i> ',
    'si_continues' => 'Si continuas',
    'mes_informacio' => 'Praa más información',
    'fempinya' => 'FemPinya',
    'es_un_bot_de_fempinya' => 'Es un bot de FemPinya.cat para gestionar las collas castelleras',
    'norma_1' => 'Es necesario que tu colla esté dada de alta en FemPinya',
    'norma_2' => 'Cada colla facilitarà un codigo a cada casteller',
    'norma_3' => 'Para registrarte escribe <code>codi</code>',
    'norma_4' => 'Una vez registrado utiliza los botones para acceder a las diferentes opciones',
    'contactar' => 'contactar',
    'missatge_enviat' => 'Tu mensaje se ha enviado',
    'escriu_contactar_seguit_missatge' => 'Escrib contactar seguido del mensaje',
    'important_teu_correu' => 'Es importante que indiques tu correo para recibir respuesta',
    'exemple' => 'Ejemplo',
    'contactar_bla_bla_bla' => 'contactar Quiero más información bla bla... micorreo@gmail.com',
    'actuacions' => 'Actuaciones',
    'assajos' => 'Ensayos',
    'activitats' => 'Actividades',
    'text_help' => '<i>Información</i> Presenta texto de ayuda',
    'text_config' =>' <i>Editable</i> Editar preferencias',
    'text_agenda' => 'Agenda <i>Información</i> Presenta agenda de la temporada',
    'text_actuacions' => ' <i>Editable</i> Parar gestionar actuaciones futuras',
    'text_assajos' => ' <i>Editable</i> Para gestionar ensayos (limite de x, según cada colla',
    'text_activitats' => ' <i>Editable</i> Para gestionar actividades limite de x según cada colla',
    'text_baixa' => '<i> baixa al bot</i> Borra los datos, no podrás hacer servir el bot y dejas de recibir mensajes',
    'confirmar_baixa' => 'Para confirmar escribe /baja',
    'done_baixa' => 'Baja realizada',
    'perfil' => 'Perfil',
    'hem_rebut' => 'Hemos recibido',
    'trobats' => 'encontrados',
    'caution' => 'Alerta',
    // 'name' => 'Nombre',
    // 'last_name' => 'Apellidos',
    // 'num_soci' => 'N. de socio',
    // 'national_id_type' => 'Documento',
    // 'national_id_number' => 'Número de documento',
    // 'family' => 'Familia',
    // 'family_head' => 'Cabeza de familia',
    // 'alias' => 'Apodo',
    // 'gender' => 'Genero',
    // 'birthdate' => 'Fecha de nacimiento',
    // 'subscription_date' => 'Fecha de ingreso',
    // 'email' => 'Correo electrónico',
    // 'email_short' => 'Correo',
    // 'emails' => 'Correos electrónicos',
    // 'phone' => 'Teléfono fijo',
    // 'mobile_phone' => 'Teléfono movil',
    // 'emergency_phone' => 'Teléfono de emergencia',
    // 'address' => 'Dirección',
    // 'postal_code' => 'Codigo postal',
    // 'city' => 'Población',
    // 'comarca' => 'Comarca',
    // 'province' => 'Provincia',
    // 'country' => 'País',
    // 'photo' => 'Foto',
    'height' => 'Altura',
    'weight' => 'Peso',
    'shoulder_height' => 'Altura de espaldas',
    // 'comments' => 'Comentarios',
    // 'years' => 'años',
    // 'profile' => 'Perfil',
    // 'dni' => 'DNI',
    // 'nie' => 'NIE',
    // 'passport' => 'Pasaporte',
    // 'gender_male' => 'Masculino',
    // 'gender_female' => 'Femenino',
    // 'gender_nsnc' => 'NS/NC',
    // 'group' => 'Grupo',
    'tancat' => 'Cerrado',
    'tria_una_opcio' => 'Elige una opción',
    'text_llistat_castellers' => 'Lista castellers',
    'text_llistat_asssitencia' => 'Lista asistencia',
    'text_recordatori' => 'Envia recordatorio',
    'text_tecnica' => 'Información técnica',
    'photo' => 'Foto',
    'opcions_rol_tecnica' => 'Opciones solo para la técnica',
    'contactar_bla_bla_bla' => 'contactar bla bla bla',
    'pots_demanar_ajuda' => 'Puedes pedir ayuda /help',
    'tria_grup_destinatari' => 'Elige grupo destinatario del mensaje',
    'preparar_missatge_per_a ' => 'Preparar mensaje para',   
    'recordatori_expres' => 'Recordatorio expres',
    'events' => 'Acontecimientos',
    'text_help_u' => '<b>help</b> <i>Información</i> Presenta texto de ayuda',
    'text_codi_u' => '<b>codi</b> <i>codi + AAA</i> permite darse de alta',
    'text_quisom_u' => '<b>fempinya</b> <i>FempPinya</i> Informa sobre el bot',
    'text_contactar_u' => '<b>contactar</b> <i>contactar + bla bla bla</i> Envia mensaje (bla bla bla) als responsables',
    'multiuser' => 'Multi usuario',
    'multiuser_condicions' => 'Puedes gestionar otros castellers com los de tu familiares, ...',
    'has_demanat' => 'Has pedido',
    'telegram.text_cercar' => '<i>/buscar + nombre</i> busca un casteller por su nombre, apellidos o alias ',
    'text_photo' => 'Para enviar una foto debes utilizar el siguiente formato',
    'text_photo_format' => '/photo url texto',
    'xxx' => 'xxx',
    'xxx' => 'xxx',
    'xxx' => 'xxx',
    'xxx' => 'xxx',
    'xxx' => 'xxx',

];
